# Biorithm-FeMom-CTG
Project description go here 
The C shared library (.so) includes three modules/units 

ECG outputs Fetal and Maternal heart rate 

UC outputs Uterine Contraction trace 

ACC generate traces in image format for visualization 


## Project structure with CMake
Project structure for two main purposes, one is for auto build using CMake and second is for later auto build and deployment using Dockerfile. Below is the project structure: 
```bash
.biorithm-fedmom-ctg
├── bin 
├── build
├── doc
├── include
├── lib
├── src
│   ├── acc
│   │   └── CMakeLists.txt
│   ├── ecg
│   │   └── CMakeLists.txt
│   └── uc
│       └── CMakeLists.txt
└── test
│    └── CMakeLists.txt
│    └── main.c  
├── CMakeLists.txt
└── README.md

``` 
## Setup CMake and GCC 
CMake version 3.13.4 <br/>
GNU Compiler Collection (GCC) version 4.8.5 

## How to build
cd build 

cmake3 .. 

make 


### Build for unit test 

### Build for .so library 

## Integrate executable files and .so lib with Node.js
Using Dockerfile to build container for deploying in AWS EC2.

