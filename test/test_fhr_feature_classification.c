// SYSTEM INCLUDE 
#include <stdio.h>
#include <stdlib.h> 
#include <float.h>
#include <limits.h>
#include <string.h>

// LOCAL INCLUDE 
#include "./unity/unity.h"

// PROJECT INCLUDE 
#include "biorithm_unit_test.h"
#include "./../fhr/biorithm_aux.h"
#include "./../fhr/fhr_findpeaks.h"
#include "./../fhr/fhr_feature_detection.h"
#include "./../fhr/fhr_feature_classification.h"

// #define TEST_FHR_FCLASS_PRINT 

#define FHR_DOUBLE_PRECISION 0.01f 


/*------------------------------------------------------------------------------
Setup: 
Description: 
------------------------------------------------------------------------------*/
void setUp(void)
{

}

/*------------------------------------------------------------------------------
Tear down:
Description: 
------------------------------------------------------------------------------*/
void tearDown(void)
{

}


void test_find_peak_ecg()
{
	// paramter for find peak ecg 
	int wlen = 25000; 
	int dmin = 15; 
	double mpeak_thresh = 2.0; 
	double fpeak_thresh = 0.2; 

	// signal input 
	unsigned int siglen = 25000 * 64; 
	double_vec *p_ecg = double_vec_new(siglen);
	read_from_txt("./data/find_peak_ecg_in.txt", p_ecg);

	// matlab output 
	unsigned int npeak_matlab = 5469;
	peak_vec *p_expected = new_peak_vec(npeak_matlab);
	double pks, width, prominence = 0.0; 
	unsigned int locs;
	FILE *p_m = fopen("./data/find_peak_ecg_out_matlab.txt", "r");
	for (unsigned i = 0; i < npeak_matlab; i++)
	{
		fscanf(p_m, "%lg %u %lg %lg\n", &pks, &locs, &width, &prominence);
		PEAK_CONSTRUCT(p_expected->p_peak[i], locs-1, 0, 0, pks, width, 
			prominence, false);
	}
	fclose(p_m);

	// find peak ecg 
	peak_vec *p_actual = find_peak_ecg(p_ecg, wlen, dmin, mpeak_thresh);

	// print result 
	#ifdef TEST_FHR_FCLASS_PRINT
	printf("actual: \n");
	printf("found %d peak \n", p_actual->len);
	print_peak_vec(p_actual, p_actual->len);
	printf("expected: \n");
	print_peak_vec(p_expected, p_expected->len);
	#endif 

	// assert 
	for (unsigned int i = 0; i < p_actual->len; i++)
	{
		if (p_actual->p_peak[i].loc != p_expected->p_peak[i].loc)
		{
			printf("FAIL at i = %u: ", i);
			printf("expected loc = %u ", p_expected->p_peak[i].loc);
			printf("actual loc = %u\n", p_actual->p_peak[i].loc);
			return;
		}
	}

	// for (unsigned int i = 0; i < p_actual->len; i++)
	// {
	// 	if (fabs(p_actual->p_peak[i].width - p_expected->p_peak[i].width)
	// 		> FHR_DOUBLE_PRECISION * fabs(p_actual->p_peak[i].width))
	// 	{
	// 		printf("FAIL at i = %u: ", i);
	// 		printf("expected loc = %3.5f ", p_expected->p_peak[i].width);
	// 		printf("actual loc = %3.5f\n", p_actual->p_peak[i].width);
	// 		return;
	// 	}
	// }

	// free mem 
}

void test_get_peak_width_rheight()
{
	// init and parameter 
	int wmax = 50; 
	unsigned int siglen = 288; 
	unsigned int npeak = 26; 

	// input 
	double_vec *p_iprod = double_vec_new(siglen);
	double_vec *p_rheight = double_vec_new(npeak);
	peak_vec *p_rpeak = new_peak_vec(npeak);

	// expected output 
	peak_vec *p_expected = new_peak_vec(npeak);
	peak_vec_read("./data/sunspot_peak_width.txt", p_expected); 

	// read innder product from file 
	read_from_txt("./data/sunspot_dat.txt", p_iprod);

	// // read p_rpeak from file 
	peak_vec_read("./data/sunspot_peak_width.txt", p_rpeak);

	// // read p_rheight from file 
	read_from_txt("./data/sunspot_rheight.txt", p_rheight);

	// // get peak width with reference height 
	peak_vec *p_mpeak = get_peak_width_rheight(p_iprod, p_rpeak,
	p_rheight, wmax);

	// assert 
	TEST_ASSERT_EQUAL_PEAK_ARRAY(p_mpeak->p_peak, p_expected->p_peak, npeak);

	// print 
	#ifdef TEST_FHR_FCLASS_PRINT
	printf("actual: \n");
	print_peak_vec(p_mpeak, 10);
	// printf("rpeak: \n");
	// print_peak_vec(p_rpeak,10);
	printf("expected: \n");
	print_peak_vec(p_expected, 10);
	#endif

	// free mem
}

void test_rmv_mpeak_from_iprod()
{
	// init and parameter 
	// init and parameter 
	int wmax        	 = 50; 
	int wlen      		 = 288;
	double rheight 		 = 2.0;
	unsigned int siglen  = 288; 
	unsigned int npeak   = 26; 

	// load input 
	double_vec *p_iprod = double_vec_new(siglen);
	double_vec *p_rheight = double_vec_new(npeak);
	peak_vec *p_rpeak = new_peak_vec(npeak);

	// expected output 
	double_vec *p_expected = double_vec_new(siglen);
	read_from_txt("./data/sunspot_rmv_peak_matlab.txt", p_expected);

	// read innder product from file 
	read_from_txt("./data/sunspot_dat.txt", p_iprod);

	// read p_rpeak from file 
	peak_vec_read("./data/sunspot_peak_width.txt", p_rpeak);

	// remove mpeak from signal 
	rmv_mpeak_from_iprod(p_iprod, p_rpeak, wlen, rheight, wmax);

	// assert
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_iprod->p_data, p_expected->p_data, siglen);

	#ifdef TEST_FHR_FCLASS_PRINT
	double_vec_print(p_iprod,p_iprod->len);
	#endif
}

void test_compute_mfpeak()
{
	// // paramter sunspot data 
	// int wlen = 288; 
	// unsigned int tkeolen = 288; 
	// unsigned int iprodlen = 288; 
	// // number of expected mpeak 
	// unsigned int nmpeak = 18; 
	// // number of expected fpeak 
	// unsigned int nfpeak = 20;

	// parameter NUH 
	int wlen = 25000;
	unsigned int tkeolen = 25000; 
	unsigned iprodlen = 25000;
	unsigned int nmpeak = 79; 
	unsigned int nfpeak = 79;

	// min peak distance 
	int p_mfpeak_dmin[2]    = {15, 15};
	// min height for mfpeak and channels 
	double p_mfpeak_hmin[FHR_NCHANNEL * 2] = {2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0}; 
	// reference height for each channel for rmv maternal peak 
	double p_rheight[FHR_NCHANNEL]     = {2.0, 2.0, 2.0, 2.0};
	// width max 
	int wmax = 50; 

	// input tkeo 
	double_vec *p_tkeo = double_vec_new(tkeolen);
	// read_from_txt("./data/sunspot_dat.txt", p_tkeo);
	read_from_txt("./data/NUH_1021_tkeo.txt",p_tkeo);

	// input iprod TODO: read 4 channel iprod from 1 file 
	double_vec **pp_iprod = malloc(FHR_NCHANNEL * sizeof(double_vec *));
	pp_iprod[0] = double_vec_new(iprodlen);
	pp_iprod[1] = double_vec_new(iprodlen);
	pp_iprod[2] = double_vec_new(iprodlen);
	pp_iprod[3] = double_vec_new(iprodlen);
	// read_from_txt("./data/sunspot_dat.txt", pp_iprod[0]);
	// read_from_txt("./data/sunspot_dat.txt", pp_iprod[1]);
	// read_from_txt("./data/sunspot_dat.txt", pp_iprod[2]);
	// read_from_txt("./data/sunspot_dat.txt", pp_iprod[3]);
	read_from_txt("./data/NUH_1021_tkeo.txt", pp_iprod[0]);
	read_from_txt("./data/NUH_1021_tkeo.txt", pp_iprod[1]);
	read_from_txt("./data/NUH_1021_tkeo.txt", pp_iprod[2]);
	read_from_txt("./data/NUH_1021_tkeo.txt", pp_iprod[3]);

	// expected output 1 mpeaak and 4 fpeak vector 


	// expected maternal peaks
	peak_vec *p_expected_mpeak = new_peak_vec(nmpeak);
	// expected fetal peaks
	peak_vec *p_expected_fpeak = new_peak_vec(nfpeak);
	// expected maternal peak location
	double_vec *p_expected_mpeak_locs = double_vec_new(nmpeak);
	// expected fetal peak location 
	double_vec *p_expected_fpeak_locs = double_vec_new(nfpeak);

	// peak_vec_read("./data/sunspot_peak_matlab.txt", p_expected_mpeak);
	// peak_vec_read("./data/sunspot_fpeak_matlab.txt", p_expected_fpeak);
	peak_vec_read("./data/NUH_1021_mpeak_matlab.txt", p_expected_mpeak);
	peak_vec_read("./data/NUH_1021_fpeak_matlab.txt", p_expected_fpeak);

	for (unsigned int i = 0; i < nmpeak; i++)
	{
		p_expected_mpeak_locs->p_data[i] = p_expected_mpeak->p_peak[i].loc;
	}

	for (unsigned int i = 0; i < nfpeak; i++)
	{
		p_expected_fpeak_locs->p_data[i] = p_expected_fpeak->p_peak[i].loc;
	}

	// print input 
	#ifdef TEST_FHR_FCLASS_PRINT
	// printf("tkeo\n");
	// double_vec_print(p_tkeo,10);
	// printf("iprod channel 0\n");
	// double_vec_print(pp_iprod[0],10);
	#endif

	// actual output compute mf peak 
	struct mfpeak *p_mfpeak = compute_mfpeak(p_tkeo, pp_iprod, wlen, p_mfpeak_dmin,
		p_mfpeak_hmin, p_rheight, wmax);

	double_vec *p_mpeak_locs = double_vec_new(p_mfpeak->p_mpeak->len);
	double_vec *p_fpeak_locs = double_vec_new(p_mfpeak->p_fpeak[0]->len);

	for (unsigned int i = 0; i < p_mfpeak->p_mpeak->len; i++) {
		p_mpeak_locs->p_data[i] = p_mfpeak->p_mpeak->p_peak[i].loc; 
	}

	for(unsigned int i = 0; i < nfpeak; i++) {
		p_fpeak_locs->p_data[i] = p_mfpeak->p_fpeak[0]->p_peak[i].loc;
	}

	// print 
	#ifdef TEST_FHR_FCLASS_PRINT
	printf("mpeak\n");
	print_peak_vec(p_mfpeak->p_mpeak,p_mfpeak->p_mpeak->len);
	printf("fpeak channel 0\n");
	print_peak_vec(p_mfpeak->p_fpeak[0],p_mfpeak->p_fpeak[0]->len);
	printf("fpeak channel 1\n");
	// print_peak_vec(p_mfpeak->p_fpeak[1],10);
	printf("fpeak channel 2\n");
	// print_peak_vec(p_mfpeak->p_fpeak[2],10);
	printf("fpeak channel 3\n");
	// print_peak_vec(p_mfpeak->p_fpeak[3],10);
	#endif

	// print
	#ifdef TEST_FHR_FCLASS_PRINT 
	for (unsigned int i = 0; i < nmpeak; i++)
	{
		printf("i = %d, expected mpeak = %3.5f, actual mpeak = %3.5f\n", i, 
			p_expected_mpeak_locs->p_data[i],
			p_mpeak_locs->p_data[i]);	
	}

	for (unsigned int i = 0; i <nfpeak; i++)
	{
		printf("i = %d, expected fpeak = %3.5f, actual fpeak = %3.5f\n", i,
			p_expected_fpeak_locs->p_data[i],
			p_fpeak_locs->p_data[i]);		
	}
	#endif

	// assert maternal peak locations 
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_expected_mpeak_locs->p_data,
		p_mpeak_locs->p_data, nmpeak);

	// assert fetal peak location 
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_expected_fpeak_locs->p_data, 
		p_fpeak_locs->p_data, nfpeak);

}

//==============================================================================
int main(void)
{

	// UNITY_BEGIN();
	// RUN_TEST(test_find_peak_ecg);
	// BIORITHM_RUN_TEST(test_get_peak_width_rheight);
	RUN_TEST(test_rmv_mpeak_from_iprod);
	RUN_TEST(test_compute_mfpeak);

	// test_compute_mfpeak();

	return 0;
}

//gcc test_fhr_feature_classification.c biorithm_unit_test.c ./unity/unity.c ./../fhr/biorithm_aux.c ./../fhr/fhr_feature_classification.c ./../fhr/fhr_findpeaks.c -lm -lgsl -lgslcblas 
