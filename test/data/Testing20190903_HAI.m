clear all;
close all
clc

recordName = '1021.csv';
% A1=csvread(recordName,1,1);
A1 = readmatrix(recordName);
A1 = A1(:,2:5);

%% Remove Nan and Zero values

nan_idx = find(isnan(A1));
A1(nan_idx) = 1e-16;

zero_idx = find(~A1);
A1(zero_idx) = 1e-16;

ch =4;
fs=500;

%% Line noise filtering 
for y=1:ch 
Wo = 50/(fs/2);
BW = Wo/35; 
[b,a] = iirnotch(Wo,BW); 
Y(:,y) = filter(b,a,A1(:,y)); 
end

for x=1:ch 
Wo = 60/(fs/2);
BW = Wo/35; 
[b,a] = iirnotch(Wo,BW); 
X(:,x) = filter(b,a,Y(:,x)); 
end

%% Removal of Baseline wander

lvl=8;
for ii =1:ch
[c,l] = wavedec(X(:,ii),lvl,'db6');
B=wrcoef('a',c,l,'db6',lvl);
AB(:,ii)=X(:,ii)-B;
end

%% High frequency components removal

for xx=1:ch
 [THR(xx),SORH(xx),KEEPAPP(xx)]=ddencmp('den','wv',AB(:,xx));
 level=4; 
 [ecg_clean(:,xx),CecgC(:,xx),LecgC(:,xx),PERF0(:,xx),PERFL2(:,xx)]=wdencmp('gbl',AB(:,xx),'coif4',level,THR(xx),SORH(xx),KEEPAPP(xx)); 
end

%% HT
tkwin = round(fs/20);

for jj=1:ch
AHFrec(:,jj)= HTfilter(ecg_clean(:,jj), 399, 0.1);
%AHFrec(:,jj)= HTfilter(AB(:,jj), 399, 0.1);

IPHT(:,jj) = angle(AHFrec(:,jj));
IFHT(:,jj) = IPHT(2:end,jj)-IPHT(1:end-1,jj);
distptsAH(:,jj)  = abs(AHFrec(2:end,jj)-AHFrec(1:end-1,jj));
distptsAHf(:,jj) = distptsAH(:,jj).*(1./IFHT(:,jj));
TKEO(:,jj)= distptsAH(tkwin+1:length(distptsAH)-tkwin-1,jj).^2 - distptsAH(1:length(distptsAH)-2*tkwin-1,jj).*distptsAH(2*tkwin+2:length(distptsAH),jj);
% normalise during 15sec window distptsAHN(:,jj) = distptsAH(:,jj)./max(distptsAH(:,jj)); 
end



%% Maternal R peak detection
tkeoPeakLocs = [];
locsf=[];
pksf =[];

for ii = 2
    for jj = 1:10*fs:length(TKEO)-10*fs-1
        [pks,locs] = findpeaks(TKEO(jj:jj+10*fs,ii),'MinPeakHeight',std(TKEO(jj:jj+10*fs,ii))*4,'MinPeakDistance',10);
        pksf = [pksf;pks+ jj];
        locsf = [locsf;(locs + jj-1)];
        
        peak.loc = locs; 
        tkeoPeakLocs = [tkeoPeakLocs; peak];
        clear locs
        clear pks 
    end
end 

figure(1000)
% figure_handle = figure;hold on 
subplot(4,1,1)
hold on
plot(TKEO(:,1))
plot(locsf, TKEO(locsf,1),'r*')
hold off
subplot(4,1,2)
hold on
plot(TKEO(:,2))
plot(locsf, TKEO(locsf,2),'r*')
hold off
subplot(4,1,3)
hold on
plot(TKEO(:,3))
plot(locsf, TKEO(locsf,3),'r*')
hold off
subplot(4,1,4)
hold on
plot(TKEO(:,4))
plot(locsf, TKEO(locsf,4),'r*')
hold off
all_ha = findobj(figure(1000), 'type', 'axes', 'tag', '' );
linkaxes( all_ha, 'x' );

%% Remove Maternal QRS 

IFHTc = IFHT;
WW = round(0.02*fs);
threshIF = 0.2*pi; 

for ll=1:4
% [mpks,mlocs] = findpeaks(abs(IFHTc(:,ll)),'MinPeakHeight',2, 'MinPeakDistance', 0);
% for mm=1:length(mlocs)
%     
%    IFHTc(mlocs(mm),ll) = (IFHTc(mlocs(mm)-1,ll)+IFHTc(mlocs(mm)+1,ll))/2; 
%    
% end

    idx = find(abs(IFHTc(:,ll)) > threshIF);
%     for mm = WW+1:length(idx)-WW-1
%         IFHTc(idx(mm),ll) = sign(IFHT(idx(mm),ll))*median(abs(IFHT(idx(mm) - WW : idx(mm) + WW, ll)));
%     end 
    IFHTc(idx,ll) = 0.0; 

end

% for ll = 1:4
%     idx1 = find(IFHTc(:,ll)>2);
%     idx2 = find(IFHTc(:,ll)<-2);
%     IFHTc(idx1,ll) = 0;
%     IFHTc(idx2,ll) = 0;
% end 

% %% Inner product - TKEO 
% ipwin = round(0.02*fs);
% IFHT_dp = IFHTc(tkwin+1:end-25-1,:);
% 
% for nn =1:ch 
%     count = 1;
% 
%     for kk = 1:length(TKEO)-ipwin-1
%     
%      IProd(count,nn) = sum((TKEO(kk:kk+ipwin-1,nn)).*(IFHT_dp(kk:kk+ipwin-1,nn)));
%      count =count+1;
%       
%     
%     end
%     
% end

%% Inner product 
ipwin = round(0.02*fs);
ipwin = 11;

for nn =1:ch 
    count = 1;

    for kk = 1:length(distptsAH)-ipwin-1
  
     IProd(count,nn) = sum((distptsAH(kk:kk+ipwin-1,nn)).*(abs(IFHTc(kk:kk+ipwin-1,nn))));
     count =count+1;
      
    end
    
end
% 

%% HAI SEP. 05 2019 
% as =  288000;
% ae =  as + 5000;
% ac  = 1;
% figure(1001)
% subplot(611)
% plot(real(AHFrec(as:ae,ac)));
% title('Re(AHFrec)')
% subplot(612)
% plot(distptsAH(as:ae,ac),'r')
% title('distpsAH')
% subplot(613)
% plot(IPHT(as:ae,ac));
% title('IPHT')
% subplot(614)
% plot(IFHT(as:ae,ac));
% title('IFHT')
% subplot(615)
% plot(IFHTc(as:ae,ac),'r');
% title('IFHTc')
% subplot(616)
% plot(IProd(as:ae,ac),'r');
% title('IProd')

figure(1001)
subplot(511)
plot(real(AHFrec(:,2)),'k')
title('real AHFrec')
subplot(512)
plot(distptsAH(:,2),'b');
title('distptsAH')
subplot(513)
plot(TKEO(:,2),'r');
title('TKEO')
subplot(514)
plot(IFHTc(:,2),'b')
title('IFHTc')
subplot(515)
plot(IProd(:,2),'m');
title('IProd')
all_ha = findobj( figure(1001), 'type', 'axes', 'tag', '' );
linkaxes( all_ha, 'x' );

% as = as + 800;
% figure(1002)
% subplot(311)
% yline(0);
% hold on;
% plot(real(AHFrec(as:ae,ac)),'k-o');
% plot(imag(AHFrec(as:ae,ac)),'r-*');
% legend('zero','real(AHFrec)','image(AHFrec)')
% subplot(312)
% plot(angle(AHFrec(as:ae,ac)));
% title('angle(AHFrec)')
% subplot(313)
% plot((IFHT(as:ae,ac)),'k');
% hold on;
% plot((IFHTc(as:ae,ac)),'r');
% legend('IFHT','IFHTc')

tkwin=tkwin;

% figure_handle = figure;hold on 
% subplot(4,1,1)
% hold on
% plot(IProd(:,1))
% plot(locsf+tkwin, IProd(locsf+tkwin,1),'r*')
% hold off
% subplot(4,1,2)
% hold on
% plot(IProd(:,2))
% plot(locsf+tkwin, IProd(locsf+tkwin,2),'r*')
% hold off
% subplot(4,1,3)
% hold on
% plot(IProd(:,3))
% plot(locsf+tkwin, IProd(locsf+tkwin,3),'r*')
% hold off
% subplot(4,1,4)
% hold on
% plot(IProd(:,4))
% plot(locsf+tkwin, IProd(locsf+tkwin,4),'r*')
% hold off
% all_ha = findobj( figure_handle, 'type', 'axes', 'tag', '' );
% linkaxes( all_ha, 'x' );

%% Detect fetal QRS 

locsff1=[];
locsff2=[];
locsff3=[];
locsff4=[];

% pWidth1 = [];
% pWidth2 = [];
% pWidth3 = [];
% pWidth4 = [];

% locsm1 = [];
% pWidthm1 = [];


pksff =[];

IProd_sum = sum(IProd,2);


    for jj = 1:10*fs:length(IProd)-10*fs-1
        [pks,locs,w] = findpeaks(IProd(jj:jj+10*fs,1),'MinPeakHeight',std(IProd(jj:jj+10*fs,1))*0.4,'MinPeakDistance',10,'WidthReference','halfheight');
        
        %pksff(:,ch) = [pksf(:,ch);pks];
        locsff1 = [locsff1;(locs + jj -1)];
%         pWidth1 = [pWidth1;w];
        
        
        
        clear locs
        clear pks 
        clear w 
    end
    
    for jj = 1:10*fs:length(IProd)-10*fs-1
        [pks,locs, w] = findpeaks(IProd(jj:jj+10*fs,2),'MinPeakHeight',std(IProd(jj:jj+10*fs,2))*0.5,'MinPeakDistance',10,'WidthReference','halfheight');
%         [pksm,locsm, wm] = findpeaks(IProd(jj:jj+10*fs,2),'MinPeakHeight',std(IProd(jj:jj+10*fs,2))*5,'MinPeakDistance',10,'WidthReference','halfheight');
        
        %pksff(:,ch) = [pksf(:,ch);pks];
        locsff2 = [locsff2;(locs + jj -1)];
%         pWidth2 = [pWidth2;w];
        
%         locsm1 = [locsm1; (locsm + jj - 1)];
%         pWidthm1 = [pWidthm1; wm];
        
        clear locs
        clear pks 
        clear w
%         clear wm
%         clear locsm
%         clear pksm
    end
    
    for jj = 1:10*fs:length(IProd)-10*fs-1
        [pks,locs,w] = findpeaks(IProd(jj:jj+10*fs,3),'MinPeakHeight',std(IProd(jj:jj+10*fs,3))*0.5,'MinPeakDistance',10,'WidthReference','halfheight');
        
        %pksff(:,ch) = [pksf(:,ch);pks];
        locsff3 = [locsff3;(locs + jj -1)];
%         pWidth3 = [pWidth3;w];
        
        clear locs
        clear pks 
        clear w;
    end
    
    for jj = 1:10*fs:length(IProd)-10*fs-1
        [pks,locs,w] = findpeaks(IProd(jj:jj+10*fs,4),'MinPeakHeight',std(IProd(jj:jj+10*fs,4))*0.3,'MinPeakDistance',10,'WidthReference','halfheight');
        
        %pksff(:,ch) = [pksf(:,ch);pks];
        locsff4 = [locsff4;(locs + jj -1)];
%         pWidth4 = [pWidth4;w];
        
        clear locs
        clear pks 
        clear w;
    end

ipwin = 10;    
locsf_tkeo_on_prod = locsf + tkwin - ipwin/2; 

figure(1002)
subplot(4,2,1)
hold on
plot(IProd(:,1))
plot(locsff1, IProd(locsff1,1),'r*')
plot(locsf_tkeo_on_prod, IProd(locsf_tkeo_on_prod,1),'ko')
hold off
subplot(4,2,2)
hold on
plot(ecg_clean(:,1))
plot(locsff1 + ipwin/2+1, ecg_clean(locsff1 + ipwin/2+1 ,1),'r*')
plot(locsf+tkwin+1, IProd(locsf+tkwin-ipwin/2,1),'ko')
hold off
subplot(4,2,3)
hold on
plot(IProd(:,2))
plot(locsff2, IProd(locsff2, 2),'r*')
plot(locsf_tkeo_on_prod, IProd(locsf_tkeo_on_prod,2),'ko')
hold off
subplot(4,2,4)
hold on
plot(ecg_clean(:,2))
plot(locsff2 + ipwin/2+1, ecg_clean(locsff2 + ipwin/2+1, 2),'r*')
plot(locsf+tkwin, IProd(locsf+tkwin-ipwin/2, 2),'ko')
hold off
subplot(4,2,5)
hold on
plot(IProd(:,3))
plot(locsff3, IProd(locsff3,3),'r*')
plot(locsf_tkeo_on_prod, IProd(locsf_tkeo_on_prod,3),'ko')
hold off
subplot(4,2,6)
hold on
plot(ecg_clean(:,3))
plot(locsff3 + ipwin/2+1, ecg_clean(locsff3 + ipwin/2+1,3),'r*')
plot(locsf+tkwin, IProd(locsf+tkwin-ipwin/2,3),'ko')
hold off
subplot(4,2,7)
hold on
plot(IProd(:,4))
plot(locsff4, IProd(locsff4,4),'r*')
plot(locsf_tkeo_on_prod, IProd(locsf_tkeo_on_prod,4),'ko')
hold off
subplot(4,2,8)
hold on
plot(ecg_clean(:,4))
plot(locsff4 + ipwin/2 +1, ecg_clean(locsff4 + ipwin/2 +1,4),'r*')
plot(locsf+tkwin, ecg_clean(locsf+tkwin-ipwin/2,4),'ko')
hold off
all_ha = findobj( figure(1002), 'type', 'axes', 'tag', '' );
linkaxes( all_ha, 'x' );

%% remove maternal signal from IProd using TKEO mark 
ipwin = 10;
IProd1 = IProd;
widtha = [];
wmax = 50; 
offset = tkwin - ipwin/2;
 
for ii = 1:4
    count = 1; 
    width.x = [];
    width.y = [];
    width.z = [];
    
    for jj = 1:10*fs:length(TKEO)-10*fs-1
         
        % locs from TKEO 
        locs = tkeoPeakLocs(count).loc; 
        count = count  +  1; 
        
        % signal from IProd 
        chunk = IProd1(jj+offset:jj+offset+10*fs,ii);
        thresh = mean(chunk);
       
        % get peak width, peak loc from TKEO 
        w = getPeakWidth(chunk, locs, thresh, wmax);
        
        w.x = w.x + jj - 1; 
        w.y = w.y + jj - 1; 
        width.x = [width.x; w.x];
        width.y = [width.y; w.y];
        width.z = [width.z; w.z];
        
        clear locs
        clear w;
    end
    widtha = [widtha; width];
    
end 

% loop through each channel 1 to 4 
for ii = 1:4
    width = widtha(ii);
    % loop through each maternal peak location obtained from TKEO 
    for kk = 1:length(locsf)
        a = width.x(kk) + tkwin - ipwin/2;
        b = width.y(kk) + tkwin - ipwin/2;
        IProd1(a:b,ii) = 0; 
    end
end

%% fetal peak only 
locsfc1 = [];
locsfc2 = [];
locsfc3 = [];
locsfc4 = [];

for jj = 1:10*fs:length(IProd1)-10*fs-1
    [pks,locs, w] = findpeaks(IProd1(jj:jj+10*fs,1),'MinPeakHeight',std(IProd1(jj:jj+10*fs,1))*3,'MinPeakDistance',10);
    locsfc1 = [locsfc1;(locs + jj -1)];
    clear locs
end

for jj = 1:10*fs:length(IProd1)-10*fs-1
    [pks,locs, w] = findpeaks(IProd1(jj:jj+10*fs,2),'MinPeakHeight',std(IProd1(jj:jj+10*fs,2))*3,'MinPeakDistance',10);
    locsfc2 = [locsfc2;(locs + jj -1)];
    clear locs
end

for jj = 1:10*fs:length(IProd1)-10*fs-1
    [pks,locs, w] = findpeaks(IProd1(jj:jj+10*fs,3),'MinPeakHeight',std(IProd1(jj:jj+10*fs,3))*3,'MinPeakDistance',10);
    locsfc3 = [locsfc3;(locs + jj -1)];
    clear locs
end

for jj = 1:10*fs:length(IProd1)-10*fs-1
    [pks,locs, w] = findpeaks(IProd1(jj:jj+10*fs,4),'MinPeakHeight',std(IProd1(jj:jj+10*fs,4))*3,'MinPeakDistance',10);
    locsfc4 = [locsfc4;(locs + jj -1)];
    clear locs
end

figure(1003)
subplot(421)
plot(IProd(:,1));
hold on; 
plot(IProd1(:,1),'m')
plot(locsf + tkwin - ipwin/2, IProd(locsf+tkwin-ipwin/2,1),'k*')
plot(locsfc1, IProd1(locsfc1,1),'ro');

subplot(422)
hold on
plot(ecg_clean(:,1))
plot(locsfc1 + ipwin/2+1, IProd1(locsfc1+ipwin/2+1,1),'r*')
plot(locsf+tkwin+1, IProd(locsf+tkwin-ipwin/2,1),'ko')
hold off

subplot(423)
plot(IProd(:,2));
hold on; 
plot(IProd1(:,2),'m')
plot(locsf + tkwin - ipwin/2, IProd(locsf+tkwin-ipwin/2,2),'k*')
plot(locsfc2, IProd1(locsfc2,2),'ro');

subplot(424)
hold on
plot(ecg_clean(:,2))
plot(locsfc2 + ipwin/2+1, IProd1(locsfc2+ipwin/2+1,2),'r*')
plot(locsf+tkwin+1, IProd(locsf+tkwin-ipwin/2,2),'ko')
hold off

subplot(425)
plot(IProd(:,3));
hold on; 
plot(IProd1(:,3),'m')
plot(locsf + tkwin - ipwin/2, IProd(locsf+tkwin-ipwin/2,3),'k*')
plot(locsfc3, IProd1(locsfc3,3),'ro');

subplot(426)
hold on
plot(ecg_clean(:,3))
plot(locsfc3 + ipwin/2+1, IProd1(locsfc3+ipwin/2+1,3),'r*')
plot(locsf+tkwin+1, IProd(locsf+tkwin-ipwin/2,3),'ko')
hold off

subplot(427)
plot(IProd(:,4));
hold on; 
plot(IProd1(:,4),'m')
plot(locsf + tkwin - ipwin/2, IProd(locsf+tkwin-ipwin/2,4),'k*')
plot(locsfc4, IProd1(locsfc4,4),'ro');

subplot(428)
hold on
plot(ecg_clean(:,4))
plot(locsfc4 + ipwin/2+1, IProd1(locsfc4 + ipwin/2 + 1,1),'r*')
plot(locsf+tkwin+1, IProd(locsf+tkwin-ipwin/2,1),'ko')
hold off
all_ha = findobj( figure(1003), 'type', 'axes', 'tag', '' );
linkaxes( all_ha, 'x' );


% figure(1004)
% subplot(511)
% plot(real(AHFrec(:,2)),'k')
% title('real AHFrec')
% subplot(512)
% plot(distptsAH(:,2),'b');
% title('distptsAH')
% subplot(513)
% plot(TKEO(:,2),'r');
% title('TKEO')
% subplot(514)
% plot(IFHTc(:,2),'b')
% title('IFHTc')
% subplot(515)
% plot(IProd(:,2),'m');
% title('IProd')
% all_ha = findobj( figure(1004), 'type', 'axes', 'tag', '' );
% linkaxes( all_ha, 'x' );

%% fetal heart rate 
FRt12 = locsfc1';
FRt22 = locsfc2';
FRt32 = locsfc3';
FRt42 = locsfc4';

% CONVERT RR INTERVAL TO BPM 
FRR12 = FRt12(2:end)-FRt12(1:end-1)  + 0*round(tkwin/2);
FHR12 = 60./(FRR12./fs);

FRR22 = FRt22(2:end)-FRt22(1:end-1) + 0*round(tkwin/2);
FHR22 = 60./(FRR22./fs);

FRR32 = FRt32(2:end)-FRt32(1:end-1) + 0*round(tkwin/2);
FHR32 = 60./(FRR32./fs);

FRR42 = FRt42(2:end)-FRt42(1:end-1) + 0*round(tkwin/2);
FHR42 = 60./(FRR42./fs);

% CONCATE THEM 
FHRt = [FHR42 FHR32 FHR22; FRt42(2:end) FRt32(2:end) FRt22(2:end)];

% RMV OUTLIERS
ULim = 200;
LLim = 100;
DistLim = 6;

% CHECK ULIM AND DLIM
for ll = 1:length(FHR12)

    if FHR12(ll)>ULim
        FHR12(ll) = NaN;
    elseif FHR12(ll)<LLim
        FHR12(ll) = NaN;
    end
    
end

% CHECK DIVLIM 
for hh = 2:length(FHR12)
    
    if abs(FHR12(hh)-FHR12(hh-1))>DistLim
        FHR12(hh) = NaN;
    end
end

for ll = 1:length(FHR22)

    if FHR22(ll)>ULim
        FHR22(ll) = NaN;
    elseif FHR22(ll)<LLim
        FHR22(ll) = NaN;
    end
    
end

for hh = 2:length(FHR22)
    if abs(FHR22(hh)-FHR22(hh-1))>DistLim
        FHR22(hh) = NaN;
    end
end

for ll = 1:length(FHR32)

    if FHR32(ll)>ULim
        FHR32(ll) = NaN;
    elseif FHR32(ll)<LLim
        FHR32(ll) = NaN;
    end
    
end

for hh = 2:length(FHR32)
    
    if abs(FHR32(hh)-FHR32(hh-1))>DistLim
        FHR32(hh) = NaN;
    end
end

for ll = 1:length(FHR42)

    if FHR42(ll)>ULim
        FHR42(ll) = NaN;
    elseif FHR42(ll)<LLim
        FHR42(ll) = NaN;
    end
    
end

for hh = 2:length(FHR42)
    
    if abs(FHR42(hh)-FHR42(hh-1))>DistLim
        FHR42(hh) = NaN;
    end
end

% SAME FOR FHRT
for ll = 1:length(FHRt)

    if FHRt(1,ll)>ULim
        FHRt(1,ll) = NaN;
    elseif FHRt(1,ll)<LLim
        FHRt(1,ll) = NaN;
    end
    
end

for hh = 2:length(FHRt)
    
    if abs(FHRt(1,hh)-FHRt(1,hh-1))>DistLim
        FHRt(1,hh) = NaN;
    end
end


%% amrish 
figure(1004)
hold on
subplot(5,1,1)
plot(FHR12)
subplot(5,1,2)
plot(FHR22)
subplot(5,1,3)
plot(FHR32)
subplot(5,1,4)
plot(FHR42)
subplot(5,1,5)
plot(FHRt(2,:),FHRt(1,:))
hold off

% fprintf('save figure ...');
% savefig(figure(1001),strcat('./fig/',recordName,'_tkeo.fig'));
% savefig(figure(1002),strcat('./fig/',recordName,'_ecg_iprod.fig'));
% savefig(figure(1003),strcat('./fig/',recordName,'_ecg_iprod1.fig'));
% savefig(figure(1004),strcat('./fig/',recordName,'_fhr_trace.fig'));

% FHRF1 = FHRt(1,:);
% FHRF1i = find(~isnan(FHRt(1,:)));
% FHRFii = find(FHRF1i==1);
% FRF = round(FHRt(2,2:end)./2);% beacuse of sampling rate. Correct by downsampling!!
% FHRF1 = FHRF1(FHRF1i)+10;
% FRF = FRF(FHRF1i);
% 
% for ii = 2:length(FHRF1)
%    if FHRF1(ii)-FHRF1(ii-1)>10
%        FHRF1(ii)=NaN;
%    end
% end
% 
% FHRFii = find(~isnan(FHRF1));
% FHRF = FHRF1(FHRFii);
% FRF = FRF(FHRFii);
% [B,I] = sort(FRF);
% FHRF = FHRF(I);
% FRF = FRF(I);
% 
% 
% FHRF1 = FHR32;
% FHRF1i = find(~isnan(FHR32));
% %FHRFii = find(FHRF1i==1);
% FRF = round(FRt32./2);% beacuse of sampling rate. Correct by downsampling!!
% FHRF1 = FHRF1(FHRF1i)+10;
% FRF = FRF(FHRF1i);
% 
% for ii = 2:length(FHRF1)
%    if FHRF1(ii)-FHRF1(ii-1)>10
%        FHRF1(ii)=NaN;
%    end
% end
% 
% FHRFii = find(~isnan(FHRF1));
% FHRF = FHRF1(FHRFii);
% FRF = FRF(FHRFii);
% 
% FHRR = [FRF' FHRF' zeros(length(FRF),1) zeros(length(FRF),1) ];
% 
% FRR42 = FRt42(2:end)-FRt42(1:end-1) + 0*round(tkwin/2);
% FHR42 = 60./(FRR42./fs);
% 
% FHRt = [FHR42 FHR32 FHR22;FRt42(2:end) FRt32(2:end) FRt22(2:end)];
% 
% 
