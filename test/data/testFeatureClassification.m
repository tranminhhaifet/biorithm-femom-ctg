%% HT SEP. 25 2019  
% test findpeak C, getPeakWidth for remove maternal peak 
clear all;
close all
clc


%% parameter 
fs = 500; 
ch =4;
mpeakThresh = 4.0; % find mpeak from best tkeo 
fpeakThresh = 2.0; % after rmv mpeak from iprod
FHR_WLEN = 10*fs; %5000
wlen = FHR_WLEN;  
dmin = 15; 
wmax = 50;

a = 1; 

if (a==1)
% recordName = "/Users/hai/matlab/biorithm/src/1021.csv";
recordName = "1002.csv";
A1 = readmatrix(recordName);
[nrow,ncol] = size(A1);
nWin  = floor(nrow/FHR_WLEN);

A1 = A1(1:FHR_WLEN * nWin,2:5);

%% Remove Nan and Zero values

nan_idx = find(isnan(A1));
A1(nan_idx) = 1e-16;

zero_idx = find(~A1);
A1(zero_idx) = 1e-16;

we = 1;
if(we == 1)
fileId = fopen("raw_ecg_in.txt","w");
fprintf(fileId, "%3.32f %3.32f %3.32f %3.32f\n", A1');
fclose(fileId);
end 

%% Line noise filtering 
for y=1:ch 
Wo = 50/(fs/2);
BW = Wo/35; 
[b,a] = iirnotch(Wo,BW); 
Y(:,y) = filter(b,a,A1(:,y)); 
end

for x=1:ch 
Wo = 60/(fs/2);
BW = Wo/35; 
[b,a] = iirnotch(Wo,BW); 
X(:,x) = filter(b,a,Y(:,x)); 
end

%% Removal of Baseline wander

lvl=8;
for ii =1:ch
[c,l] = wavedec(X(:,ii),lvl,'db6');
B=wrcoef('a',c,l,'db6',lvl);
AB(:,ii)=X(:,ii)-B;
end

%% High frequency components removal

for xx=1:ch
 [THR(xx),SORH(xx),KEEPAPP(xx)]=ddencmp('den','wv',AB(:,xx));
 level=4; 
 [ecg_clean(:,xx),CecgC(:,xx),LecgC(:,xx),PERF0(:,xx),PERFL2(:,xx)]=wdencmp('gbl',AB(:,xx),'coif4',level,THR(xx),SORH(xx),KEEPAPP(xx)); 
end

%% HT
tkwin = round(fs/20);

for jj=1:ch
AHFrec(:,jj)= HTfilter(ecg_clean(:,jj), 399, 0.1);
%AHFrec(:,jj)= HTfilter(AB(:,jj), 399, 0.1);

IPHT(:,jj) = angle(AHFrec(:,jj));
IFHT(:,jj) = IPHT(2:end,jj)-IPHT(1:end-1,jj);
distptsAH(:,jj)  = abs(AHFrec(2:end,jj)-AHFrec(1:end-1,jj));
distptsAHf(:,jj) = distptsAH(:,jj).*(1./IFHT(:,jj));
TKEO(:,jj)= distptsAH(tkwin+1:length(distptsAH)-tkwin,jj).^2 - distptsAH(1:length(distptsAH)-2*tkwin,jj).*distptsAH(2*tkwin+1:length(distptsAH),jj);
% normalise during 15sec window distptsAHN(:,jj) = distptsAH(:,jj)./max(distptsAH(:,jj)); 
end


% tkeo = TKEO(1:64*FHR_WLEN,2);
tkeo = TKEO(1:end,4);
dlmwrite("find_peak_ecg_in.txt", tkeo, 'precision', '%3.32f');
end 

% tkeo = dlmread("find_peak_ecg_in.txt");
nwin = floor(length(tkeo)/wlen);

pksAll = [];
locsAll = [];
wAll = [];
pAll = [];
% whAll = [];


  width.x = [];
  width.y = [];
  width.z = [];
  width.h = [];
  
  meanW  = zeros(nwin,1);
  rHeight = [];
  ecgNorm1 = [];

for kk = 1:nwin 
   
    ecgWin_1 = tkeo((kk-1)*wlen+1 : kk*wlen);
    
    meanW(kk) = mean(ecgWin_1);
    
    ecgWin_2 = ecgWin_1./max(abs(ecgWin_1)); 
    hmin = mpeakThresh * std(ecgWin_2); 
    
    [pks, locs, w, p] = findpeaks(ecgWin_2, 'MinPeakDistance', dmin, 'MinPeakHeight',  hmin, 'WidthReference', 'halfheight');
%     wh = getPeakWidth(ecgWin_1, locs, 2.0*mean(ecgWin_1), wmax);
    
    fprintf("std=%3.15f window %d has %d peak \n", std(ecgWin_2), kk, length(locs));
    
    locs = locs + wlen * (kk-1);
   
    
    pksAll = [pksAll;pks];
    locsAll = [locsAll;locs];
%     wAll = [wAll;w];
    pAll = [pAll;p];
%     whAll = [whAll;wh];
    
%     wh.x = wh.x + (kk-1) * wlen ; 
%     wh.y = wh.y + (kk-1) * wlen ; 
    
%     wh.h = ones(length(locs),1)*2*mean(ecgWin_1);
    
%     width.x = [width.x; wh.x];
%     width.y = [width.y; wh.y];
%     width.z = [width.z; wh.z];
    
%     width.h = [width.h; wh.h];
    
  
    ecgNorm1 = [ecgNorm1; ecgWin_2];
    
end 

mPeakMatlab = locsAll;


%% get peak width of each peak 
rHeight = zeros(length(locsAll),1);
for kk = 1:length(locsAll)
    pkloc = locsAll(kk); 
    rHeight(kk) = meanW( floor(pkloc / wlen) + 1); 
end 

width = getPeakWidthA(tkeo, locsAll, 2.0*rHeight, wmax);



fprintf("found %d mpeak from tkeo\n", length(locsAll));

t = 0;
if (t==1)
fileId = fopen("find_peak_ecg_out_matlab.txt","w");
fprintf(fileId, "%3.32f %u %3.32f %3.32f \n", [pksAll, locsAll, wAll, pAll]');
fclose(fileId);
end 

figure(1)
subplot(311)
plot(ecgNorm1);
hold on; 
plot(locsAll, ecgNorm1(locsAll),'ko');

b = 1;
if (b==1)
    figure(1)
    subplot(312)
    hold on; 
    for kk = 1:length(width.z)
        x1 = width.x(kk);
        x2 = width.y(kk);
        y1 = width.h(kk);
        line([x1,x2], [y1, y1], 'Color', 'red');
    end 
end 

%% WIDTH HISTOGRAM 
% % figure(2)
% histogram(width.z)

%% removal maternal peak 
iprod_rmv_mpeak = tkeo;
for kk = 1:length(width.x)
    % check 
    if (width.x(kk) < width.y(kk))
        iprod_rmv_mpeak(width.x(kk) : width.y(kk)) = 0.0;
    end 
end

figure(1)
subplot(312)
plot(tkeo)
hold on; 
plot(locsAll,tkeo(locsAll),'ko');

d = 0;
npeak = length(locsAll);
if (d==1)
    
% write tkeo 
fileId = fopen("NUH_1021_tkeo.txt","w");
fprintf(fileId,"%3.48f\n",tkeo');
fclose(fileId);

% write maternal peak 
fileId = fopen("NUH_1021_mpeak_matlab.txt", "w");
fprintf(fileId,"%d %d %d %3.15f %3.15f %3.15f %d\n",[locsAll-1,zeros(npeak,1), ... 
    zeros(npeak,1), pksAll,wAll,pAll, zeros(npeak,1)]');
fclose(fileId);


% write iprod after rmv maternal peak 
fileId = fopen("NUH_1021_tkeo_rmv_maternal.txt","w");
fprintf(fileId, "%3.48f\n",iprod_rmv_mpeak');
fclose(fileId);

% write fetal peak to file 
fileId = fopen("NUH_1021_fpeak_matlab.txt", "w");
fprintf(fileId,"%d %d %d %3.15f %3.15f %3.15f %d\n",[locsAll-1,zeros(npeak,1), ... 
    zeros(npeak,1), pksAll,wAll,pAll, zeros(npeak,1)]');
fclose(fileId);

end

%% find fetal peak after rmv maternal peak 
mpeakThresh = 4.0; 
pksAll = [];
locsAll = [];
wAll = [];
pAll = [];
whAll = [];
wmax = 50;

  width.x = [];
  width.y = [];
  width.z = [];
  width.h = [];
  
%   widthRef = [];
  ecgNorm2 = [];
  

for kk = 1:nwin 
   
    ecgWin = iprod_rmv_mpeak((kk-1)*wlen+1 : kk*wlen);
    ecgWin = ecgWin./max(abs(ecgWin)); 
    hmin = fpeakThresh * std(ecgWin); 
    
    [pks, locs, w, p] = findpeaks(ecgWin, 'MinPeakDistance', dmin, 'MinPeakHeight',  hmin, 'WidthReference', 'halfheight');
%     wh = getPeakWidth(ecgWin, locs, mean(ecgWin), wmax);
    
    fprintf("std=%3.15f window %d has %d peak \n", std(ecgWin), kk, length(locs));
    
    locs = locs + wlen * (kk-1);
    
    pksAll = [pksAll;pks];
    locsAll = [locsAll;locs];
%     wAll = [wAll;w];
    pAll = [pAll;p];
%     whAll = [whAll;wh];
    
%     wh.x = wh.x + (kk-1) * wlen ; 
%     wh.y = wh.y + (kk-1) * wlen ;    
%     wh.h = ones(length(locs),1)*mean(ecgWin);
    
%     width.x = [width.x; wh.x];
%     width.y = [width.y; wh.y];
%     width.z = [width.z; wh.z];  
%     width.h = [width.h; wh.h];
   
    ecgNorm2 = [ecgNorm2; ecgWin];
    
end 
fprintf("found %d fpeak from iprod\n", length(locsAll));

fPeakMatlab = locsAll;

%% 


figure(1)
subplot(312)
hold on; 
plot(locsAll, iprod_rmv_mpeak(locsAll),'r*');
% plot(ix,'k-+');

e = 0; 


if (e==1)

ix = dlmread("iprod_rmv_mpeak_c.txt");


nfpeak = length(locsAll);
    
fileId = fopen("NUH_1021_fpeak_matlab.txt", "w");
fprintf(fileId,"%d %d %d %3.15f %3.15f %3.15f %d\n",[locsAll-1, zeros(nfpeak,1), ... 
zeros(nfpeak,1), pksAll,wAll,pAll, zeros(nfpeak,1)]');
fclose(fileId);   
end


%% load c result 
mpeak_c_ch_1 = readmatrix("mpeak_out_c_ch_0.txt");
mpeak_c_ch_2 = readmatrix("mpeak_out_c_ch_1.txt");
mpeak_c_ch_3 = readmatrix("mpeak_out_c_ch_2.txt");
mpeak_c_ch_4 = readmatrix("mpeak_out_c_ch_3.txt");

fpeak_c_ch_1 = readmatrix("fpeak_out_c_ch_0.txt");
fpeak_c_ch_2 = readmatrix("fpeak_out_c_ch_1.txt");
fpeak_c_ch_3 = readmatrix("fpeak_out_c_ch_2.txt");
fpeak_c_ch_4 = readmatrix("fpeak_out_c_ch_3.txt");

mPeakLocC1 = mpeak_c_ch_4(:,1) + 1;
fPeakLocC1 = fpeak_c_ch_4(:,1) + 1; 

figure(1)
subplot(311)
plot(mPeakLocC1,ecgNorm1(mPeakLocC1,1),'m+');
subplot(312)
plot(fPeakLocC1,tkeo(fPeakLocC1,1),'m+');
subplot(313)
plot(ecgNorm2)
hold on
plot(locsAll,ecgNorm2(locsAll),'ro')
plot(fPeakLocC1,ecgNorm2(fPeakLocC1),'m+')
all_ha = findobj(figure(1), 'type', 'axes', 'tag', '' );
linkaxes(all_ha, 'x' );


figure(1001)
subplot(411)
plot(ecg_clean(:,1))
subplot(412)
plot(ecg_clean(:,2))
subplot(413)
plot(ecg_clean(:,3))
subplot(414)
plot(ecg_clean(:,4))
all_ha = findobj(figure(1001), 'type', 'axes', 'tag', '' );
linkaxes(all_ha, 'x' );


%% test gnuplot 


fileId = fopen("mpeak_c.txt","w");
fprintf(fileId, "%3.15f %3.15f\n", [mPeakLocC1, tkeo(mPeakLocC1)]');
fclose(fileId);

fileId = fopen("mpeak_matlab.txt","w");
fprintf(fileId, "%3.15f %3.15f\n", [mPeakMatlab, tkeo(mPeakMatlab)]');
fclose(fileId);

fileId = fopen("fpeak_matlab.txt","w");
fprintf(fileId, "%3.15f %3.15f\n", [fPeakMatlab, tkeo(fPeakMatlab)]');
fclose(fileId);

fileId = fopen("fpeak_c.txt","w");
fprintf(fileId, "%3.15f %3.15f\n", [fPeakLocC1, tkeo(fPeakLocC1)]');
fclose(fileId);


fileId = fopen("iprod_matlab.txt","w");
fprintf(fileId, "%3.15f\n", iprod_rmv_mpeak);
fclose(fileId);

fileId = fopen("tkeo_matlab.txt","w");
fprintf(fileId, "%3.15f\n", tkeo);
fclose(fileId);

fileId = fopen("ecg_clean.txt","w");
fprintf(fileId, "%3.15f\n", ecg_clean(:,4));
fclose(fileId);

fileId = fopen("mpeak_ecg_clean.txt","w");
fprintf(fileId, "%3.15f %3.15f\n", [mPeakMatlab + tkwin, ecg_clean(mPeakMatlab+tkwin,4)]');
fclose(fileId);

fileId = fopen("fpeak_ecg_clean.txt","w");
fprintf(fileId, "%3.15f %3.15f\n", [fPeakMatlab + tkwin, ecg_clean(fPeakMatlab+tkwin,4)]');
fclose(fileId);


%% CONVERT PEAK LOCS TO RR INTERVAL AND HEART RATE 
mRR = mPeakMatlab(2:end)-mPeakMatlab(1:end-1);
fRR = fPeakMatlab(2:end) -fPeakMatlab(1:end-1);

mHR =  (fs * 60)./mRR; 
fHR =  (fs * 60)./fRR;

% RMV OUTLIERS
LOUTLIER = NaN;
UOUTLIER = NaN; 
MULim = 150;
MLLim = 50;
% DistLim = 6;
for kk = 1:length(mHR)
    if (mHR(kk) < MLLim) 
        mHR(kk) = LOUTLIER;
    elseif (mHR(kk) > MULim)
        mHR(kk) = UOUTLIER;
    else
        
    end 
end
 
FULim = 200;
FLLim = 100;
% DistLim = 6;
for kk = 1:length(fHR)
    if (fHR(kk) < FLLim) 
        fHR(kk) = LOUTLIER;
    elseif (fHR(kk) > FULim)
        fHR(kk) = UOUTLIER;
    else
        
    end 
end


figure(1002)
subplot(211)
plot(mPeakMatlab(2:end,1), mHR,'b*')
axis([0 mPeakMatlab(end) 0 300])

figure(1002)
subplot(212)
plot(fPeakMatlab(2:end,1), fHR,'r*');
axis([0 fPeakMatlab(end) 0 300])

%% SAMPLING HEART RATE 
fhrBinLen = 125; 
[siglen,nchannel] = size(ecg_clean);
nbin = siglen / fhrBinLen; 
sampleFHR = zeros(nbin,nchannel);

n = 1; 
for kk = 1:length(fPeakMatlab) - 1
    binId = round(fPeakMatlab(kk) / fhrBinLen); 
    sampleFHR(binId) = fHR(kk); 
end


figure(1003)
subplot(211)
hold on; 
plot(sampleFHR(:,1),'bsq')
aaa = readmatrix("fheartrate_trace_all.txt");
plot(aaa(:,4),'ro');
legend('Matlab','C')
title('FHR CHANNEL 4 NUH1021')
subplot(212)
plot(aaa(:,5),'ro')
title('FHR COMBINED ALL CHANNEL IN C, NUH1021')
axis([0 length(sampleFHR) 0 300])




