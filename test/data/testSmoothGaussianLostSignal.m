%% H.T. 08 OCT 2019 
close all; 
clear all; 
clc; 

NUH = 1; 
if(NUH==1)
xa = readmatrix("fheartrate_trace_all.txt");
x  = xa(:,4);

% C OUTPUT 
% xFC = readmatrix("/Users/hai/c-workspace/biorithm-femom-ctg/src/fhr_baseline/test/data/NUH1021_FHR_CH4_FASTWAVE_C.txt");
% xSC = readmatrix("/Users/hai/c-workspace/biorithm-femom-ctg/src/fhr_baseline/test/data/NUH1021_FHR_CH4_SLOWWAVE_C.txt");

% xFC  = readmatrix("/Users/hai/c-workspace/biorithm-femom-ctg/src/fhr_baseline/test/data/NUH1021_FHR_FAST_1.txt");
% xSC  = readmatrix("/Users/hai/c-workspace/biorithm-femom-ctg/src/fhr_baseline/test/data/NUH1021_FHR_SLOW_1.txt");
% xBC  = readmatrix("/Users/hai/c-workspace/biorithm-femom-ctg/src/fhr_baseline/test/data/NUH1021_FHR_BASE_1.txt");
% xDC  = readmatrix("/Users/hai/c-workspace/biorithm-femom-ctg/src/fhr_baseline/test/data/NUH1021_FHR_DIFF_1.txt"); 
% xADC = readmatrix("/Users/hai/c-workspace/biorithm-femom-ctg/src/fhr_baseline/test/data/NUH1021_FHR_AD_1.txt");

xFC  = readmatrix("FASTWAVE_CH_4.txt");
xSC  = readmatrix("SLOWWAVE_CH_4.txt");
xBC  = readmatrix("BASELINE_CH_4.txt");
xADC = readmatrix("ADCEL_CH_4.txt");

end

if (NUH==2)
load("1001m.mat");
x = val(1,1:5000)'./100;
end

if(NUH==3)
x = readmatrix("FSD_FHR_7.txt");
xFC = readmatrix("/Users/hai/c-workspace/biorithm-femom-ctg/src/fhr_baseline/test/data/FSD_FHR_7_FASTWAVE_C.txt");
xSC = readmatrix("/Users/hai/c-workspace/biorithm-femom-ctg/src/fhr_baseline/test/data/FSD_FHR_7_SLOWWAVE_C.txt");
end

fs    = 4; 
wFast = 15*fs  + 1; 
wSlow = 180*fs + 1;

xFast  = smoothgl(x, wFast, "gaussian");
xSlow1 = smoothgl(x, wSlow, "gaussian");
xSlow2 = smoothgl(xFast, wSlow, "rgaussian");


figure(1)
subplot(211)
hold on; 
plot(x,'k*')
plot(xFast,'b','LineWidth',2)
plot(xSlow1,'r','LineWidth',2);
plot(xSlow2,'m','LineWidth',2);
title('BASELINE C')
% axis([0 length(x) 0 300])

figure(1)
subplot(212)
title('BASELINE C')
% plot(xFC,'b','LineWidth',2);
plot(xBC,'b','LineWidth',2);
hold on; 
plot(xSC,'m','LineWidth',2);
% legend('baseline','slowwave')

%% PLOT ADCEL 
FHR = x; 
FHRMax = 250;

for i = 1:length(xADC(:,1))
    if(xADC(i,3) >0)
        figure(1);
        subplot(212);
        xarea = [floor(xADC(i,1)):floor(xADC(i,2))];
        yarea = FHRMax*ones(size(xarea));
        at = area(xarea, yarea, 0*min(FHR));
        at.FaceAlpha=0.2;
        at.FaceColor='g';
    end
    if(xADC(i,3) <0)
        figure(1);
        subplot(212);
        xarea = [floor(xADC(i,1)):floor(xADC(i,2))];
        yarea = FHRMax*ones(size(xarea));
        at = area(xarea, yarea, 0*min(FHR));
        at.FaceAlpha=0.2;
        at.FaceColor='r';
    end 
end 


