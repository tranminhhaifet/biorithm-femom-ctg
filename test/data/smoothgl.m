%% H.T. 08 OCT 2019 
% smooth signal with lost samples 

function y = smoothgl(x, wlen, method)

% H.T 08 OCT 2019
FHRULIM = 250; 
FHRLLIM = 10;

if (strcmp(method,'gaussian'))
    
    rw = ones(length(x),1);
    
    % H.T 08 OCT 2019 
    rw(abs(x) < FHRLLIM) =  0.0000001;
    rw(abs(x) > FHRULIM) =  0.0000001;
    
    y = rgauss(x, wlen, rw);
    
elseif (strcmp(method, 'rgaussian'))
    niter = 3;
    rw = ones(length(x),1);
    
    % H.T 08 OCT 2019 
%     rw(abs(x) < FHRLLIM) =  0.0000001;
%     rw(abs(x) > FHRULIM) =  0.0000001;
    
    y = rgauss(x, wlen, rw);
    
    for i = 1:niter
        
        rw = computeWRobust(x - y);
        
        rw(abs(x)<10) = 0.0000001;
        
        y = rgauss(x, wlen, rw); 
        
    end 
    
end

end 


% local weighted linear regression 
function y = rgauss(x, wlen, rw)
    halfWlen = floor((wlen-1)/2);
    y    = zeros(size(x));  
    % left most 
    Y = x(1:wlen); 
    for k = 1:halfWlen
        RW = rw(1:wlen); 
        gw = gausswin(2*(wlen-1)+1 - 2*(k-1));
        W = gw(wlen-2*(k-1):end);
        W = W.*RW;
        y(k) = 1/sum(W)*sum(W.*Y);
    end 
    
    % interior point 
    for k = 1:length(x)-wlen+1
        W = gausswin(wlen);
        Y = x(k:k+wlen-1); 
        % weight      
        RW = rw(k:k+wlen-1);
        W = W.*RW;       
        % smoothed point 
        y(k + halfWlen) =  1/sum(W)*sum(W.*Y);
    end 
    
    % right most 
    Y = x(length(x)-wlen+1:1:length(x));
    for k = 1:halfWlen
        % weight  
        gw = gausswin(wlen + 2*(k-1));
        W  = gw(1:end-2*(k-1));
        RW = rw(length(x)-wlen+1:1:length(x));
        W = W.*RW;
        % smoothed point 
        y(length(x)-halfWlen+k) = 1/sum(W)*sum(W.*Y);
    end 
end 


function w = computeWRobust(r)
    w = ones(length(r),1); 
    MAD = median(abs(r));
    for i = 1:length(r)
        if(abs(r(i))>= 6.0*MAD)
            w(i) = 0.0000001;
        else 
            w(i) = (1-(r(i)/(6*MAD))^2)^2;
        end
    end 
end

