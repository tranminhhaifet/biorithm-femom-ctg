#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "biorithm_aux.h"

/*------------------------------------------------------------------------------
Setup: 
Description: 
------------------------------------------------------------------------------*/
void setUp(void)
{

}

/*------------------------------------------------------------------------------
Tear down:
Description: 
------------------------------------------------------------------------------*/
void tearDown(void)
{

}


/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_double_vec_new() 
{
    printf("test double_vec_new\n");
    double_vec *v = double_vec_new(10);
    for (int i = 0; i < 10; i++) 
    {
        printf("x = %5.15f\n", v->p_data[i]);
    }
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_hannwin() 
{
    double_vec *hw_c = double_vec_new(8192); // hann win by c
    double_vec *hw_m = double_vec_new(8192); // hann win by matlab

    // read hannw computed by matlab 
    read_from_txt("./../data/hannwmatlab.txt", hw_m);

    // compute hannw by c 
    hannwin(hw_c, 8192);

    // print to console 
    for (unsigned int i = 0; i < 10; i++)
    {
        printf("hwc = %2.24f, hwm = %2.24f, error = %2.24f\n", 
            hw_m->p_data[i], hw_m->p_data[i], 
            fabs(double_vec_get(hw_m,i) - double_vec_get(hw_c,i)));
    }

    // write hannw to file 
    write_to_txt(&(hw_c), 1, "./../data/hannwin.txt");
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_gausswin()
{
    unsigned int N  = 15;    
    double_vec *gwc = double_vec_new(N);
    double_vec *gwm = double_vec_new(N);

    // gausswin by matlab 
    read_from_txt("../data/gwm.txt", gwm);

    // gausswin by c 
    gausswin(gwc, N, 2.5);

    for (unsigned int i = 0; i < double_vec_length(gwc); i++) 
    {
        printf("i = %d, gwc = %3.15f, gwm = %3.15f\n", i, double_vec_get(gwc,i), 
                                                          double_vec_get(gwm,i));
    }

    // compare 
    double_vec_sub(gwc, gwm);
    double_vec_print(gwm, N);

    // write to file 
    write_to_txt(&gwc, 1, "./../data/gwc.txt");
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_smooth()
{
    double_vec *p_x = double_vec_new(1543);

    read_from_txt("./../data/FSD_FHR_7.txt", p_x);

    smooth(p_x, 61, "gaussian", 2.5);

    double_vec_print(p_x, 30);

    write_to_txt(&p_x, 1, "./../data/FSD_FHR_7_smoothed.txt");
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_smoothwg()
{
    double_vec *p_x = double_vec_new(2400);

    read_from_txt("./../data/sinx.txt", p_x);

    smooth(p_x, 61, "wgaussian", 2.5);

    double_vec_print(p_x, 30);

    write_to_txt(&p_x, 1, "./../data/sinxSmoothedCWG.txt");
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_double_vec_add()
{
    printf("tes add vector \n");

    double_vec *p_a = double_vec_new(10);
    double_vec *p_b = double_vec_new(10);

    for (unsigned int i = 0; i < 10; i++) 
    {
        double_vec_set(p_a, i, (double)i);
        double_vec_set(p_b, i, (double)2*i);
    }

    double_vec_add(p_a, p_b);

    double_vec_print(p_b, 10);

}

void test_double_vec_sub()
{
    printf("tes sub vector \n");

    double_vec *p_a = double_vec_new(10);
    double_vec *p_b = double_vec_new(10);

    for (unsigned int i = 0; i < 10; i++) 
    {
        double_vec_set(p_a, i, (double)i);
        double_vec_set(p_b, i, (double)2*i);
    }

    double_vec_sub(p_a, p_b);
    double_vec_print(p_b, 10);

}

void test_double_vec_mul()
{
    printf("test multiply vector \n");
    double_vec *p_a = double_vec_new(10);
    double_vec *p_b = double_vec_new(10);

    for (unsigned int i = 0; i < 10; i++) 
    {
        double_vec_set(p_a, i, (double)i);
        double_vec_set(p_b, i, (double)2*i);
    }

    double_vec_mul(p_a, p_b); 
    double_vec_print(p_b, 10); 
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_double_vec_abs()
{
    double_vec *p = double_vec_new(10);

    for (unsigned int i = 0; i < 10; i++) 
    {
        double_vec_set(p, i, -10.0);
    }

    double_vec_abs(p);

    double_vec_print(p, 10);
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_qsort_double()
{
    double_vec *p = double_vec_new(6);
//    VEC(p,0) = 10;
//    VEC(p,1) = 2.2;
//    VEC(p,2) = 12;
//    VEC(p,3) = 0;
//    VEC(p,4) = 1;
//    VEC(p,5) = 16;

    qsort(p->p_data, 6, sizeof(double), compare_double);
    double_vec_print(p, 6);

    printf("median = %lg\n", find_median(p));
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_find_median()
{
    double_vec *p_x = double_vec_new(6); 
    double_vec_set(p_x, 0, 0.0); 
    double_vec_set(p_x, 1, 1.0); 
    double_vec_set(p_x, 2, 2.0); 
    double_vec_set(p_x, 3, 3.0); 
    double_vec_set(p_x, 4, 4.0); 
    double_vec_set(p_x, 5, 5.0); 

    printf("median = %lg, correct median = %lg\n", 
           find_median(p_x), double_vec_get(p_x,2));

}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_mad_weight()
{
    double_vec *p_o = double_vec_new(10);
    double_vec_set(p_o, 5, 10.0);
    double_vec_print(p_o, 10);

    printf("MAD = %2.24f \n", find_median(p_o));

    double_vec *p_x = double_vec_new(10);
    double_vec *p_w = double_vec_new(10);
    mad_weight(p_o, p_x, p_w);
    double_vec_print(p_w, 10);
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_find_cross_zero()
{
    double_vec *x = double_vec_new(5);
    double_vec *y = double_vec_new(5);

    for (unsigned int i = 0; i < 5; i++) {
        double_vec_set(x, i, (double)i);
    }

    double_vec_set(y, 0, 1.0);
    double_vec_set(y, 1, -1.0);
    double_vec_set(y, 2, 2.0);
    double_vec_set(y, 3, 3.0);
    double_vec_set(y, 4, -4.0);

    double_vec *z = find_cross_zero(x, y);
    if (z==NULL) {
        printf("no cross zero \n");
    } else {
        double_vec_print(z, double_vec_length(z));
    }
}

/*------------------------------------------------------------------------------
* Function Name: test_size_max
* Description  : print SIZE_MAX 
*-----------------------------------------------------------------------------*/ 
void test_size_max()
{
    printf("size_max = %lu\n", SIZE_MAX);
}

/*------------------------------------------------------------------------------
* Function Name: test_double_vec_print
* Description  : 
*-----------------------------------------------------------------------------*/ 
void test_double_vec_print()
{
    // double_vec *p = NULL;

    double_vec *p = double_vec_new(10);

    double_vec_print(p, 10);
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_double_vec_get()
{
    double_vec *p = double_vec_new(10);
    // printf("x = %3.24f\n", double_vec_get(p, 100));
    printf("x = %3.24f\n", double_vec_get(p, 0));
    // printf("x = %3.24f\n", double_vec_get(p, -10));

    double_vec *p1 = NULL; 
    printf("x = %3.24f\n", double_vec_get(p1, 100));
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_double_vec_set()
{
    double_vec *p = double_vec_new(10);
    // double_vec_set(p, 100, 12.0);
    double_vec_print(p, 10);
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_double_sort()
{
    double_vec *p = double_vec_new(5);
    double_vec_set(p, 0, 1.0);
    double_vec_set(p, 1, 5.0);
    double_vec_set(p, 2, 2.0);
    double_vec_set(p, 3, 4.0);
    double_vec_set(p, 4, 3.0);

    double_vec_sort(p);

    double_vec_print(p, 10);
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_double_vec_length()
{
    double_vec *p = double_vec_new(10);
    printf("length = %zu \n", double_vec_length(p));
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_double_vec_write()
{
    printf("test write to text \n");
    double_vec *p = double_vec_new(10);
    double_vec_set(p, 0, 10.0);
    double_vec_write(p, "./../data/doublevec.txt");
    printf("OK\n");
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_write_to_txt()
{
    double_vec **p = malloc(3 * sizeof(double_vec *));
    p[0] = double_vec_new(10);
    p[1] = double_vec_new(10);
    p[2] = double_vec_new(10);

    write_to_txt(p, 3, "./../data/doublevec_3_col.txt");
    write_to_txt(&p[0], 1, "./../data/doublevec_1_col.txt");

}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_point_vec_new()
{
    printf("test point vec new \n");
    point_vec *p = point_vec_new(5);
    point_vec_print(p, 5);

}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_point_vec_get()
{
    printf("test point vec get\n");
    point_vec *p = point_vec_new(10);

    point tmp = point_vec_get(p, 3);

    PRINT_POINT(tmp);
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
void test_point_vec_set()
{
    printf("test point vect set \n");
    point_vec *p = point_vec_new(10);

    point tmp; 
    tmp.x = 100.0; 
    tmp.y = 200.0; 

    point_vec_set(p, 0, tmp);

    PRINT_POINT(point_vec_get(p, 0));
}


void test_double_vec_read()
{
    double_vec *p = double_vec_new(100);
    double_vec_read(p, "./../data/read_double_vec.txt");
    double_vec_print(p, 100);
}


void test_find_mean()
{
    double_vec *p = double_vec_new(5);
    double_vec_set(p, 0, 0.0);
    double_vec_set(p, 1, 2.0);
    double_vec_set(p, 2, 3.0);
    double_vec_set(p, 3, 4.0);
    double_vec_set(p, 4, 5.0);
    printf("mean = %3.15f\n", find_mean(p));
    printf("median = %3.15f\n\n", find_median(p));
}

void test_find_quantile()
{
    printf("find quantile of a vector\n");

    double_vec *p = double_vec_new(10);

    for (unsigned int i = 0; i < 10; i++)
    {
        double_vec_set(p, i, i + 1.0);
    }

    double qthresh = 0.5;
    double qvalue = find_quantile(p, qthresh);

    printf("qthresh = %2.3f, qvalue = %5.5f\n", qthresh, qvalue);
}

void test_compare_double()
{
    double a = 10.1;
    double b = 20.2;
    double c = -20.2;
    printf("compare %5.5f with %5.5f is %d \n", a, b, compare_double(&a, &b));
    printf("compare %5.5f with %5.5f is %d \n", b, b, compare_double(&b, &b));
    printf("compare %5.5f with %5.5f is %d \n", b, c, compare_double(&b, &c));

}


void test_str_concat()
{
    char *p1 = "./../test/data/nuh_1029/";
    char *p2 = "clean_ecg_matlab.txt";

    char *p = str_concat(p1, p2);

    printf("%s\n", p);

    free(p);

}

int main ()
{

    test_str_concat();

    // test_compare_double();

    // test_double_vec_new();

    // test_hannwin();

    // test_gausswin();

    // test_point();

    // test_point_vec();

    // test_smooth();

    // test_smoothwg();

    // test_qsort_double();

    // test_double_sort();

    // test_double_vec_add();

    // test_double_vec_abs();

    // test_find_cross_zero();

    // test_mad_weight();  

    // test_find_median();

    // test_size_max();

    // test_double_vec_print();

    // test_double_vec_get();

    // test_double_vec_set();

    // test_double_vec_length();

    // test_double_vec_print();

    // test_double_vec_add();

    // test_double_vec_sub();

    // test_double_vec_mul();

    // test_double_vec_write();

    // test_write_to_txt();

    // test_point_vec_new();

    // test_point_vec_get();

    // test_point_vec_set();

    // test_double_sort();

    // test_double_vec_subvec();

    // test_find_mean();    

    // test_find_quantile();

    return 0;
}

// gcc -std=c99 test_biorithm_aux.c ./../src/biorithm_aux.c -lm -lgsl -lwavelib -I /usr/local/include/gsl/