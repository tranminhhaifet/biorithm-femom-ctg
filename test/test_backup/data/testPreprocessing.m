%% HT SEP. 25 2019 
clear all; 
close all; 
clc; 
a = 1; 

if (a == 1)
recordName = "/Users/hai/matlab/biorithm/src/1021.csv";
% A1=csvread(recordName,1,1);
A1 = readmatrix(recordName);
A1 = A1(1:32768,2:5);

%% Remove Nan and Zero values

nan_idx = find(isnan(A1));
A1(nan_idx) = 1e-16;

zero_idx = find(~A1);
A1(zero_idx) = 1e-16;

ch =4;
fs=500;

%% Line noise filtering 
for y=1:ch 
Wo = 50/(fs/2);
BW = Wo/35; 
[b,a] = iirnotch(Wo,BW); 
Y(:,y) = filter(b,a,A1(:,y)); 
end

for x=1:ch 
Wo = 60/(fs/2);
BW = Wo/35; 
[b,a] = iirnotch(Wo,BW); 
X(:,x) = filter(b,a,Y(:,x)); 
end

%% Removal of Baseline wander

lvl=8;
for ii =1:ch
    [c,l] = wavedec(X(:,ii),lvl,'db6');
%     B=wrcoef('a',c,l,'db6',lvl);
%     AB(:,ii)=X(:,ii)-B;
    c(1:l(1)) = 0.0; 
    AB(:,ii) = waverec(c,l,'db6');
    
end

%% High frequency components removal

for xx=1:ch
 [THR(xx),SORH(xx),KEEPAPP(xx)]=ddencmp('den','wv',AB(:,xx));
 level=4; 
 [ecg_clean(:,xx),CecgC(:,xx),LecgC(:,xx),PERF0(:,xx),PERFL2(:,xx)]=wdencmp('gbl',AB(:,xx),'coif4',level,THR(xx),SORH(xx),KEEPAPP(xx)); 
end

end 

%% shrinkage layer 
x_wave = AB(1:32768,2);
dlmwrite("x_wave.txt", x_wave, 'precision', "%3.32f");



%% thresh 
[A,D] = dwt(x_wave,'db1');
noiselev = median(abs(D))/0.6745;
thresh = sqrt(2*log(length(x_wave)))*noiselev;

%% shrink 
nlayer = 4; 
wname = "coif4";
[C,L] = wavedec(x_wave,nlayer,wname);
approx = appcoef(C,L,wname);
cd = detcoef(C,L,[1:1:nlayer]);

[nrow,ncol] = size(cd);

for n = 1:nlayer
    for k = 1:length(cd{n})
        if (abs(cd{n}(k)) < thresh) 
            cd{n}(k) = 0.0;
        else
            getsign = sign(cd{n}(k) - thresh);
            cd{n}(k) = getsign*(abs(cd{n}(k)) - thresh);
        end 
    end 
end

a = 1; 
Cnew = [approx;cd{4};cd{3};cd{2};cd{1}];

%% denoise 
xdenoised = waverec(Cnew, L, wname);

if (a==1)
    dlmwrite("x_wave_out_matlab.txt", C, 'precision', "%3.32f");
    dlmwrite("shrink_out_matlab.txt", Cnew, 'precision', "%3.32f");
    dlmwrite("wdenoised_matlab.txt", xdenoised, 'precision', "%3.32f");
end


%% test powerline remove 
plineIn = A1(:,2);
plineOut = X(:,2);

powerline = 0; 
if (powerline==1)
dlmwrite("powerline_rmv_in.txt",plineIn,'precision',"%3.32f");
dlmwrite("powerline_rmv_out_matlab.txt",plineOut,'precision',"%3.32f");
end 

%% test baseline remove 

baselineIn = X(:,2);
baselineOut = AB(:,2);
baseline = 0; 
if (baseline == 1)
dlmwrite("baseline_rmv_in.txt",baselineIn,'precision',"%3.32f");
dlmwrite("baseline_rmv_out_matlab.txt",baselineOut,'precision',"%3.32f");
end 

%% rmv high frequency noise 
hidenoise = 1; 
denoiseIn = AB(:,2);
denoiseOut = ecg_clean(:,2); 

if(hidenoise==1)
dlmwrite("denoise_in.txt",denoiseIn,'precision',"%3.32f");
dlmwrite("denoise_out_matlab.txt",denoiseOut,'precision',"%3.32f");    
end 


%% wavelet coeiff 
w = 0; 
if (w==1)
coif4 = wfilters("coif4");
coif4 = coif4./sqrt(2);
coif4 = flip(coif4);
fileId = fopen("coif4.txt",'w');
fprintf(fileId, "%3.60e\n", coif4);
fclose(fileId);

db6 = wfilters("db6");
db6 = flip(db6);
fileId = fopen("db6.txt",'w');
fprintf(fileId, "%3.60e\n", db6);
fclose(fileId);
end 


%%
fileId = fopen("raw_ecg_in.txt","w");
fprintf(fileId, "%3.32f %3.32f %3.32f %3.32f\n", A1');
fclose(fileId);

fileId = fopen("clean_ecg_out_matlab.txt","w");
fprintf(fileId, "%3.32f %3.32f %3.32f %3.32f\n", ecg_clean');
fclose(fileId);

