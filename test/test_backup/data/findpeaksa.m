% clear all; close all; clc; 
% 
% % parameters for findpeaksa 
% sel    = 10; % peak > leftMinima + sel & peak > rightMinima + sel
% thresh = 0; % peak > thresh
% minPeakDistance = 0; % two peaks sapreated at least minPeakDistance 
% 
% % input 
% % x0 = [1 5 1 1 8 6 8 1 2 3 4 5 4 3 2 1 1 2 2 2 2]';
% load sunspot.dat;
% avSpots = sunspot(:,2);
% x0 = avSpots;
% 
% 
% [Ypk, Xpk, iLB, iRB] = findPeakSel(x0, sel, thresh);
% 
% [wxPk, iLBh, iRBh] = getPeakWidth(x0, Xpk, iLB, iRB);
% 
% refHeight = x0(Xpk(5))/2;
% 
% iLeft = findLeftIntercept(x0, Xpk(5), iLB(5), 41.7);
% 
% b1 = linterp(iLeft,iLeft+1,x0(iLeft),x0(iLeft+1),x0(Xpk(5)),0); 
% 
% iRight = findRightIntercept(x0, Xpk(5),iRB(5), refHeight) ;
% 
% b2 = linterp(iRight, iRight-1, x0(iRight),x0(iRight-1), x0(Xpk(5)), 0);
% 
% % output 
% % Ypk: peak value, Xpk peak location
% % Wpk indicates left and right intercep location
% % [Ypk, Xpk, Wpk] = findpeaksa(x0, sel, thresh, minPeakDistance); 
% 
% figure(1)
% plot(x0, '-o')
% hold on; 
% plot(Xpk, x0(Xpk), 'r*')
% 
% for i = 1:length(Xpk)
%     x1 = wxPk(i,1);
%     x2 = wxPk(i,2);
%     y1 = Ypk(i)/2;
%     line([x1,x2], [y1, y1], 'Color', 'red');
% end 



function [Ypk, Xpk, Wpk] = findpeaksa(Yin, sel, thresh, minPeakDistance)
%==========================================================================
%    COMPANY: BIORITHM PTE 
%    AUTHOR: HAI TRAN 
%    CREATED DATE: 10 July 2019
%==========================================================================
%    FINDPEAKSA  
%==========================================================================

% find peaks with sel and thresh conditions 
[Ypk, Xpk, iLB, iRB] = findPeakSel(Yin, sel, thresh);

% find peaks with min peak distance 
iPk = findPeakMinDistance(Yin, Xpk, minPeakDistance); 
Xpk = Xpk(iPk);
Ypk = Ypk(iPk);

% compute peak width of each peak 
Wpk = getPeakWidth(Yin, Xpk, iLB, iRB); 

% Wpk = Wpk(:,2) - Wpk(:,1);
% prepare final output 

end

%--------------------------------------------------------------------------
function [Ypk, Xpk, iLB, iRB] = findPeakSel(Yin, sel, thresh)

% copy input to temp variable 
x0 = Yin;

% compute first derivative dx0
dx0 = x0(2:end) - x0(1:end-1);

% find indecies where dx0 changes sign 
dx0(dx0==0) = -eps;
ind = find(dx0(1:end-1).*dx0(2:end) < 0) + 1; 
ind = [1; ind; numel(x0)];

% compute x 
x = x0(ind);

% init 
minMag  = min(x);
tmpMag  = minMag; 
minLeft = x(1);
flag    = false;
len     = numel(x);

% TODO: shoud be better here!
peakLoc   = zeros(len, 1);
peakMag   = zeros(len, 1);
iLB       = zeros(len, 1);
iRB       = zeros(len, 1);
iLB(1) = 1; 
iRB(1) = 1;

% loop through x 
cInd = 1;
if x(1) >= x(2)
    k = 0;
else 
    k = 1; 
end 

while k < len

   k = k + 1; 
   % reset flag 
   if flag 
       flag = false;
       tmpMag = minMag;
   end 
    
   % find a peak by looking at rising slope
   if x(k) > tmpMag && x(k) > minLeft + sel && x(k) > thresh
       tmpMag = x(k);
       tmpLoc = ind(k);
   end 
   
   % check boundary index 
   if(k == len)
       break;
   end 
   % find the tail of the peak on right side 
   k = k + 1; 
   if (~flag) && tmpMag > x(k) + sel 
       flag = true; 
       peakLoc(cInd) = tmpLoc;
       peakMag(cInd) = tmpMag;
       iRB(cInd) = ind(k);
       minLeft = x(k);
       cInd = cInd + 1; 
       iLB(cInd)  = ind(k);
   elseif x(k) <= minLeft
       minLeft = x(k);
       iLB(cInd)  = ind(k);
   end 
end 

% output 
iPk = find(peakLoc>0);
Xpk = peakLoc(iPk);
Ypk = peakMag(iPk);
iLB = iLB(iPk);
iRB = iRB(iPk);

end 


%--------------------------------------------------------------------------
function idx = findPeakMinDistance(Yin, iPk, Pd)

% copy peak values and location to a temporary place 
pks  = Yin(iPk);
locs = iPk; 

% order peaks from large to small 
[~, sortIdx] = sort(pks, 'descend');

locs_temp = locs(sortIdx); 

idelete = ones(size(locs_temp)) < 0;

for i = 1:length(locs_temp)
    if ~idelete(i)
        R = locs_temp(i) + Pd; 
        L = locs_temp(i) - Pd; 
        idelete = idelete | (locs_temp>=L)&(locs_temp<=R); 
        idelete(i) = 0;
    end
end 

% report back indices in consecutive order 
idx = sort(sortIdx(~idelete)); 

end 

%--------------------------------------------------------------------------
function [wxPk, iLBh, iRBh] = getPeakWidth(y, iPk, iLB, iRB)

% base 
base = zeros(size(iPk)); 

% border the width by no more than the lowest valley between this peak and
% the next peak 
% iLBh = [iLB(1); max( iLB(2:end), iRB(1:end-1))];
% iRBh = [min( iRB(1:end-1), iRB(2:end)); iRB(end)];

iLBh = [iLB(1); min(iLB(2:end), iRB(1:end-1))];
iRBh = [max(iLB(2:end), iRB(1:end-1)); iRB(end)];

% get the width boundaries of each peak 
wxPk = getHalfMaxBounds(y,  iPk, base, iLBh, iRBh);

end

%--------------------------------------------------------------------------
% function [bPk, iLB, iRB] = getPeakBase(y, iPk)
% 
%     iLB = zeros(size(iPK), 'like', iPk);
%     iRB = zeros(size(iPK), 'like', iPk);
%     
% end 

%--------------------------------------------------------------------------
function bounds = getHalfMaxBounds(y, iPk, base, iLB, iRB)

n = length(iPk);
bounds = zeros(n, 2, 'like', y);

for i = 1:n
    % compute the desired reference level at half-height or half-prominence
    refHeight = (y(iPk(i)) + base(i))/2; 
    
    % compute the index of the left-intercept at half max 
    iLeft = findLeftIntercept(y,iPk(i),iLB(i),refHeight);
    if iLeft < iLB(i)
        bounds(i,1) = iLB(i);
    else
        bounds(i,1) = linterp(iLeft,iLeft+1,y(iLeft),y(iLeft+1),y(iPk(i)),base(i)); 
    end 
    
    % compute the index of the right intercerpt 
    iRight = findRightIntercept(y,iPk(i),iRB(i),refHeight);
    if iRight > iRB(i)
        bounds(i,2) = iRB(i); 
    else
        bounds(i,2) = linterp(iRight,iRight-1,y(iRight),y(iRight-1),y(iPk(i)),base(i));
    end 
  
end 

end 

%--------------------------------------------------------------------------
function idx = findLeftIntercept(y, idx, borderIdx, refHeight)
% decrement index until you pass under the reference height or pass the
% index of the left border, whichever comes first 
while idx>=borderIdx && y(idx) > refHeight
    idx = idx - 1; 
end 
end 

%--------------------------------------------------------------------------
function idx = findRightIntercept(y, idx, borderIdx, refHeight)
% increment index until you pass under the reference height or pass the
% index of the right border, whichever comes first 
while idx<=borderIdx && y(idx) > refHeight
    idx = idx + 1; 
end 
end 

%--------------------------------------------------------------------------
function xc = linterp(xa,xb,ya,yb,yc,bc)

xc = xa + (xb-xa)*(0.5*(yc+bc)-ya)/(yb-ya);

% TODO: L'Hospital's rule when -Inf is encountered. 
    
end 