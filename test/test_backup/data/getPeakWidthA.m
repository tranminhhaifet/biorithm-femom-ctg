function width = getPeakWidthA(x, locs, thresh, wmax)
%T.H 12 SEP 2019: get peak width with two conditions:
% 1. width < wmax 
% 2. magnitude > thresh 

N = length(x); 

width.x = zeros(length(locs),1); 
width.y = zeros(length(locs),1); 
width.z = zeros(length(locs),1); 
width.h = zeros(length(locs),1); 

% loop through each peak 
for k = 1:length(locs)
    
    % location of peak k 
    pLoc = locs(k); 
    
    if (thresh(k) < x(pLoc))
    
    % go to the right until first point under thresh 
    for i = 1:wmax
        % check bound 
        if (i+pLoc) >= N
            break; 
        end 
        if(x(i+pLoc) <= thresh(k))
            break;
        end 
    end 
    xR = i + pLoc;
    
    % go to the left until first point under thresh 
    for i = 1:wmax
        % check bound 
        if (pLoc-i) <= 0
            break; 
        end
        if(x(pLoc-i) <= thresh(k))
            break; 
        end
    end
    xL = pLoc - i;
    
    % width 
    width.x(k) = xL;
    width.y(k) = xR;
    width.z(k) = xR - xL;
    width.h(k) = thresh(k);
    else
    width.x(k) = pLoc;
    width.y(k) = pLoc;
    width.z(k) = xR - xL;
    width.h(k) = thresh(k);
    end
end 

end