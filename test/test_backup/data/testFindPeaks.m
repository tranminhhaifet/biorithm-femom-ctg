%% HT. SEP. 26 2019 TEST FINDPEAKS

load sunspot.dat;
avSpots = sunspot(:,2);

[pks,locs,w,p] = findpeaks(avSpots,'MinPeakDistance',5,'MinPeakHeight',2*std(avSpots), ... 
'WidthReference','halfheight'); 

figure(1)
subplot(211)
plot(avSpots,'b-*');
hold on; 
plot(locs, avSpots(locs),'ro');



%% get peak width 
thresh = 2.0; 
wmax   = 50; 
wp     = getPeakWidth(avSpots, locs, thresh, wmax);


a = 1;
npeak = length(locs); 

if (a==1)
locs = locs  -1; 
wp.x = wp.x  -1; 
wp.y = wp.y  -1; 

% write to file 
fileId = fopen("sunspot_peak_matlab.txt", "w");
fprintf(fileId,"%d %d %d %3.15f %3.15f %3.15f %d\n",[locs,zeros(npeak,1), ... 
    zeros(npeak,1), pks,w,p, zeros(npeak,1)]');
fclose(fileId);

fileId = fopen("sunspot_dat.txt", "w");
fprintf(fileId,"%3.15f\n",avSpots');
fclose(fileId);

refHeight = ones(length(locs),1); 
refHeight = thresh * refHeight;
fileId = fopen("sunspot_rheight.txt", "w");
fprintf(fileId,"%3.15f\n",refHeight');
fclose(fileId);

fileId = fopen("sunspot_peak_width.txt", "w");
fprintf(fileId,"%d %d %d %3.15f %3.15f %3.15f %d\n",[locs, wp.x, wp.y, ... 
    zeros(npeak,1), wp.z, zeros(npeak,1), zeros(npeak,1)]');
fclose(fileId);
end 

%% remove maternal peak 
thresh = 2.0*mean(avSpots); 
wmax   = 50; 
wp1     = getPeakWidth(avSpots, locs+1, thresh, wmax);
xnew = avSpots;
for k = 1:npeak
    if (wp1.x(k) < wp1.y(k))
        for ii = wp1.x(k) : wp1.y(k)
            xnew(ii) = 0.0; 
        end
    end
end

figure(1)
subplot(211)
plot(xnew,'r-*')

figure(1)
subplot(211)
hold on; 
line([0 length(avSpots)],[thresh thresh])

b = 1;
if (b==1)
    fileId = fopen("sunspot_rmv_peak_matlab.txt", "w");
    fprintf(fileId,"%3.15f\n",xnew');
    fclose(fileId);
end

figure(1)
subplot(212)
hold on; 
plot(xnew)

%% find peak 
[fpks,flocs,fw,fp] = findpeaks(xnew,'MinPeakDistance',5,'MinPeakHeight',2*std(xnew), ... 
'WidthReference','halfheight'); 
nfpeak = length(flocs);

figure(1)
subplot(212)
plot(flocs,xnew(flocs),'ro')

c = 1;
if (c==1)
flocs = flocs  -1; 

% write to file 
fileId = fopen("sunspot_fpeak_matlab.txt", "w");
fprintf(fileId,"%d %d %d %3.15f %3.15f %3.15f %d\n",[flocs,zeros(nfpeak,1), ... 
    zeros(nfpeak,1), fpks,fw,fp, zeros(nfpeak,1)]');
fclose(fileId);
end 


