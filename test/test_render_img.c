//gcc main_sepearate_func.c render_img.c render_img.h -I./plplot/include -lplplot -g
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "biorithm_aux.h"
#include "render_img.h"

#define TEST_RENDER_PRINT


void read_from_bin(char *path, double_vec *p)
{
    FILE *f_name = fopen(path, "rb");
    if (NULL == f_name)
    {
        printf("Error open file %s\n", path);
        exit(1);
    }
    else
    {
        fread(p->p_data, sizeof(double), p->len, f_name);
        fclose(f_name);
    }
}

int main()
{

    const char *p_testcase = "NUH1021";

    if (strcmp(p_testcase,"NUH1021")==0)
    {
        // LOAD INPUT 
        unsigned int siglen = 12920;
        double_vec *p_fhr   = double_vec_new(siglen);
        double_vec *p_mhr   = double_vec_new(siglen);
        double_vec *p_ua    = double_vec_new(siglen);
        baseline *p_fbase = malloc(sizeof(baseline));

        //
        double_vec **pp_mfhr = malloc(5 * sizeof(double_vec *));
        pp_mfhr[0] = double_vec_new(siglen);
        pp_mfhr[1] = double_vec_new(siglen);
        pp_mfhr[2] = double_vec_new(siglen);
        pp_mfhr[3] = double_vec_new(siglen);
        pp_mfhr[4] = double_vec_new(siglen);

        read_from_txt_cols("./../test/data/fheartrate_trace_all.txt", pp_mfhr, 5);
        double_vec_read(p_fhr, "./../test/data/BASELINE_CH_4.txt");
        double_vec_read(p_mhr, "./../test/data/BASELINE_CH_4.txt");
        double_vec_read(p_ua, "./../test/data/BASELINE_CH_4.txt");

        #ifdef TEST_RENDER_PRINT
        // double_vec_print(p_fhr,10);
        double_vec_print(pp_mfhr[3],100);
        #endif 

        p_fbase->max_var = 10.00;
        p_fbase->mean_var = 0.01;
        p_fbase->ndecel  = 10;
        p_fbase->naccel  = 20;
        p_fbase->p_base  = p_fhr;
        p_fbase->p_fast  = p_fhr; 
        p_fbase->p_slow  = p_fhr;
        p_fbase->p_diff  = p_fhr; 
        p_fbase->p_ad    = NULL; 

        // RENDER IMAGE 
        render_img(3, pp_mfhr[3], p_mhr, p_fbase, p_ua, 
            "./../test/data/trace");

        render_img(2, pp_mfhr[3], p_mhr, p_fbase, p_ua, 
            "./../test/data/trace");

        render_img(1, pp_mfhr[3], p_mhr, p_fbase, p_ua, 
            "./../test/data/trace");

    }

    if (strcmp(p_testcase,"FHRBIN")==0)
    {
        // LOAD INPUT 
        unsigned int siglen = 10140;
        double_vec *p_fhr   = double_vec_new(siglen);
        double_vec *p_mhr   = double_vec_new(siglen);
        double_vec *p_ua    = double_vec_new(siglen);
        baseline *p_fbase   = malloc(sizeof(baseline));

        p_fbase->max_var = 10.00;
        p_fbase->mean_var = 0.01;
        p_fbase->ndecel  = 10;
        p_fbase->naccel  = 20;
        p_fbase->p_base  = p_fhr;
        p_fbase->p_fast  = p_fhr; 
        p_fbase->p_slow  = p_fhr;
        p_fbase->p_diff  = p_fhr; 
        p_fbase->p_ad    = NULL; 

        read_from_bin("./../test/data/fhr.bin", p_fhr);
        read_from_bin("./../test/data/fhr.bin", p_mhr);
        read_from_bin("./../test/data/fhr.bin", p_ua);

        #ifdef TEST_RENDER_PRINT
        double_vec_print(p_fhr,10);
        #endif 

        // RENDER IMAGE 
        render_img(1, p_fhr, p_mhr, p_fbase, p_ua, 
            "./../test/data/trace");

        render_img(2, p_fhr, p_mhr, p_fbase, p_ua, 
            "./../test/data/trace");

        render_img(3, p_fhr, p_mhr, p_fbase, p_ua, 
            "./../test/data/trace");
    }
    

    if (strcmp(p_testcase, "CTU1001")==0)
    {

        // LOAD INPUT 
        unsigned int siglen = 5000;
        double_vec *p_fhr   = double_vec_new(siglen);
        double_vec *p_mhr   = double_vec_new(siglen);
        double_vec *p_ua    = double_vec_new(siglen);
        baseline *p_fbase   = malloc(sizeof(baseline));
        p_fbase->p_base = double_vec_new(siglen);


        double_vec_read(p_fhr,   "./../test/data/CTU_1001m.txt");
        double_vec_read(p_mhr,    "./../test/data/CTU_1001m.txt");
        double_vec_read(p_ua,     "./../test/data/CTU_1001m.txt");
        double_vec_read(p_fbase->p_base,  "./../test/data/CTU101Baseline.txt");

        p_fbase->max_var = 10.00;
        p_fbase->mean_var = 0.01;
        p_fbase->ndecel  = 10;
        p_fbase->naccel  = 20;
        // p_fbase->p_base  = p_fhr;
        p_fbase->p_fast  = p_fhr; 
        p_fbase->p_slow  = p_fhr;
        p_fbase->p_diff  = p_fhr; 
        p_fbase->p_ad    = NULL; 



        #ifdef TEST_RENDER_PRINT
        double_vec_print(p_fhr,10);
        #endif 

        // RENDER IMAGE 
        render_img(1, p_fhr, p_mhr, p_fbase, p_ua, 
            "./../test/data/trace");

        render_img(2, p_fhr, p_mhr, p_fbase, p_ua, 
            "./../test/data/trace");

        render_img(3, p_fhr, p_mhr, p_fbase, p_ua, 
            "./../test/data/trace");
    }


    return 0;
}