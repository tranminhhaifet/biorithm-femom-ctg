#include <stdio.h>
#include <stdlib.h>


#include "biorithm_aux.h"
#include "fhr_app_log.h"

void test_peaklog_vec_new()
{
	unsigned int npeak = 10; 
	struct peaklog_vec *p = peaklog_vec_new(npeak);

	// CHECK ASSCESS NULL 
	CHECK_ACCESS_NULL_POINTER(p);

	// PRINT PEAK LOG 
	for (unsigned int i = 0; i < p->len; i++)
	{
		printf("i=%ul,tkloc=%ul,iploc=%ul,ecloc=%ul,ecga=%3.15f,tkeoa=%3.15f,iproda=%3.15f,width=%3.15f,flag=%d\n", 
		i, p->p_peaklog[i].tkloc, p->p_peaklog[i].iploc, p->p_peaklog[i].ecloc, 
		p->p_peaklog[i].ecga, p->p_peaklog[i].tkeoa, p->p_peaklog[i].iproda, 
		p->p_peaklog[i].width, p->p_peaklog[i].flag);
	}

	// FREE MEM 
	peaklog_vec_delete(p);
}


void test_peaklog_vec_write()
{
	unsigned int npeak = 10; 
	struct peaklog_vec *p = peaklog_vec_new(npeak);

	// CHECK ASSCESS NULL 
	CHECK_ACCESS_NULL_POINTER(p);

	// WRIT TO FILE 
	peaklog_vec_write(p, "./../../test/data/peaklog_test.txt");

	// 
	printf("WRITE FINISHED\n");
}

void test_peaklog_vec_read()
{
	// READ FROM FILE 
	struct peaklog_vec *p = peaklog_vec_read("./../../test/data/peaklog_test.txt", 10);

	// CHECK ASSCESS NULL
	CHECK_ACCESS_NULL_POINTER(p);

	// PRINT RESULT 
	peaklog_vec_print(p, p->len);
}


int main(void)
{
	test_peaklog_vec_new();

	test_peaklog_vec_write();

	test_peaklog_vec_read();

	return 0; 
}