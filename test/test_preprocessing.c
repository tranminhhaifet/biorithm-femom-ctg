// SYSTEM INCLUDE
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

// LOCAL INCLUDE
#include "biorithm_aux.h"
#include "biorithm_wavelib.h"
#include "preproc.h"

// UNITY TEST 
#include "unity.h"
#include "unity_internals.h"


// DEBUG PRINT
// #define TEST_PREPROC_PRINT

void setUp()
{

}

void tearDown()
{

}

void test_replace_nan()
{
    // input and parameter
    unsigned int siglen = 10;
    double_vec *p_x = double_vec_new(siglen);
    for (unsigned int i = 0; i < siglen; i++)
    {
        p_x->p_data[i] = 10.01 + i;
    }
    p_x->p_data[8] = NAN;

    // print input
#ifdef TEST_PREPROC_PRINT
    printf("input: \n");
    double_vec_print(p_x, siglen);
#endif

    // expected output
    double_vec *p_exp = double_vec_new(siglen);
    for (unsigned int i = 0; i < siglen; i++)
    {
        p_exp->p_data[i] = p_x->p_data[i];
    }
    p_exp->p_data[8] = DLB_MIN;

    // replace nan
    preproc_replace_nan(&p_x, 1);

    // print actual output
#ifdef TEST_PREPROC_PRINT
    printf("actual otuput: \n");
    double_vec_print(p_x, siglen);
#endif

    // assert
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp->p_data, p_x->p_data, siglen);

}


void test_design_iir_filter()
{
    // input and parameters
    double f0 = 50.0;
    struct iir_filter *p_filter = (struct iir_filter*)malloc(sizeof(struct iir_filter));
    p_filter = preproc_iirnotch(FHR_SAMPLING_FREQ, f0);

    // expected output 50Hz
    double exp_num[3] = {0.991103635646810, -1.603639368850131, 0.991103635646810};
    double exp_dnum[3] = {1.000000000000000, -1.603639368850131, 0.982207271293620};

    // expected output 60Hz
//    double exp_num[3] = {0.989343199319934, -1.442400308113921, 0.989343199319934};
//    double exp_dnum[3] = {1.000000000000000, -1.442400308113921, 0.978686398639868};

    // print output
    #ifdef TEST_PREPROC_PRINT
    for (int i = 0; i < 3; i++)
    {
        printf("num[%d]=%3.15f, denum[%d]=%3.15f\n", i, p_filter->p_num[i], i,
               p_filter->p_dnum[i]);
    }
    #endif

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(exp_num, p_filter->p_num, 3);
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(exp_dnum, p_filter->p_dnum, 3);
}

void test_iir_filter()
{
    // input
    unsigned int siglen = 1000;
    double_vec *p_x = double_vec_new(siglen);
    read_from_txt("./../test/data/iir_in.txt", p_x);

    // iir filter
    double f0 = 50.0;
    struct iir_filter *p_filter = (struct iir_filter*)malloc(sizeof (struct iir_filter));
    p_filter = preproc_iirnotch(FHR_SAMPLING_FREQ, f0);
    preproc_filter(p_x, p_filter);

    // expected output
    double_vec *p_exp = double_vec_new(siglen);
    read_from_txt("./../test/data/iir_out_matlab.txt", p_exp);

    // print output
#ifdef TEST_PREPROC_PRINT
    for (unsigned int i = 0; i < 10; i++)
    {
        printf("i=%d, actual=%3.15f, expected=%3.15f, err=%3.15f\n", i,
               p_x->p_data[i], p_exp->p_data[i],
               p_x->p_data[i] - p_exp->p_data[i]);
    }
#endif

    // assert
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_x->p_data, p_exp->p_data, siglen);
}

void test_rmv_powerline()
{
    // load input 
    unsigned int siglen = 65536;
    double_vec *p_x = double_vec_new(siglen);
    read_from_txt("./../test/data/powerline_rmv_in.txt", p_x);

    // load expected output 
    double_vec *p_expected = double_vec_new(siglen);
    read_from_txt("./../test/data/powerline_rmv_out_matlab.txt", p_expected);

    // power line remove 
    preproc_rmv_powerline(&p_x, 1);

    // print 
    #ifdef TEST_PREPROC_PRINT 
    for (unsigned int i = 0; i < 10; i++)
    {
        printf("i=%ul, actual=%3.15f, expected=%3.15f\n", i, p_x->p_data[i],
            p_expected->p_data[i]);
    }
    #endif

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_x->p_data, p_expected->p_data,siglen);
}

void test_rmv_baseline()
{
    // load input 
    unsigned int siglen = 32768;
    double_vec *p_x = double_vec_new(siglen);
    read_from_txt("./../test/data/baseline_rmv_in.txt", p_x);

    // power line remove 
    preproc_rmv_baseline(&p_x, 1);

    // write output to file 
    double_vec_write(p_x,"./../test/data/baseline_rmv_out_c.txt");

    // load expected output 
    double_vec *p_expected = double_vec_new(siglen);
    read_from_txt("./../test/data/baseline_rmv_out_matlab.txt", p_expected);

    // print 
    #ifdef TEST_PREPROC_PRINT 
    printf("%s\n", __func__);
    for (unsigned int i = 0; i < 10; i++)
    {
        printf("i=%ul, actual=%3.15f, expected=%3.15f\n", i, p_x->p_data[i],
        p_expected->p_data[i]);
    }
    #endif

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_x->p_data, p_expected->p_data,siglen-1000);
}

void test_rmv_highfreq_noise()
{
    // load input 
    unsigned int siglen = 32768;
    double_vec *p_x = double_vec_new(siglen);
    read_from_txt("./../test/data/denoise_in.txt", p_x);

    // load expected output 
    double_vec *p_expected = double_vec_new(siglen);
    read_from_txt("./../test/data/denoise_out_matlab.txt", p_expected);

    // power line remove 
    preproc_rmv_highfreq_noise(&p_x, 1);

    // print 
    #ifdef TEST_PREPROC_PRINT 
    for (unsigned int i = 0; i < siglen; i++)
    {
        printf("i=%ul, actual=%3.25f, expected=%3.25f\n", i, p_x->p_data[i],
            p_expected->p_data[i]);
    }
    #endif

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_x->p_data, p_expected->p_data,siglen);
}

void test_preprocess_ecg()
{
    // load input 
    unsigned int siglen = 32768;
    double_vec *pp_ecg[FHR_NCHANNEL];
    pp_ecg[0]=double_vec_new(siglen);
    pp_ecg[1]=double_vec_new(siglen);
    pp_ecg[2]=double_vec_new(siglen);
    pp_ecg[3]=double_vec_new(siglen);
    read_from_txt_cols("./../test/data/preproc_utest_raw_ecg_in.txt", pp_ecg, FHR_NCHANNEL);

    // expected output 
    double_vec *pp_expected[FHR_NCHANNEL];
    pp_expected[0]=double_vec_new(siglen);
    pp_expected[1]=double_vec_new(siglen);
    pp_expected[2]=double_vec_new(siglen);
    pp_expected[3]=double_vec_new(siglen);
    read_from_txt_cols("./../test/data/preproc_utest_clean_ecg_matlab.txt", pp_expected, FHR_NCHANNEL);


    #ifdef TEST_PREPROC_PRINT
    printf("input channel 0: \n");
    double_vec_print(pp_ecg[0], 10);
    #endif

    // preprocessing ecg 4 channel  
    preproc_preprocess_ecg(pp_ecg, FHR_NCHANNEL);

    #ifdef TEST_PREPROC_PRINT
    for (int i = 0; i < FHR_NCHANNEL; i++)
    {
        printf("channel %d\n", i);
        for (unsigned int j = 0; j < 10; j++)
        {
            printf("i=%ul, actual=%3.25f, expected=%3.25f, err=%3.25f\n", j, pp_ecg[i]->p_data[j],
                pp_expected[i]->p_data[j], pp_ecg[i]->p_data[j] - pp_expected[i]->p_data[j]);
        }
    }
    #endif

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_ecg[0]->p_data, pp_expected[0]->p_data, siglen-1000);
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_ecg[1]->p_data, pp_expected[1]->p_data, siglen-1000);
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_ecg[2]->p_data, pp_expected[2]->p_data, siglen-1000);
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_ecg[3]->p_data, pp_expected[3]->p_data, siglen-1000);
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_replace_nan);
    RUN_TEST(test_design_iir_filter);
    RUN_TEST(test_iir_filter);
    RUN_TEST(test_rmv_powerline);
    RUN_TEST(test_rmv_baseline);
    RUN_TEST(test_rmv_highfreq_noise);
    RUN_TEST(test_preprocess_ecg);

    return 0; 
}


// gcc test_preprocessing.c ./unity/unity.c ./../preprocessing/biorithm_aux.c ./../preprocessing/biorithm_wavelib.c ./../preprocessing/preproc.c -lm -lgsl -lgslcblas -lwavelib