// SYSTEM INCLUDE 
#include <stdio.h>
#include <stdlib.h>

// LOCAL INCLUDE FHR UNIT 
#include "biorithm_aux.h"
#include "fhr_findpeaks.h"
#include "fhr_heartrate_cal.h"

// LOCAL INCLUDE UNITY TEST 
#include "unity.h"

// DEFINE PRINT AND DEBUG 
#define TEST_FHR_ACC_PRINT


void setUp(void)
{

}

void tearDown()
{

}


void test_rpeak_vec_new()
{
	struct rpeak_vec *p = rpeak_vec_new(10);

	rpeak_vec_print(p,5);
}

void test_rpeak_vec_write()
{

	struct rpeak_vec *p = rpeak_vec_new(10);

	p->p_rpeak[5].loc = 100; 
	p->p_rpeak[5].rri = 200; 
	p->p_rpeak[5].bpm = 155.5;

	// rpeak_vec_print(p,10);

	rpeak_vec_write(p, "./../test/data/rpeak_test.txt");


}

void test_rpeak_vec_read()
{
	struct rpeak_vec *p = rpeak_vec_read("./../test/data/rpeak_test.txt", 10);

	rpeak_vec_print(p,10);
}

void test_compute_heartrate()
{
	//	PRAMETER 
	double ULIM = 200.0;
	double LLIM = 50.0;
	double DLIM = 6.0;
	// INPUT 
	unsigned int npeak = 139; 
	peak_vec *p = new_peak_vec(npeak);
	peak_vec_read("./../test/data/mpeak_out_c_ch_4.txt", p);

	// PRINT INTPUT 
	#ifdef TEST_FHR_ACC_PRINT
	print_peak_vec(p, npeak);
	#endif 	

	// COMPUTE HEART RATE 
	struct rpeak_vec *p_heartrate = compute_heartrate(p, ULIM, LLIM, DLIM);

	//PRINT OUTPUT 
	#ifdef TEST_FHR_ACC_PRINT
	rpeak_vec_print(p_heartrate, npeak);
	#endif 

	// LOG OUTPUT TO FILE 
	rpeak_vec_write(p_heartrate, "./../test/data/rpeak_out_c_ch_4.txt");

	// ASSERT 
}

void test_sample_heartrate()
{
	// INPUT 
	unsigned int npeak = 139;
	struct rpeak_vec **pp_rpeak = malloc(4 * sizeof(struct rpeak_vec *));
	pp_rpeak[0] = rpeak_vec_read("./../test/data/fheartrate_log_ch_1.txt", npeak);
	pp_rpeak[1] = rpeak_vec_read("./../test/data/fheartrate_log_ch_2.txt", npeak);
	pp_rpeak[2] = rpeak_vec_read("./../test/data/fheartrate_log_ch_3.txt", npeak);
	pp_rpeak[3] = rpeak_vec_read("./../test/data/fheartrate_log_ch_4.txt", npeak);

	#ifdef TEST_FHR_ACC_PRINT
	printf("INPUT RPEAK VECTOR CHANNEL 0: \n");
	rpeak_vec_print(pp_rpeak[0], npeak);
	#endif 

	// EXPECTED OUTPUT 

	// SAMPLE FHR 
	double_vec **pp_trace = sample_heartrate(pp_rpeak, 50000);

	// PRINT OUTPUT 
	printf("OUTPUT HEART RATE SAMPLED: \n");
	#ifdef TEST_FHR_ACC_PRINT
	for (unsigned int i = 0; i < pp_trace[0]->len; i++)
	{
		printf("binid=%ul, fhr_ch1=%3.2f, fhr_ch2=%3.2f, fhr_ch3=%3.2f, fhr_ch4=%3.2f, fhr_chsum=%3.2f\n",
		i, pp_trace[0]->p_data[i], pp_trace[1]->p_data[i], pp_trace[2]->p_data[i],
		pp_trace[3]->p_data[i], pp_trace[4]->p_data[i]); 
	}
	#endif

	// ASSERT 
}

int main(void)
{
   // UNITY_BEGIN();

   // RUN_TEST(test_rpeak_vec_new);
   // RUN_TEST(test_rpeak_vec_write);
   // RUN_TEST(test_rpeak_vec_read);
   // RUN_TEST(test_compute_heartrate);
   RUN_TEST(test_sample_heartrate);

   return 0;
}

