// SYSTEM INCLUDE
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

// LIB INCLUDE 
#include "./unity/unity.h"
#include "./unity/unity_internals.h"

// PROJECT INCLUDE 
#include "./../preprocessing/preproc.h"
#include "./../preprocessing/biorithm_wavelib.h"


// DEBUG PRINT
// #define TEST_BWAVELIB_PRINT

void setUp()
{

}

void tearDown()
{

}


void test_get_start_idx_layer(void)
{
	// ============================ TEST CASE 1=================================
	// create wavelib object 
	struct param_wave wave = {32768, 12, "db4"};
	struct wavelib_object *p_wave = create_wavelib_object(wave);

	// read input signal from file 
	double_vec *p = double_vec_new(32768);
	double_vec_read(p, "./data/x_wave.txt");

	// dwt 
	dwt(p_wave->wt, p->p_data);

	// expected output starting index of each layer [A,D12,D11,...,D1]
	int exp_start_idx[13] = {0, 14, 28, 50, 88, 158, 292, 554, 1072, 2102, 4156, 
							 8258, 16455}; 

	// call the function to test and assert
	TEST_ASSERT_EQUAL_INT(exp_start_idx[1], get_start_idx_layer(p_wave->wt, 12));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[2], get_start_idx_layer(p_wave->wt, 11));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[3], get_start_idx_layer(p_wave->wt, 10));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[4], get_start_idx_layer(p_wave->wt, 9));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[5], get_start_idx_layer(p_wave->wt, 8));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[6], get_start_idx_layer(p_wave->wt, 7));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[7], get_start_idx_layer(p_wave->wt, 6));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[8], get_start_idx_layer(p_wave->wt, 5));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[9], get_start_idx_layer(p_wave->wt, 4));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[10], get_start_idx_layer(p_wave->wt, 3));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[11], get_start_idx_layer(p_wave->wt, 2));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[12], get_start_idx_layer(p_wave->wt, 1));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[0], get_start_idx_layer(p_wave->wt, 0));

	// free mem
	delete_wavelib_object(p_wave);
}


void test_weight_wlayer()
{


}

void test_shrink_wlayer()
{
	// set param 
	unsigned int signlen = 32768;
	int nlayer = 4; 
	double uthresh = 0.00001770983446194867330604136302;
	double p_thresh[5] = {-1.0, uthresh, uthresh, uthresh, uthresh};
	struct param_wave wave = {signlen, nlayer, "coif4"};
	struct wavelib_object *p_wave = create_wavelib_object(wave);

	// load input from file 
	double_vec *p_x = double_vec_new(signlen);
	double_vec_read(p_x, "./data/x_wave.txt");

	// load expected from file 
	unsigned int woutlen = 32794; 
	double_vec *p_exp = double_vec_new(woutlen);
	double_vec_read(p_exp, "./data/shrink_out_matlab.txt");

	#ifdef TEST_BWAVELIB_PRINT
		double_vec_print(p_x, 10);
	#endif

	// dwt 
	dwt(p_wave->wt, p_x->p_data);

	// shrinking layer 
	shrink_wlayer(p_wave->wt, p_thresh);

	// print 
	#ifdef TEST_BWAVELIB_PRINT
	for (int i = 2044; i < 2065; i++)
	{
		printf("%d, actual=%3.32f, expected=%3.32f\n", i, p_wave->wt->output[i],
			p_exp->p_data[i]);
	}
	#endif 

	// assert 
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_wave->wt->output, p_exp->p_data, woutlen);
}


void test_find_universal_thresh()
{
	// set input and parameter for wavelet 
	unsigned int signlen = 32768;
	int nlayer = 1; 
	double p_thresh[4] = {-1.0, -1.0, -1.0, -1.0};
	struct param_wave wave = {signlen, nlayer, "coif4"};
	struct wavelib_object *p_wave = create_wavelib_object(wave);

	// load input from file 
	double_vec *p_x = double_vec_new(signlen);
	double_vec_read(p_x, "./data/x_wave.txt");

	// actual output 
	double expected = 0.0000177098344619486733060; 

	// find threshold 
	double uthresh = find_universal_thresh(p_x);

	// print 
	#ifdef TEST_BWAVELIB_PRINT
	printf("uthresh = %3.25f\n",uthresh);
	#endif

	// assert 
	TEST_ASSERT_EQUAL_DOUBLE(uthresh, expected);

}

void test_wdenoise()
{
	// set input and parameter for wavelet 
	unsigned int siglen = 32768;
	int nlayer = 4; 
	struct param_wave wave = {siglen, nlayer, "coif4"};
	struct wavelib_object *p_wave = create_wavelib_object(wave);

	// load input from file 
	double_vec *p_x = double_vec_new(siglen);
	double_vec_read(p_x, "./data/x_wave.txt");

	// load expected output 
	double_vec *p_expected = double_vec_new(siglen);
	double_vec_read(p_expected, "./data/wdenoised_matlab.txt");

	// denoise 
	wdenoise(p_x, wave, "SORH", true);

	// print 
	#ifdef TEST_BWAVELIB_PRINT
	for (int i = 0; i < 1000; i++) 
	{
		printf("i=%d, actual=%3.25f, expected=%3.25f, err=%3.25f\n",
			i, p_x->p_data[i], p_expected->p_data[i],
			(p_x->p_data[i] - p_expected->p_data[i]) / fabs(p_x->p_data[i]));
	}
	#endif

	// assert 
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_x->p_data, p_expected->p_data,siglen);
}


int main(void)
{

	UNITY_BEGIN();
    RUN_TEST(test_get_start_idx_layer);
    RUN_TEST(test_shrink_wlayer);
    RUN_TEST(test_find_universal_thresh);
    RUN_TEST(test_wdenoise);

}

// gcc test_biorithm_wavelib.c ./../preprocessing/biorithm_wavelib.c ./../preprocessing/biorithm_aux.c ./unity/unity.c -lm -lgsl -lgslcblas -lwavelib