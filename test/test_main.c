/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 24 OCT 2019 
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             24  OCT 2019    T.H          Original Code
*******************************************************************************/
// SYSTEM INCLUDES
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>

// LOCAL INCLUDES
#include "biorithm_aux.h"				   // AUXILIARY FUNCTION 	
#include "biorithm_wavelib.h"			   // WAVELIB 
#include "preproc.h"					   // PREPROCESSING
#include "fhr_feature_detection.h"	       // FEATURE DETECTION
#include "fhr_feature_classification.h"    // FEATURE CLASSIFICATION
#include "fhr_baseline.h"				   // BASELINE AND ADCEL 	
#include "fhr_app_log.h"				   // LOG APP OUTPUT TO FILE	
#include "fhr_heartrate_cal.h"			   // COMPUTE HEART RATE
#include "render_img.h"					   // PLPLOT RENDER IMAGE 	
#include "fhr_ua.h"						   // UA UNIT 	

// UNITTY TEST 
#include "unity.h"
#include "unity_internals.h"
#include "biorithm_unit_test.h"

// DEBUG AND PRINT 
#define FHR_APP_PROGRESS_PRINT 

#define FHR_APP_PRINT

#define FHR_APP_LOG

// #define FHR_APP_DEBUG

#define FHR_INTEGRATION_TEST 

// #define FHR_LOG_DIR "./../test/data/"		 // DIECTORY STORE LOG 

#define FHR_TKEO_IPROD_MODE "TKEO"        // TKEO MODE FOR R PREK DETECTION	
// #define FHR_TKEO_IPROD_MODE "IPROD"          // TKEO MODE FOR R PREK DETECTION	
		

// MIN PEAK DISTANCE 
static int GP_MFPEAK_DMIN[2] = {10, 10};

// MIN HEIGHT MPEAK AND FPEAK FOR EACH CHANNEL 
static double GP_MFPEAK_HMIN[2 * FHR_NCHANNEL] = {4.0,4.0,4.0,4.0,
												  3.0,3.0,3.0,3.0};

// REFERENCE HEIGHT THRESHOLD OF EACH CHANNEL FOR RMV MPEAK FROM IPROD 
static double GP_RHEIGHT[FHR_NCHANNEL] = {1.0, 1.0, 1.0, 1.0};

// MXIMUM WIDTH OF QRS 
static int G_FHR_QRS_WMAX = 50; 

// SET CHANNEL FOR RENDER FHR TRACE AND UA TRACE
static const int G_BEST_MCH_ID    = 1;          // BEST MATERNAL CHANNEL 
static const int G_RENDER_FCH_ID  = 3; 			// CHANNEL FOR RENDER FETAL
static const int G_RENDER_UCH_ID  = 3; 			// CHANNEL FOR RENDER UA 

// HEART RATE ULIM LLIM AND DLIM 


// INPUT ARG: (1) INPUT_DATA_PATH, (2) INPUT_DATA_LENGTH, (3) OUTPUT_DATA_PATH

char *str_concat(char *p1, char* p2)
{
	char *ps = malloc(strlen(p1) + strlen(p2) + 20); 
	sprintf(ps, "%s%s", p1, p2);
	return ps; 
}

/*------------------------------------------------------------------------------
Setup: 
Description: 
------------------------------------------------------------------------------*/
void setUp(void)
{

}

/*------------------------------------------------------------------------------
Tear down:
Description: 
------------------------------------------------------------------------------*/
void tearDown(void)
{

}


void  test_main()
{

	//=================================PATH BASE================================
	char *p_path_base = "./../test/data/nuh_1014/";


	//=================================EXPECTED OUTPUT==========================
	printf("LOADING EXPECTED OUTPUT: \n");
	// EXPECTED OUTPUT FOR TEST 
	unsigned int siglen        = 25000*10; 
	unsigned int tkeolen_exp   = siglen - 1 - 2 * FHR_TKWIN; 
	unsigned int iprodlen_exp  = siglen - 1 - FHR_IPWIN + 1; 

	// READ EXPECTED CLEAN ECG AFTER PREPROCESSING
	double_vec *pp_ecg_exp[FHR_NCHANNEL];
	pp_ecg_exp[0] = double_vec_new(siglen);
	pp_ecg_exp[1] = double_vec_new(siglen);
	pp_ecg_exp[2] = double_vec_new(siglen);
	pp_ecg_exp[3] = double_vec_new(siglen);
	read_from_txt_cols(str_concat(p_path_base, "clean_ecg_matlab.txt"), pp_ecg_exp, FHR_NCHANNEL);

	// READ EXPECTED TKEO
	double_vec *pp_tkeo_exp[FHR_NCHANNEL];
	pp_tkeo_exp[0] = double_vec_new(tkeolen_exp);
	pp_tkeo_exp[1] = double_vec_new(tkeolen_exp);
	pp_tkeo_exp[2] = double_vec_new(tkeolen_exp);
	pp_tkeo_exp[3] = double_vec_new(tkeolen_exp);
	read_from_txt_cols(str_concat(p_path_base, "tkeo_matlab.txt"), pp_tkeo_exp, FHR_NCHANNEL);

	// READ EXPECTED IRPOD 
	double_vec *pp_iprod_exp[FHR_NCHANNEL];
	pp_iprod_exp[0] = double_vec_new(iprodlen_exp);
	pp_iprod_exp[1] = double_vec_new(iprodlen_exp);
	pp_iprod_exp[2] = double_vec_new(iprodlen_exp);
	pp_iprod_exp[3] = double_vec_new(iprodlen_exp);
	read_from_txt_cols(str_concat(p_path_base, "iprod_matlab.txt"), pp_iprod_exp, FHR_NCHANNEL);

	// READ EXPECTED MPEAK TKEO 
	unsigned int nmpeak_ch1_exp = 507; 
	unsigned int nmpeak_ch2_exp = 540; 
	unsigned int nmpeak_ch3_exp = 575; 
	unsigned int nmpeak_ch4_exp = 759; 

	double_vec *pp_tkeo_mpeak_exp[FHR_NCHANNEL];
	pp_tkeo_mpeak_exp[0] = double_vec_new(nmpeak_ch1_exp);
	pp_tkeo_mpeak_exp[1] = double_vec_new(nmpeak_ch2_exp);
	pp_tkeo_mpeak_exp[2] = double_vec_new(nmpeak_ch3_exp);
	pp_tkeo_mpeak_exp[3] = double_vec_new(nmpeak_ch4_exp);

	double_vec_read(pp_tkeo_mpeak_exp[0], str_concat(p_path_base, "mpeak_tkeo_ch1_matlab.txt"));
	double_vec_read(pp_tkeo_mpeak_exp[1], str_concat(p_path_base, "mpeak_tkeo_ch2_matlab.txt"));
	double_vec_read(pp_tkeo_mpeak_exp[2], str_concat(p_path_base, "mpeak_tkeo_ch3_matlab.txt"));
	double_vec_read(pp_tkeo_mpeak_exp[3], str_concat(p_path_base, "mpeak_tkeo_ch4_matlab.txt"));

	// READ EXPECTED FPEAK TKEO 
	unsigned int nfpeak_tkeo_ch1_exp = 824; 
	unsigned int nfpeak_tkeo_ch2_exp = 908; 
	unsigned int nfpeak_tkeo_ch3_exp = 823; 
	unsigned int nfpeak_tkeo_ch4_exp = 1209; 

	double_vec *pp_tkeo_fpeak_exp[FHR_NCHANNEL];
	pp_tkeo_fpeak_exp[0] = double_vec_new(nfpeak_tkeo_ch1_exp);
	pp_tkeo_fpeak_exp[1] = double_vec_new(nfpeak_tkeo_ch2_exp);
	pp_tkeo_fpeak_exp[2] = double_vec_new(nfpeak_tkeo_ch3_exp);
	pp_tkeo_fpeak_exp[3] = double_vec_new(nfpeak_tkeo_ch4_exp);

	double_vec_read(pp_tkeo_fpeak_exp[0], str_concat(p_path_base, "fpeak_tkeo_ch1_matlab.txt"));
	double_vec_read(pp_tkeo_fpeak_exp[1], str_concat(p_path_base, "fpeak_tkeo_ch2_matlab.txt"));
	double_vec_read(pp_tkeo_fpeak_exp[2], str_concat(p_path_base, "fpeak_tkeo_ch3_matlab.txt"));
	double_vec_read(pp_tkeo_fpeak_exp[3], str_concat(p_path_base, "fpeak_tkeo_ch4_matlab.txt"));

	// READ EXPECTED FPEAK IPROD
	unsigned int nfpeak_iprod_ch1_exp = 712; 
	unsigned int nfpeak_iprod_ch2_exp = 606; 
	unsigned int nfpeak_iprod_ch3_exp = 629; 
	unsigned int nfpeak_iprod_ch4_exp = 930; 

	double_vec *pp_iprod_fpeak_exp[FHR_NCHANNEL];
	pp_iprod_fpeak_exp[0] = double_vec_new(nfpeak_iprod_ch1_exp);
	pp_iprod_fpeak_exp[1] = double_vec_new(nfpeak_iprod_ch2_exp);
	pp_iprod_fpeak_exp[2] = double_vec_new(nfpeak_iprod_ch3_exp);
	pp_iprod_fpeak_exp[3] = double_vec_new(nfpeak_iprod_ch4_exp);

	double_vec_read(pp_iprod_fpeak_exp[0], str_concat(p_path_base, "fpeak_iprod_ch1_matlab.txt"));
	double_vec_read(pp_iprod_fpeak_exp[1], str_concat(p_path_base, "fpeak_iprod_ch2_matlab.txt"));
	double_vec_read(pp_iprod_fpeak_exp[2], str_concat(p_path_base, "fpeak_iprod_ch3_matlab.txt"));
	double_vec_read(pp_iprod_fpeak_exp[3], str_concat(p_path_base, "fpeak_iprod_ch4_matlab.txt"));


	//==================================INPUT RAW ECG ========================== 
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("LOADING DATA FROM FILE DISK: \n");
	#endif

	// NUMBER OF WINDOW 
	double_vec *pp_ecg[FHR_NCHANNEL];
	
	pp_ecg[0] = double_vec_new(siglen);
	pp_ecg[1] = double_vec_new(siglen);
	pp_ecg[2] = double_vec_new(siglen);
	pp_ecg[3] = double_vec_new(siglen);

	// READ INPUT DATA FROM FILE 
	read_from_txt_cols(str_concat(p_path_base, "raw_ecg.txt"), pp_ecg, FHR_NCHANNEL);

	#ifdef FHR_APP_PRINT
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		printf("raw ecg channel %d: \n", i);
		double_vec_print(pp_ecg[i], 10);
	}
	#endif 

	//==============================PREPROCESSING===============================
	// PREPROCESSING 
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("PREPROCESSING: \n");
	#endif
	preproc_preprocess_ecg(pp_ecg, FHR_NCHANNEL);

	// PRINT PREPROCESSING OUTPUT 
	#ifdef FHR_APP_PRINT
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		printf("clean ecg channel %d: \n", i);
		double_vec_print(pp_ecg[i], 10);
	}
	#endif 


	// LOG PREPROCESSING OUTPUT TO FILE 
	#ifdef FHR_APP_LOG
	write_to_txt(pp_ecg, FHR_NCHANNEL, str_concat(p_path_base,"clean_ect.txt"));
	#endif 

	// ASSERT 
	#ifdef FHR_INTEGRATION_TEST
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_ecg_exp[0]->p_data, pp_ecg[0]->p_data, siglen);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_ecg_exp[1]->p_data, pp_ecg[1]->p_data, siglen);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_ecg_exp[2]->p_data, pp_ecg[2]->p_data, siglen);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_ecg_exp[3]->p_data, pp_ecg[3]->p_data, siglen);
	#endif

    //=================================== UA ===================================
	// BUFFER ECG DATA TO UA 
	double_vec *pp_ecg_ua[FHR_NCHANNEL];
	pp_ecg_ua[0] = double_vec_new(siglen);
	pp_ecg_ua[1] = double_vec_new(siglen);
	pp_ecg_ua[2] = double_vec_new(siglen);
	pp_ecg_ua[3] = double_vec_new(siglen);

	double_vec_copy(pp_ecg[0], pp_ecg_ua[0]);
	double_vec_copy(pp_ecg[1], pp_ecg_ua[1]);
	double_vec_copy(pp_ecg[2], pp_ecg_ua[2]);
	double_vec_copy(pp_ecg[3], pp_ecg_ua[3]);

	//=========================FEATURE DETECTION ===============================
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("FEATURE DETECTION\n");
	#endif

	// FEATURE DETECTION 
	struct fhrfeature *p_feature = detect_fhr_feature(pp_ecg);

	// PRINT FEATURE DETECTION OUTPUT 
	#ifdef FHR_APP_PRINT
	// TKEO 
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
	    printf("tkeo channel %ul length %zu\n", i, p_feature->pp_tkeo[i]->len);
	    double_vec_print(p_feature->pp_tkeo[i],10);
	}
	// IPROD
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
	    printf("iprod channel %ul length %zu\n", i, p_feature->pp_iprod[i]->len);
	    double_vec_print(p_feature->pp_iprod[i],10);
	}
	#endif 

	// LOG TKEO AND IPROD TO FILE 
	#ifdef FHR_APP_LOG
	write_to_txt(p_feature->pp_tkeo, FHR_NCHANNEL,  str_concat(p_path_base, "tkeo.txt"));
	write_to_txt(p_feature->pp_iprod, FHR_NCHANNEL, str_concat(p_path_base, "iprod.txt"));
	#endif

	#ifdef FHR_INTEGRATION_TEST
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_feature->pp_tkeo[0]->p_data,
	pp_tkeo_exp[0]->p_data, tkeolen_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_feature->pp_tkeo[1]->p_data,
	pp_tkeo_exp[1]->p_data, tkeolen_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_feature->pp_tkeo[2]->p_data,
	pp_tkeo_exp[2]->p_data, tkeolen_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_feature->pp_tkeo[3]->p_data,
	pp_tkeo_exp[3]->p_data, tkeolen_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_feature->pp_iprod[0]->p_data,
	pp_iprod_exp[0]->p_data, iprodlen_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_feature->pp_iprod[1]->p_data,
	pp_iprod_exp[1]->p_data, iprodlen_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_feature->pp_iprod[2]->p_data,
	pp_iprod_exp[2]->p_data, iprodlen_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_feature->pp_iprod[3]->p_data,
	pp_iprod_exp[3]->p_data, iprodlen_exp);
	#endif

	//=========================FEATURE CLASSIFICATION ==========================
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("FEATURE CLASSIFICATION BEST MATERNAL CHANNEL %d\n", G_BEST_MCH_ID);
	#endif

	double_vec **pp_iprod = malloc(FHR_NCHANNEL * sizeof(double_vec *));
	unsigned int iprodlen = p_feature->pp_tkeo[0]->len;
	pp_iprod[0] = double_vec_new(iprodlen);
	pp_iprod[1] = double_vec_new(iprodlen);
	pp_iprod[2] = double_vec_new(iprodlen);
	pp_iprod[3] = double_vec_new(iprodlen);
	unsigned int align = floor((2*FHR_TKWIN - FHR_IPWIN + 1) / 2.0); 

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("%s MODE\n", FHR_TKEO_IPROD_MODE);
	#endif

	// TKEO MODE MEANS TO SET IPROD TO TKEO 
	if (strcmp(FHR_TKEO_IPROD_MODE,"TKEO")==0)
	{
		for (unsigned int i = 0; i< FHR_NCHANNEL; i++)
		{
			for (unsigned int j = 0; j < iprodlen; j++)
			{
				pp_iprod[i]->p_data[j] = p_feature->pp_tkeo[i]->p_data[j];
			}
		}
	}

	// TKEO MODE MEANS TO SET IPROD TO TKEO 
	if (strcmp(FHR_TKEO_IPROD_MODE,"IPROD")==0)
	{
		// IPROD MODE MEANS ALIGN TIME IPROD TO TKEO 
		for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
		{
			for (unsigned int j = 0; j < iprodlen; j++)
			{
				pp_iprod[i]->p_data[j] = p_feature->pp_iprod[i]->p_data[align + j];
			}
		}
	}

	// FIND M AND FPEAK FROM TKEO AND IRPOD 
	struct mfpeak *p_mfpeak = compute_mfpeak(p_feature->pp_tkeo, pp_iprod, 
	FHR_FINDPEAK_WLEN, GP_MFPEAK_DMIN, GP_MFPEAK_HMIN, GP_RHEIGHT, G_FHR_QRS_WMAX,
	G_BEST_MCH_ID);
	
	// PRINT FEATURE CLASSIFICATION OUTPUT 
	#ifdef FHR_APP_PRINT
	printf("channel 1 has %ul mpeak: \n", p_mfpeak->p_mpeak[0]->len);
	printf("channel 2 has %ul mpeak: \n", p_mfpeak->p_mpeak[1]->len);
	printf("channel 3 has %ul mpeak: \n", p_mfpeak->p_mpeak[2]->len);
	printf("channel 4 has %ul mpeak: \n", p_mfpeak->p_mpeak[3]->len);

	printf("channel 1 has %ul fpeak: \n", p_mfpeak->p_fpeak[0]->len);
	printf("channel 2 has %ul fpeak: \n", p_mfpeak->p_fpeak[1]->len);
	printf("channel 3 has %ul fpeak: \n", p_mfpeak->p_fpeak[2]->len);
	printf("channel 4 has %ul fpeak: \n", p_mfpeak->p_fpeak[3]->len);
	#endif

	#ifdef FHR_APP_LOG
	struct peaklog_vec **pp_mpeaklog = log_mpeak(pp_ecg, p_feature, p_mfpeak);
	struct peaklog_vec **pp_fpeaklog = log_fpeak(pp_ecg, p_feature, p_mfpeak);

	// printf("TKEO MPEAK CHANNEL 1\n");
	// peaklog_vec_print(pp_mpeaklog[0], 10);
	// printf("TKEO MPEAK CHANNEL 2\n");
	// peaklog_vec_print(pp_mpeaklog[1], 10);
	// printf("TKEO MPEAK CHANNEL 3\n");
	// peaklog_vec_print(pp_mpeaklog[2], 10);
	// printf("TKEO MPEAK CHANNEL 4\n");
	// peaklog_vec_print(pp_mpeaklog[3], 10);

	// printf("TKEO FPEAK CHANNEL 1\n");
	// peaklog_vec_print(pp_fpeaklog[0], 10);
	// printf("TKEO FPEAK CHANNEL 2\n");
	// peaklog_vec_print(pp_fpeaklog[1], 10);
	// printf("TKEO FPEAK CHANNEL 3\n");
	// peaklog_vec_print(pp_fpeaklog[2], 10);
	// printf("TKEO FPEAK CHANNEL 4\n");
	// peaklog_vec_print(pp_fpeaklog[3], 10);

	// TKEO AND IPROD AFTER RMV MPEAK 


	// MPEAK AND FPEAK LOG 
	peaklog_vec_write(pp_mpeaklog[0], str_concat(p_path_base,"mpeak_app_log_ch1.txt"));
	peaklog_vec_write(pp_mpeaklog[1], str_concat(p_path_base,"mpeak_app_log_ch2.txt"));
	peaklog_vec_write(pp_mpeaklog[2], str_concat(p_path_base,"mpeak_app_log_ch3.txt"));
	peaklog_vec_write(pp_mpeaklog[3], str_concat(p_path_base,"mpeak_app_log_ch4.txt"));

	peaklog_vec_write(pp_fpeaklog[0], str_concat(p_path_base,"fpeak_app_log_ch1.txt"));
	peaklog_vec_write(pp_fpeaklog[1], str_concat(p_path_base,"fpeak_app_log_ch2.txt"));
	peaklog_vec_write(pp_fpeaklog[2], str_concat(p_path_base,"fpeak_app_log_ch3.txt"));
	peaklog_vec_write(pp_fpeaklog[3], str_concat(p_path_base,"fpeak_app_log_ch4.txt"));
	#endif


	double_vec *pp_tkeo_mpeak[FHR_NCHANNEL];
	double_vec *pp_tkeo_fpeak[FHR_NCHANNEL];

	pp_tkeo_mpeak[0] = double_vec_new(p_mfpeak->p_mpeak[0]->len);
	pp_tkeo_mpeak[1] = double_vec_new(p_mfpeak->p_mpeak[1]->len);
	pp_tkeo_mpeak[2] = double_vec_new(p_mfpeak->p_mpeak[2]->len);
	pp_tkeo_mpeak[3] = double_vec_new(p_mfpeak->p_mpeak[3]->len);

	pp_tkeo_fpeak[0] = double_vec_new(p_mfpeak->p_fpeak[0]->len);
	pp_tkeo_fpeak[1] = double_vec_new(p_mfpeak->p_fpeak[1]->len);
	pp_tkeo_fpeak[2] = double_vec_new(p_mfpeak->p_fpeak[2]->len);
	pp_tkeo_fpeak[3] = double_vec_new(p_mfpeak->p_fpeak[3]->len);


	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		// MPEAK LOC FOUND FROM TKEO 
		for (unsigned int j = 0; j < p_mfpeak->p_mpeak[i]->len; j++)
		{
			pp_tkeo_mpeak[i]->p_data[j] = p_mfpeak->p_mpeak[i]->p_peak[j].loc; 
		}

		// FPEAK LOC FOUND FROM TKEO AFTER RMV MPEAK 
		for (unsigned int j = 0; j < p_mfpeak->p_fpeak[i]->len; j++)
		{
			pp_tkeo_fpeak[i]->p_data[j] = p_mfpeak->p_fpeak[i]->p_peak[j].loc; 
		}
	}

	// LOG TO COMPARE MATLAB 
	double_vec_write(pp_tkeo_mpeak[0], str_concat(p_path_base, "mpeak_tkeo_ch1.txt"));
	double_vec_write(pp_tkeo_mpeak[1], str_concat(p_path_base, "mpeak_tkeo_ch2.txt"));
	double_vec_write(pp_tkeo_mpeak[2], str_concat(p_path_base, "mpeak_tkeo_ch3.txt"));
	double_vec_write(pp_tkeo_mpeak[3], str_concat(p_path_base, "mpeak_tkeo_ch4.txt"));

	if (strcmp(FHR_TKEO_IPROD_MODE,"TKEO")==0)
	{
		double_vec_write(pp_tkeo_fpeak[0], str_concat(p_path_base, "fpeak_tkeo_ch1.txt"));
		double_vec_write(pp_tkeo_fpeak[1], str_concat(p_path_base, "fpeak_tkeo_ch2.txt"));
		double_vec_write(pp_tkeo_fpeak[2], str_concat(p_path_base, "fpeak_tkeo_ch3.txt"));
		double_vec_write(pp_tkeo_fpeak[3], str_concat(p_path_base, "fpeak_tkeo_ch4.txt"));

		write_to_txt(pp_iprod, FHR_NCHANNEL, str_concat(p_path_base, "tkeo_rmv_mpeak.txt"));
	}

	if (strcmp(FHR_TKEO_IPROD_MODE,"IPROD")==0)
	{
		double_vec_write(pp_tkeo_fpeak[0], str_concat(p_path_base, "fpeak_iprod_ch1.txt"));
		double_vec_write(pp_tkeo_fpeak[1], str_concat(p_path_base, "fpeak_iprod_ch2.txt"));
		double_vec_write(pp_tkeo_fpeak[2], str_concat(p_path_base, "fpeak_iprod_ch3.txt"));
		double_vec_write(pp_tkeo_fpeak[3], str_concat(p_path_base, "fpeak_iprod_ch4.txt"));

		write_to_txt(pp_iprod, FHR_NCHANNEL, str_concat(p_path_base, "iprod_rmv_mpeak.txt"));
	}

	// ASSERT MPEAK FOUND FROM TKEO 

	#ifdef FHR_INTEGRATION_TEST
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_tkeo_mpeak_exp[0]->p_data, 
	pp_tkeo_mpeak[0]->p_data, nmpeak_ch1_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_tkeo_mpeak_exp[1]->p_data, 
	pp_tkeo_mpeak[1]->p_data, nmpeak_ch2_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_tkeo_mpeak_exp[2]->p_data, 
	pp_tkeo_mpeak[2]->p_data, nmpeak_ch3_exp);

	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_tkeo_mpeak_exp[3]->p_data, 
	pp_tkeo_mpeak[3]->p_data, nmpeak_ch4_exp);


	// ASSERT FPEAK FOUND FROM TKEO AFTER RMV MPEAK 
	if (strcmp(FHR_TKEO_IPROD_MODE,"TKEO")==0)
	{
		TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_tkeo_fpeak_exp[0]->p_data, 
		pp_tkeo_fpeak[0]->p_data, nfpeak_tkeo_ch1_exp);

		TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_tkeo_fpeak_exp[1]->p_data, 
		pp_tkeo_fpeak[1]->p_data, nfpeak_tkeo_ch2_exp);

		TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_tkeo_fpeak_exp[2]->p_data, 
		pp_tkeo_fpeak[2]->p_data, nfpeak_tkeo_ch3_exp);

		TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_tkeo_fpeak_exp[3]->p_data, 
		pp_tkeo_fpeak[3]->p_data, nfpeak_tkeo_ch4_exp);
	}

	if (strcmp(FHR_TKEO_IPROD_MODE,"IPROD")==0)
	{
		TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_iprod_fpeak_exp[0]->p_data, 
		pp_tkeo_fpeak[0]->p_data, nfpeak_iprod_ch1_exp);

		TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_iprod_fpeak_exp[1]->p_data, 
		pp_tkeo_fpeak[1]->p_data, nfpeak_iprod_ch2_exp);

		TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_iprod_fpeak_exp[2]->p_data, 
		pp_tkeo_fpeak[2]->p_data, nfpeak_iprod_ch3_exp);

		TEST_ASSERT_EQUAL_DOUBLE_ARRAY(pp_iprod_fpeak_exp[3]->p_data, 
		pp_tkeo_fpeak[3]->p_data, nfpeak_iprod_ch4_exp);
	}
	#endif

	//========================= HEART RATE CALCULATION =========================
	// TODO DOUBLE CHECK 
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("COMPUTING HEART RATE FROM PEAK LOCS: \n");
	#endif

	struct rpeak_vec **pp_mheartrate =  malloc(FHR_NCHANNEL * sizeof(struct rpeak_vec *)); 
	struct rpeak_vec **pp_fheartrate =  malloc(FHR_NCHANNEL * sizeof(struct rpeak_vec *));

	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		pp_mheartrate[i] = compute_heartrate(p_mfpeak->p_mpeak[i], FHR_MBPM_ULIM, 
		FHR_MBPM_LLIM, FHR_MBPM_DLIM);

		pp_fheartrate[i] = compute_heartrate(p_mfpeak->p_fpeak[i], FHR_FBPM_ULIM,
		FHR_FBPM_LLIM, FHR_FBPM_DLIM);
	}

	#ifdef FHR_APP_PRINT
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		printf("maternal heart rate channel %ul: \n", i);
		if (pp_mheartrate[i] != NULL)
		{
			rpeak_vec_print(pp_mheartrate[i],10);			
		}
	}
	#endif 

	#ifdef FHR_APP_PRINT
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		printf("fetal heart rate channel %ul: \n", i);
		if (pp_fheartrate[i] != NULL)
		{
			rpeak_vec_print(pp_fheartrate[i],10);
		}
	}
	#endif


	#ifdef FHR_APP_LOG
	rpeak_vec_write(pp_mheartrate[0], str_concat(p_path_base,"mheartrate_app_log_ch1.txt"));
	rpeak_vec_write(pp_mheartrate[1], str_concat(p_path_base,"mheartrate_app_log_ch2.txt"));
	rpeak_vec_write(pp_mheartrate[2], str_concat(p_path_base,"mheartrate_app_log_ch3.txt"));
	rpeak_vec_write(pp_mheartrate[3], str_concat(p_path_base,"mheartrate_app_log_ch4.txt"));
	
	rpeak_vec_write(pp_fheartrate[0], str_concat(p_path_base,"fheartrate_app_log_ch1.txt"));
	rpeak_vec_write(pp_fheartrate[1], str_concat(p_path_base,"fheartrate_app_log_ch2.txt"));
	rpeak_vec_write(pp_fheartrate[2], str_concat(p_path_base,"fheartrate_app_log_ch3.txt"));
	rpeak_vec_write(pp_fheartrate[3], str_concat(p_path_base,"fheartrate_app_log_ch4.txt"));
	#endif

	//=============================BASELINE=====================================
	//SAMPLING HEART RATE FIX 250ms
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("FINDING BASELINE: \n");
	#endif

	double_vec **pp_ftrace = sample_heartrate(pp_fheartrate, siglen);
	double_vec **pp_mtrace = sample_heartrate(pp_mheartrate, siglen);


	#ifdef FHR_APP_PRINT
	for (unsigned int i = 0; i < pp_ftrace[0]->len; i++)
	{
		printf("binid=%ul, fhr_ch1=%3.2f, fhr_ch2=%3.2f, fhr_ch3=%3.2f, fhr_ch4=%3.2f, fhr_chsum=%3.2f\n",
		i, pp_ftrace[0]->p_data[i], pp_ftrace[1]->p_data[i], pp_ftrace[2]->p_data[i],
		pp_ftrace[3]->p_data[i], pp_ftrace[4]->p_data[i]); 
	}
	#endif 

	#ifdef FHR_APP_LOG
	if (strcmp(FHR_TKEO_IPROD_MODE,"TKEO")==0)
	{
		write_to_txt(pp_mtrace, FHR_NCHANNEL+1, str_concat(p_path_base, "mheartrate_trace_tkeo.txt"));
		write_to_txt(pp_ftrace, FHR_NCHANNEL+1, str_concat(p_path_base, "fheartrate_trace_tkeo.txt"));
	}

	if (strcmp(FHR_TKEO_IPROD_MODE,"IPROD")==0)
	{
		write_to_txt(pp_mtrace, FHR_NCHANNEL+1, str_concat(p_path_base, "mheartrate_trace_irpod.txt"));
		write_to_txt(pp_ftrace, FHR_NCHANNEL+1, str_concat(p_path_base, "fheartrate_trace_iprod.txt"));
	}
	#endif

	//FIND BASELINE AND ADCEL 
	baseline **pp_baseline = malloc((FHR_NCHANNEL + 1) * sizeof(baseline *));
	for (unsigned int i = 0; i < FHR_NCHANNEL + 1; i++)
	{
		pp_baseline[i] = find_baseline(pp_ftrace[i], FHR_BIN_FREQ);
	} 

	#ifdef FHR_APP_LOG
	//BASELINE 
	double_vec_write((pp_baseline[0]->p_base), str_concat(p_path_base, "baseline_ch1.txt"));
	double_vec_write((pp_baseline[1]->p_base), str_concat(p_path_base, "baseline_ch2.txt"));
	double_vec_write((pp_baseline[2]->p_base), str_concat(p_path_base, "baseline_ch3.txt"));
	double_vec_write((pp_baseline[3]->p_base), str_concat(p_path_base, "baseline_ch4.txt"));
	double_vec_write((pp_baseline[4]->p_base), str_concat(p_path_base, "baseline_ch5.txt"));
	//FASTWAVE
	double_vec_write((pp_baseline[0]->p_fast), str_concat(p_path_base, "fastwave_ch1.txt"));
	double_vec_write((pp_baseline[1]->p_fast), str_concat(p_path_base, "fastwave_ch2.txt"));
	double_vec_write((pp_baseline[2]->p_fast), str_concat(p_path_base, "fastwave_ch3.txt"));
	double_vec_write((pp_baseline[3]->p_fast), str_concat(p_path_base, "fastwave_ch4.txt"));
	double_vec_write((pp_baseline[4]->p_fast), str_concat(p_path_base, "fastwave_ch5.txt"));
	//SLOWWAVE
	double_vec_write((pp_baseline[0]->p_slow), str_concat(p_path_base, "slowwave_ch1.txt"));
	double_vec_write((pp_baseline[1]->p_slow), str_concat(p_path_base, "slowwave_ch2.txt"));
	double_vec_write((pp_baseline[2]->p_slow), str_concat(p_path_base, "slowwave_ch3.txt"));
	double_vec_write((pp_baseline[3]->p_slow), str_concat(p_path_base, "slowwave_ch4.txt"));
	double_vec_write((pp_baseline[4]->p_slow), str_concat(p_path_base, "slowwave_ch5.txt"));
	//ADCEL

	if(pp_baseline[0]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_base, "adcel_ch1.txt"), (pp_baseline[0]->p_ad));
	}

	if(pp_baseline[1]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_base, "adcel_ch2.txt"), (pp_baseline[1]->p_ad));
	}

	if(pp_baseline[2]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_base, "adcel_ch3.txt") ,(pp_baseline[2]->p_ad));
	}

	if(pp_baseline[3]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_base, "adcel_ch4.txt"), (pp_baseline[3]->p_ad));
	}

	if(pp_baseline[4]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_base, "adcel_ch5.txt"), (pp_baseline[4]->p_ad));
	}
	#endif

	//============================= UA DATA ====================================
	int a = 0; 
	if (a==1)
	{
	struct uatrace *pp_ua[FHR_NCHANNEL];

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("UA PROCESSING CHANNEL 1\n");
	#endif
	pp_ua[0] = compute_uc_trace(pp_ecg_ua[0]);

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("UA PROCESSING CHANNEL 2\n");
	#endif
	pp_ua[1] = compute_uc_trace(pp_ecg_ua[1]);

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("UA PROCESSING CHANNEL 3\n");
	#endif
	pp_ua[2] = compute_uc_trace(pp_ecg_ua[2]);

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("UA PROCESSING CHANNEL 4\n");
	#endif
	pp_ua[3] = compute_uc_trace(pp_ecg_ua[3]);

	#ifdef FHR_APP_LOG
	uatrace_write(pp_ua[0], str_concat(p_path_base, "uatrace_ch_1.txt"));
	uatrace_write(pp_ua[1], str_concat(p_path_base, "uatrace_ch_2.txt"));
	uatrace_write(pp_ua[2], str_concat(p_path_base, "uatrace_ch_3.txt"));
	uatrace_write(pp_ua[3], str_concat(p_path_base, "uatrace_ch_4.txt"));
	#endif
	// CALL UA COMPUTATION HERE 
	double_vec *pp_uat = double_vec_new(pp_ftrace[G_RENDER_UCH_ID]->len);
	unsigned int uaoffset = floor( (pp_uat->len - pp_ua[G_RENDER_UCH_ID]->p_uc->len) / 2); 

	for (unsigned int i = 0; i < pp_ua[G_RENDER_UCH_ID]->p_scaleuc->len; i++)
	{
		pp_uat->p_data[i + uaoffset] = pp_ua[G_RENDER_UCH_ID]->p_scaleuc->p_data[i];
	}

	//=============================PLPLOT RENDER IMAGE==========================
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("PLPLOT RENDER IMAGE:\n");
	#endif

	// TRACE SPEED 1CM/MINUTE
	int renstate_1 = render_img(1, pp_ftrace[G_RENDER_FCH_ID], pp_mtrace[3], 
	pp_baseline[G_RENDER_FCH_ID], pp_uat, str_concat(p_path_base, "trace_1cm"));

	// TRACE SPEED 2CM/MINUTE
	int renstate_2 = render_img(2, pp_ftrace[G_RENDER_FCH_ID], pp_mtrace[3], 
	pp_baseline[G_RENDER_FCH_ID], pp_uat, str_concat(p_path_base, "trace_2cm"));

	// TRACE SPEED 3CM/MINUTE
	int renstate_3 = render_img(3, pp_ftrace[G_RENDER_FCH_ID], pp_mtrace[3], 
	pp_baseline[G_RENDER_FCH_ID], pp_uat, str_concat(p_path_base, "trace_3cm"));

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("RENDER IMAGE STATE: %d %d %d\n", renstate_1, renstate_2, renstate_3);
	#endif

	//=============================FREE MEM=====================================
	// FREE RAW ECG FHR 
	double_vec_delete(pp_ecg[0]);
	double_vec_delete(pp_ecg[1]);
	double_vec_delete(pp_ecg[2]);
	double_vec_delete(pp_ecg[3]);

	// FREE RAW ECG UA 
	double_vec_delete(pp_ecg_ua[0]);
	double_vec_delete(pp_ecg_ua[1]);
	double_vec_delete(pp_ecg_ua[2]);
	double_vec_delete(pp_ecg_ua[3]);

	// FREE PREPROCESSING OUTPUT 

	// FREE DETECTION OUTPUT
	double_vec_delete(p_feature->pp_tkeo[0]);
	double_vec_delete(p_feature->pp_tkeo[1]);
	double_vec_delete(p_feature->pp_tkeo[2]);
	double_vec_delete(p_feature->pp_tkeo[3]);
	double_vec_delete(p_feature->pp_iprod[0]);
	double_vec_delete(p_feature->pp_iprod[1]);
	double_vec_delete(p_feature->pp_iprod[2]);
	double_vec_delete(p_feature->pp_iprod[3]);
	free(p_feature); 

	// FREE CLASSIFICATION OUTPUT 
	double_vec_delete(pp_iprod[0]);
	double_vec_delete(pp_iprod[1]);
	double_vec_delete(pp_iprod[2]);
	double_vec_delete(pp_iprod[3]);

	delete_peak_vec(p_mfpeak->p_mpeak[0]);
	delete_peak_vec(p_mfpeak->p_mpeak[1]);
	delete_peak_vec(p_mfpeak->p_mpeak[2]);
	delete_peak_vec(p_mfpeak->p_mpeak[3]);

	delete_peak_vec(p_mfpeak->p_fpeak[0]);
	delete_peak_vec(p_mfpeak->p_fpeak[1]);
	delete_peak_vec(p_mfpeak->p_fpeak[2]);
	delete_peak_vec(p_mfpeak->p_fpeak[3]);

	#ifdef FHR_APP_LOG
	peaklog_vec_delete(pp_mpeaklog[0]);
	peaklog_vec_delete(pp_mpeaklog[1]);
	peaklog_vec_delete(pp_mpeaklog[2]);
	peaklog_vec_delete(pp_mpeaklog[3]);
	peaklog_vec_delete(pp_fpeaklog[0]);
	peaklog_vec_delete(pp_fpeaklog[1]);
	peaklog_vec_delete(pp_fpeaklog[2]);
	peaklog_vec_delete(pp_fpeaklog[3]);
	#endif

	// HEART RATE CALCULATION 
	rpeak_vec_delete(pp_mheartrate[0]);
	rpeak_vec_delete(pp_mheartrate[1]);
	rpeak_vec_delete(pp_mheartrate[2]);
	rpeak_vec_delete(pp_mheartrate[3]);

	rpeak_vec_delete(pp_fheartrate[0]);
	rpeak_vec_delete(pp_fheartrate[1]);
	rpeak_vec_delete(pp_fheartrate[2]);
	rpeak_vec_delete(pp_fheartrate[3]);

	// FREE BASELINE OUTPUT 
	double_vec_delete(pp_ftrace[0]);
	double_vec_delete(pp_ftrace[1]);
	double_vec_delete(pp_ftrace[2]);
	double_vec_delete(pp_ftrace[3]);
	double_vec_delete(pp_ftrace[4]);

	double_vec_delete(pp_mtrace[0]);
	double_vec_delete(pp_mtrace[1]);
	double_vec_delete(pp_mtrace[2]);
	double_vec_delete(pp_mtrace[3]);
	double_vec_delete(pp_mtrace[4]);

	double_vec_delete(pp_baseline[0]->p_base);
	double_vec_delete(pp_baseline[1]->p_base);
	double_vec_delete(pp_baseline[2]->p_base);
	double_vec_delete(pp_baseline[3]->p_base);
	double_vec_delete(pp_baseline[4]->p_base);

	double_vec_delete(pp_baseline[0]->p_fast);
	double_vec_delete(pp_baseline[1]->p_fast);
	double_vec_delete(pp_baseline[2]->p_fast);
	double_vec_delete(pp_baseline[3]->p_fast);
	double_vec_delete(pp_baseline[4]->p_fast);

	double_vec_delete(pp_baseline[0]->p_slow);
	double_vec_delete(pp_baseline[1]->p_slow);
	double_vec_delete(pp_baseline[2]->p_slow);
	double_vec_delete(pp_baseline[3]->p_slow);
	double_vec_delete(pp_baseline[4]->p_slow);

	double_vec_delete(pp_baseline[0]->p_diff);
	double_vec_delete(pp_baseline[1]->p_diff);
	double_vec_delete(pp_baseline[2]->p_diff);
	double_vec_delete(pp_baseline[3]->p_diff);
	double_vec_delete(pp_baseline[4]->p_diff);

	ad_point_vec_delete(pp_baseline[0]->p_ad);
	ad_point_vec_delete(pp_baseline[1]->p_ad);
	ad_point_vec_delete(pp_baseline[2]->p_ad);
	ad_point_vec_delete(pp_baseline[3]->p_ad);

	// FREE UA PLOT 
	double_vec_delete(pp_uat);

	// FREE STRING 
	#ifdef FHR_LOG_DIR
	free(p_preproc_logfile);
	free(p_tkeo_logpath);
	free(p_iprod_logpath);
	#endif 

	// FREE PLOT OUTPUT 
}
}


void test_findpeaks()
{

	// INPUT
	char *p_path_base   = "./../test/data/nuh_1029/";
	double peak_thresh       = 3.0; 
	int dmin            = 10; 
	unsigned int siglen = 249949; 
	unsigned int wlen   = 5000; 

	// 
	double_vec *pp_tkeo[FHR_NCHANNEL];
	pp_tkeo[0] = double_vec_new(siglen);
	pp_tkeo[1] = double_vec_new(siglen);
	pp_tkeo[2] = double_vec_new(siglen);
	pp_tkeo[3] = double_vec_new(siglen);

	read_from_txt_cols(str_concat(p_path_base, "tkeo_rmv_mpeak.txt"), pp_tkeo, FHR_NCHANNEL);
	// peak_vec *p_peak = find_peak_ecg(pp_tkeo[1], wlen, dmin, peak_thresh);
	// peak_vec_write(str_concat(p_path_base, "fpeak_tkeo_test.txt"), p_peak);


	//
	peak_vec *p_peak_ecg = new_peak_vec(0);    		// final output 
	peak_vec *p_peak_win = new_peak_vec(0); 		// peak vec of a window 
	double_vec *p_ecg_win = double_vec_new(wlen);   // a window of ecg 
	int nwin = (int)floor(siglen / wlen);		// number of window 

	// paramters for findpeak 
	double std, sel = 0.0; 

	for(int i = 0; i < nwin; i++)
	{
		// extract tempwin
		double_vec_get_chunk(pp_tkeo[1], p_ecg_win, i * wlen);

		//  normalize
		normalize(p_ecg_win);

		// parameters for findpeaks 
		std = find_std(p_ecg_win);

		// findpeaks of a window ecg 
		peak_vec *p_peak_win = find_peaks(p_ecg_win, sel, std * peak_thresh, dmin); 			

		#ifdef FHR_FCLASS_DEBUG
		printf("std = %3.15f window = %d has %ul peak\n", std, i, 
		p_peak_win->len);
		#endif 

		if (i==29)
		{
			print_peak_vec(p_peak_win, p_peak_win->len);
		}

		// shift peak loc due to doing window by window find peak 
		shift_peak_loc(p_peak_win, (int)(i * wlen));

		// concate to p_peak_ecg 
		concate_peak_vec(p_peak_ecg, p_peak_win);

	}

	 peak_vec_write(str_concat(p_path_base, "fpeak_tkeo_test.txt"), p_peak_ecg);

	 //

}

void test_findpeaks_1 ()
{
	char *p_path_base = "./../test/data/nuh_1029/";
	int dmin = 10; 
	double peak_thresh = 3.0;
	double sel = 0; 
	

	unsigned int siglen = 5000; 
	double_vec *p = double_vec_new(siglen);
	double_vec_read(p, "./../test/data/nuh_1029/test_find_peak_in.txt");

	printf("INPUT\n");
	double_vec_print(p, 10);

	double std = find_std(p);

	printf("findpeaks\n");

	peak_vec *p_peak_win = find_peaks(p, sel, std * peak_thresh, dmin); 

	print_peak_vec(p_peak_win, p_peak_win->len);

	peak_vec_write(str_concat(p_path_base, "test_findpeaks_out.txt"), p_peak_win);
}


int main(void)
{
   UNITY_BEGIN();

   RUN_TEST(test_main);

   // test_findpeaks();

   // test_findpeaks_1();

   return 0;
}


