#include <stdio.h>
#include "fhr_ua.h"
#include "biorithm_aux.h"
#include "unity.h"

// STATIC GLOBAL VARIABLE 
static const struct param_stft g_param_stft = {FHR_UA_SQI_WLEN, FHR_UA_SQI_NSHIFT, 
	         FHR_UA_MIN_FREQ, FHR_UA_MAX_FREQ,FHR_UA_HI_MIN_FREQ,
	         FHR_UA_HI_MAX_FREQ, SAMPLING_FREQUENCY};


static const struct param_wuc g_param_wuc = {FHR_UA_WNAME, FHR_UA_NLAYER, 
	         FHR_UA_WFIRST_LAYER, FHR_UA_WLAST_LAYER, FHR_UA_WCHUNK};

static const struct param_tden g_param_tden = {FHR_UA_TDENOISE_SQI_WLEN,
	         FHR_UA_TDENOISE_IMP_WLEN, FHR_UA_TDENOISE_NSHIFT, FHR_UA_MEAN_LEVEL, 
	         FHR_UA_SQI_MAD_LEVEL};


/*------------------------------------------------------------------------------
Setup: 
Description: 
------------------------------------------------------------------------------*/
void setUp(void)
{

}

/*------------------------------------------------------------------------------
Tear down:
Description: 
------------------------------------------------------------------------------*/
void tearDown(void)
{

}

/*------------------------------------------------------------------------------
Function to test: wavelib_obj *create_wavelib_obj(param_wave *p_param)
Description: 
------------------------------------------------------------------------------*/
void test_create_wavelib_object(void)
{
	// ============================ TEST CASE 1=================================
	struct param_wave wave = {32768, 12, "db4"}; // input 

	// expected output wtfilters from Matlab 
	double exp_lpd[8] = {-0.0105974, 0.0328830, 0.0308414, -0.1870348, -0.0279838, 
						0.6308808, 0.7148466, 0.2303778};

	double exp_hpd[8] = {-0.2303778, 0.7148466, -0.6308808, -0.0279838, 0.1870348, 
					  0.0308414, -0.0328830, -0.0105974};

	double exp_lpr[8] = {0.2303778, 0.7148466, 0.6308808, -0.0279838, -0.1870348, 
					 0.0308414, 0.0328830, -0.0105974};

	double exp_hpr[8] = {-0.0105974, -0.0328830, 0.0308414, 0.1870348, -0.0279838, 
					 -0.6308808, 0.7148466, -0.2303778};

	// call the function 
	struct wavelib_object *p_wave = create_wavelib_object(wave);

	// wave_summary(p_wave_1->obj);
	
	// ASSERT
	TEST_ASSERT_EQUAL_INT(32768, p_wave->wt->siglength);
	TEST_ASSERT_EQUAL_INT(12, p_wave->wt->J);
	TEST_ASSERT_EQUAL_STRING("db4", p_wave->wave->wname); 

	// ASSERT wfilters 
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(exp_lpd, p_wave->wave->lpd, 8);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(exp_hpd, p_wave->wave->hpd, 8);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(exp_lpr, p_wave->wave->lpr, 8);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(exp_hpr, p_wave->wave->hpr, 8);
}


/*------------------------------------------------------------------------------
Function to test: void delete_wavelib_obj(wavelib_obj *p_wave)
Description: 
------------------------------------------------------------------------------*/
void test_delete_wavelib_object(void)
{

}

/*------------------------------------------------------------------------------
Function to test: unsigned int get_start_idx_layer(wt_object wt, 
unsigned int layer_id)
Description: dwt output [A, DJ, ...., D1]. This function return the starting 
index of D_{layer_id}
------------------------------------------------------------------------------*/
void test_get_start_idx_layer(void)
{
	// ============================ TEST CASE 1=================================
	// create wavelib object 
	struct param_wave wave = {32768, 12, "db4"};
	struct wavelib_object *p_wave = create_wavelib_object(wave);

	// read input signal from file 
	double_vec *p = double_vec_new(32768);
	double_vec_read(p, "./../test/data/x_wave.txt");

	// dwt 
	dwt(p_wave->wt, p->p_data);

	// 
	// wt_summary(p_wave->wt);

	// expected output starting index of each layer [A,D12,D11,...,D1]
	int exp_start_idx[13] = {0, 14, 28, 50, 88, 158, 292, 554, 1072, 2102, 4156, 
							 8258, 16455}; 

	// call the function to test and assert
	TEST_ASSERT_EQUAL_INT(exp_start_idx[1], get_start_idx_layer(p_wave->wt, 12));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[2], get_start_idx_layer(p_wave->wt, 11));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[3], get_start_idx_layer(p_wave->wt, 10));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[4], get_start_idx_layer(p_wave->wt, 9));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[5], get_start_idx_layer(p_wave->wt, 8));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[6], get_start_idx_layer(p_wave->wt, 7));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[7], get_start_idx_layer(p_wave->wt, 6));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[8], get_start_idx_layer(p_wave->wt, 5));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[9], get_start_idx_layer(p_wave->wt, 4));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[10], get_start_idx_layer(p_wave->wt, 3));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[11], get_start_idx_layer(p_wave->wt, 2));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[12], get_start_idx_layer(p_wave->wt, 1));
	TEST_ASSERT_EQUAL_INT(exp_start_idx[0], get_start_idx_layer(p_wave->wt, 0));


	// ============================ TEST CASE 2=================================
}

/*------------------------------------------------------------------------------
Function to test: void wave_null_layer(wt_object wt, unsigned int s, 
unsigned int e);
Description: given input is coefficient of dwt, this function keep coefficients 
of layer s to layer e (included s and e). In other words, this function will 
null layer 0 to (s-1) and null layer e to J. 
------------------------------------------------------------------------------*/
void test_wave_null_layer(void)
{

}
/*------------------------------------------------------------------------------
Function to test: void extract_uc_from_ecg(const uc_param g_param_wuc, 
double_vec *p_ecg)
Description: this function extract uterine contraction signal band (0.5-5Hz) 
from raw ECG using wavelet and procssing one shot wavelet dec/rec.
------------------------------------------------------------------------------*/
void test_extract_uc_from_ecg(void)
{
	// ============================ TEST CASE 1=================================

	// read data for test 
	unsigned int N = 100000;	
	double_vec *p_ecg = double_vec_new(N);
	// double_vec_read(p_ecg, "./../data/x_wave.txt");

	// call the function extract uc 
	// extract_uc_from_ecg(g_param_wuc, p_ecg);

	// read expected output computed by matlab 
	double_vec *p_exp = double_vec_new(N);
	// double_vec_read(p_exp, "./../data/x_rec_matlab_unity.txt");

	// write actual output to file 
	// double_vec_write(p_ecg, "./../data/x_rec_c_unity.txt");

	// assert 
	// TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp->p_data, p_ecg->p_data, N);?

	// free mem 
	// double_vec_delete(p_ecg);
	// double_vec_delete(p_exp);

	// ============================ TEST CASE 2=================================
	// input ice001
	double_vec_read(p_ecg, "./../test/data/ice1001.txt");

	// call function to extract uc 
	extract_uc_from_ecg(g_param_wuc, p_ecg);

	// write output to file 
	double_vec_write(p_ecg, "./../test/data/ice1001_extract_uc_from_uce_c.txt");

	// read expected output from Matlab 
	double_vec_read(p_exp, "./../test/data/ice001_extract_uc_from_uce_matlab.txt");

	// assert 
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp->p_data, p_ecg->p_data, N);

}

/*------------------------------------------------------------------------------
Function to test: void extract_uc_from_ecg_chunk(const uc_param g_param_wuc, 
double_vec *p_ecg);
Description: this function extract uterine contractoin signal band (0.5 - 5Hz)
from raw ECG by using wavelet and processing chunk by chunk 
------------------------------------------------------------------------------*/
void test_extract_uc_from_ecg_chunk(void)
{

}

/*------------------------------------------------------------------------------
Function to test: void null_outlier(double_vec *p_chunk, double level);
Description: null outlier in time domain by comparing with mean
------------------------------------------------------------------------------*/
void test_null_outlier(void)
{
	// ============================ TEST CASE 1=================================
    // input 
    double level = 4.0; 
	double_vec *p_d = double_vec_new(5);
	double_vec_set(p_d, 0, 5.0);
	double_vec_set(p_d, 1, 0.0);
	double_vec_set(p_d, 2, 110.0);
	double_vec_set(p_d, 3, 1.0);
	double_vec_set(p_d, 4, 2.0);

    // expected output 
    double_vec *p_exp = double_vec_new(5); 
    double_vec_set_chunk(p_exp, p_d, 0);
    p_exp->p_data[2] = 0.0; 
    double_vec_print(p_exp,5);

    // actual output 
    null_outlier(p_d, level);
    double_vec_print(p_d,5);

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp, p_d, 5);

    // free me 
    double_vec_delete(p_d);
    double_vec_delete(p_exp);
    p_d = NULL;
    p_exp = NULL;
}

/*------------------------------------------------------------------------------
Function to test: void rmv_spike(const tden_param *p_param, double_vec *p_sig);
Description: rmv spikes from signal after wavelet 
------------------------------------------------------------------------------*/
void test_rmv_spike(void)
{

}

/*------------------------------------------------------------------------------
Function to test: void fft_real_chunk(double_vec *p_re_in, double_vec *p_cm_out);
Description: fft a chunk of real signal 
------------------------------------------------------------------------------*/
void test_fft_real_chunk(void)
{
	// ============================ TEST CASE 1=================================
    // input 
    unsigned int fftlen = 2048;
    double_vec *p_x = double_vec_new(fftlen);
    double_vec_read(p_x, "./../test/data/test_fft_in.txt");

    // expected output computed by FFT Matlab 
    double_vec *p_exp = double_vec_new(2 * fftlen);
    double_vec_read(p_exp, "./../test/data/test_fft_out.txt");

    // actual output 
    double_vec *p_act = double_vec_new(2 * fftlen);
    fft_real_chunk(p_x, p_act);

    // assert  
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp->p_data, p_act->p_data, 2 * fftlen);

}

/*------------------------------------------------------------------------------
Function to test: double fft_to_power(double_vec *p_fft, double fs, double fmin, 
double fmax);
Description: this function compute sum of magnitude of FFT output within a range 
from fmin to fmax. 
------------------------------------------------------------------------------*/
void test_fft_to_power(void)
{
	// ============================ TEST CASE 1=================================
	// input 
	unsigned int fftlen = 8192;
	double_vec *p = double_vec_new(fftlen * 2);
	double_vec_read(p, "./../test/data/test_fft_to_power_in.txt");

	// expected output 
	double exp_sqi = 18416600817.9206695557; 

	// actual output 
    double act_sqi = fft_to_power(p,g_param_stft.sampling_freq,
                          g_param_stft.hi_min_freq,g_param_stft.hi_max_freq);

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE(exp_sqi, act_sqi);

}

/*------------------------------------------------------------------------------
Function to test: double fft_to_uce(double_vec *p_fft, double fs, double fmin, 
double fmax);
Description: this function compute uterine contraction energy from FFT output
------------------------------------------------------------------------------*/
void test_fft_to_uce(void)
{
	// ============================ TEST CASE 1=================================
	// input 
	double_vec *p = double_vec_new(8192*2);
	double_vec_read(p, "./../test/data/fft_out_one_window_m.txt");

	// expected output 
	double exp_uce = 457855168894.459838867187500;

	// actual output 
    double act_uce = fft_to_uce(p,g_param_stft.sampling_freq,
                          g_param_stft.uc_min_freq,g_param_stft.uc_max_freq);

	// assert 
	TEST_ASSERT_EQUAL_DOUBLE(exp_uce, act_uce);

}

/*------------------------------------------------------------------------------
Function to test: point_vec *compute_uc_sqi(const stft_param g_param_stft, 
double_vec *p_ecg);
Description: this function return a sqi vector, each sqi is coressponding to 
a window of raw ECG signal. Windows are overlapping. 
------------------------------------------------------------------------------*/
void test_compute_uc_sqi(void)
{
	// ============================ TEST CASE 1=================================
	// input 
	double_vec *p_ecg = double_vec_new(100000);
	double_vec_read(p_ecg, "./../test/data/ice1001.txt");
	double_vec *p_uc_sqi = NULL;
	
	// expected sqi from Matlab 
	double_vec *p_exp = double_vec_new(180);
	double_vec_read(p_exp, "./../test/data/ice001_sqi_m.txt");

	// actual sqi 
	p_uc_sqi = compute_uce_sqi_stft(g_param_stft, p_ecg);
	double_vec *p_act = double_vec_new(p_uc_sqi->len / 2);

	for (unsigned int i = 0; i < p_uc_sqi->len / 2; i++)
	{
		p_act->p_data[i] = (p_uc_sqi->p_data[2 * i + 1]);
	}

	// write output to file 
	double_vec_write(p_act, "./../test/data/ice001_sqi_raw_c.txt");

	// assert 
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp->p_data, p_act->p_data, 180);

	// // free mem 
	double_vec_delete(p_ecg);
	double_vec_delete(p_uc_sqi);
	double_vec_delete(p_act);

	// ============================ TEST CASE 2=================================
	// // input 
	// p_ecg = double_vec_new(989013);
	// double_vec_read(p_ecg, "./../test/data/x_1005.txt");

	// // expected sqi from Matlab 
	// p_exp = double_vec_new(1916);
	// // double_vec_read(p_exp, "./../data/ice001_sqi_m.txt");
	// double_vec_read(p_exp, "./../test/data/1005_sqi_m.txt");

	// // actual sqi 
	// p_uc_sqi = compute_uce_sqi_stft(g_param_stft, p_ecg);
	// p_act = double_vec_new(p_uc_sqi->len / 2);
	// for (unsigned int i = 0; i < p_uc_sqi->len / 2; i++)
	// {
	// 	p_act->p_data[i] = (p_uc_sqi->p_data[2 * i + 1]);
	// }

	// // write output to file 
	// double_vec_write(p_act, "./../test/data/1005_sqi_c.txt");

	// // assert 
	// TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp->p_data, p_act->p_data, 1916);

	// // free mem 
	// double_vec_delete(p_uc_sqi);
	// double_vec_delete(p_ecg);
	// double_vec_delete(p_exp);
	// double_vec_delete(p_act);

}

/*------------------------------------------------------------------------------
Function to test: void rmv_low_sqi_window(double_vec *p_ecg, point_vec *p_uc_sqi);
Description: this function remove low sqi window signal 
------------------------------------------------------------------------------*/
void test_rmv_low_sqi_window(void)
{
	// ============================ TEST CASE 1=================================
}


/*------------------------------------------------------------------------------
Function to test: point_vec *compute_uc_trace(double_vec *p_ecg);
Description: this function copute uterine contraction trace 
------------------------------------------------------------------------------*/
void test_compute_uc_trace(void)
{
	// // ============================ TEST CASE 1=================================
	// input ice001 
	unsigned int N = 100000;
	double_vec *p_ecg = double_vec_new(N);
	double_vec_read(p_ecg, "./../test/data/ice1001.txt");
	
	// actual output 
	struct uatrace *pp_ua = malloc(sizeof(struct uatrace));
	pp_ua = compute_uc_trace(p_ecg);

	// write result to file 
	double_vec_write(pp_ua->p_sqi, "./../test/data/ice001_trace_sqi_c.txt");
	double_vec_write(pp_ua->p_uc,  "./../test/data/ice001_trace_uc_c.txt");
	double_vec_write(pp_ua->p_scaleuc,  "./../test/data/ice001_trace_scaleuc_c.txt");
	
	// expected output from Matlab 
	double_vec *p_exp_uc = double_vec_new(180);
	double_vec *p_exp_sqi = double_vec_new(180);
	double_vec_read(p_exp_uc, "./../test/data/ice001_uc_trace_matlab.txt");

	// assert 
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp_uc->p_data, pp_ua->p_uc->p_data, 180);

	// // free mem 
	double_vec_delete(pp_ua->p_sqi);
	double_vec_delete(pp_ua->p_uc);
	double_vec_delete(pp_ua->p_scaleuc);
	free(pp_ua);
	double_vec_delete(p_ecg);
	double_vec_delete(p_exp_uc);
	double_vec_delete(p_exp_sqi);

	// // ============================ TEST CASE 2=================================
	// // input ice001 
	// // init 
	// N = 989013;
	// // double_vec *p_ecg = NULL; 
	// // double_vec *p_trace = NULL;
	// // double_vec *p_uc = NULL;
	// // double_vec *p_sqi = NULL; 
	// // double_vec *p_exp_uc = NULL;

	
	// p_ecg = double_vec_new(N);
	// double_vec_read(p_ecg, "./../test/data/x_1005.txt");

	// // actual output 
	// p_trace = compute_uc_trace(p_ecg);
	// p_uc = double_vec_new((unsigned int)(p_trace->len /2));
	// p_sqi = double_vec_new((unsigned int)(p_trace->len /2));

	// for (unsigned int i = 0; i < p_trace->len / 2; i++)
	// {
	// 	p_sqi->p_data[i] = (p_trace->p_data[2 * i + 1]);
	// 	p_uc->p_data[i]  = (p_trace->p_data[2 * i]);
	// }

	// // write result to file 
	// double_vec_write(p_sqi, "./../test/data/1005_trace_sqi_c.txt");
	// double_vec_write(p_uc,  "./../test/data/1005_trace_uc_c.txt");
	
	// // // expected output from Matlab 
	// p_exp_uc = double_vec_new(1916);
	// double_vec_read(p_exp_uc, "./../test/data/1005_uc_trace_matlab.txt");

	// // // assert 
	// TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp_uc->p_data, p_uc->p_data, 1916);

	// // free mem 
	// double_vec_delete(p_sqi);
	// double_vec_delete(p_uc);
	// double_vec_delete(p_ecg);
	// // double_vec_delete(p_trace);
	// // double_vec_delete(p_exp_uc);
	// // double_vec_delete(p_exp_sqi);

}


int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_create_wavelib_object);
    RUN_TEST(test_get_start_idx_layer);
	RUN_TEST(test_extract_uc_from_ecg);
	// RUN_TEST(test_null_outlier);
	// RUN_TEST(test_fft_real_chunk);
	RUN_TEST(test_fft_to_power);
	RUN_TEST(test_fft_to_uce);
	RUN_TEST(test_compute_uc_sqi);
	RUN_TEST(test_compute_uc_trace);

    return 0;
}


// gcc test_fhr_ua_unity.c ./unity/unity.c ./../src/fhr_ua.c ./../src/biorithm_aux.c -lm -lgsl -lgslcblas -lwavelib -I /usr/local/include/gsl/