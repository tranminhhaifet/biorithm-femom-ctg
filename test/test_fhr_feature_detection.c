// SYSTEM INCLUDE 
#include <stdio.h>
#include <stdlib.h>

// LOCAL INCLUDE FHR UNIT 
#include "biorithm_aux.h"
#include "fhr_feature_detection.h"

// LOCAL INCLUDE UNITY TEST 
#include "unity.h"


// DEFINE DEBUG AND PRINT LEVEL 
// #define PRINT_COMPARE
// #define TEST_FDETECT_PRINT 

void setUp(void)
{

}

void tearDown()
{

}

/*------------------------------------------------------------------------------
* Function Name: test_compute_hilbert_irf 
* Description: test computing Hilbert impulse response function by compare 
* with pre-computed result in Matlab. 
*-----------------------------------------------------------------------------*/ 
void test_compute_hilbert_irf()
{
    double_vec *p_actual = NULL;
    p_actual = compute_hilbert_irf(FHR_HT_KAISER_WLEN, FHR_HT_KAISER_BETA);

    // load expected output from file 
    double_vec *p_expected = double_vec_new(FHR_HT_KAISER_WLEN);
    read_from_txt("./../test/data/hilbert_irf_matlab.txt", p_expected);

    // print compare result to screen 
    #ifdef TEST_FDETECT_PRINT
    for (unsigned int i = 0; i < 10; i++)
    {
        printf("i = %ul, irf_c = %3.15f, irf_m = %3.15f, err = %1.15f \n", 
            i, p_actual->p_data[i], p_expected->p_data[i],
            p_expected->p_data[i] - p_expected->p_data[i]);
    }
    #endif 

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_expected->p_data, p_actual->p_data, 
        p_actual->len);

    // free mem 
    double_vec_delete(p_actual);
    double_vec_delete(p_expected);

}

/*------------------------------------------------------------------------------
* Function Name: test_ht_filter 
* Description: test Hilbert transform by output with analytic signal pre-computed
* int Matlab. 
*-----------------------------------------------------------------------------*/ 
void test_ht_filter()
{
    // init 
    unsigned int siglen = 32768;
    double_vec *p_x = double_vec_new(siglen);
    double_vec *p_actual = NULL;
    double_vec *p_expected = double_vec_new(siglen * 2);

    // load input for Hilbert transform 
    read_from_txt("./../test/data/ht_in.txt", p_x);

    // perform Hilbert transform 
    p_actual = ht_filter(p_x, FHR_HT_KAISER_WLEN, FHR_HT_KAISER_BETA);

    // load matlab output to compare 
    read_from_txt("./../test/data/ht_out_matlab.txt", p_expected);

    // print compare to console 
    #ifdef PRINT_COMPARE 
    for (unsigned int i = 0; i < siglen * 2; i++)
    {
        printf("i=%ul, yc_real=%3.15f, ym_real=%3.15f, err=%1.15f\n", 
            i, p_actual->p_data[i], p_expected->p_data[i],
            p_actual->p_data[i] - p_expected->p_data[i]);
    }
    #endif

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_actual->p_data, p_expected->p_data, 
        p_actual->len);

    // free mem
    double_vec_delete(p_x);
    double_vec_delete(p_actual);
    double_vec_delete(p_expected);

}

void test_compute_distpts()
{
    // init 
    unsigned int siglen = 32768; 

    // load input 
    double_vec *p_x = double_vec_new(siglen * 2); 
    
    read_from_txt("./../test/data/dispt_in.txt", p_x);

    // compute dist in c
    double_vec *p_actual = compute_distpts(p_x);

    // write output to file 
    double_vec_write(p_actual, "./../test/data/dispt_out_c.txt");

    // load dist from matlab or expected output 
    double_vec *p_expected = double_vec_new((siglen - 1) * 2);

    read_from_txt("./../test/data/dispt_out_matlab.txt", p_expected);

    // compare 
    #ifdef PRINT_COMPARE
    for (unsigned int i = 0; i < 1023*2; i++)
    {
        printf("i=%ul, pdist_c=%3.15f, pdist_m=%3.15f, error=%1.15f\n", 
            i, p_actual->p_data[i], p_expected->p_data[i], 
            p_actual->p_data[i] - p_expected->p_data[i]);
    }
    #endif

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_actual->p_data, p_expected->p_data,
        p_actual->len);

    // free mem
    double_vec_delete(p_x);
    double_vec_delete(p_actual);
    double_vec_delete(p_expected);

}

void test_compute_tkeo()
{
    // load input 
    unsigned int siglen = 32767;
    unsigned int tkwin = 25; 

    double_vec *p_x = double_vec_new(siglen);
    double_vec *p_cx = double_vec_new(2 * siglen); 

    read_from_txt("./../test/data/tkeo_in.txt", p_x);
    for (unsigned int i = 0; i < siglen; i++)
    {
        p_cx->p_data[2*i] = p_x->p_data[i];
        p_cx->p_data[2*i+1] = 0.0;
    }

    // compute tkeo 
    double_vec *p_actual = NULL; 
    p_actual = compute_tkeo(p_cx, tkwin);

    // write output to file 
    double_vec_write(p_actual, "./../test/data/tkeo_out_c.txt");

    // load matlab output 
    double_vec *p_expected = double_vec_new(siglen - 2 * tkwin);

    read_from_txt("./../test/data/tkeo_out_matlab.txt", p_expected);

    // compare 
    #ifdef TEST_FDETECT_PRINT
    for (unsigned int i = 0; i < siglen-2*tkwin; i++) 
    {
        printf("i=%ul, tkeo_c=%3.25f, tkeo_m=%3.25f, err=%1.25f\n", i, 
            p_actual->p_data[i], p_expected->p_data[i], 
            p_actual->p_data[i] - p_expected->p_data[i]);
    }
    #endif

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_actual->p_data, p_expected->p_data,
    p_actual->len);

    // free mem
    double_vec_delete(p_x);
    double_vec_delete(p_actual);
    double_vec_delete(p_expected);

}


void test_compute_iprod()
{
    // load input 
    unsigned int siglen = 32767; 
    unsigned int ipwin  = 11; 

    double_vec *p_x = double_vec_new(siglen * 2);

    read_from_txt("./../test/data/iprod_in.txt", p_x);

    // compute iprod in c 
    double_vec *p_actual = NULL;
    p_actual = compute_iprod(p_x, ipwin);

    #ifdef PRINT_COMPARE
    printf("p_actual length = %lu\n", p_actual->len);
    #endif

    // write output to file 
    double_vec_write(p_actual, "./../test/data/iprod_out_c.txt");

    // load matlab expected 
    double_vec *p_expected = double_vec_new(siglen - ipwin + 1);

    read_from_txt("./../test/data/iprod_out_matlab.txt", p_expected);

    // compare 
    #ifdef TEST_FDETECT_PRINT
    for (unsigned int i = 0; i < siglen - ipwin + 1; i++) 
    {
        printf("i=%ul, iprod_c=%3.15f, iprod_m=%3.15f, err=%1.15f\n", i, 
            p_actual->p_data[i], p_expected->p_data[i], 
            p_actual->p_data[i] - p_expected->p_data[i]);
    }
    #endif

    // assert 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_actual->p_data, p_expected->p_data,
    p_actual->len);

    // free mem
    double_vec_delete(p_x);
    double_vec_delete(p_actual);
    double_vec_delete(p_expected);

}


void test_detect_fhr_feature()
{
    // init input output and parameters 
    unsigned int ecglen   = 32768;
    unsigned int divlen   = ecglen - 1; 
    unsigned int tkeolen  = divlen - 2 * FHR_TKWIN; 
    unsigned int iprodlen = divlen - FHR_IPWIN + 1; 

    double_vec *p_ecg[FHR_NCHANNEL];
    p_ecg[0] = double_vec_new(ecglen);
    p_ecg[1] = double_vec_new(ecglen);
    p_ecg[2] = double_vec_new(ecglen);
    p_ecg[3] = double_vec_new(ecglen);

    // load input 
    read_from_txt_cols("./../test/data/fhr_detection_ecg_in.txt",p_ecg,FHR_NCHANNEL);
    
    // expected output 
    double_vec *p_exp_tkeo[FHR_NCHANNEL];
    double_vec *p_exp_iprod[FHR_NCHANNEL];
    p_exp_tkeo[0]  = double_vec_new(tkeolen);
    p_exp_tkeo[1]  = double_vec_new(tkeolen);
    p_exp_tkeo[2]  = double_vec_new(tkeolen);
    p_exp_tkeo[3]  = double_vec_new(tkeolen);
    p_exp_iprod[0] = double_vec_new(iprodlen);
    p_exp_iprod[1] = double_vec_new(iprodlen);
    p_exp_iprod[2] = double_vec_new(iprodlen);
    p_exp_iprod[3] = double_vec_new(iprodlen);

    read_from_txt_cols("./../test/data/fhr_detection_tkeo_out_matlab.txt",p_exp_tkeo,FHR_NCHANNEL);
    read_from_txt_cols("./../test/data/fhr_detection_iprod_out_matlab.txt",p_exp_iprod,FHR_NCHANNEL);


    #ifdef TEST_FDETECT_PRINT
    for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
    {
        printf("ecg channel %d, length = %zu\n", i, p_ecg[i]->len);
        double_vec_print(p_ecg[i],10);
    }
    #endif

    struct fhrfeature *p_feature = detect_fhr_feature(p_ecg);

    // print actual output 
    #ifdef TEST_FDETECT_PRINT
    for (unsigned int i = 0; i <  10; i++)
    {
        printf("act_tkeo = %3.15f, exp_tkeo = %3.15f, error=%3.15f\n", 
        p_feature->pp_tkeo[0]->p_data[i], p_exp_tkeo[0]->p_data[i],
        p_feature->pp_tkeo[0]->p_data[i] - p_exp_tkeo[0]->p_data[i]);
    }
    #endif

    #ifdef TEST_FDETECT_PRINT
    for (unsigned int i = 0; i < 10; i++)
    {
        printf("act_iprod = %3.15f, exp_iprod = %3.15f, error=%3.15f\n", 
        p_feature->pp_iprod[0]->p_data[i], p_exp_iprod[0]->p_data[i],
        p_feature->pp_iprod[0]->p_data[i] - p_exp_iprod[0]->p_data[i]);
    }
    #endif

    // assert 
    for (int i = 0; i < FHR_NCHANNEL; i++)
    {
        TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp_tkeo[i]->p_data, 
            p_feature->pp_tkeo[i]->p_data, tkeolen);    

        TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_exp_iprod[i]->p_data, 
            p_feature->pp_iprod[i]->p_data, iprodlen);    
    }
}


int main(void)
{
   UNITY_BEGIN();
   RUN_TEST(test_compute_hilbert_irf);
   RUN_TEST(test_ht_filter);
   RUN_TEST(test_compute_distpts);
   RUN_TEST(test_compute_tkeo);
   RUN_TEST(test_compute_iprod);
   RUN_TEST(test_detect_fhr_feature);

   return 0;
}


//gcc test_fhr_feature_detection.c ./../fhr/biorithm_aux.c ./../fhr/fhr_feature_detection.c ./unity/unity.c -lm -lgsl -lgslcblas