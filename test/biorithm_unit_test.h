/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_unit_test.h
* Original Author: TRAN MINH HAI
* Date created: 26 SEP 2019
* Purpose: support for unit test 
*
*
* Revision History:
* Ref No      Date           Who          	Detail
*             26 SEP 2019    TRAN MINH HAI           
********************************************************************************/
// SYSTEM INCLUDE 

// LOCAL INCLUDE 
#include "biorithm_aux.h"
#include "fhr_findpeaks.h"

#define FHR_FPK_PRECISION 0.001f 


// 
#define BIORITHM_RUN_TEST(test_function) 									\
{																			\
	printf(":%d %s: ",__LINE__, #test_function);							\
	test_function(); 														\
}																			\



/*------------------------------------------------------------------------------
Function name:
Description  : 
------------------------------------------------------------------------------*/
int compare_equal_peak(const peak *p1, const peak *p2);

/*------------------------------------------------------------------------------
Function name: TEST_ASSERT_EQUAL_PEAK_ARRAY(const *peak p1, const *peak p2, 
unsigned int len)
Description: 
------------------------------------------------------------------------------*/
void TEST_ASSERT_EQUAL_PEAK_ARRAY(peak *p1, peak *p2, unsigned int len);

