
/*********************************************************************************
*      Copyright ©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: acc_test.c
* Original Author: HAI TRAN
* Date created: <15 AUG 2019>
* Purpose: test all functions at all levels
*
* Revision History:
* Ref No      Date           Who          Detail
* <task ID>   <originated>   <originator> Original Code
* <task ID>   <DD MMM YYYY>  <initials>   <fix/change description>
*********************************************************************************/
// SYSTEM INCLUDES
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>

// LOCAL INCLUDES
#include "biorithm_aux.h"
#include "fhr_baseline.h"

// LOCAL INCLUDES 
#include "unity.h"

// PRINT MODE  
#define TEST_FHR_BASELINE_PRINT
#define TEST_FHR_BASELINE_LOG 

/*------------------------------------------------------------------------------
Setup: 
Description: 
------------------------------------------------------------------------------*/
void setUp(void)
{

}

/*------------------------------------------------------------------------------
Tear down:
Description: 
------------------------------------------------------------------------------*/
void tearDown(void)
{

}


void test_find_outlier()
{
    // INPUT
    unsigned int siglen = 10; 
    double_vec *p_mark = NULL;
    double_vec *p_val = double_vec_new(siglen);

    // SET OUTLTIER 
    p_val->p_data[0] = 150.0;
    #ifdef TEST_FHR_BASELINE_PRINT
    printf("input with outlier\n");
    double_vec_print(p_val,siglen);
    #endif

    // FIND OUTLIER 
    p_mark = find_outlier(p_val);
    #ifdef TEST_FHR_BASELINE_PRINT
    printf("outlier location in mark vector\n");
    double_vec_print(p_mark, siglen);
    #endif

    // FREE MEM 
    double_vec_delete(p_val);
    double_vec_delete(p_mark);
}

void test_outlier()
{
    printf("is outlier = %d \n", is_fhr_outlier(100.0));
    printf("is outlier = %d \n", is_fhr_outlier(10.0));
    printf("is outlier = %d \n", is_fhr_outlier(1.0));
    printf("is outlier = %d \n", is_fhr_outlier((double)NAN));
}

void test_gausswin()
{
    // INPUT
    unsigned int wlen = 15;  
    double alpha = 2.5; 
    double_vec *p_actual = double_vec_new(wlen);
    
    // EXPECTED OUTPUT 
    double_vec *p_expected = double_vec_new(wlen);
    read_from_txt("./../test/data/gw_matlab.txt", p_expected);

    // GAUSS WINDOW DESIGN 
    gausswin(p_actual, wlen, alpha);

    // CHECK OUTPUT 
    for (unsigned int i = 0; i < p_actual->len; i++) 
    {
        printf("i = %d, actual = %3.15f, expected = %3.15f\n", i, p_actual->p_data[i], p_expected->p_data[i]);
    }

    // ASSERT 
     TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_actual->p_data, p_expected->p_data, wlen);
}


void test_smooth()
{
    // INPUT
    unsigned int siglen = 1543;
    double_vec *p_x = double_vec_new(siglen);
    read_from_txt("./../test/data/FSD_FHR_7.txt", p_x);

    // EXPECTED OUTPUT SMOOTHEA FUNCTION HANDMADE 
    double_vec *p_expected = double_vec_new(siglen);
    read_from_txt("./../test/data/FSD_FSH_7_smootheda_matlab.txt", p_expected);

    // SMOOTH 
    smooth(p_x, 61, "gaussian", 2.5);

    // PRINT OUTPUT 
    #ifdef TEST_FHR_BASELINE_PRINT
    for (unsigned int i = 0; i < 10; i++)
    {
        printf("i=%ul,actual=%3.15f,expected=%3.15f\n",i,p_x->p_data[i],p_expected->p_data[i]);
    }
    #endif 

    // WRITE C OUTPUT TO FILE 
    #ifdef TEST_FHR_BASELINE_LOG
    write_to_txt(&p_x, 1, "./../test/data/FSD_FHR_7_smoothed_c.txt");
    #endif 

    // ASSERT 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_x->p_data, p_expected->p_data, siglen);

}


void test_get_fast_fhr()
{
    double fs = 4.0;

    const char *p_testcase = "NUH_1021";

    // TEST CASE 1 FSD_FHR_7
    if (strcmp(p_testcase, "FSD_FHR_7")==0)
    {
        double_vec *p = double_vec_new(1543);
        read_from_txt("./../test/data/FSD_FHR_7.txt", p);
        get_fast_fhr(p, fs);
        write_to_txt(&p, 1, "./../test/data/FSD_FHR_7_FASTWAVE_C.txt");
        double_vec_print(p, 30);
    }

    // TEST CASE 2 FSD_FHR_8

    // TEST CASE 3 NUH_1021
    if (strcmp(p_testcase, "NUH_1021")==0)
    {
       double_vec *p = double_vec_new(12920);
       read_from_txt("./../test/data/NUH1021_FHR_CH4.txt", p);
       get_fast_fhr(p, fs);
       write_to_txt(&p, 1, "./../test/data/NUH1021_FHR_CH4_FASTWAVE_C.txt");
       double_vec_print(p, 30);
    }

}

void test_get_slow_fhr()
{
    double fs = 4.0;
    const char *p_testcase = "NUH_1021";

    if(strcmp(p_testcase, "FSD_FHR_7")==0)
    {

        double_vec *p = double_vec_new(1543);
        read_from_txt("./../test/data/FSD_FHR_7.txt", p);
        get_slow_fhr(p, fs);
        write_to_txt(&p, 1, "./../test/data/FSD_FHR_7_SLOWWAVE_C.txt");
        double_vec_print(p, 30);
    }

    if(strcmp(p_testcase, "NUH_1021")==0)
    {
        double_vec *p = double_vec_new(12920);
        read_from_txt("./../test/data/NUH1021_FHR_CH4_FASTWAVE_C.txt", p);
        get_slow_fhr(p, fs);
        write_to_txt(&p, 1, "./../test/data/NUH1021_FHR_CH4_SLOWWAVE_C.txt");
        double_vec_print(p, 30);
    }
}

void test_find_cross_zero()
{
    unsigned int siglen = 5; 
    double_vec *p_x = double_vec_new(siglen);
    double_vec *p_y = double_vec_new(siglen);

    for (unsigned int i = 0; i < siglen; i++) 
    {
        p_x->p_data[i] = i;
    }

    p_y->p_data[0] = 1;
    p_y->p_data[1] = -1;
    p_y->p_data[2] = 2;
    p_y->p_data[3] = 3;
    p_y->p_data[4] = -4;

    // FIND ZERO CROSSING 
    double_vec *p_zc = find_cross_zero(p_x, p_y);

    // EXPECTED OUTPUT 
    double expected[3] = {1/2.0, 4/3.0, 24/7.0};

    // PRINT OUTPUT 
    #ifdef TEST_FHR_BASELINE_PRINT
    for (unsigned int i = 0; i < 3; i++)
    {
        printf("actual=%3.15f,expected=%3.15f\n",p_zc->p_data[i], expected[i]);
    }
    #endif

    // ASSERT 
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(p_zc->p_data, expected, 3);
}


void test_find_adcel_locs()
{
    double fs = 0;
    double_vec *p_y = double_vec_new(5);
    p_y->p_data[0] = 1;
    p_y->p_data[1] = -16;
    p_y->p_data[2] = 2;
    p_y->p_data[3] = 30;
    p_y->p_data[4] = -4;

    double height = 15.0; 
    double width  = 0;

    ad_point_vec *p_locs = find_adcel_locs(p_y, fs);

    if (p_locs == NULL) 
    {
        printf("no adcel \n");
    } else 
    {
        for (unsigned int i = 0; i < p_locs->len; i++) 
        {
            PRINT_AD_POINT(p_locs->p_data[i]);
        }
    }
}


void test_find_baseline() 
{
    const char *p_testcase = "NUH_1021";
    
    if (strcmp(p_testcase, "FSD_FHR_7")==0)
    {
        double fs = 2.5145; // FSD7
        // double fs = 2.17; // FSD8
        double_vec *p_x = double_vec_new(1543);//FSD7
        // double_vec *p_x = double_vec_new(1479); //FSD8
        read_from_txt("./../test/data/FSD_FHR_7.txt", p_x);
        baseline *p_base = NULL;


        // FIND BASELINE 
        p_base = find_baseline(p_x, fs);

        #ifdef TEST_FHR_BASELINE_PRINT
        printf("number of decel = %d \n", p_base->ndecel);
        printf("number of accel = %d \n", p_base->naccel);
        printf("max var = %3.5f \n", p_base->max_var);
        printf("mean var = %3.5f \n", p_base->mean_var);
        double_vec_print(p_base->p_base, 10); 

        if (p_base->p_ad != NULL) 
        {
            for (unsigned int i = 0; i < (p_base->p_ad->len); i++) 
            {
                if( fabs(p_base->p_ad->p_data[i].z) > 0 ) 
                {
                    PRINT_AD_POINT((p_base->p_ad->p_data[i]));   
               }
            }
        }
        #endif

        // write output to txt 
        #ifdef TEST_FHR_BASELINE_LOG
        write_to_txt(&(p_base->p_slow), 1, "./../test/data/FHR_SLOW_1.txt");
        write_to_txt(&(p_base->p_fast), 1, "./../test/data/FHR_FAST_1.txt");
        write_to_txt(&(p_base->p_base), 1, "./../test/data/FHR_BASE_1.txt");
        write_to_txt(&(p_base->p_diff), 1, "./../test/data/FHR_DIFF_1.txt");
        ad_point_vec_write("./../test/data/FHR_AD_1.txt", (p_base->p_ad));
        #endif
    }

    if (strcmp(p_testcase, "NUH_1021")==0)
    {
        double fs = 4;  
        double_vec *p_x = double_vec_new(12920);
        read_from_txt("./../test/data/NUH1021_FHR_CH4.txt", p_x);
        baseline *p_base = NULL;

        // FIND BASELINE 
        p_base = find_baseline(p_x, fs);

        #ifdef TEST_FHR_BASELINE_PRINT
        printf("number of decel = %d \n", p_base->ndecel);
        printf("number of accel = %d \n", p_base->naccel);
        printf("max var = %3.5f \n", p_base->max_var);
        printf("mean var = %3.5f \n", p_base->mean_var);
        double_vec_print(p_base->p_base, 10); 

        if (p_base->p_ad != NULL) 
        {
            for (unsigned int i = 0; i < (p_base->p_ad->len); i++) 
            {
                if( fabs(p_base->p_ad->p_data[i].z) > 0 ) 
                {
                    PRINT_AD_POINT((p_base->p_ad->p_data[i]));   
               }
            }
        }
        #endif

        // write output to txt 
        #ifdef TEST_FHR_BASELINE_LOG
        write_to_txt(&(p_base->p_slow), 1, "./../test/data/NUH1021_FHR_SLOW_1.txt");
        write_to_txt(&(p_base->p_fast), 1, "./../test/data/NUH1021_FHR_FAST_1.txt");
        write_to_txt(&(p_base->p_base), 1, "./../test/data/NUH1021_FHR_BASE_1.txt");
        write_to_txt(&(p_base->p_diff), 1, "./../test/data/NUH1021_FHR_DIFF_1.txt");
        ad_point_vec_write("./../test/data/NUH1021_FHR_AD_1.txt", (p_base->p_ad));
        #endif
    }

}

void test_find_baseline_fsd()
{
    // LOAD INPUT 
    double fs = 2.17; 
    double_vec *p_x = double_vec_new(5000);
    read_from_txt("./../test/data/CTU_1001m.txt", p_x);


    // FIND BASELINE 
    baseline *p_base = NULL; 
    p_base = find_baseline(p_x, fs);

    #ifdef TEST_FHR_BASELINE_PRINT
    printf("number of decel = %d \n", p_base->ndecel);
    printf("number of accel = %d \n", p_base->naccel);
    printf("max var = %3.5f \n", p_base->max_var);
    printf("mean var = %3.5f \n", p_base->mean_var);
    double_vec_print(p_base->p_base, 10); 

    if (p_base->p_ad != NULL) 
    {
        for (unsigned int i = 0; i < (p_base->p_ad->len); i++) 
        {
           if( fabs(p_base->p_ad->p_data[i].z) > 0 ) 
           {
                PRINT_AD_POINT((p_base->p_ad->p_data[i]));   
           }
        }
    }
    #endif 

    // write output to txt 
    #ifdef TEST_FHR_BASELINE_LOG
    write_to_txt(&(p_base->p_slow), 1, "./../test/data/CTU_001_FHR_SLOW.txt");
    write_to_txt(&(p_base->p_fast), 1, "./../test/data/CTU_001_FHR_FAST.txt");
    write_to_txt(&(p_base->p_base), 1, "./../test/data/CTU_001_FHR_BASE.txt");
    write_to_txt(&(p_base->p_diff), 1, "./../test/data/CTU_001_FHR_DIFF.txt");
    ad_point_vec_write("./../test/data/CTU_001_FHR_AD.txt", (p_base->p_ad));
    #endif

}

void test_get_adcel_width()
{
    double fs = 0.1; 
    double_vec *p_x = double_vec_new(10);
    
    // accel 
    // VEC(p_x,0) = 1.0;
    // VEC(p_x,1) = 2.0;
    // VEC(p_x,2) = 17.0;
    // VEC(p_x,3) = 14.0;
    // VEC(p_x,4) = 14.0;
    // VEC(p_x,5) = 17.0; 
    // VEC(p_x,6) = 16.0;
    // VEC(p_x,7) = 2.0;
    // VEC(p_x,8) = 1.0;
    // VEC(p_x,9) = 1.0;

    // decel
    p_x->p_data[0] = -1.0;
    p_x->p_data[1] = -2.0;
    p_x->p_data[2] = -17.0;
    p_x->p_data[3] = -14.0;
    p_x->p_data[4] = -14.0;
    p_x->p_data[5] = -17.0; 
    p_x->p_data[6] = -16.0;
    p_x->p_data[7] = -2.0;
    p_x->p_data[8] = -1.0;
    p_x->p_data[9] = -1.0;
 
    // no 
    // VEC(p_x,0) = 1.0;
    // VEC(p_x,1) = 2.0;
    // VEC(p_x,2) = 1.0;
    // VEC(p_x,3) = 1.0;
    // VEC(p_x,4) = 1.0;
    // VEC(p_x,5) = 1.0; 
    // VEC(p_x,6) = 1.0;
    // VEC(p_x,7) = 1.0;
    // VEC(p_x,8) = 1.0;
    // VEC(p_x,9) = 1.0;

    // double width = get_adcel_width(0, 9, p_x, fs, "zc_width");
    double width = get_adcel_width(0, 9, p_x, fs, "th_width");

    printf("width = %3.15f\n", width); 

}


int main()
{
    UNITY_BEGIN();
    // RUN_TEST(test_find_outlier);
    // RUN_TEST(test_outlier);
    // RUN_TEST(test_gausswin);
    // RUN_TEST(test_smooth);
    // RUN_TEST(test_get_fast_fhr);
    // RUN_TEST(test_get_slow_fhr);
    // RUN_TEST(test_find_cross_zero);
    // RUN_TEST(test_find_adcel_locs);
    // RUN_TEST(test_find_baseline);
    // RUN_TEST(test_find_baseline_fsd);

    return 0;
}
