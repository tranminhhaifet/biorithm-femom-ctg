/*******************************************************************************
*      Copyright ©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_unit_test.c
* Original Author: TRAN MINH HAI 
* Date created: 26 SEP 2019
* Purpose: Implement findpeaks algorithm
*
*
* Revision History:
* Ref No      Date           Who          	Detail
* <task ID>   <originated>   <originator> 	Original Code
* NA 		  26 SEP 2019	 TRAN MINH HAI   
*******************************************************************************/
// SYSTEM INCLUDE 
#include <stdio.h>
#include <stdlib.h>


// LOCAL INCLUDE 
#include "biorithm_unit_test.h"


/*------------------------------------------------------------------------------
Function name:
Description  : 
------------------------------------------------------------------------------*/
int compare_equal_peak(const peak *p1, const peak *p2)
{
	// uint location
	if (p1->loc != p2->loc)
	{
		#ifdef FHR_FPK_PRINT
		printf("loc1=%d, loc2=%d\n", p1->loc, p2->loc);
		#endif
		return -1; 
	}

	// double magnitude
	if (fabs(p1->mag - p2->mag) > FHR_FPK_PRECISION * fabs(p1->mag))
	{
		#ifdef FHR_FPK_PRINT
		printf("mag1=%3.5f, mag2=%3.5f\n", p1->mag, p2->mag);
		#endif
		return - 1; 
	}

	// uint ilb
	if (p1->ilb != p2->ilb) 
	{
		#ifdef FHR_FPK_PRINT
		printf("ilb2=%d, ilb2=%d\n", p1->ilb, p2->ilb);
		#endif
		return -1; 
	}

	// uint irb
	if (p1->irb != p2->irb)
	{
		#ifdef FHR_FPK_PRINT
		printf("irb1=%d, irb2=%d\n", p1->irb, p2->irb);
		#endif
		return -1;
	}

	// double width
	if (fabs(p1->width - p2->width) > 
		FHR_FPK_PRECISION * fabs(p1->width))
	{
		#ifdef FHR_FPK_PRINT
		printf("width1=%3.5f, width2=%3.5f\n", p1->width, p2->width);
		#endif
		return -1;
	}

	// double prominence
	if (fabs(p1->prominence - p2->prominence) > 
		FHR_FPK_PRECISION * fabs(p1->prominence))
	{
		#ifdef FHR_FPK_PRINT
		printf("prominence1=%3.5f, prominence2=%3.5f\n", p1->prominence, 
			p2->prominence);
		#endif
		return -1; 
	}

	// bool flag 
	if (p1->flag != p2->flag)
	{
		#ifdef FHR_FPK_PRINT
		printf("flag1=%d, flag2=%d\n", p1->flag, p2->flag);
		#endif
		return -1; 
	}

	return 1; 
}

/*------------------------------------------------------------------------------
Function name: TEST_ASSERT_EQUAL_PEAK_ARRAY(const *peak p1, const *peak p2, 
unsigned int len)
Description: 
------------------------------------------------------------------------------*/
void TEST_ASSERT_EQUAL_PEAK_ARRAY(peak *p1, peak *p2, unsigned int len)
{
	int equal = 1; 

	for (unsigned int i = 0; i < len; i++)
	{
		equal = compare_equal_peak(&p1[i], &p2[i]);

		if (equal != 1)
		{
			printf(":FAIL at %d \n", i);
			printf("expected at %d: ", i);
			PRINT_PEAK(p1[i]);
			printf("  actual at %d: ", i);
			PRINT_PEAK(p2[i]);
			return;
		}
	}

	printf(":PASS\n");
}

