# base image 
FROM ubuntu:18.04

# environment variable 
ENV PATH="/home/cmake-3.14.5-Linux-x86_64/bin:${PATH}"
ENV LD_LIBRARY_PATH="/usr/local/lib"

# apt install packages include gcc 
RUN apt update && \
    apt-get install -y build-essential && \ 
    apt-get install -y nano && \
    apt-get install -y wget && \
    apt-get install -y unzip 

# download and install cmake 
RUN wget -P /home/ https://github.com/Kitware/CMake/releases/download/v3.14.5/cmake-3.14.5-Linux-x86_64.tar.gz
RUN tar -xvzf /home/cmake-3.14.5-Linux-x86_64.tar.gz -C /home/

# download and install gnu gsl lib
RUN wget -P /home/ https://mirror.freedif.org/GNU/gsl/gsl-2.5.tar.gz 
RUN tar -xvzf /home/gsl-2.5.tar.gz -C /home/
RUN cd /home/gsl-2.5/ && ./configure && make && make install && cd ~

# download and install gnuplot lib
RUN wget -P /home/ "https://sourceforge.net/projects/gnuplot/files/gnuplot/5.2.7/gnuplot-5.2.7.tar.gz"
RUN tar -xvzf /home/gnuplot-5.2.7.tar.gz -C /home/
RUN cd /home/gnuplot-5.2.7 && ./configure && make && make install && cd ~ 

# clone and install biorithm lib
RUN cd /home/ && mkdir biorithm-femom-ctg 
ADD . /home/biorithm-femom-ctg/
RUN cd /home/biorithm-femom-ctg/src/ecg/build  rm -rf * && cmake .. && make && make install 
RUN cd /usr/local/bin && chmod 700 app_ecg 

# clone and install wavelib 
git clone git@bitbucket.org:tranminhhaifet/wavelibrafat.git

cmake .. 
sudo make && sudo make install 
sudo cp Bin/libwauxlib.a Bin/libwavelib.a /usr/local/lib/
sudo cp header/wauxlib.h header/wavelib.h /usr/local/include/


# remove downloaded file 
RUN cd /home/ && rm *.gz 

# clone and install web-server 

# start web-server 

/plplot/5.15.0 Source/plplot-5.15.0.tar.gz: released on 2019-06-01 23:38:15 UTC