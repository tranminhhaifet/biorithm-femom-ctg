/*********************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software must not be copied, printed, distributed, or reproduced in whole
* or in part or otherwise disclosed without prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
*    Filename: render_img.h
*    Original Author: Hoan
*    Date created: 03 JUL 2019
*    Purpose: <The main header file for render image unit>
*
*    Changes:
*    Ref No          Date         Who          Detail
*    <issue/task ID> <date>       <initials>   <fix/change description>
*
*********************************************************************************/
#ifndef RENDER_IMG_H
#define RENDER_IMG_H

// SYSTEM INCLUDES
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

// LIB INCLUDE 
#include <plConfig.h>
#include <plplot.h>

// LOCAL INCLUDE 
#include "biorithm_aux.h"
#include "fhr_baseline.h"
#include "fhr_heartrate_cal.h"
#include "fhr_ua.h"

// DEBUG AND PRINT
#define RENDER_IMG_PRINT
// #define RENDER_IMG_DEBUG

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
int render_img(unsigned int trace_speed, double_vec *p_fhr, double_vec *p_mhr, 
baseline *p_fbase, double_vec *p_ua, const char *p_path);

#endif