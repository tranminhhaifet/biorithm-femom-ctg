
/*******************************************************************************
*      Copyright ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 03 JUL 2019
* Purpose: clean and re-oganise code, all auxlilary functions will be here
*
*
* Revision History:
* Ref No      Date           Who               Detail
*             03 JUL 2019    Hoan              Original Code
*             08 OCT 2019    TRAN MINH HAI     Integration  
*******************************************************************************/
// SYSTEM INCLUDE 

// LOCAL INCLUDE 
#include "render_img.h"

// DEBUG MODE 
// #define RENDER_DEBUG 

// STATIC CONST 
static const PLINT SAMPLE_RATE = 4;            // SAMPLE RATE OF FHR AND UA

static const PLINT PAPER_SCALE = 1;            // PAPER SCALE 
static const PLINT ANNOTATION = 0; 
static const PLINT TOT_ROWS = 1; 
static const PLINT TIME_MULT = 1; 

static const PLINT BLOCK_SIZE_MM = 260;        // WIDTH AND HEIGHT OF A BLOCK
static const PLFLT BOX_SIZE_MM = 13;           // WIDTH OF SQUARE BOX 
static const PLFLT DPI_VAL = 90.0;             // DPI FOR RESOULTION  
static const PLFLT BLOCK_SIZE_MIN = 10;        // BLOCK SIZE IN MINUTE 
static const PLINT BLOCK_SIZE_SEC = 600;       // BLOCK SIZE IN SECOND
static const PLFLT MAX_SIGLEN = 300000;        // MAX BLOCK SIZE IN SAMPLE
static const PLINT HEIGHT_SEPARATION[5] = {2, 6, 3, 17, 2}; // IN BOX SIZE 

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/ 
int render_img(unsigned int trace_speed, double_vec *p_fhr, double_vec *p_mhr, 
baseline *p_fbase, double_vec *p_ua, const char *p_path)
{
    // SIGLEN IN TERM OF NUMBER OF SAMPLE EACH 250ms 
    unsigned int siglen = p_fhr->len; 
    unsigned int siglen_mhr = p_mhr->len;


    // SETUP DEVICE OUTPUT     
    plsdev("psc"); 

    // SETUP FILE PATH FOR OUTPUT TRACE
    char *filename = malloc(strlen(p_path) + 50);
    sprintf(filename, "%s%s%d%s%s", p_path, "trace_", trace_speed, "cm", ".ps");
    printf("filename = %s\n", filename);
    plsfnam(filename); 

    // SETUP PARAMETER FOR PPLOT 
    int num_blk = 0;
    int num_row = 0;
    int num_time_blk = 0;
    int paper_len = 0;
    int paper_height = 0;
    int row_length = 0; 
    int sample_blk = 0; 

    // NUMBER OF SAMPLE PER BLOCK OF 10 MINUTES 
    sample_blk = SAMPLE_RATE * BLOCK_SIZE_SEC;

    //  NUMBER OF BLOCK EACH 10 MINUTES 
    num_time_blk = ceil(p_fhr->len / sample_blk);
    
    // NUMBER OF ROW DEFAULT 1 
    paper_len    = BLOCK_SIZE_MM * num_time_blk * trace_speed;
    paper_height = BOX_SIZE_MM * (HEIGHT_SEPARATION[0] + HEIGHT_SEPARATION[1]
    + HEIGHT_SEPARATION[2] + HEIGHT_SEPARATION[3] + HEIGHT_SEPARATION[4]); 

    #ifdef RENDER_DEBUG
    // printf("tracespeed = %d, nblock = %d, paperlen = %d, paperheight = %d\n", 
     // trace_speed, num_time_blk, paper_len, paper_height);
    #endif 

    // DEFINE VIEWPORT FOR FHR AND MHR
    PLFLT xminv1 =  (4.0 * BOX_SIZE_MM) / paper_len;
    PLFLT xmaxv1 = 1.0 - (4.0 * BOX_SIZE_MM) / paper_len;
    PLFLT yminv1 = (1.0 * BOX_SIZE_MM * (HEIGHT_SEPARATION[0] + HEIGHT_SEPARATION[1] 
    + HEIGHT_SEPARATION[2])) / paper_height;
    PLFLT ymaxv1 = (1.0 * BOX_SIZE_MM * (HEIGHT_SEPARATION[0] + HEIGHT_SEPARATION[1]
    + HEIGHT_SEPARATION[3]))/ paper_height;

    // DEFINE VIEWPORT FOR UA 
    PLFLT xminv2 = xminv1; 
    PLFLT xmaxv2 = xmaxv1; 
    PLFLT yminv2 = (1.0 * BOX_SIZE_MM * (HEIGHT_SEPARATION[0]))/ paper_height;
    PLFLT ymaxv2 = (1.0 * BOX_SIZE_MM * (HEIGHT_SEPARATION[0] 
    + HEIGHT_SEPARATION[1])) / paper_height; 

    // WIDTH of PLPTEX
    PLFLT pltexw = BLOCK_SIZE_SEC / (20.0 * trace_speed) ;

    // DEFINE VIEWPORT FOR MARK CONNECT
    PLFLT xminv3 = xminv1 - BOX_SIZE_MM / paper_len;
    PLFLT xmaxv3 = xminv1;
    PLFLT yminv3 = yminv2; 
    PLFLT ymaxv3 = ymaxv1; 

    // SET COLOR 
    plscol0(0, 255, 255, 255); /* White, color 0, background */
    plscol0(15, 0, 0, 0);      /* Black, color 15 */

    // GET PAGE 
    PLFLT xp0, yp0;
    PLINT xleng0, yleng0, xoff0, yoff0;

    // SET PAGE
    plspage(DPI_VAL, DPI_VAL, paper_height, paper_len, 0, 0);

    // INIT PLPLOT 
    plinit();

    // DATA BUFFER FOR PLOT TODO: MORE MEANING VARIABLE NAME 
    PLFLT *x  = malloc(sizeof(PLFLT) * (siglen));  // TIME INDEX 
    PLFLT *y  = malloc(sizeof(PLFLT) * (siglen));  // FETAL HEART RATE 

    PLFLT *x_m  = malloc(sizeof(PLFLT) * (siglen_mhr));  // TIME INDEX 
    PLFLT *y_m  = malloc(sizeof(PLFLT) * (siglen_mhr));  // FETAL HEART RATE 


    PLFLT *y2 = malloc(sizeof(PLFLT) * (siglen));  // FETAL BASE LINE 
    PLFLT *y3 = malloc(sizeof(PLFLT) * (siglen));  // MATERNAL HEART RATE 
    PLFLT *to = malloc(sizeof(PLFLT) * (siglen));  // MATERNAL UA 

    // TODO: DOUBLE CHECK     
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    // ANNOTATION STRING 
    char connect_string[255];

    // TODO: DOUBLE CHECK 
    pladv(0); //Advance the (sub-)page

    // PREPARE FETAL HEAR TRATE DATA FOR PLPLOT 
    for (unsigned int i = 0; i < siglen; i++)
    {
        // CONVERT SAMPLE INDEX TO TIME IN MINUTE OR SECOND 
        x[i]  = (1.0 * i) / (SAMPLE_RATE * TIME_MULT);  

        // FETAL HEART RATE 
        y[i]  = p_fhr->p_data[i];

        // FETAL BASELINE 
        y2[i] = p_fbase->p_base->p_data[i];

        // UA
        to[i] = p_ua->p_data[i];
    }


    // MHR DATA FOR PLOT 
    for (unsigned int i = 0 ; i < siglen_mhr; i++)
    {
        x_m[i] = (1.0 * i) / (SAMPLE_RATE * TIME_MULT);

        y_m[i] = p_mhr->p_data[i];
    }

    // AXIS LIMIT OR RANGE 
    PLFLT xmin_fhr = 0.0;               // TIME MIN 
    PLFLT xmax_fhr = x[siglen -1];      // TIME MAX 
    PLFLT ymin_fhr = 50.0;              // HEART RATE MIN 
    PLFLT ymax_fhr = 220.0;             // HEART RATE MAX 

    PLFLT xmin_ua = 0.0;                // STARTING TIME UA
    PLFLT xmax_ua = x[siglen -1]; ;     // END TIME UA 
    PLFLT ymin_ua = 0;                  // MIN AMPLITUDE UA
    PLFLT ymax_ua = 120;                // MAX AMPLITUDE UA

    #ifdef RENDER_DEBUG
    printf("xmin=%lg,xmax=%lg,ymin=%lg,ymax=%lg\n",xmin_fhr, xmax_fhr,
    ymin_fhr, ymax_fhr );
    #endif 
 
    // BUFFER FOR 2 POINTS 
    PLFLT lx[2], ly[2];

    // 
    for (num_row = 0; num_row < TOT_ROWS; num_row++)
    {    
        plwidth(3);
        // DEFINE VIEWPORT FOR FHR AND MHR 
        plvpas(xminv1, xmaxv1, yminv1, ymaxv1, 0.00);
        // plvpor(xminv1, xmaxv1, yminv1, ymaxv1);

        // DEFINE WINDOW FOR FHR AND MHR 
        plwind(xmin_fhr, xmax_fhr, ymin_fhr, ymax_fhr);

        //SET CHARACTER SIZE 
        plschr(3.0, 1.0);

        // SET COLOR 
        plcol0(15);

        // TIME FORMAT 
        pltimefmt("%H:%M");

        // SET CORLOR 
        plcol0(15);

        plschr(3.0, 1.0);

        plbox("ghdnitbc", BLOCK_SIZE_SEC,  20 * trace_speed , "ghbc", 20, 2); //180 1

        pllab("","","FHR");

        // SET LINE STYLE 
        pllsty(1);
        plwidth(5);
        plcol0(15);
        
        // LOOOP THROUGH ALL FHR SAMPLES TO PLOT FHR 
        for (int i = 0; i < siglen - 1; i++)
        {   
            lx[0] = x[i];
            lx[1] = x[i + 1];
            ly[0] = y[i];
            ly[1] = y[i + 1];

            plpoin(2, lx, ly, 1);

            // plline(2, lx, ly);

            // SKIP IF EITHER IS NAN 
            if (ly[0] < FHR_FBPM_ULIM && ly[0] > FHR_FBPM_LLIM &&
                ly[1] < FHR_FBPM_ULIM && ly[1] > FHR_FBPM_LLIM &&
                fabs(ly[1]-ly[0]) < FHR_FBPM_DLIM)
            {
                // plline(2, lx, ly);
//                 plpoin(2, lx, ly, 1);
            }    
        }

        // LOOP THROUGH ALL MHR SAMPLES TO PLOT MRF AS LINE 

        pllsty(1);
        plwidth(5);
        plcol0(9);
        for (int i = 0; i < siglen_mhr - 1; i++)
        {
            lx[0] = x_m[i];
            lx[1] = x_m[i+1];
            ly[0] = y_m[i];
            ly[1] = y_m[i+1];

            // plline(2,lx, ly);
            plpoin(2, lx, ly, 1);

            // SKIP IF EITHER IS NAN 
            if (ly[0] < FHR_MBPM_ULIM && ly[0] > FHR_MBPM_LLIM &&
                ly[1] < FHR_MBPM_ULIM && ly[1] > FHR_MBPM_LLIM &&
                fabs(ly[1]-ly[0]) < FHR_MBPM_DLIM)
            {
                // plline(2, lx, ly);
                // plpoin(2, lx, ly, 1);
            }   
        }


        // LOOOP THROUGH ALL FHR SAMPLES TO PLOT FHR BASELINE
        pllsty(1);
        plwidth(3);
        plcol0(1);

        for (int i = 0; i < siglen - 1; i++)
        {   
            lx[0] = x[i];
            lx[1] = x[i + 1];
            ly[0] = y2[i];
            ly[1] = y2[i + 1];

            // SKIP IF EITHER IS NAN 
            if (ly[0] < FHR_FBPM_ULIM && ly[0] > FHR_FBPM_LLIM &&
                ly[1] < FHR_FBPM_ULIM && ly[1] > FHR_FBPM_LLIM)
            {
                plline(2, lx, ly);
                // plpoin(2, lx, ly, 1);
            }    
        }

        // SET COLOR 
        plcol0(5);
        plwidth(3);

        // SET CHARACTER SIZE 
        plschr(3.0, 1.0);
        float coord_lab;

        // PLOT PLPTEX FOR Y AXIS 
        while (num_blk <= num_time_blk)
        {
            //TODO: 
            coord_lab = 0.09 + num_blk * BLOCK_SIZE_SEC; //0.12

            // WITH OF PLPTEX 
            PLFLT lab_px[] = {coord_lab, coord_lab, 
            coord_lab + pltexw, coord_lab + pltexw};

            PLFLT lab_py[] = {ymin_fhr + 1, ymax_fhr - 1, ymax_fhr - 1, ymin_fhr + 1};

            // SET COLOR 
            plcol0(7);

            // FILL COLOR 
            plfill(4, lab_px, lab_py);

            // SET CHAR 
            plschr(3.0, 1);

            // SET COLOR 
            plcol0(15);

            // 
            #ifdef RENDER_DEBUG
            printf("coord_lab = %3.15f\n", coord_lab);
            #endif 

            plptex(coord_lab, 60, 0, 0, 0, "60");
            plptex(coord_lab, 80, 0, 0, 0, "80");
            plptex(coord_lab, 100, 0, 0, 0, "100");
            plptex(coord_lab, 120, 0, 0, 0, "120");
            plptex(coord_lab, 140, 0, 0, 0, "140");
            plptex(coord_lab, 160, 0, 0, 0, "160");
            plptex(coord_lab, 180, 0, 0, 0, "180");
            plptex(coord_lab, 200, 0, 0, 0, "200");
            plschr(3.0, 1.0);

            num_blk++;
        }

        // ADD VERTIAL LINE IN CASE OF PLBOX NOT SO GOOD 
        lx[0] = coord_lab;
        lx[1] = coord_lab;
        ly[0] = ymin_fhr;
        ly[1] = ymax_fhr;
        plline(2,lx,ly);

        //============================  PLOT UA ================================
        plcol0(15);
        plschr(3.0, 1.0);
        plwidth(3);
        // DEFINE VIEWPORT FOR UA 
        plvpas(xminv2, xmaxv2, yminv2, ymaxv2, 0.00);
        
        // DEFINE WINDOW FOR UA
        plwind(xmin_ua, xmax_ua, ymin_ua, ymax_ua);

        plbox("ghdnitbc", BLOCK_SIZE_SEC, 20 * trace_speed, "ghbc",
        20, 1); 

        // LOOOP THROUGH ALL FHR SAMPLES TO PLOT FHR 
        plwidth(5);
        for (int i = 0; i < siglen - 1; i++)
        {   
            lx[0] = x[i];
            lx[1] = x[i + 1];
            ly[0] = to[i];
            ly[1] = to[i + 1];

            // SKIP IF EITHER IS NAN 
            if (ly[0] < FHR_UA_MAX_AMP && ly[0] > FHR_UA_MIN_AMP &&
                ly[1] < FHR_UA_MAX_AMP && ly[1] > FHR_UA_MIN_AMP)
            {
                plline(2, lx, ly);
            }    
        }

        plwidth(3);
        pllab("Time (minute)","","");

        // PLTEX MARK YAXIS STICK 
        PLFLT lab_px[] = {0.09, 0.09, 0.09 + pltexw, 0.09 + pltexw};
        PLFLT lab_py[] = {1, 121, 121, 1};
        plcol0(7);
        plfill(4, lab_px, lab_py);

        plcol0(15);
        plschr(3.0, 1.0);
        plptex(0, 20,  0, 0, 0,  "20");
        plptex(0, 40,  0, 0, 0,  "40");
        plptex(0, 60,  0, 0, 0,  "60");
        plptex(0, 80,  0, 0, 0,  "80");
        plptex(0, 100, 0, 0, 0, "100");

        // ADD VERTIAL LINE IN CASE OF PLBOX NOT SO GOOD 
        lx[0] = coord_lab;
        lx[1] = coord_lab;
        ly[0] = ymin_ua;
        ly[1] = ymax_ua;
        plline(2,lx,ly);

        // CONNECT MARK 
        plvpas(xminv3, xmaxv3, yminv3, ymaxv3, 0.0);

        plwind(0, 1, 0, 1);
        plbox("", 1, 1, "", 1, 1);
        PLFLT px_label_xaxis[] = {0, 0, 1, 1};
        PLFLT py_label_yaxis[] = {0, 1, 1, 0};

        plcol0(3);
        plfill(4, px_label_xaxis, py_label_yaxis);

        plschr(3.0, 1);
        plcol0(15);
        sprintf(connect_string, "CONNECT %d-%02d-%02d 00:00", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
        plptex(0.5, 0.5, 0, -1, 0.5, connect_string);
        
    }

    // Close PLplot library
    plend();
    printf("PLOT END\n");

    // FREE MEM 
    free(x);
    free(y);
    free(y2);
    free(y3);
    free(filename);

    // RETURN 
    return 1;
}