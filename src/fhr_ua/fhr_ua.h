/*******************************************************************************
*      Copyright Ã‚Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: fhr_ua.h
* Original Author: TRAN MINH HAI
* Date created: 03 JUL 2019
* Purpose: compuare uterince contraction trace from raw ecg 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             21 AUG 2019    T.H   		  Rework 
*             02 SEP 2019    T.H          Rework Error Handling, Define Params
********************************************************************************/
#ifndef BIORITHM_FHR_UA_HEADER
#define BIORITHM_FHR_UA_HEADER

#include <wavelib.h>
#include "biorithm_aux.h"
#include "biorithm_wavelib.h"

// ON OFF DEBUG 
#define FHR_UA_DEBUG 

#define SAMPLING_FREQUENCY 500  

// WAVELET EXTRACT UC FROM ECG 
// {12layer, "db4-5", D6-D9}, {16layer, "db4-5-6, D6-D11"}
#define FHR_UA_WNAME "db6" 					// MOTHER WAVELET NAME 
#define FHR_UA_NLAYER 12 					// NUMBER OF WAVELET LAYER FOR DEC
#define FHR_UA_WFIRST_LAYER 6				// FIRST LAYER CONTAIN UC
#define FHR_UA_WLAST_LAYER 10 				// LASTT LAYER CONTAIN UC
#define FHR_UA_WCHUNK 32768 				// CHUNK BY CHUNK WAVELET

// STFT SQI WINDOW 
#define FHR_UA_SQI_WLEN 8192				// FFT WINDOW FOR SQI ESTIMATE 
#define FHR_UA_SQI_NSHIFT 125 				// 4Hz SAMPLING SHOULD BE FS/4 
#define FHR_UA_HI_MIN_FREQ 6.0 				// MIN FREQ. OF  NOISE 			
#define FHR_UA_HI_MAX_FREQ 125.0 			// MAX FREQ. OF  NOISE 

// STFT UTERINE CONTRACTION WINDOW 
#define FHR_UA_UCE_WLEN 8192 				// WINDOW FOR STFT UC FORCE 
#define FHR_UA_UCE_NSHIFT 125 				// 4Hz SAMPLING SHOULD BE FS/4
#define FHR_UA_MIN_FREQ 0.2					// MIN UC FREQ. 
#define FHR_UA_MAX_FREQ 5.0 				// MAX UC FREQ. 

// TIME DENOISE 
#define FHR_UA_TDENOISE_SQI_WLEN 8192		// 
#define FHR_UA_TDENOISE_IMP_WLEN 16384		// WINDOW FOR IMPULSIVE DENOISE
#define FHR_UA_TDENOISE_NSHIFT 125 			// SHIFTING FOR IMPO DENOISE
#define FHR_UA_MEAN_LEVEL 5.0 				// THRESH FOR IMP SAMPLE 
#define FHR_UA_SQI_MAD_LEVEL 6.0 			// THRESH FOR LOW SQI WINDOW 

#define FHR_UA_SCALE_MAD 10.0 				// TO SCALE UA FOR PLOTING 
#define FHR_UA_MAX_AMP 	 100.0 				// MAX VALUE OF UA 
#define FHR_UA_MIN_AMP   0.0 


// MACRO FOR COMPLEX NUMBER
#define REAL(z,i) ((z)[2*(i)])
#define IMAG(z,i) ((z)[2*(i)+1])

// STFT SQI PARAMETERS 
struct param_stft 
{
	unsigned int wlen; 			    // hann window length 
	unsigned int nshift;            // moving window nshift point 
	double uc_min_freq;	            // min freq. of uterine contraction
	double uc_max_freq;             // max freq. of uterince contraction
	double hi_min_freq;             // min freq. of interference 
	double hi_max_freq; 			// max freq. of interference
	double sampling_freq;           // sampling frequency 
};

// WAVELET UTERINE CONTRACTION PARAMETERS 
struct param_wuc
{	
	char * p_wname;                 // mother wavelet name 
	unsigned int nlayer;            // number of wavelet layer
	unsigned int first_layer;       // first layer contains uc signal 
	unsigned int last_layer;	    // last layer contains uc signal 
    unsigned int wlen;              // window length for wavelet per window
};

// TIME DENOISE PARAMETERS 
struct param_tden
{
	unsigned int sqi_wlen;          // rmv. low sqi window 
	unsigned int imp_wlen;          // window to find spikes 
	unsigned int nshift;            // shifting point 
	double level_mean;              // level * mean = threshold 
	double level_mad;               // level * mad  = threshold
};

// OUTPUT UA TRACE
struct uatrace 
{
	double_vec *p_uc; 
	double_vec *p_scaleuc;
	double_vec *p_sqi;
};

// OPERATIONS
/*==============================================================================
*                                  Wavelet
*=============================================================================*/

/*------------------------------------------------------------------------------
* Function name: 
* Description: extract uterine contraction signal band by using wavelet. First, 
* decompose input ECG into N layer (default N = 12) and extract only layer {9,
* 8, 7, 6, 5}. In general, each layer is multiplied by a soft weight rather than 
* 0 or 1. This function is one short processing. 
------------------------------------------------------------------------------*/
void extract_uc_from_ecg(struct param_wuc param, double_vec *p_ecg);


/*------------------------------------------------------------------------------
* Function name:
* Description: Similar to extract_uc_from_ecg, but this function is window by 
* window processing, so it is suitable for real-time processing. 
------------------------------------------------------------------------------*/
void extract_uc_from_ecg_chunk(struct param_wuc param, double_vec *p_ecg);

/*==============================================================================
*                          Time Domain Denoise
*=============================================================================*/
void null_outlier(double_vec *p_chunk, double level);

void rmv_spike(struct param_tden param, double_vec *p_sig);

/*==============================================================================
*               Compute Uterine Contraction and Signal Quality Index
*=============================================================================*/
void fft_real_chunk(double_vec *p_re_in, double_vec *p_cm_out);

double fft_to_power(double_vec *p_fft, double fs, double fmin, double fmax);

double fft_to_uce(double_vec *p_fft, double fs, double fmin, double fmax);

/*------------------------------------------------------------------------------
* Function name:
* Description: this function detect low signal quality (SQI) window, if a window 
* is low SQI, the window is ignored when processing to prevent unstable scale.  
------------------------------------------------------------------------------*/
void rmv_low_sqi_window(struct param_tden param, double_vec *p_ecg, 
double_vec *p_uc_sqi);

/*------------------------------------------------------------------------------
* Function name:
* Description: this function convert spectrum energy to uterine contraction 
* force. TODO: scale output to range [0-100]
------------------------------------------------------------------------------*/
double_vec *compute_uce_sqi_stft(struct param_stft param, double_vec *p_ecg);

double_vec *scale_ua(double_vec *ua);
/*------------------------------------------------------------------------------
* Function name:
* Description: this is the main function for ua unit, which compute the ua 
* trace form raw ECG signal. 
------------------------------------------------------------------------------*/
struct uatrace *compute_uc_trace(double_vec *p_ecg);

/*------------------------------------------------------------------------------
* Function name:
* Description:  
------------------------------------------------------------------------------*/
void uatrace_write(struct uatrace *pp_ua, const char *p_path);


#endif //BIORITHM_FHR_UA_HEADER