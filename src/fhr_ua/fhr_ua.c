/*******************************************************************************
*         Copyright Ã‚Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: fhr_ua.c
* Original Author: TRAN MINH HAI
* Date created: 03 JUL 2019
* Purpose: compute unterine contraction trace from raw ECG signal. The final
* outputs consist of a vector of uterine contraction (uc), each value of uc has
* an attached value of signal quality index (SQI).
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             03 JUL 2019    T.H          Original Code
*             20 AUG 2019    T.H          Rework 
*             02 SEP 2019    T.H          Rework Error Handling, Parameters
*******************************************************************************/
// SYSTEM INCLUDES
#include <math.h>
#include <stdio.h>

// PROJECT INCLUDES
#include <wavelib.h>
#include <gsl_fft_complex.h>
#include <gsl_sort.h>

// LOCAL INCLUDES
#include "biorithm_aux.h"
#include "biorithm_wavelib.h"
#include "fhr_ua.h"


// STTFT PARAMETER FOR SQI 
static const struct param_stft g_param_sqi_stft = {
    FHR_UA_SQI_WLEN, FHR_UA_SQI_NSHIFT, FHR_UA_MIN_FREQ, FHR_UA_MAX_FREQ, 
    FHR_UA_HI_MIN_FREQ, FHR_UA_HI_MAX_FREQ, SAMPLING_FREQUENCY };

// STFT PARAMETERS FOR UTERINE CONTRACTION ENERGY 
static const struct param_stft g_param_uce_stft = {
    FHR_UA_UCE_WLEN, FHR_UA_UCE_NSHIFT, FHR_UA_MIN_FREQ, FHR_UA_MAX_FREQ, 
    FHR_UA_HI_MIN_FREQ, FHR_UA_HI_MAX_FREQ, SAMPLING_FREQUENCY };

// WAVELET PARAMETERS FOR UC EXTRACTION 
static const struct param_wuc g_param_wuc = { 
    FHR_UA_WNAME, FHR_UA_NLAYER, FHR_UA_WFIRST_LAYER, 
    FHR_UA_WLAST_LAYER, FHR_UA_WCHUNK};

// TIME DENOISE PARAMETERS FOR RMV. LOW SQI WINDOW AND SPIKES 
static const struct param_tden g_param_tden = {
    FHR_UA_TDENOISE_SQI_WLEN, FHR_UA_TDENOISE_IMP_WLEN, FHR_UA_TDENOISE_NSHIFT, 
    FHR_UA_MEAN_LEVEL, FHR_UA_SQI_MAD_LEVEL};

// WEIGHT FOR WAVELET LAYER 
static const double g_weight_wuc[FHR_UA_NLAYER+1] = {0.0, 0.0, 0.0, 0.0, 1.0, 1.0,
1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0};

// OPERATIONS
/*------------------------------------------------------------------------------
* Function Name: extract_uc_from_ecg
* Description  : extract uterine contraction signal band from raw ecg 
* range of uterine contraction freq. band is specified from struct nparam_wuc.
*-----------------------------------------------------------------------------*/
void extract_uc_from_ecg(struct param_wuc param, double_vec *p_ecg)
{ 	
	// init wavelet transform object
    struct param_wave wave = {(int)p_ecg->len, param.nlayer, param.p_wname};
	struct wavelib_object *p_wave = create_wavelib_object(wave);

	// decompose to nlayer by dwt 
	dwt(p_wave->wt, p_ecg->p_data);

	// extract only from uc_first_layer to uc_last_layer
	weight_wlayer(p_wave->wt, g_weight_wuc);

	// reconstruct by idwt 
	idwt(p_wave->wt, p_ecg->p_data);

	// free mem 
	delete_wavelib_object(p_wave);
}

/*------------------------------------------------------------------------------
* Function Name: extract_uc_from_ecg
* Description  : extract uterine contraction from raw ecg, processing chunk 
* by chunk instead of one short wavelet transform. 
*------------------------------------------------------------------------------*/
void extract_uc_from_ecg_chunk(struct param_wuc param, double_vec *p_ecg)
{
	// init wavelet transform object
    struct param_wave wave = {(int)param.wlen, param.nlayer, param.p_wname};
	struct wavelib_object *p_wave = create_wavelib_object(wave);	

	// number of chunk 
    double nchunk = floor((double)p_ecg->len / (double)param.wlen);

	// chunk by chunk processing 
	double_vec *p_chunk = double_vec_new(param.wlen);
	for (unsigned int i = 0; i < nchunk; i++)
	{
		// get a chunk from ecg 
		double_vec_get_chunk(p_ecg, p_chunk, i * param.wlen);

		// dwt decompose to nlayer 
		dwt(p_wave->wt, p_chunk->p_data);

		// null unwanted layer 
        weight_wlayer(p_wave->wt, g_weight_wuc);

		// idwt reconstruct 
		idwt(p_wave->wt, p_chunk->p_data);

		// update output 
		double_vec_set_chunk(p_ecg, p_chunk, i * param.wlen);
	}

	// set the rest of signal to 0 
	for (unsigned int i = 0; i < p_ecg->len - nchunk * param.wlen; i ++)
	{
        p_ecg->p_data[(unsigned int)nchunk * param.wlen + i] = 0.0;
	}

	// free mem and output 
	delete_wavelib_object(p_wave);
	double_vec_delete(p_chunk);
}

/*------------------------------------------------------------------------------
* Function Name: null_spike 
* Description  : null outliers within a chunk data by comparing with mean * level
*-----------------------------------------------------------------------------*/
void null_outlier(double_vec *p_chunk, double level)
{
	double thresh = 0.0;

	// convert to abs 
	double_vec *p_abs = double_vec_new(p_chunk->len);
	double_vec_set_chunk(p_abs, p_chunk, 0);
	double_vec_abs(p_abs);

	// find mean
	thresh = level * find_mean(p_abs);

	// nulling 
	for (unsigned int i = 0; i < p_chunk->len; i++)
	{
		if ( (fabs(p_chunk->p_data[i]) - thresh) >= 0.0) 
		{
            // TODO: replace by mean
			// replace by zero
			p_chunk->p_data[i] = 0.0;
		}
	}

	// free mem 
	double_vec_delete(p_abs);
}

/*------------------------------------------------------------------------------
* Function Name: remove_spike  
* Description  : remove spikes from signal after wavelet processing, 
* by looping though window by window basic no overlapping.
* Within each window, outliers are detected by comparing with level * mean.
*-----------------------------------------------------------------------------*/
void rmv_spike(struct param_tden param, double_vec *p_sig)
{
	double_vec *p_chunk = double_vec_new(param.imp_wlen);

    double nchunk = floor((double)p_sig->len / (double)param.imp_wlen);

	for(unsigned int i = 0; i < nchunk; i++)
	{
		// get a chunk data from sig 
		double_vec_get_chunk(p_sig, p_chunk, i * param.imp_wlen);

		// null outlier by comparing with quantile value 
        null_outlier(p_chunk, param.level_mean);

		// update input signal 
		double_vec_set_chunk(p_sig, p_chunk, i * param.imp_wlen);
	}

	double_vec_delete(p_chunk);
}

/*------------------------------------------------------------------------------
* Function Name: fft_real_chunk 
* Description  : fft a chunk of real signal using gsl_complex_fft
*-----------------------------------------------------------------------------*/
void fft_real_chunk(double_vec *p_re_in, double_vec *p_cm_out)
{
	// insert real signal p_re_in into complex p_cm_out for FFT 
    for (size_t i = 0; i < p_re_in->len; i++)
	{
		IMAG(p_cm_out->p_data,i) = 0.0; // set IMAG to zero
		REAL(p_cm_out->p_data,i) = p_re_in->p_data[i];
	}

	// gsl fft 
	gsl_fft_complex_radix2_forward(p_cm_out->p_data, 1, p_re_in->len);
}
/*------------------------------------------------------------------------------
* Function Name: fft_to_power
* Description  : power of a sub-band from fmin(Hz) to fmax(Hz)
*------------------------------------------------------------------------------*/
double fft_to_power(double_vec *p_fft, double fs, double fmin, double fmax)
{
    double power = 0.0;
    double freq_resolution = fs / (p_fft->len / 2.0); // p_fft is a complex vector
    double fmin_idx = ceil(fmin / freq_resolution) - 1;
    double fmax_idx = ceil(fmax / freq_resolution) - 1;

    #ifdef FHR_UA_DEBUG
	// printf("fresolution = %5.5f, fmin = %5.5f, fmax = %5.5f\n", 
    // freq_resolution, fmin_idx, fmax_idx);
    #endif

	// power of high frequency interference band  
    for(int i = (int)fmin_idx; i <= fmax_idx; i++)
	{
        power +=  pow(REAL(p_fft->p_data,i),2) + pow(IMAG(p_fft->p_data,i),2);
	}

	// TODO: normalize after sum 

    return power;
}
/*------------------------------------------------------------------------------
* Function Name: fft_to_force
* Description  : compute energy of uterine contraction from output of FFT 
*-----------------------------------------------------------------------------*/ 
double fft_to_uce(double_vec *p_fft, double fs, double fmin, double fmax)
{
	// uterine contraction energy 
	double uce = 0.0; 
    double freq_resolution = fs / (p_fft->len / 2.0);

	// index 
    double uc_min_idx = ceil(fmin / freq_resolution) - 1;
    double uc_max_idx = ceil(fmax / freq_resolution) - 1;

	// TODO energy = Omega * Omega * P * P
    for(int i = (int)uc_min_idx; i <= uc_max_idx; i++)
	{
		uce += pow(REAL(p_fft->p_data,i), 2.0) + pow(IMAG(p_fft->p_data,i), 2.0);
	}

	// TODO: normalize after sum 

	return uce; 
}


/*------------------------------------------------------------------------------
* Function Name: compute_uce_sqi 
* Description  : compute contraction energy and sqi for STFT windows. In other
* word, each value of contraction energy is attached with an SQI value. SQI value
* is computed as power of high frequency band or the ratio between power of UC 
* with power of high frequecy band. 
*------------------------------------------------------------------------------*/
double_vec *compute_uce_sqi_stft(struct param_stft param, double_vec *p_ecg)
{
	// number of chunk 
    unsigned int nchunk = floor((double)(p_ecg->len - param.wlen) / (double)param.nshift) + 1;

	// hann window 
	double_vec *p_hannw = double_vec_new(param.wlen);
	hannwin(p_hannw, param.wlen);

	// init vector of sqi 
    double_vec *p_uc = double_vec_new(2 * nchunk);

	// buffer for FFT complex number 
	double_vec *p_re  = double_vec_new(param.wlen);
	double_vec *p_cm  = double_vec_new(2 * param.wlen);

    for(unsigned int i = 0; i < nchunk; i++)
	{	
		// extract a chunk from ecg 
		double_vec_get_chunk(p_ecg, p_re, param.nshift * i);

		// hann window 
		double_vec_mul(p_hannw, p_re);

		// fft 
		fft_real_chunk(p_re, p_cm);

        // compute uterine contraction energy
        p_uc->p_data[2 * i] = fft_to_uce(p_cm, param.sampling_freq,
                       param.uc_min_freq, param.uc_max_freq);

        // compute SQI 
        p_uc->p_data[2 * i + 1] = fft_to_power(p_cm, param.sampling_freq,
                       param.hi_min_freq, param.hi_max_freq);
	} 

	// free mem  
	double_vec_delete(p_re);
	double_vec_delete(p_cm);
    double_vec_delete(p_hannw);

	return p_uc;
}

/*------------------------------------------------------------------------------
* Function Name: rmv_low_sqi_window 
* Description  : loop through each window, SQI < threshold, then null the window
* or null porition of the window
*-----------------------------------------------------------------------------*/ 
void rmv_low_sqi_window(struct param_tden param, double_vec *p_ecg,
                        double_vec *p_uc_sqi)
{
	// inint 
	double thresh = 0.0; 
	double_vec *p_sqi  = double_vec_new((unsigned int)((p_uc_sqi->len) / 2));
	double_vec *p_null = double_vec_new(param.sqi_wlen);

    // buffer vector of sqi to find median
	for (unsigned int i = 0; i < p_uc_sqi->len / 2; i++)
	{
		p_sqi->p_data[i] = p_uc_sqi->p_data[2*i + 1]; 
	}

	// find median of sqi 
	thresh = param.level_mad * find_median(p_sqi);

	// loop through ecg chunk by chunk 
	for (unsigned int i = 0; i < p_uc_sqi->len / 2; i++)
	{
		if (p_sqi->p_data[i] >= thresh)
		{
			// array bound check
			if (i * param.nshift + param.sqi_wlen >= p_ecg->len)
			{
				printf("ERROR ACCESS BOUND ARRAY: %s %d\n", __FILE__, __LINE__);	
				break;
			}
			else
			{
				double_vec_set_chunk(p_ecg, p_null, i * param.nshift);
	            // TODO: null portion of low SQI window
			}
		}
	}

	// free mem 
	 double_vec_delete(p_sqi);
	 double_vec_delete(p_null);
}

double_vec *scale_ua(double_vec *p_ua)
{
	// init output 
	double_vec *p_scale = double_vec_new(p_ua->len);

	// find median and remove outlier 
	double mad = find_median(p_ua); 

	// remove outlier by compare with mad 
	for (unsigned int i = 0; i < p_ua->len; i++)
	{
		if (fabs(p_ua->p_data[i]) > FHR_UA_SCALE_MAD * mad)
		{
			p_scale->p_data[i] = 0.0; 
			// p_scale->p_data[i] = p_ua->p_data[i];
		}
		else
		{
			p_scale->p_data[i] = p_ua->p_data[i];
		}
	}

	// find max 
	double max = find_max(p_scale);

	// sclae 
	for (unsigned int i = 0; i < p_ua->len; i++)
	{
		p_scale->p_data[i] = FHR_UA_MAX_AMP * (p_scale->p_data[i] / max); 
	}

	return p_scale;
}

/*------------------------------------------------------------------------------
* Function Name: compute_uc_trace
* Description  : as a main function of the ua unit, this function outputs a vec.
* of uterince contraction, and corressponding sqi value for each sample of
* contraction point.
*-----------------------------------------------------------------------------*/
struct uatrace *compute_uc_trace(double_vec *p_ecg)
{
	// check input 
	if (p_ecg == NULL)
	{
		printf("ACCESS NULL MEMORY: %s %d\n", __FILE__, __LINE__);
		exit(0);
	}

	// init output 
	struct uatrace *pp_ua = malloc(sizeof(struct uatrace)); 

    // STFT compute SQI per window with overlap
    #ifdef FHR_UA_DEBUG
    printf("FHR_UA::COMPUTE_UCE_SQI:STFT\n");
    #endif 
	double_vec *p_uc_sqi_raw = compute_uce_sqi_stft(g_param_sqi_stft, p_ecg);

	// wavelet to extract uterine contraction band 
	#ifdef FHR_UA_DEBUG
    printf("FHR_UA::EXTRACT_UC_FROM_ECG\n");
    #endif 
	extract_uc_from_ecg(g_param_wuc, p_ecg);

	// remove low sqi window 
	#ifdef FHR_UA_DEBUG
    printf("FHR_UA::RMV_LOW_SQI_WINDOW\n");
    #endif 
	rmv_low_sqi_window(g_param_tden, p_ecg, p_uc_sqi_raw);

	// time domain denoise to remove spikes 
	#ifdef FHR_UA_DEBUG
    printf("FHR_UA::RMV_SPIKE\n");
    #endif 
	rmv_spike(g_param_tden, p_ecg);

    // STFT compute Uterince Contraction per window with overlap
    #ifdef FHR_UA_DEBUG
    printf("FHR_UA::COMPUTE_UCE_SQI_STFT\n");
    #endif 
	double_vec *p_uc_sqi_fine = compute_uce_sqi_stft(g_param_uce_stft, p_ecg);

	// form output 
	unsigned int ualen = (p_uc_sqi_raw->len) / 2;  
	pp_ua->p_uc  = double_vec_new(ualen);
	pp_ua->p_sqi = double_vec_new(ualen);

	for (unsigned int i = 0; i < ualen ; i++)
	{
		pp_ua->p_sqi->p_data[i] = p_uc_sqi_raw->p_data[2 * i + 1]; 
		pp_ua->p_uc->p_data[i]  = p_uc_sqi_fine->p_data[2 * i]; 
	}

	// scale for plotting 
	#ifdef FHR_UA_DEBUG
	printf("FHR_UA::SCALE_UA\n");
	#endif 
	pp_ua->p_scaleuc = scale_ua(pp_ua->p_uc);

	// free mem 
	double_vec_delete(p_uc_sqi_raw);
	double_vec_delete(p_uc_sqi_fine);

	// return 
	return pp_ua;
}


void uatrace_write(struct uatrace *pp_ua, const char *p_path)
{
	// OPEN FILE FOR WRITTING
	FILE *p_file;
	p_file = fopen(p_path, "w");

	// CHECK ACCESS NULL POINTER 
	CHECK_ACCESS_NULL_POINTER(p_file);

	// WRITE PEAK TO FILE 
	for (unsigned int i = 0; i < pp_ua->p_sqi->len; i++)
	{
		fprintf(p_file, "%3.32f %3.32f %3.32f\n", pp_ua->p_sqi->p_data[i],
		pp_ua->p_uc->p_data[i],pp_ua->p_scaleuc->p_data[i]);
	}

	fclose(p_file);
}	

// gcc -std=c99 fhr_ua.c biorithm_aux.c fhr_ua.c -lm -lgsl -lwavelib -I /usr/local/include/gsl/