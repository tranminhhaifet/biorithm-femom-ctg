/*******************************************************************************
*      Copyright ©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: fhr_feature_classification.c
* Original Author: TRAN MINH HAI 
* Date created: 25 SEP 2019
* Purpose:  find maternal peak from tkeo, remove maternal peak from the inner 
* product, and finally find fetal peak from the inner product 
*
*
* Revision History:
* Ref No      Date           Who          	Detail
* <task ID>   <originated>   <originator> 	Original Code
* <task ID>   <DD MMM YYYY>  <initials>   	<fix/change description>
* NA 		  25 SEP 2019	 TRAN MINH HAI   
*******************************************************************************/
// SYSTEM INCLUDE 
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// LOCAL INCLUDE 
#include "biorithm_aux.h"				    
#include "fhr_findpeaks.h"				    
#include "fhr_feature_classification.h"		

/*------------------------------------------------------------------------------
* Function Name: shift_peak_loc(peak_vec *p, int offset)
* Description  : after findpeak for a window, it is needed to aligned the peak
* location for whole signal input. 
*-----------------------------------------------------------------------------*/ 
void shift_peak_loc(peak_vec *p, int offset)
{
	for(int i=0; i < VEC_LEN(p); i++)
	{
		P_VEC(p,i).loc = P_VEC(p,i).loc + offset;
	}
}

/*------------------------------------------------------------------------------
* Function Name: classify_peak(peak_vec *p, double thresh)
* Description  : classify maternal peak and fetal peak using magnitude thresh.
*-----------------------------------------------------------------------------*/ 
void classify_peak(peak_vec *p, double thresh)
{
	for(int i = 0; i < VEC_LEN(p); i++)
	{
		if (p->p_peak[i].mag > thresh)
		{
			p->p_peak[i].flag = true;
		} else
		{
			p->p_peak[i].flag = false;
		}
	}
}

/*------------------------------------------------------------------------------
* Function Name: concate_peak_vec(peak_vec *p_a, peak_vec *p_b)
* Description  : concate two peak vectors into one vector. 
*-----------------------------------------------------------------------------*/ 
void concate_peak_vec(peak_vec *p_a, peak_vec *p_b)
{
	// init sum vector s
	peak_vec *p_s = new_peak_vec(VEC_LEN(p_a) + VEC_LEN(p_b));

	// copy vector a into s 
	for(int i = 0; i < VEC_LEN(p_a); i++)
	{
		PEAK_COPY(P_VEC(p_s,i), P_VEC(p_a,i));
	}

	// copy vector b into s 
	for(int j = 0; j < VEC_LEN(p_b); j++)
	{
		PEAK_COPY(P_VEC(p_s, VEC_LEN(p_a) + j), P_VEC(p_b,j));
	}

	// free mem pointed by pointer 
	free(p_a->p_peak);

	// point poniter a to the sum vector 
	p_a->len = p_s->len;
	p_a->p_peak = p_s->p_peak;

	// free mem 
	delete_peak_vec(p_b);
	free(p_s);
}

/*------------------------------------------------------------------------------
* Function Name: find_peak_chunk_by_chunk(double_vec *p, int wlen, 
* double mpeak_thresh, double fpeak_thresh)
* Description  : findpeaks maternal and fetal chunk by chunk, each chunk of 
* wlen samples. 
*-----------------------------------------------------------------------------*/ 
peak_vec *find_peak_ecg(double_vec *p, int wlen, int dmin, double peak_thresh)
{
	// init 
	peak_vec *p_peak_ecg = new_peak_vec(0);    		// final output 
	peak_vec *p_peak_win = new_peak_vec(0); 		// peak vec of a window 
	double_vec *p_ecg_win = double_vec_new(wlen);   // a window of ecg 
	int nwin = (int)floor(VEC_LEN(p) / wlen);		// number of window 

	// paramters for findpeak 
	double std, sel = 0.0; 

	for(int i = 0; i < nwin; i++)
	{
		// extract tempwin
		double_vec_get_chunk(p, p_ecg_win, i * wlen);

		//  normalize
		normalize(p_ecg_win);

		// parameters for findpeaks 
		std = find_std(p_ecg_win);

		// findpeaks of a window ecg 
		peak_vec *p_peak_win = find_peaks(p_ecg_win, sel, std * peak_thresh, dmin); 			

		#ifdef FHR_FCLASS_DEBUG
		printf("std = %3.15f window = %d has %ul peak\n", std, i, 
		p_peak_win->len);
		#endif 

		// shift peak loc due to doing window by window find peak 
		shift_peak_loc(p_peak_win, (int)(i * wlen));

		// concate to p_peak_ecg 
		concate_peak_vec(p_peak_ecg, p_peak_win);

	}

	#ifdef FHR_FCLASS_DEBUG
	// printf("found %d peak\n", p_peak_ecg->len);
	#endif

	return p_peak_ecg;
}

/*------------------------------------------------------------------------------
// Function name: get_peak_width_rheight
// Description: 
// GET PEAK WIDTH WITH REFERENCE HEIGHT 
// Input: p_iprod is the inner product 
// Input: p_rpeak is reference peak where want to get width 
// Input: p_rheight is reference height (rheigh) for each peak as noise level 
// Output: peak_vec contain width information  
------------------------------------------------------------------------------*/
peak_vec *get_peak_width_rheight(double_vec *p_iprod, peak_vec *p_rpeak, 
double_vec *p_rheight, int wmax)
{
	// init param and output 
	unsigned int i, j, k, loc = 0; 
	peak_vec *p_w = new_peak_vec(p_rpeak->len);

	// loop through each reference peak 
	for (unsigned int i = 0; i < p_rpeak->len; i++)
	{
		// location of the peak 
		loc = p_rpeak->p_peak[i].loc;

		// check reference heigh greater than peak 
		if (p_rheight->p_data[i] > p_iprod->p_data[loc])
		{
			p_w->p_peak[i].ilb 	 = loc; 	
			p_w->p_peak[i].irb 	 = loc; 	
			p_w->p_peak[i].width = 0.0; 

		}
		else 
		{	
			// go left to find the fist sample less than p_rheight 
			j = 0; 
			for (j = 0; j < wmax; j++)
			{
				// check bound index 
				if (loc - j <= 0)
				{
					break;
				}

				// found the first sample less than p_rheight
				if (p_iprod->p_data[loc - j] <= p_rheight->p_data[i])
				{
					break;
				}
			}
			p_w->p_peak[i].ilb = loc - j; 

			// go right to find the first sample less than p_rheight
			k = 0; 
			for (k = 0; k < wmax; k++)
			{
				// check bound index 
				if ((loc + k) >= (p_iprod->len - 1))
				{
					break;
				}

				// found the first sample less than p_rheight
				if (p_iprod->p_data[loc + k] <= p_rheight->p_data[i])
				{
					break;
				}
			}
			p_w->p_peak[i].irb = loc + k;

			// get peak width of ith peak 
			p_w->p_peak[i].width = k + j; 

		}
		
		// set peak locs same as reference peak loc 
		p_w->p_peak[i].loc = p_rpeak->p_peak[i].loc;
	}

	// free mem

	// return 
	return p_w;
}

/*------------------------------------------------------------------------------
// Function name: rmv_mpeak_from_iprod
// Description: 
// REMOVE MATERNAL FROM INNTER PRODUCT 
// Input: p_rpeak contains maternal peak found int the best TKEO channel 
// Input: p_iprod the inner product wanted to clean
// Input: rheight is the reference height for finding weight (noise level)
// Input: wmax maximum width of QRS for finding peak width of mpeak 
// Output: the inner product after maternal peak is removed. 
------------------------------------------------------------------------------*/ 
void rmv_mpeak_from_iprod(double_vec *p_iprod, peak_vec *p_rpeak, int wlen,
double rheight, int wmax)
{
	// peak loc 
	unsigned int pkloc = 0;
	// number of window 
	unsigned int nwin = floor(p_iprod->len / wlen);
	// vector of mean for each window of iprod 
	double_vec *p_mean = double_vec_new(nwin);
	// buffer window 		
	double_vec *p_win = double_vec_new(wlen);		
	// vector of rheight for each peak 
	double_vec *p_rheight = double_vec_new(p_rpeak->len); 

	// compute mean for each window of the inner product 
	for (unsigned int i = 0; i < nwin; i++)
	{
		// get a window of the inner product 
		double_vec_get_chunk(p_iprod, p_win, i * wlen);

		// normalize the window 
		// normalize(p_win);

		// update the iprod 
		// double_vec_set_chunk(p_iprod, p_win, i * wlen);

		// compute rheight for all peak of ith window 
		p_mean->p_data[i] = find_mean(p_win) * rheight; 
	}

	// compute rheight for each peak 
	for (unsigned int i = 0; i < p_rheight->len; i++)
	{
		// location of the peak 
		pkloc = p_rpeak->p_peak[i].loc;

		// find the window contain the peak loc 
		p_rheight->p_data[i] = p_mean->p_data[(unsigned int)(pkloc / wlen)]; 
	}

	// get peak width for each peak 
	peak_vec *p_mpeak = get_peak_width_rheight(p_iprod, p_rpeak, p_rheight, wmax);

	#ifdef FHR_FCLASS_DEBUG
	// print_peak_vec(p_mpeak,10);
	#endif

	// removal mpeak from inner product 
	for (unsigned int i = 0; i < p_rpeak->len; i++)
	{
		if (p_mpeak->p_peak[i].ilb < p_mpeak->p_peak[i].irb)
		{
			#ifdef FHR_FCLASS_DEBUG
			// printf("OK\n");
			#endif 
			for (unsigned int j = p_mpeak->p_peak[i].ilb; j <= p_mpeak->p_peak[i].irb; j++)
			{
				// TODO: check bound before accessing 
				p_iprod->p_data[j] = 0.0; 
			}
		}
	}

	// free mem
	double_vec_delete(p_mean);
	double_vec_delete(p_rheight);
	double_vec_delete(p_win);
}

/*------------------------------------------------------------------------------
// FEATURE CLASSIFICATION 
// Funtion Name: compute_mfpeak 
// Description: output both maternal and fetal peak with input tkeo and iprd.
// Input: tkeo of the best channel 
// Input: inner product of four channels 
// Input: wlen window length 
// Input: p_mfpeak_dmin is dmin for maternal and fetal peak 
// Input: p_mfpeak_hmin is the minimum magnitude of mfpeak for each channel
// Input: p_rheight is reference height for rmv maternal each channel
// Input: wmax is max width of maternal peak for rmv maternal 
// Output: maternal peak vector of the best tkeo channel 
// Output: four fetal peak vector corressponding to four channel 
-----------------------------------------------------------------------------*/
struct mfpeak *compute_mfpeak(double_vec **pp_tkeo, double_vec **pp_iprod,
unsigned int wlen, int *p_mfpeak_dmin, double *p_mfpeak_hmin, 
double *p_rheight, int wmax, int best_mch_id)
{
	// init output and param
	struct mfpeak *p_mfpeak = malloc(sizeof(struct mfpeak));
	p_mfpeak->p_fpeak[0]=NULL; 
	p_mfpeak->p_fpeak[1]=NULL;
	p_mfpeak->p_fpeak[2]=NULL;
	p_mfpeak->p_fpeak[3]=NULL;

	p_mfpeak->p_mpeak[0]=NULL; 
	p_mfpeak->p_mpeak[1]=NULL;
	p_mfpeak->p_mpeak[2]=NULL;
	p_mfpeak->p_mpeak[3]=NULL;

	// FIND MPEAK FROM ALL CHANNEL 
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		p_mfpeak->p_mpeak[i] = find_peak_ecg(pp_tkeo[i], wlen, p_mfpeak_dmin[0],
		p_mfpeak_hmin[i]);

		#ifdef FHR_FCLASS_DEBUG
		// printf("found %ul maternal peak\n", p_mfpeak->p_mpeak[i]->len);
		#endif
	}

	// for each electrode channel 
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		// removel maternal from inner product 
		rmv_mpeak_from_iprod(pp_iprod[i], p_mfpeak->p_mpeak[best_mch_id], 
			wlen, p_rheight[i], wmax);

		// LOG
		#ifdef FHR_FCLASS_LOG
		#endif

		// find fetal peak 
		p_mfpeak->p_fpeak[i] = find_peak_ecg(pp_iprod[i], wlen, 
			p_mfpeak_dmin[1], p_mfpeak_hmin[FHR_NCHANNEL + i]);
	}

	#ifdef FHR_FCLASS_DEBUG
	// write_to_txt(pp_iprod,1,"./data/iprod_rmv_mpeak_c.txt");
	#endif	

	// return 
	return p_mfpeak;
}
