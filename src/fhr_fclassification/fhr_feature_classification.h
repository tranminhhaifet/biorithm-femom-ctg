/*******************************************************************************
*      Copyright ©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_findpeaks.h
* Original Author: TRAN MINH HAI 
* Date created: 24 SEP 2019
* Purpose: Implement findpeaks algorithm 
*
*
* Revision History:
* Ref No      Date           Who          	Detail
* <task ID>   <originated>   <originator> 	Original Code
* NA          24 SEP 2019 	 TRAN MINH HAI  
*******************************************************************************/

#ifndef FHR_FEATURE_CLASSIFICATION
#define FHR_FEATURE_CLASSIFICATION

// SYSTEM INCLUDES
#include<stdbool.h>
#include<stdlib.h>

// LOCAL INCLUDE
#include "biorithm_aux.h"
#include "fhr_findpeaks.h"

// CONSTANT 
#define FHR_MPEAK_THRESH_CH1 	2.0  // MATERNAL PEAK > THRESH 
#define FHR_MPEAK_THRESH_CH2 	2.0  // MATERNAL PEAK > THRESH 
#define FHR_MPEAK_THRESH_CH3 	2.0  // MATERNAL PEAK > THRESH 
#define FHR_MPEAK_THRESH_CH4 	2.0  // MATERNAL PEAK > THRESH 
#define FHR_MPEAK_THRESH_BCH    2.0  // MATERNAL PEAK > THRESH

#define FHR_FPEAK_THRESH_CH1 	2.0  // FETAL PEAK > THRESH
#define FHR_FPEAK_THRESH_CH2 	0.1  // FETAL PEAK > THRESH
#define FHR_FPEAK_THRESH_CH3 	0.1  // FETAL PEAK > THRESH
#define FHR_FPEAK_THRESH_CH4 	0.1  // FETAL PEAK > THRESH

#define FHR_FINDPEAK_WLEN       5000 // 10 * FREQ. SAMPLING
#define FHR_MINPEAK_DIST 	    10.0 // PEAK DISTANCE > THRESH
#define FHR_MPEAK_WIDTH_MAX 	50   // MAX WIDTH OF MPEAK TKEO DOMAIN
#define FHR_MPEAK_HREF 	        2.0  // REFERENCE HEIGHT TO FIND WIDTH

#ifndef FHR_SAMPLING_FREQ
#define FHR_SAMPLING_FREQ 	    500  // SAMPLING FREQ  
#endif

#ifndef FHR_NCHANNEL
#define FHR_NCHANNEL			4 	// NUMBER OF ELECTRODE CHANNEL 
#endif

// DEBUG MODE
#ifndef FHR_FCLASS_DEBUG
#define FHR_FCLASS_DEBUG
#endif

// LOG 
#define FHR_FCLASS_LOG 

// DEFINE DATA TYPE
struct mfpeak 
{
	peak_vec *p_mpeak[FHR_NCHANNEL];
	peak_vec *p_fpeak[FHR_NCHANNEL];
};

/*------------------------------------------------------------------------------
* Function Name: concate_peak_vec(peak_vec *p_a, peak_vec *p_b)
* Description  : concate two peak vectors into one vector 
*-----------------------------------------------------------------------------*/ 
void concate_peak_vec(peak_vec *p_a, peak_vec *p_b);

/*------------------------------------------------------------------------------
* Function Name: shift_peak_loc(peak_vec *p, int offset)
* Description  : after findpeak for a window, it is needed to aligned the peak
* location for whole signal input. 
*-----------------------------------------------------------------------------*/ 
void shift_peak_loc(peak_vec *p, int offset);

/*------------------------------------------------------------------------------
* Function Name: find_peak_chunk_by_chunk(double_vec *p, int wlen, 
* double mpeak_thresh, double fpeak_thresh)
* Description  : findpeaks maternal and fetal chunk by chunk, each chunk of 
* wlen samples. 
*-----------------------------------------------------------------------------*/ 
peak_vec *find_peak_ecg(double_vec *p, int wlen, int dmin, double peak_thresh);

/*------------------------------------------------------------------------------
* Function Name: classify_peak(peak_vec *p, double thresh)
* Description  : classify Maternal peak versus Fetal peak based on peak 
* magnitude and peak width 
*-----------------------------------------------------------------------------*/ 
void classify_peak(peak_vec *p, double thresh);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
peak_vec *get_peak_width_rheight(double_vec *p_iprod, peak_vec *p_rpeak, 
	double_vec *p_rheight, int wmax);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
void rmv_mpeak_from_iprod(double_vec *p_iprod, peak_vec *p_rpeak, int wlen,
double rheight, int wmax);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
struct mfpeak *compute_mfpeak(double_vec **pp_tkeo, double_vec **pp_iprod,
unsigned int wlen, int *p_mfpeak_dmin, double *p_mfpeak_hmin,
double *p_rheight, int wmax, int best_mch_id);


#endif //FHR_FEATURE_CLASSIFICATION
