/*******************************************************************************
*      Copyright Ã‚Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.h
* Original Author: TRAN MINH HAI
* Date created: 03 JUL 2019
* Purpose: header file for BIORITHM_AUX which providing supporting functions for
* other units such as find_median, smooth, etct.
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             15 JUN 2019    T.H          Original Code
*             21 AUG 2019    T.H          Rework 
*             02 SEP 2019    T.H          Rework Error Handling
********************************************************************************/
#ifndef BIORITHM_AUX_HEADER
#define BIORITHM_AUX_HEADER

// SYSTEM INCLUCDE
#include <math.h>
#include <float.h>
#include <errno.h>
#include <stdio.h>

// GSL GNU LIB
#include <gsl/gsl_complex.h>

// DEBUG ENALBE
#define BIORITHM_AUX_DEBUG

// CONST 
#ifndef SMOOTH_RGAUSSIAN_NITER          // NUMBER INTERATION RGAUSS SMOOTH
#define SMOOTH_RGAUSSIAN_NITER 5
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// MACRO PRINT POINT 
#define PRINT_POINT(p) {printf("x = %3.15f, y = %3.15f\n", (p).x, (p).y);}

// MACRO MEM ALLOCATION ERROR 
#define CHECK_ALLOC_ERROR(P)                                              \
    if ((P) == NULL)                                                      \
    {                                                                     \
        printf("ERROR MEMORY ALLOCATION (%s:%d)\n", __FILE__, __LINE__);  \
        return NULL;                                                      \
    }                                                                     \

// MACRO CHECK DIVISOR ZERO 
#define CHECK_DIVISOR_ZERO(X)                                             \
    {                                                                     \
        int _val = (X);                                                   \
        if(fabs(_val) <= FLT_MIN)                                         \
        {                                                                 \
            printf("ERROR DIVISOR ZERO (%s:%d)\n", __FILE__, __LINE__);   \
            exit(-1);                                                     \
        }                                                                 \
    }                                                                     \

// MACRO CHECK ACCESS NULL POINTER
#define CHECK_ACCESS_NULL_POINTER(P)                                      \
    if((P)==NULL)                                                         \
    {                                                                     \
        printf("ERROR ACCESS NULL POINTER (%s:%d)\n", __FILE__, __LINE__);\
        exit(-1);                                                         \
    }                                                                     \

// MACRO CHECK ACCESS NULL POINTER                                                             
#define ERROR_OVERFLOW                                                   \
    {printf("ERROR OVERFLOW (%s:%d)\n", __FILE__, __LINE__); exit(-1);}  \

// MACRO CHECK ARRAY BOUND
#define CHECK_ARRAY_BOUND(i, imax)                                       \
    if((i) >= (imax))                                                    \
    {                                                                    \
        printf("ERROR ARRAY BOUND (%s:%d)\n", __FILE__, __LINE__);       \
        exit(-1);                                                        \
    }                                                                    \

// DOUBLE_VECTOR 
typedef struct {
    size_t len;
    double *p_data;
} double_vec;

// INTEGER VECTOR 
typedef struct {
        int len;
        int *p_data;
    } int_vec;

// POINT STRUCT 
typedef struct {
    double x;
    double y;
} point;

// POINT VECTOR 
typedef struct {
    point *p_data;
    size_t len;
} point_vec;

// COMPLEX VECTOR 
typedef struct {
    int len; 
    gsl_complex *p_data; 

} complex_double_vec;

// OPERATIONS 
/*==============================================================================
*                          AUX DATA STRUCTURE & ALGORITHM 
* 1. VECTOR data type and associated algorithms including SORT, MEDIAN, MEAN, 
*    DOT PRODUCT, SCALE, NORM, PRINT 
* 2. POINT data type (x,y) and associated algorithms FIND CROSSING ZERO
*=============================================================================*/
// INTEGER VECTOR 
int_vec *int_vec_new(size_t len);

void int_vec_delete(int_vec *p);

// DOUBLE VECTOR 
double_vec *double_vec_new(size_t len);

void double_vec_delete(double_vec *p);

size_t double_vec_length(double_vec *p);

void double_vec_set(double_vec *p, size_t idx, double x);

double double_vec_get(double_vec *p, size_t idx);

void double_vec_print(double_vec *p, size_t n);

void double_vec_add(double_vec *p_s, double_vec *p_d);

void double_vec_sub(double_vec *p_s, double_vec *p_d);

void double_vec_mul(double_vec *p_s, double_vec *p_d);

double double_vec_dot(double_vec *p_a, double_vec *p_b);

void double_vec_scale(double_vec *p_a, double scale);

void double_vec_abs(double_vec *p);

void double_vec_sort(double_vec *p);

double double_vec_sum(double_vec *p);

void double_vec_copy(double_vec *p_s, double_vec *p_d);

void double_vec_get_chunk(double_vec *p_o, double_vec *p_c, size_t sidx);

void double_vec_set_chunk(double_vec *p_o, double_vec *p_c, size_t sidx);

void double_vec_read(double_vec *, const char *p_path);

// COMPLEX DOUBLE VEC 
complex_double_vec *complex_double_vec_new(int len);

// POINT VECTOR 
point_vec *point_vec_new(size_t len);

void point_vec_delete(point_vec *p);

point point_vec_get(point_vec *p, size_t idx);

void point_vec_set(point_vec *p, size_t idx, point tmp);

void point_vec_print(point_vec *p, size_t n);

void double_vec_write(double_vec *p_d, char const *p_path);

// ALGORITHM 
void mad_weight(double_vec *p_o, double_vec *p_x, double_vec *p_w);

int compare_double(const void *p1,  const void *p2);

double find_median(double_vec *p);

double find_mean(double_vec *p);

double find_min(double_vec *p);

double find_max(double_vec *p);

double find_max_abs(double_vec *p);

double find_std(double_vec *p);

double find_quantile(double_vec *p_x, double qthresh);

void normalize(double_vec *p);

//void mad_weight(double_vec *p_o, double_vec *p_x, double_vec *p_w);

double_vec *find_cross_zero(double_vec *p_x, double_vec *p_y);

/*==============================================================================
*                                 AUX IO 
* 1. read from text with multiple columns (TODO)
* 2. write to text with multiple columns 
*===============================================================================*/

void read_from_txt(const char* p_path, double_vec *p_buff);

void read_from_txt_cols(const char* p_path, double_vec **p_buff, int ncol);

void write_to_txt(double_vec **p_d, unsigned int ncol, const char* p_path);

char *str_concat(char *p1, char *p2);

/*==============================================================================
*                                 AUX SIGNAL PROCESSING 
* 1. Design GAUSS and HANN window 
* 2. SMOOTH with methods: GAUSS, RGAUSS, LOWESS, RLOWESS, MEDIAN, AVERAGING 
*===============================================================================*/
// DESIGN WINDOW
void hannwin(double_vec *p_hw, unsigned int N);

void gausswin(double_vec *p_gw, unsigned int wlen, double alpha);

void kaiserwin(double_vec *p_kw, unsigned int wlen, double beta);

// SMOOTH AND FITTING 
void smooth(double_vec *p, unsigned int wlen, const char *method, double alpha);

void smoothg(double_vec *p, unsigned int wlen, double alpha);

void smoothrg(double_vec *p, unsigned int wlen, double alpha, unsigned int niter);

void smoothwg(double_vec *p_x, unsigned int wlen, double alpha, double_vec *p_w);

// FILTER DESIGN 

// FILTER OPERATION 
double_vec *conv(double_vec *p_x, double_vec *p_h, const char *mode);

// HILBERT 

// TEO 

// FIND PEAKS

#endif // BIOARITHM_AUX_HEADER