/*******************************************************************************
*      Copyright Ã‚Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 03 JUL 2019
* Purpose: clean and re-oganise code, all auxlilary functions will be here
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             03 JUL 2019    T.H          Original Code
*             21 AUG 2019    T.H          Rework Seperate Auxiliary
*             02 SEP 2019    T.H          Rework Eror Handling
*             16 SEP 2019    T.H          Rework add Kaiser window, conv
*******************************************************************************/
// SYSTEM INCLUDES
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stddef.h>

// GSL GNU LIB 
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_complex.h>

// LOCAL INCLUDES
#include "biorithm_aux.h"

// OPERATIONS 
/*==============================================================================
*                          AUX DATA STRUCTURE & ALGORITHM 
* 1. VECTOR data type and associated algorithms including SORT, MEDIAN, MEAN, 
*    DOT PRODUCT, SCALE, NORM, PRINT 
* 2.  data type (x,y) and associated algorithms FIND CROSSING ZERO
*===============================================================================*/

/*------------------------------------------------------------------------------
* Function Name: int_vec_new 
* Description  : create a vector of integer given len and init to ZERO
*-----------------------------------------------------------------------------*/
int_vec *int_vec_new(size_t len)
{
    if (len == 0 || len > SIZE_MAX / sizeof(int)) 
    {
        ERROR_OVERFLOW;
        return NULL;
    }
    int_vec *p_vec = malloc(sizeof(int_vec));
    CHECK_ALLOC_ERROR(p_vec);

    // allocate and init to zero by calloc
    p_vec->len = len;
    p_vec->p_data = calloc(len, sizeof(int));
    CHECK_ALLOC_ERROR(p_vec->p_data);

    return p_vec;
}

/*------------------------------------------------------------------------------
* Function Name: int_vec_delete
* Description  : 
*-----------------------------------------------------------------------------*/
void int_vec_delete(int_vec *p)
{
    CHECK_ACCESS_NULL_POINTER(p);
    free(p->p_data);
    free(p);
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_new
* Description  : create a vector of double given len and init to ZERO
*-----------------------------------------------------------------------------*/
double_vec * double_vec_new(size_t len)
{
    if (len == 0 || len > SIZE_MAX / sizeof(double)) 
    {
        ERROR_OVERFLOW;
        return NULL;
    }
    double_vec *p_vec = malloc(sizeof(double_vec));
    CHECK_ALLOC_ERROR(p_vec);

    // allocate and init to zero by calloc
    p_vec->len = len;
    p_vec->p_data = calloc(len, sizeof(double));
    CHECK_ALLOC_ERROR(p_vec->p_data);

    #ifdef BIORITHM_AUX_DEBUG
    // printf("length = %ul\n", p_vec->len);
    #endif 

    return p_vec;
}


/*------------------------------------------------------------------------------
* Function Name: complex_double_vec_new
* Description  : create a vector of complex double given len and init to ZERO
*-----------------------------------------------------------------------------*/
complex_double_vec *complex_double_vec_new(int len){

    complex_double_vec *p_vec = malloc(sizeof(complex_double_vec));
    p_vec->p_data = malloc(len * sizeof(gsl_complex));
    p_vec->len = len;

    for (int i = 0; i < len; ++i)
     {
        GSL_REAL(p_vec->p_data[i]) = 0.0;
        GSL_IMAG(p_vec->p_data[i]) = 0.0;
     } 

     return p_vec;
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_get
* Description  : get an number from vector given its index 
*-----------------------------------------------------------------------------*/ 
double double_vec_get(double_vec *p, size_t idx)
{   
    // error access NULL pointer 
    CHECK_ACCESS_NULL_POINTER(p);

    // error access over bound array 
    CHECK_ARRAY_BOUND(idx, p->len);

    // correct index 
    return p->p_data[idx];
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_set
* Description  : insert a number to vector at location idx
*-----------------------------------------------------------------------------*/ 
void double_vec_set(double_vec *p, size_t idx, double x)
{
    // error access NULL pointer 
    CHECK_ACCESS_NULL_POINTER(p);

    // error access over bound array 
    CHECK_ARRAY_BOUND(idx, p->len);

    // correct index 
    p->p_data[idx] = x;

}

/*------------------------------------------------------------------------------
* Function Name: double_vec_length
* Description  : get length of double vector 
*-----------------------------------------------------------------------------*/ 
size_t double_vec_length(double_vec *p)
{
    CHECK_ACCESS_NULL_POINTER(p);
    return p->len;
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_delete
* Description  : create a vector of integer, init to ZERO
*-----------------------------------------------------------------------------*/
void double_vec_delete(double_vec *p)
{
    CHECK_ACCESS_NULL_POINTER(p);
    free(p->p_data);
    free(p);
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_print
* Description  : print n first element of a double vector
*-----------------------------------------------------------------------------*/
void double_vec_print(double_vec *p, size_t n)
{
    CHECK_ACCESS_NULL_POINTER(p);

    for (size_t i = 0; (i < double_vec_length(p) && i < n); i++) 
    {
        printf("p[%zu] = %3.24f\n", i, double_vec_get(p,i));
    }
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_write
* Description  : write double vecto to destinated path 
*-----------------------------------------------------------------------------*/ 
void double_vec_write(double_vec *p_d, char const *p_path)
{
    FILE *p_file;
    p_file = fopen(p_path, "w");

    // TODO EROR OPEN FILE HANDLE 

    for (size_t i = 0; i < double_vec_length(p_d); i++)
    {
         fprintf(p_file, "%2.24f\n", double_vec_get(p_d,i));
    }

    // close file 
    fclose(p_file);
}


/*------------------------------------------------------------------------------
* Function Name: double_vec_sort
* Description  : sort double vec using qsort, default ascending order 
*-----------------------------------------------------------------------------*/ 
void double_vec_sort(double_vec *p)
{
    // TODO handle error 
    qsort(p->p_data, p->len, sizeof(double), compare_double);

}

/*------------------------------------------------------------------------------
* Function Name: double_vec_sum
* Description  : 
*-----------------------------------------------------------------------------*/ 
double double_vec_sum(double_vec *p)
{
  double sum = 0.0; 

  for (size_t i = 0; i < p->len; i++) 
  {
    sum = sum + p->p_data[i]; 
  }

  return sum; 
}

/*------------------------------------------------------------------------------
* Function Name: find_quantile
* Description  : find a threshold so that (a < threshold) contains q percent 
* of energy. For example threshold = find_quantile(x, 0.99), so sample smaller 
* than threshold will contain 0.99 total energy of x. 
*-----------------------------------------------------------------------------*/ 
double find_quantile(double_vec *p_x, double qthresh)
{
    double qvalue    = 0.0;
    double total     = 0.0;  
    double cumsum    = 0.0; 
    double_vec *p_b  = double_vec_new(p_x->len);

    // buffer original signal 
    double_vec_set_chunk(p_b, p_x, 0);

    // convert to abs 
    double_vec_abs(p_b);

    // sort by acesding order 
    double_vec_sort(p_b);

    // cumsum and find threshold 
    double threshold = total = qthresh * double_vec_sum(p_b);

    // 
    unsigned int i = 0;
    for (i = 0; i < p_x->len; i++)
    {
        cumsum = cumsum + p_b->p_data[i];

        if (cumsum > threshold)
        {
            break;
        }
    }

    // found qvalue 
    qvalue = p_b->p_data[i]; 

    // free mem 
    double_vec_delete(p_b);

    return qvalue;
}



/*------------------------------------------------------------------------------
* Function Name: double_vec_add
* Description  : add source p_s to destimation p_d
*-----------------------------------------------------------------------------*/
void double_vec_add(double_vec *p_s, double_vec *p_d)
{
    double tmp = 0.0; 
    for (size_t i = 0; i < double_vec_length(p_d); i++) 
    {
        tmp = double_vec_get(p_d, i) + double_vec_get(p_s, i);
        double_vec_set(p_s, i, tmp);
    }
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_sub
* Description  : substract source p_s from destimation p_d
*-----------------------------------------------------------------------------*/
void double_vec_sub(double_vec *p_s, double_vec *p_d)
{
    double tmp = 0.0;
    for (size_t i = 0; i < double_vec_length(p_s); i++) 
    {
        tmp = double_vec_get(p_d, i) - double_vec_get(p_s, i);
        double_vec_set(p_d, i, tmp);
    }
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_mul
* Description  : elemense wise multiple the destimation p_d by source p_s
*-----------------------------------------------------------------------------*/
void double_vec_mul(double_vec *p_s, double_vec *p_d)
{
    double tmp = 0.0;
    for (size_t i = 0; i < double_vec_length(p_s); i++) 
    {
        tmp = double_vec_get(p_s, i) * double_vec_get(p_d, i);
        double_vec_set(p_d, i, tmp);
    }
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_dot
* Description  : 
*-----------------------------------------------------------------------------*/
double double_vec_dot(double_vec *p_a, double_vec *p_b)
{
    double tmp = 0;
    for (size_t i = 0; i < double_vec_length(p_a); i++) 
    {
        tmp = tmp + double_vec_get(p_a,i) * double_vec_get(p_b,i);
    }
    return tmp;
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_scale
* Description  :
*-----------------------------------------------------------------------------*/
void double_vec_scale(double_vec *p_a, double scale)
{
    double tmp = 0.0;
    for (size_t i = 0; i < double_vec_length(p_a); i++) 
    {
        tmp = double_vec_get(p_a,i) * scale;
        double_vec_set(p_a,i,tmp);
    }
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_scale
* Description  :
*-----------------------------------------------------------------------------*/
void normalize(double_vec *p)
{
    double max = find_max_abs(p);

    // check max before divinng 
    if (max > 0)
    {
        for(int i = 0; i < p->len; i++)
        {
            p->p_data[i] = p->p_data[i] / fabs(max); 
        }   
    }
}


/*------------------------------------------------------------------------------
* Function Name: double_vec_scale
* Description  : copy source p_s to destimation p_d
*-----------------------------------------------------------------------------*/
void double_vec_copy(double_vec *p_s, double_vec *p_d)
{
    double tmp = 0.0;
    for (size_t i = 0; i < double_vec_length(p_s); i++) 
    {
        tmp = double_vec_get(p_s, i);
        double_vec_set(p_d, i, tmp);
    }
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/
void double_vec_get_chunk(double_vec *p_o, double_vec *p_c, size_t sidx)
{
    for(size_t i = 0; i < p_c->len; i++)
    {
        p_c->p_data[i] = p_o->p_data[sidx + i];
    }
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  :
*-----------------------------------------------------------------------------*/
void double_vec_set_chunk(double_vec *p_o, double_vec *p_c, size_t sidx)
{
    for(size_t i = 0; i < p_c->len; i++)
    {
        p_o->p_data[sidx + i] =  p_c->p_data[i];

    }
}

/*------------------------------------------------------------------------------
* Function Name: abs_vec
* Description  :
*-----------------------------------------------------------------------------*/
void double_vec_abs(double_vec *p)
{
    double tmp = 0.0; 
    for (size_t i = 0; i < double_vec_length(p); i++) 
    {
        tmp = fabs(double_vec_get(p,i));
        double_vec_set(p, i, tmp) ;
    }
}

/*------------------------------------------------------------------------------
* Function Name: double_vec_read
* Description  : read from txt 
*-----------------------------------------------------------------------------*/

void double_vec_read(double_vec *p, const char *p_path)
{
    FILE *p_file;
    p_file = fopen(p_path,"r");

    // TODO handle open file error 

    // read from txt to double vec 
    double tmp = 0.0;

    for(size_t i = 0; i < double_vec_length(p); i++)
    {
        fscanf(p_file, "%lg", &tmp);
        double_vec_set(p, i, tmp);
    }
}

/*------------------------------------------------------------------------------
* Function Name: point_vec_new
* Description  : create a vector of point
*-----------------------------------------------------------------------------*/
point_vec *point_vec_new(size_t len)
{
    if (len == 0 || len > SIZE_MAX / sizeof(point)) 
    {
        ERROR_OVERFLOW;
        return NULL;
    }
    point_vec *p = malloc(sizeof (point_vec));
    CHECK_ALLOC_ERROR(p);

    p->len = len;
    p->p_data = calloc(len, sizeof(point));
    CHECK_ALLOC_ERROR(p->p_data);
    
    return p;
}

/*------------------------------------------------------------------------------
* Function Name: point_vec_get
* Description  : 
*-----------------------------------------------------------------------------*/
point point_vec_get(point_vec *p, size_t idx)
{
    // TODO handle error 

    point tmp; 
    tmp.x = (p->p_data[idx]).x;
    tmp.y = (p->p_data[idx]).y;
    return tmp;
}

/*------------------------------------------------------------------------------
* Function Name: point_vect_set
* Description  : 
*-----------------------------------------------------------------------------*/
void point_vec_set(point_vec *p, size_t idx, point tmp)
{
    // TODO handle error 

    (p->p_data[idx]).x = tmp.x;
    (p->p_data[idx]).y = tmp.y;
}


/*------------------------------------------------------------------------------
* Function Name: point_vec_length
* Description  : 
*-----------------------------------------------------------------------------*/
size_t point_vec_length(point_vec *p)
{
    if (p==NULL)
    {
        CHECK_ACCESS_NULL_POINTER(p);
        exit(1);
    }
    return p->len;
}

/*------------------------------------------------------------------------------
* Function Name: point_vec_print
* Description  : print first n point of point vec to console
*-----------------------------------------------------------------------------*/
void point_vec_print(point_vec *p, size_t n)
{
    CHECK_ACCESS_NULL_POINTER(p);

    point tmp; 
    for (size_t i = 0; i < point_vec_length(p) && i < n; i++)
    {
        tmp = point_vec_get(p, i);
        PRINT_POINT(tmp);
    }
}

/*------------------------------------------------------------------------------
* Function Name: point_vec_delete
* Description  :
*-----------------------------------------------------------------------------*/
void point_vec_delete(point_vec *p)
{
    CHECK_ACCESS_NULL_POINTER(p);
    free(p->p_data);
    free(p);
}
/*------------------------------------------------------------------------------
* Function Name: compare_double
* Description  :
*-----------------------------------------------------------------------------*/
int compare_double(const void *p1, const void *p2)
{
    if ((*(const double *)p1) > (*(const double *)p2))
    {
        return 1;
    }

    return -1;

}


/*------------------------------------------------------------------------------
* Function Name: find_mean
* Description  : find mean of a double vector  
*-----------------------------------------------------------------------------*/
double find_mean(double_vec * p)
{
    double mean = 0.0;

    for (unsigned int i = 0; i < p->len; i++)
    {
        mean = mean + p->p_data[i];
    }

    mean = mean / p->len;

    return mean; 
}

/*------------------------------------------------------------------------------
* Function Name: find_min
* Description  : find min of a double vector  
*-----------------------------------------------------------------------------*/
double find_min(double_vec *p)
{
    double min = p->p_data[0];

    for (unsigned int i = 1; i < p->len; i++)
    {
        if (min  > p->p_data[i]) 
        {
            min = p->p_data[i];
        }
    }

    return min;
}

/*------------------------------------------------------------------------------
* Function Name: find_max
* Description  : find max of a double vector  
*-----------------------------------------------------------------------------*/
double find_max(double_vec *p)
{
    double max = p->p_data[0];

    for (unsigned int i = 1; i < p->len; i++) 
    {
        if (max < p->p_data[i])
        {
            max = p->p_data[i];
        }
    }

    return max; 
}

/*------------------------------------------------------------------------------
* Function Name: find_max_abs
* Description  : find max of a double vector (abs)
*-----------------------------------------------------------------------------*/
double find_max_abs(double_vec *p)
{
    double max = fabs(p->p_data[0]);

    for (unsigned int i = 1; i < p->len; i++) 
    {
        if (max < fabs(p->p_data[i]))
        {
            max = fabs(p->p_data[i]);
        }
    }

    return max; 
}

/*------------------------------------------------------------------------------
* Function Name: find_std
* Description  : find std of a double vector  
*-----------------------------------------------------------------------------*/
double find_std(double_vec *p)
{
    // init 
    double std = 0.0;

    // find mean of input  
    double mean = find_mean(p);

    for (unsigned int i = 0; i < p->len; i++)
    {
        std = std + pow(p->p_data[i] - mean, 2);
    }

    std = sqrt(std / (p->len - 1));

    return std; 
}

/*------------------------------------------------------------------------------
* Function Name: find_median
* Description  : the function finds median of a double vector. The function 
* make use of standard qsort from <math.h> which required compare_double function
*-----------------------------------------------------------------------------*/
double find_median(double_vec *p)
{
    double median = 0.0;

    // error access NULL pointer 
    CHECK_ACCESS_NULL_POINTER(p);

    // buffer the original data 
    size_t N = double_vec_length(p);
    double_vec *p_b = double_vec_new(N);
    double_vec_copy(p, p_b);

    // sort in ascending order
    qsort(p_b->p_data, p_b->len, sizeof(double), compare_double);

    if (N % 2 == 0) 
    {
        unsigned int mid = (unsigned int)(N/2);
        median = 0.5 * (double_vec_get(p_b, mid) + double_vec_get(p_b, mid-1));
    } else 
    {
        unsigned int mid = (unsigned int)((N-1)/2);
        median = double_vec_get(p_b, mid);
    }

    // free mem 
    double_vec_delete(p_b);

    return median;
}

/*------------------------------------------------------------------------------
* Function Name: mad_weight (bisquare weight function)
* Description  : the function compute weights for robust locally weighted linear
* regression and weights for weighted Gaussian smooth, bisquare weight function. 
* REF. "Robust Locally Weighted Regression and Smoothing Scatterplots", William 
* S. Cleveland, 1979
*-----------------------------------------------------------------------------*/
void mad_weight(double_vec *p_o, double_vec *p_x, double_vec *p_w)
{
    double MAD = 0.0;
    double r   = 0.0;

    // buffer original vector data 
    size_t N = double_vec_length(p_o);
    double_vec *p_bx = double_vec_new(N);
    double_vec *p_bo = double_vec_new(N);
    double_vec_copy(p_x, p_bx);
    double_vec_copy(p_o, p_bo);

    // difference between two vector
    double_vec_sub(p_bx, p_bo); // substract p_bx from p_bo
    double_vec_abs(p_bo); // convert to abs

    // find median SHOULD BE > 0 
    MAD = find_median(p_bo) + 0.0000001; 

    // MAD weight
    for (unsigned int i = 0; i < N; i++) 
    {
        if (double_vec_get(p_bo,i) >= 6.0*MAD) 
        {
            double_vec_set(p_w, i, 0.0000001); // avoid dividing by zero 
        } else 
        {
            r = double_vec_get(p_bo, i) / (6.0*MAD);
            double_vec_set(p_w, i , (1.0 - r*r)*(1.0 - r*r));
        }
    }

    // free mem
    double_vec_delete(p_bo);
    double_vec_delete(p_bx);
}

/*------------------------------------------------------------------------------
* Function Name: find_cross_zero
* Description: find crossing zero locations of a function. Simply loop through
* x(n) and (x+1) and check x(n)*x(n+1) < 0 then find intercept with the ZERO 
* horizontal line. 
*-----------------------------------------------------------------------------*/
double_vec *find_cross_zero(double_vec *p_x, double_vec *p_y)
{
    double_vec *p_t = double_vec_new(double_vec_length(p_x));
    double_vec *p_z = NULL; 
    double x1 = 0.0;
    double x2 = 0.0;
    double y1 = 0.0;
    double y2 = 0.0;
    unsigned int count = 0;

    // TODO ERROR CHECKING HT AUG 21 2019

    // find intercept 
    for (unsigned int i = 0; i < double_vec_length(p_y) - 1; i++) 
    {
        x1 = double_vec_get(p_x, i);
        x2 = double_vec_get(p_x, i + 1);
        y1 = double_vec_get(p_y, i);
        y2 = double_vec_get(p_y, i + 1);
        if ( y1 * y2 < 0 ) 
        {
            double_vec_set(p_t, count, (x1*y2 - x2*y1) / (y2 - y1));
            count += 1;
        }
    }

    // coutput only crossing zero points 
    if( count > 0 ) {
        p_z = double_vec_new(count);
        unsigned int j = 0; 
        for (unsigned int i = 0; i < double_vec_length(p_t); i++) 
        {
            if (double_vec_get(p_t, i) > 0.0) 
            {
                double_vec_set(p_z, j, double_vec_get(p_t, i));
                j = j + 1; 
            }
        }
    }

    // free mem 
    double_vec_delete(p_t);
    
    return p_z;
}


/*==============================================================================
*                                 AUX SIGNAL PROCESSING 
* 1. Design GAUSS and HANN window 
* 2. SMOOTH with methods: GAUSS, RGAUSS, LOWESS, RLOWESS, MEDIAN, AVERAGING 
* 3. 
*===============================================================================*/

/*------------------------------------------------------------------------------
* Function Name: design_gauss_window
* Description  : design gaussian window similar to function gausswin in Matlab
*-----------------------------------------------------------------------------*/
void gausswin(double_vec *p_gw, unsigned int wlen, double alpha)
{
    int halflen   =  (wlen - 1) /2;
    double sigma  =  (wlen - 1.0) / (2.0 * alpha);

    for (int n = 0; n < wlen; n++)
    {
        p_gw->p_data[n] = exp( -(n - halflen) * (n - halflen) / (2.0 * sigma * sigma) );
        
        #ifdef BIORITHM_AUX_DEBUG
        // printf("i = %ul, g = %3.15f, sigma=%3.15f, n=%df\n", n, 
        // p_gw->p_data[n],sigma, n-halflen);
        #endif
    }
}

/*-----------------------------------------------------------------------------
* Function Name: hannwin
* Description  : compute hannwin 
*-----------------------------------------------------------------------------*/
void hannwin(double_vec *hw, unsigned int N)
{
    for (unsigned int i = 0; i < N; i++) 
    {
        hw->p_data[i] = sin(M_PI * i / (N - 1)) * sin(M_PI * i / (N - 1)); 
    }
}

/*-----------------------------------------------------------------------------
* Function Name: kaiswerwin
* Description  :  
*-----------------------------------------------------------------------------*/
void kaiserwin(double_vec *p_kw, unsigned int wlen, double beta) 
{
    // init length of the window 
    double numerator[wlen];
    double x = 0.0;
    double N = (wlen-1.0)/2.0; 

    // denumerator by Bessel modified first order 
    double denumerator = gsl_sf_bessel_I0(beta);
    
    // compute each sample of kaiser window 
    for(int i = 0; i < wlen; i++){
        x = (i - N) / N;
        numerator[i] = gsl_sf_bessel_I0(beta * sqrt(1.0 - x * x));
        p_kw->p_data[i] = numerator[i] / denumerator;
    }
}

/*------------------------------------------------------------------------------
* Function Name: smoothg
* Description: smooth using guassian window. It is noted that at left and right
* border, there are several methods for processing such as non-symmetric window.
* This function uses shorted window for half left and half right borders. In addition,
* to ignore outliers, the function use a mark called p_w and initialized to ones.
* For ease implementation, zeros are padded to left and right borders.
*-----------------------------------------------------------------------------*/
void smoothg(double_vec *p_x, unsigned int wlen, double alpha)
{
    // init 
    unsigned int halflen = (unsigned int)((wlen-1)/2);
    double_vec *gw = double_vec_new(wlen);
    double tmp = 0.0;
    double scale = 0.0;

    // TODO: compute weight to ignore outliers and signal lost 
    double_vec *p_w = double_vec_new(double_vec_length(p_x) + wlen -1);

    // zero padd to borders 
    double_vec *p_z = double_vec_new(double_vec_length(p_x) + wlen);

    // zero padd and init weight to ones 
    for( unsigned int i = 0; i < double_vec_length(p_x); i++) 
    {
        double_vec_set(p_z, i + halflen, double_vec_get(p_x,i));
        double_vec_set(p_w, i + halflen, 1.0);
    }

    // init guassian window
    gausswin(gw, wlen, alpha);

    // moving window
    for (unsigned int i = 0; i < double_vec_length(p_x); i++) 
    {
        tmp = 0.0;
        scale = 0.0;
        for (unsigned int j = 0; j < wlen; j++) 
        {

            tmp = tmp + double_vec_get(gw,j) * double_vec_get(p_w,i+j) 
                        * double_vec_get(p_z,i +j);
            scale = scale + double_vec_get(gw,j) * double_vec_get(p_w,i+j); 
        }

        double_vec_set(p_x, i, tmp / (scale + 0.0000001));
    }

    // free mem
    double_vec_delete(gw);
    double_vec_delete(p_z);
}

/*------------------------------------------------------------------------------
* Function Name: smoothwg
* Description  : similar to smoothg but the weighted p_w vector is given as and
* input 
*-----------------------------------------------------------------------------*/
void smoothwg(double_vec *p_x, unsigned int wlen, double alpha, double_vec *p_w)
{
    unsigned int halflen = (unsigned int)((wlen-1)/2);
    double_vec *gw   = double_vec_new(wlen);
    double_vec *px_z = double_vec_new(double_vec_length(p_x) + wlen);
    double_vec *pw_z = double_vec_new(double_vec_length(p_w) + wlen);

    double tmp = 0.0;
    double scale = 0.0;

    // init guassian window
    gausswin(gw, wlen, alpha);

    // padd zeros to bounds
    for( unsigned int i = 0; i < double_vec_length(p_x); i++) 
    {
        double_vec_set(pw_z, i + halflen, double_vec_get(p_w,i));
        double_vec_set(px_z, i + halflen, double_vec_get(p_x,i));
    }

    // moving window
    for (unsigned int i = 0; i < double_vec_length(p_x); i++) 
    {
        tmp = 0.0;
        scale = 0.0;
        for (unsigned int j = 0; j < wlen; j++) 
        {
            scale = scale + double_vec_get(gw, j) * double_vec_get(pw_z, i + j);
            tmp = tmp + double_vec_get(gw, j) * double_vec_get(pw_z, i +j ) 
                        * double_vec_get(px_z, i + j);
        }
        double_vec_set(p_x, i, tmp / scale);
    }

    // free mem
    double_vec_delete(gw);
    double_vec_delete(pw_z);
    double_vec_delete(px_z);
}


/*------------------------------------------------------------------------------
* Function Name: smoothrg
* Description  : robust weighted gaussian smooth which is similar to robust locally
* weighted linear regression but not requiring solving least square equation.
*-----------------------------------------------------------------------------*/
void smoothrg(double_vec *p_x, unsigned int wlen, double alpha, unsigned int niter)
{
    double_vec *p_w = double_vec_new(double_vec_length(p_x));
    double_vec *p_o = double_vec_new(double_vec_length(p_x));

    // buffer original signal
    double_vec_copy(p_x, p_o);

    // init smooth
    smoothg(p_x, wlen, alpha);

    // loop
    for (unsigned int i = 0; i < niter; i++) 
    {

        // calculate weight from difference between p_x and p_o
        mad_weight(p_o, p_x, p_w);

        // reset p_x to original p_o
        double_vec_copy(p_o, p_x);

        // smooth the weighted p_x
        smoothwg(p_x, wlen, alpha, p_w);

    }

    // free mem
    free(p_w);
    free(p_o);
}

/*------------------------------------------------------------------------------
* Function Name: smooth
* Description: organize all smooth type into a function so that users can 
* easily use. Will implement: lowess, less, moving average, moving median,... 
*-----------------------------------------------------------------------------*/
void smooth(double_vec *p, unsigned int wlen, const char* method, double alpha)
{
    if(strcmp(method, "gaussian")==0) 
    {
        smoothg(p, wlen, alpha);

    } else if (strcmp(method, "rgaussian")==0)
    {
        smoothrg(p, wlen, alpha, SMOOTH_RGAUSSIAN_NITER);

    } else if (strcmp(method, "wgaussian")==0)
    {
        // smoothwg(p, wlen, alpha, p_w);

    } else
    {
        printf("NOT SUPPORTED SMOOTH MODE KEEP SAME INPUT\n");
    }

}

/*------------------------------------------------------------------------------
* Function Name: conv
* Description: convolution with different modes
* SAME: output length is same as input length L = N 
* VALID: 
* FULL: output lengtth L = N + M - 1
*-----------------------------------------------------------------------------*/
 double_vec *conv(double_vec *p_x, double_vec *p_h, const char*mode)
 {
    // init 
    double s = 0; 
    unsigned int filtlen = p_h->len; 
    unsigned int halflen = (unsigned int)(filtlen - 1) / 2; 

    // start index for SAME MODE 
    unsigned int start_idx = (unsigned int)ceil(filtlen / 2.0); 

    #ifdef DEBUG
    printf("halflen = %ul, filtlen = %ul, start_idx = %ul\n", halflen, filtlen, 
        start_idx);
    #endif

    // init output 
    double_vec *p_y = double_vec_new(p_x->len);

    // register buffer 
    double_vec *p_reg = double_vec_new(p_h->len); 

    // init register for SAME mode 
    for (unsigned int i = 0; i < filtlen; i++) 
    {
        p_reg->p_data[i] = 0.0; 
    } 

    // conv operation 
    for (unsigned int i = 0; i < p_x->len + filtlen - 1; i++)
    {
        // left shift register 
        for (unsigned int j = 0; j < filtlen - 1; j++)
        {
            p_reg->p_data[j] = p_reg->p_data[j+1];
        }

        // load input to register 
        if (i > p_x->len - 1) 
        {
             p_reg->p_data[filtlen - 1] = 0.0;
        }
        else 
        {
            p_reg->p_data[filtlen - 1] = p_x->p_data[i];   
        }
        
        // inner product 
        s = 0; 

        for (unsigned int k = 0; k < filtlen; k++)
        {
            s = s + p_reg->p_data[k] * p_h->p_data[filtlen - k - 1];
        }

        // same mode 
        if (( i >= start_idx - 1) && (i <= p_x->len + start_idx - 2)) 
        {
            #ifdef DEBUG
                printf("i=% ul, s=%3.15f\n", i, s);
            #endif
            p_y->p_data[i - start_idx + 1] = s; 
        }
    }


    // free mem 
    double_vec_delete(p_reg);

    return p_y;
 }

/*==============================================================================
*                                 AUX IO 
* 1. read from text with multiple columns (TODO)
* 2. write to text with multiple columns 
*===============================================================================*/

/*------------------------------------------------------------------------------
* Function Name: read_from_txt
* Description  :
*-----------------------------------------------------------------------------*/
void read_from_txt(const char* p_path, double_vec *p_buff)
{

    FILE *p_file;
    p_file = fopen(p_path,"r");

    // check error open file
    CHECK_ACCESS_NULL_POINTER(p_file);

    double tmp = 0.0;

    for(int i = 0; i < (int)double_vec_length(p_buff); i++)
    {
        fscanf(p_file, "%lg", &tmp);
        p_buff->p_data[i] = tmp;
    }
}

/*------------------------------------------------------------------------------
* Function Name: read_from_txt
* Description  :
*-----------------------------------------------------------------------------*/
void read_from_txt_cols(const char* p_path, double_vec **p_buff, int ncol)
{

    FILE *p_file;
    p_file = fopen(p_path,"r");

    // check error open file
    CHECK_ACCESS_NULL_POINTER(p_file);

    double tmp_1, tmp_2, tmp_3, tmp_4, tmp_5, tmp_6 = 0.0;

    // 2 channel 
    if (ncol == 2)
    {
            unsigned int siglen = p_buff[0]->len; 
            for (unsigned int i = 0; i < siglen ; i++)
            {
                fscanf(p_file, "%lg %lg\n", &tmp_1, &tmp_2);
                p_buff[0]->p_data[i] = tmp_1;
                p_buff[1]->p_data[i] = tmp_2;
            }   
    }

    // 3 channel 
    if (ncol == 3)
    {
            unsigned int siglen = p_buff[0]->len; 
            for (unsigned int i = 0; i < siglen ; i++)
            {
                fscanf(p_file, "%lg %lg %lg\n", &tmp_1, &tmp_2, &tmp_3);
                p_buff[0]->p_data[i] = tmp_1;
                p_buff[1]->p_data[i] = tmp_2;
                p_buff[2]->p_data[i] = tmp_3;
            }   
    }

    // 4 channel 
    if (ncol == 4)
    {
            unsigned int siglen = p_buff[0]->len; 
            for (unsigned int i = 0; i < siglen ; i++)
            {
                fscanf(p_file, "%lg %lg %lg %lg\n", &tmp_1, &tmp_2, &tmp_3, &tmp_4);
                p_buff[0]->p_data[i] = tmp_1;
                p_buff[1]->p_data[i] = tmp_2;
                p_buff[2]->p_data[i] = tmp_3;
                p_buff[3]->p_data[i] = tmp_4;
            }   
    }

    // 5 channel 
    if (ncol == 5)
    {
            unsigned int siglen = p_buff[0]->len; 
            for (unsigned int i = 0; i < siglen ; i++)
            {
                fscanf(p_file, "%lg %lg %lg %lg %lg\n", &tmp_1, &tmp_2, &tmp_3, &tmp_4, &tmp_5);
                p_buff[0]->p_data[i] = tmp_1;
                p_buff[1]->p_data[i] = tmp_2;
                p_buff[2]->p_data[i] = tmp_3;
                p_buff[3]->p_data[i] = tmp_4;
                p_buff[4]->p_data[i] = tmp_5;
            }   
    }

    // 6 channel 
    if (ncol == 6)
    {
            unsigned int siglen = p_buff[0]->len; 
            for (unsigned int i = 0; i < siglen ; i++)
            {
                fscanf(p_file, "%lg %lg %lg %lg %lg %lg\n", &tmp_1, &tmp_2, 
                    &tmp_3, &tmp_4, &tmp_5, &tmp_6);
                p_buff[0]->p_data[i] = tmp_1;
                p_buff[1]->p_data[i] = tmp_2;
                p_buff[2]->p_data[i] = tmp_3;
                p_buff[3]->p_data[i] = tmp_4;
                p_buff[4]->p_data[i] = tmp_5;
                p_buff[5]->p_data[i] = tmp_6;
            }   
    }
    
}
/*------------------------------------------------------------------------------
* Function Name: write_to_txt
* Description  :
*-----------------------------------------------------------------------------*/ 
void write_to_txt(double_vec **p_d, unsigned int ncol, const char* p_path)
{
    // OPEN FILE FOR WRITTING
    FILE *p_file;
    p_file = fopen(p_path, "w");

    // CHECK ACCESS NULL POINTER 
    CHECK_ACCESS_NULL_POINTER(p_file);

    // write 1 colum data to txt
    if (ncol==1) {
        for (size_t i = 0; i < double_vec_length(p_d[0]); i++)
        {
            fprintf(p_file, "%2.24f\n", double_vec_get(p_d[0],i));
        }
    }

    // write 2 colum data to txt
    if  (ncol==2) {
        for(size_t i = 0; i < double_vec_length(p_d[0]); i++)
        {
            fprintf(p_file, "%2.24f %2.24f\n", double_vec_get(p_d[0],i), 
                                               double_vec_get(p_d[1],i));
        }
    }

    // write 3 colum data to txt
    if (ncol==3) {
        for(size_t i = 0; i < double_vec_length(p_d[0]); i++)
        {
            fprintf(p_file, "%2.24f %2.24f %2.24f\n", double_vec_get(p_d[0],i), 
                         double_vec_get(p_d[1],i), double_vec_get(p_d[2],i));
        }
    }

    // write 4 colum data to txt
    if (ncol==4){
        for(size_t i = 0; i < double_vec_length(p_d[0]); i++)
        {
            fprintf(p_file, "%2.24f %2.24f %2.24f %2.24f\n", 
                double_vec_get(p_d[0], i), double_vec_get(p_d[1], i), 
                double_vec_get(p_d[2], i), double_vec_get(p_d[3], i));
        }
    }

    // write 5 colum data to txt
    if (ncol==5){
        for(size_t i = 0; i < double_vec_length(p_d[0]); i++)
        {
            fprintf(p_file, "%2.24f %2.24f %2.24f %2.24f %2.24f\n", 
                double_vec_get(p_d[0],i), double_vec_get(p_d[1],i), 
                double_vec_get(p_d[2],i), double_vec_get(p_d[3],i), 
                double_vec_get(p_d[4],i));
        }
    }

    if(ncol>5){
        printf("does not support > 5 colum\n");
    }

    fclose(p_file);
}

char *str_concat(char *p1, char *p2)
{
    char *p = malloc(strlen(p1) + strlen(p2) + 20); 

    sprintf(p, "%s%s", p1, p2);

    return p;
}



