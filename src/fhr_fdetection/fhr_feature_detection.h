/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: fhr_feature_detection.h
* Original Author: TRAN MINH HAI
* Date created: 20 SEP 2019
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             20 SEP 2019    T.H          to test fhr_feature_detection 
********************************************************************************/
#ifndef FHR_FDETECTION_H
#define FHR_FDETECTION_H

// SYSTEM INCLUDE 

// LOCAL INCLUDE 
#include "biorithm_aux.h"

// DEFINE CONSTANT 
#ifndef FHR_NCHANNEL 
#define FHR_NCHANNEL 			4 	    // NUMBER OF ECG CHANNEL 
#endif
#define FHR_TKWIN				25      // TKEO WINDOW LENGTH 
#define FHR_IPWIN 				11      // INNER RPODUCT WINDOW LENGTH 
#define FHR_HT_KAISER_BETA 		0.1  	// BETA TO DESIGN KAISER WINDOW 
#define FHR_HT_KAISER_WLEN 		399  	// KAISER WINDOW LENGTH 
#define FHR_IF_THESHOLD 		0.2     // INSTANEOUS FREQ. LESS THAN 0.2*FS
#define FHR_IF_DBL_MIN 			1e-20   // AVOID DIVIDING ZERO IN ATAN2

// DATA TYPE 
struct fhrfeature 
{
	double_vec *pp_tkeo[FHR_NCHANNEL];
	double_vec *pp_iprod[FHR_NCHANNEL];
	double sqi[FHR_NCHANNEL];
};


// DEBUG ENALBE
#define FHR_FDETECTION_DEBUG 

// FUNCTION DECLARATION

/*------------------------------------------------------------------------------
* Description: compute_hilbert_irf computes Hilbert impulse response function 
* (irf) and then weighted by a Kaiser window with length of wlen and beta. 
*-----------------------------------------------------------------------------*/ 
double_vec *compute_hilbert_irf(unsigned int wlen, double beta);

/*------------------------------------------------------------------------------
* Description: hi_filter perform Hilbert transform onto input signal. The input
* real double signal is convoluted with a Hilbert impulse response function (irf)
* to output imaginary part of analytic signal. Wlen and beta are parameters for 
* Hilbert irf. 
*-----------------------------------------------------------------------------*/ 
double_vec *ht_filter(double_vec *p_real, unsigned int wlen, double beta);

/*------------------------------------------------------------------------------
* Description: compute_distpts computes first derivative of mangnitude, and first
* derivative of phase of analytic signal after HT. The derivative of mang. will 
* be passed to TKEO, and derivative of phase will be passed to inner product. 
* Input is complex signal after HT. Output: EVEN index (2*i) for derivative of 
* magnitude, and ODD index (2*i+1) for derivative of phase. 
*-----------------------------------------------------------------------------*/ 
double_vec *compute_distpts(double_vec *p_ht);

/*------------------------------------------------------------------------------
* Description: compute_iprod computes the inner product between instaneous 
* frequency and first derivative of magnitude, per window basic. Input p_dist 
* containt mag. at EVEN index (2*i) and phase at ODD index (2*i+1). Output length
* = length(p_dist) - ipwin +1
*-----------------------------------------------------------------------------*/ 
double_vec *compute_iprod(double_vec *p_dist, unsigned int ipwin);

/*------------------------------------------------------------------------------
* Description: compute TKEO of input p_dist with tkeo window of tkwin. 
*-----------------------------------------------------------------------------*/ 
double_vec * compute_tkeo(double_vec *p_dist, unsigned int tkwin);

/*------------------------------------------------------------------------------
* Function name: detect_fhr_feature()
* Description: compute tkeo and iprod of four ECG channel 
* Input: double_vec **p_ecg is clean ECG of four channel after preprocessing
* Output: struct fhr_feature {double_vec *pp_tkeo[4], double_vec *pp_iprod,
* double p_sqi[4]}. Output consist of tkeo, iprod and sqi of four channel; 
*-----------------------------------------------------------------------------*/ 
struct fhrfeature *detect_fhr_feature(double_vec **p_ecg);


#endif // FHR_FDETECTION_H
