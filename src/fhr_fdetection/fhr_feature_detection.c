/*******************************************************************************
*      Copyright ©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: fhr_feature_detection.c
* Original Author: HAI TRAN
* Date created: 18 JUL 2019
* Purpose: this unit receives ECG signal after PREPROCESSING UNIT, and output  
* TKEO and the INNER PRODUCT (IProd) between instaneous frequency and first 
* derivative of magnitude of analytic signal after Hilbert transform. 
* (1) Perform Hilbert transform to get analytic complex signal 
* (2) From the analytic signal, compute distpts as first derivative of magnitude, 
*     and compute IF as instaneous frequency of analytic signal 
* (3) Convolution the distpts with IF per window basic. 
*
* Revision History:
* Ref No      Date           Who          Detail
* N/A         18 JUL 2019    H.T          Original Code
* N/A         16 SEP 2019    H.T          REWORK 
********************************************************************************/
// SYSTEM INCLUDE 
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>

// LOCAL 
#include "biorithm_aux.h"
#include "fhr_feature_detection.h"

/*------------------------------------------------------------------------------
* Function Name: compute_hilbert_irf
* Description: compute Hilbert filter impulse response function 
*-----------------------------------------------------------------------------*/ 
double_vec *compute_hilbert_irf(unsigned int wlen, double beta)
{
    // init output 
    double_vec *p_irf = double_vec_new(wlen);

    // Kaiser window 
    double_vec *p_w = double_vec_new(wlen); 
    kaiserwin(p_w, wlen, beta);

    // Hilbert impulse response function 
    unsigned int halflen = (wlen - 1) / 2;
    double hn = 0.0;
    double I_M_PI = 1.0 / M_PI; 

    // only imag part of Hilbert irf
    for(unsigned int i = 1; i <= halflen; i+=2){
        hn = 2 * I_M_PI / i; 
        p_irf->p_data[ halflen + i ] = hn; 
        p_irf->p_data[ halflen - i ] = -hn;            
    }

    // Windowing irf by Kaiser window 
    for (unsigned int i = 0; i < wlen; i++)
    {
        p_irf->p_data[i] = p_irf->p_data[i] * p_w->p_data[i];
    }

    // free mem 
    double_vec_delete(p_w);

    return p_irf;
}

/*------------------------------------------------------------------------------
* Function Name: ht_filter
* Description  : perform hibert transform and fitler 
*-----------------------------------------------------------------------------*/ 
double_vec *ht_filter(double_vec *p_real, unsigned int wlen, double beta)
{
    // init output 
    size_t siglen = p_real->len;

    // analytic signal output
    double_vec *p_ht = double_vec_new(2 * siglen); 

    // imag of analytic signal output 
    double_vec *p_imag = NULL; 

    // Hilbert impulse response function 
    double_vec *p_irf = NULL; 

    // compute Hilbert filter impulse response function (irf)
    p_irf = compute_hilbert_irf(wlen, beta);

    // conv. input signal with irf 
    p_imag = conv(p_real, p_irf, "same");

    // form complex analytic signal 
    for (size_t i = 0; i < siglen; i++)
    {
        p_ht->p_data[2 * i]  = p_real->p_data[i];
        p_ht->p_data[2 * i + 1]  = p_imag->p_data[i];
    }

    // free mem 
    double_vec_delete(p_imag);
    double_vec_delete(p_irf);

    return p_ht;
}


/*------------------------------------------------------------------------------
* Function Name: compute_distpts
* Description  : comput distpts as first derivative of magnitude of analytic
* signal after HT, and also compute instanenous frequency as first derivative 
* of phase of the analytic signal. 
*-----------------------------------------------------------------------------*/ 
double_vec *compute_distpts(double_vec *p_ht)
{
    double dr = 0.0;    // derivative of real 
    double di = 0.0 ;   // derivative of imag
    double angle = 0.0; // derivative of phase 

    // init output 
    double_vec *p_dist = double_vec_new(p_ht->len - 2); 

    // compute abs of first derivative 
    for (unsigned int i = 0; i < p_ht->len / 2 -1; i++)
    {   
        // derivative of real part 
        dr = p_ht->p_data[2 * (i + 1)] - p_ht->p_data[2 * i];

        // derivative of imag part 
        di = p_ht->p_data[2 * (i + 1) + 1] - p_ht->p_data[2 * i + 1];

        // abs of derivative 
        p_dist->p_data[2 * i] = sqrt(dr * dr + di * di); 

        // derivative of phase or instaneous frequency 
        // real(conj(z1)*z2) = x1*x2 + y1*y2 
        dr = p_ht->p_data[2 * i] * p_ht->p_data[2 * (i + 1)] 
           + p_ht->p_data[2 * i + 1] * p_ht->p_data[ 2 * (i + 1) + 1]; 

        // imag(conj(z1)*z2) = x1*y2 - x2*y1
        di = p_ht->p_data[2 * i] * p_ht->p_data[2 * (i + 1) + 1] 
           - p_ht->p_data[2 * (i + 1)] * p_ht->p_data[2 * i + 1];

        // check input before atan2 to avoid dividing by zero 
        if ( fabs(dr) < FHR_IF_DBL_MIN) 
        {
            angle = 0.0; 
        } else 
        {
            angle = fabs(atan2(di, dr));
        }

        // remove outlier of IF > 0.2 * PI
        if (fabs(angle) > FHR_IF_THESHOLD * M_PI)
        {
            angle = 0.0; 
        }

        p_dist->p_data[2 * i + 1] = angle;
    }

    return p_dist;
}

/*------------------------------------------------------------------------------
* Function Name: compute_iprod  
* Description  : compute inner product between distpts and intaneouse frequency 
*-----------------------------------------------------------------------------*/ 
double_vec *compute_iprod(double_vec *p_dist, unsigned int ipwin)
{
    // inint output 
    double s = 0.0; 
    double_vec *p_iprod = double_vec_new(p_dist->len / 2 - ipwin + 1);

    // moving window and compute inner product 
    for (unsigned int i = 0; i < p_dist->len / 2 - ipwin + 1; i++)
    {
        s = 0.0; 

        // inner product between manginute and intaneous frequency 
        for (unsigned int j = 0; j < ipwin; j++) 
        {
            s = s + p_dist->p_data[2 * (i + j)] * p_dist->p_data[2 * (i + j) + 1]; 
        }

        p_iprod->p_data[i] = s; 
    }

    return p_iprod;
}

/*------------------------------------------------------------------------------
* Function Name: compute_tkeo  
* Description  : compute tkeo 
* Input: abs of first derivative of analytic signal after HT 
* Output: tkeo as double vector 
*-----------------------------------------------------------------------------*/ 
double_vec * compute_tkeo(double_vec *p_dist, unsigned int tkwin) 
{
    // init output 
    // p_dist is complex vector and dist is at event index 
    unsigned int divlen = p_dist->len / 2; 
    unsigned int tkeolen = divlen - 2 * tkwin;

    double_vec *p_tkeo = double_vec_new(tkeolen);

    // tkeo(n)  = x(n+w)*x(n+w) - x(n)x(n+2*w)
    for(unsigned int i = 0; i < tkeolen; i++)
    {
        p_tkeo->p_data[i] = p_dist->p_data[2*tkwin+2*i] * p_dist->p_data[2*tkwin+2*i]  
        - p_dist->p_data[2*i] * p_dist->p_data[2*2*tkwin+2*i];

        if (p_tkeo->p_data[i] < 0.0) 
        {
            p_tkeo->p_data[i] = 0.0;
        } 

    }

    return p_tkeo;
}

/*------------------------------------------------------------------------------
* Function name: detect_fhr_feature()
* Description: compute tkeo and iprod of four ECG channel 
* Input: double_vec **p_ecg is clean ECG of four channel after preprocessing
* Output: struct fhr_feature {double_vec *pp_tkeo[4], double_vec *pp_iprod,
* double p_sqi[4]}. Output consist of tkeo, iprod and sqi of four channel; 
% Note: tkwin, ipwin, kaiser win are fixed parameters.
*-----------------------------------------------------------------------------*/ 
struct fhrfeature *detect_fhr_feature(double_vec **p_ecg)
{
    // input signal length four channel has same length 
    unsigned int siglen   = p_ecg[0]->len;
    unsigned int divlen   = siglen - 1; 
    unsigned int tkeolen  = divlen - 2 * FHR_TKWIN; 
    unsigned int iprodlen = divlen - FHR_IPWIN + 1; 

    // init tkeo output 
    struct fhrfeature *p_feature = malloc(sizeof(struct fhrfeature)); 
    p_feature->pp_tkeo[0]  = double_vec_new(tkeolen); 
    p_feature->pp_tkeo[1]  = double_vec_new(tkeolen);     
    p_feature->pp_tkeo[2]  = double_vec_new(tkeolen);     
    p_feature->pp_tkeo[3]  = double_vec_new(tkeolen);     
    
    // init iprod output 
    p_feature->pp_iprod[0] = double_vec_new(iprodlen);    
    p_feature->pp_iprod[1] = double_vec_new(iprodlen);    
    p_feature->pp_iprod[2] = double_vec_new(iprodlen);    
    p_feature->pp_iprod[3] = double_vec_new(iprodlen);

    // init sqi output 
    for (int i = 0; i < FHR_NCHANNEL; i++)
    {
        p_feature->sqi[i] = 0.0; 
    }

    // buffer for Hilbert and derivative output 
    double_vec *p_ht = double_vec_new(2 * siglen);  // complex vector 
    double_vec *p_div = double_vec_new(2 * divlen); // complex vector 

    #ifdef FHR_FDETECTION_DEBUG
    // printf("tkeolen = %u\n", p_feature->pp_tkeo[0]->len);
    #endif 
    
    // compute tkeo and iprod for each channel 
    for (int i = 0; i < FHR_NCHANNEL; i++)
    {
        #ifdef FHR_FDETECTION_DEBUG
        printf("feature detection processing channel %d\n", i);
        #endif
        // Hilbert filter
        p_ht = ht_filter(p_ecg[i], FHR_HT_KAISER_WLEN, FHR_HT_KAISER_BETA); 

        // Derivation 
        p_div = compute_distpts(p_ht);

        // TKEO 
        p_feature->pp_tkeo[i] = compute_tkeo(p_div, FHR_TKWIN);

        // Inner product 
        p_feature->pp_iprod[i] = compute_iprod(p_div, FHR_IPWIN);
    }

    // free mem 
    double_vec_delete(p_ht);
    double_vec_delete(p_div);

    // return 
    return p_feature;
}