/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 03 OCT 2019
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             03 OCT 2019    T.H          Original Code
*******************************************************************************/
// SYSTEM INCLUDES
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>

// PROJECT INCLUDE WAVELIB
#include <wavelib.h>

// LOCAL INCLUDES
#include "biorithm_aux.h"
#include "biorithm_wavelib.h"

/*------------------------------------------------------------------------------
* Function Name: create_wavelib_object
* Description  : create wavelib object from wavelib for configuration 
*-----------------------------------------------------------------------------*/
struct wavelib_object *create_wavelib_object(struct param_wave wave)
{
	struct wavelib_object *p_wave = malloc(sizeof(struct wavelib_object));

	p_wave->wave = wave_init(wave.p_wname); // init wave 

    p_wave->wt  = wt_init(p_wave->wave, "dwt", wave.wlen, wave.nlayer); 

	setDWTExtension(p_wave->wt, "sym"); // extension mode 

	setWTConv(p_wave->wt, "direct"); // transform by convoltion 

	return p_wave;
}

/*------------------------------------------------------------------------------
* Function Name: delete_wavelib_object
* Description  : 
*------------------------------------------------------------------------------*/
void delete_wavelib_object(struct wavelib_object *p_wave)
{
	wt_free(p_wave->wt);
	wave_free(p_wave->wave);
}

/*------------------------------------------------------------------------------
* Function Name: get_start_idx_layer
* Description  : return starting index of a layer [A,DJ,...,D1]
*-----------------------------------------------------------------------------*/
unsigned int get_start_idx_layer(wt_object wt, unsigned int layer_id)
{
    unsigned int sidx = 0;

    // approximation coefficient starts from zero index 
    if (layer_id == 0)
    {
    	sidx = 0;
    } else 
    {
    	unsigned int k = (unsigned int)(wt->J) + 1 - layer_id;

	    for (unsigned int i = 0; i < k; i++)
	    {
	        sidx = sidx + (unsigned int)wt->length[i];
	    }
    }
    return sidx; 
}

/*------------------------------------------------------------------------------
* Function Name: weight_wlayer
* Description  : dwt decompose input into wavelet coeff as [A, D12,...,D1]
* this function multiplies each layer by a weight from weight vector p_weight.
* to remove approximation layer p_weight[0] = 0 
* to extract only D6, D7, D8, D9, D10, then 
* p_weight = {0,0,0,1,1,1,1,1,0,0,0,0,0} 
*-----------------------------------------------------------------------------*/
void weight_wlayer(wt_object wt, const double *p_weight)
{
	int sidx = 0; 

	// for each layer (J -i +1), i=0 means approximation layer
	for (int i = 0; i < wt->J + 1; i++)
	{	 
		for (int j = sidx; j < sidx + wt->length[i]; j++)
		{
			wt->output[j] = p_weight[i] * wt->output[j];
		}

		sidx += wt->length[i]; 
	}
}

/*------------------------------------------------------------------------------
* Function Name: shrink_layer
* Description  : 
*-----------------------------------------------------------------------------*/
void shrink_wlayer(wt_object wt, double *p_thresh)
{
	int sidx = 0; 
	double sign = 1.0;

	// for each layer (J -i +1), i=0 means approximation layer
	for (int i = 0; i < wt->J + 1; i++)
	{	 
		// check a layer wanted to be shirkage or not
		if (p_thresh[i] >= 0)
		{
			// check each value in a layer and soft thresholding 
			for (int j = sidx; j < sidx + wt->length[i]; j++)
			{
				#ifdef BIORITHM_WAVELIB_DEBUG
				// printf("i=%d,j=%d,wtout[%d]=%3.15f,thresh=%3.15f\n",
				// i,j,j,wt->output[j],p_thresh[i]);
				#endif 

				if (fabs(wt->output[j]) < p_thresh[i])
				{
					wt->output[j] = 0.0;
				}
				else 
				{
					sign = (wt->output[j] - p_thresh[i] >= 0 ? 1 : -1);
					wt->output[j] = sign * (fabs(wt->output[j]) - p_thresh[i]); 
				}
			}
		} 
		// update starting index of a layer 
		sidx += wt->length[i]; 
	}
}

/*------------------------------------------------------------------------------
* Function Name: find_universal_thresh
* Description  :  
*-----------------------------------------------------------------------------*/
double find_universal_thresh(double_vec *p_x)
{
    // init median 	
    double median = 0.0; 

    // wavelet parameter for finding universal threshold 
    struct param_wave wave = {p_x->len, 1, "db1"};

    // constructe wavelib object 
    struct wavelib_object *p_wave = create_wavelib_object(wave);

    // decompose only 1 level using db1 
    dwt(p_wave->wt, p_x->p_data);

    // staring index of highest frequency layer 1 
    int dlen = p_wave->wt->length[p_wave->wt->J];
    int sidx = p_wave->wt->length[0];
    double_vec *p_abs = double_vec_new(dlen); 

    // buffer coefficient of finest layer to abs to find median 
    for (int i = 0; i < dlen; i++) 
    {
        p_abs->p_data[i] = fabs(p_wave->wt->output[sidx + i]);
    }

    // median of the abs 
    median = find_median(p_abs);

    // free mem 
    double_vec_delete(p_abs);
    delete_wavelib_object(p_wave);

    return (median / 0.6745) * sqrt(2.0 * log(p_wave->wt->outlength));
}

/*------------------------------------------------------------------------------
* Function Name: wdenoise
* Description  : keepapp = true mean do not shriking the approximate layer. 
*-----------------------------------------------------------------------------*/
void wdenoise(double_vec *p_x, struct param_wave wave, const char *sorh, bool keepapp)
{
	// construct wavelib object 
	struct wavelib_object *p_wave = create_wavelib_object(wave);

	// init threshold for each layer to zero 
	double p_thresh[wave.nlayer + 1];
	for (int i = 0; i < wave.nlayer; i++)
	{
		p_thresh[i] = 0.0; 
	}
	
	// decompose into wname.nlayer 
	dwt(p_wave->wt, p_x->p_data);

	// find the universal threshold 
	double uthresh = find_universal_thresh(p_x);

	#ifdef BIORITHM_WAVELIB_DEBUG
	// printf("uthresh = %3.15f\n", uthresh);
	#endif

	for (int i = 0; i < wave.nlayer + 1; i++)
	{
		p_thresh[i] = uthresh;

		#ifdef BIORITHM_WAVELIB_DEBUG
		// printf("p_thresh[%d]=%3.15f\n", i, p_thresh[i]);
		#endif
	}

	// keepapp true means do not shriking the approximation
	if (keepapp == 1)
	{
		p_thresh[0] = -1.0; 
	}

	// soft thresholding
	shrink_wlayer(p_wave->wt, p_thresh); 

	// reconstruct 
	idwt(p_wave->wt, p_x->p_data);

	// free mem 
	wave_free(p_wave->wave);
}


