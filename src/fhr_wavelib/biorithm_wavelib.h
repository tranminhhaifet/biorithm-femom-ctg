/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.h
* Original Author: TRAN MINH HAI
* Date created: 01 OCT 2019
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             01 OCT 2019    T.H          Original Code
********************************************************************************/
#ifndef BIORITHM_WAVELIB_H
#define BIORITHM_WAVELIB_H

// SYSTEM INCLUDE 
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// PROJECT INCLUDE 
#include <wavelib.h>

// LOCAL INCLUDE 

// DEBUG MODE 
#define BIORITHM_WAVELIB_DEBUG

// WAVELET PARMATERS FOR CREATE WAVELIB OBJECT 
struct param_wave
{
    int wlen;			             // wavelet length
	unsigned int nlayer;		     // number of layer
	char *p_wname; 			         // mother wavelet name 
};

// WAVELIB OBJECT 
struct wavelib_object
{
	wave_object wave;                // containt configuration such as wname
	wt_object wt;                    // perform transform function 
} ;


// FUNCTION 
/*------------------------------------------------------------------------------
* Function Name: create_wavelib_object
* Description  : create wavelib object from wavelib for configuration 
*-----------------------------------------------------------------------------*/
struct wavelib_object *create_wavelib_object(struct param_wave wave);

/*------------------------------------------------------------------------------
* Function Name: delete_wavelib_object
* Description  : 
*------------------------------------------------------------------------------*/
void delete_wavelib_object(struct wavelib_object *p_wave);

/*------------------------------------------------------------------------------
* Function Name: get_start_idx_layer
* Description  : return starting index of a layer [A,DJ,...,D1]
*-----------------------------------------------------------------------------*/
unsigned int get_start_idx_layer(wt_object wt, unsigned int layer_id);

/*------------------------------------------------------------------------------
* Function Name: weight_wlayer
* Description  : dwt decompose input into wavelet coeff as [A, D12,...,D1]
* this function multiplies each layer by a weight from weight vector p_weight.
* to remove approximation layer p_weight[0] = 0 
* to extract only D6, D7, D8, D9, D10, then 
* p_weight = {0,0,0,1,1,1,1,1,0,0,0,0,0} 
*-----------------------------------------------------------------------------*/
void weight_wlayer(wt_object wt, const double *p_weight);

/*------------------------------------------------------------------------------
* Function Name: find_universal_thresh
* Description  :  
*-----------------------------------------------------------------------------*/
double find_universal_thresh(double_vec *p_x);

/*------------------------------------------------------------------------------
* Function Name: shrink_layer
* Description  : 
*-----------------------------------------------------------------------------*/
void shrink_wlayer(wt_object wt, double *p_thresh);

/*------------------------------------------------------------------------------
* Function Name: wdenoise
* Description  : keepapp = true mean do not shriking the approximate layer. 
*-----------------------------------------------------------------------------*/
void wdenoise(double_vec *p_x, struct param_wave wave, const char *sorh, bool keepapp);



#endif // BIORITHM_WAVELIB_H
