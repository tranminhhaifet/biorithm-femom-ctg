/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 04 OCT 2019 
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             04  OCT 2019    T.H          Original Code
*			  24  OCT 2019    T.H          After tested with NUH1021, 1029, 1014 
*******************************************************************************/
// SYSTEM INCLUDES
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>

// LOCAL INCLUDES
#include "biorithm_aux.h"				   // AUXILIARY FUNCTION 	
#include "biorithm_wavelib.h"			   // WAVELIB 
#include "preproc.h"					   // PREPROCESSING
#include "fhr_feature_detection.h"	       // FEATURE DETECTION
#include "fhr_feature_classification.h"    // FEATURE CLASSIFICATION
#include "fhr_baseline.h"				   // BASELINE AND ADCEL 	
#include "fhr_app_log.h"				   // LOG APP OUTPUT TO FILE	
#include "fhr_heartrate_cal.h"			   // COMPUTE HEART RATE
#include "render_img.h"					   // PLPLOT RENDER IMAGE 	
#include "fhr_ua.h"						   // UA UNIT 	

// DEBUG AND PRINT 
#define FHR_APP_PROGRESS_PRINT 
// #define FHR_APP_PRINT
#define FHR_APP_LOG
// #define FHR_APP_DEBUG

// #define FHR_TKEO_IPROD_MODE "TKEO"        // TKEO MODE FOR R PREK DETECTION	
#define FHR_TKEO_IPROD_MODE "IPROD"          // TKEO MODE FOR R PREK DETECTION	
		
// MIN PEAK DISTANCE 
static int GP_MFPEAK_DMIN[2] = {10, 10};

// MIN HEIGHT MPEAK AND FPEAK FOR EACH CHANNEL 
static double GP_MFPEAK_HMIN[2 * FHR_NCHANNEL] = {4.0,4.0,4.0,4.0,
												  3.0,3.0,3.0,3.0};

// REFERENCE HEIGHT THRESHOLD OF EACH CHANNEL FOR RMV MPEAK FROM IPROD 
static double GP_RHEIGHT[FHR_NCHANNEL] = {1.0, 1.0, 1.0, 1.0};

// MXIMUM WIDTH OF QRS 
static int G_FHR_QRS_WMAX = 50; 

// SET CHANNEL FOR RENDER FHR TRACE AND UA TRACE
static const int G_BEST_MCH_ID    = 3;          // BEST MATERNAL CHANNEL 
static const int G_RENDER_FCH_ID  = 3; 			// CHANNEL FOR RENDER FETAL
static const int G_RENDER_UCH_ID  = 3; 			// CHANNEL FOR RENDER UA 

// HEART RATE ULIM LLIM AND DLIM 

int main()
{
    struct mfpeak *p_mfpeak = malloc(sizeof(struct mfpeak));
	p_mfpeak->p_mpeak[0]=new_peak_vec(2577); 
	p_mfpeak->p_mpeak[1]=new_peak_vec(2640); 
	p_mfpeak->p_mpeak[2]=new_peak_vec(2650); 
	p_mfpeak->p_mpeak[3]=new_peak_vec(2648); 

	p_mfpeak->p_fpeak[0]=new_peak_vec(4307);  
	p_mfpeak->p_fpeak[1]=new_peak_vec(4489); 
	p_mfpeak->p_fpeak[2]=new_peak_vec(4494); 
	p_mfpeak->p_fpeak[3]=new_peak_vec(4500); 
    
    peak_vec_read("/home/ubuntu/hai/data/1002mlocsch1.txt",p_mfpeak->p_mpeak[0]);
    peak_vec_read("/home/ubuntu/hai/data/1002mlocsch2.txt",p_mfpeak->p_mpeak[1]);
    peak_vec_read("/home/ubuntu/hai/data/1002mlocsch3.txt",p_mfpeak->p_mpeak[2]);
    peak_vec_read("/home/ubuntu/hai/data/1002mlocsch4.txt",p_mfpeak->p_mpeak[3]);
    
    peak_vec_read("/home/ubuntu/hai/data/1002flocsch1.txt",p_mfpeak->p_fpeak[0]);
    peak_vec_read("/home/ubuntu/hai/data/1002flocsch2.txt",p_mfpeak->p_fpeak[1]);
    peak_vec_read("/home/ubuntu/hai/data/1002flocsch3.txt",p_mfpeak->p_fpeak[2]);
    peak_vec_read("/home/ubuntu/hai/data/1002flocsch4.txt",p_mfpeak->p_fpeak[3]);
    
    return 0;
}