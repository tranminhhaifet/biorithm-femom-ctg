/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 04 OCT 2019 
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             04  OCT 2019    T.H          Original Code
*			  24  OCT 2019    T.H          After tested with NUH1021, 1029, 1014 
*******************************************************************************/
// SYSTEM INCLUDES
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>

// LOCAL INCLUDES
#include "biorithm_aux.h"				   // AUXILIARY FUNCTION 	
#include "biorithm_wavelib.h"			   // WAVELIB 
#include "preproc.h"					   // PREPROCESSING
#include "fhr_feature_detection.h"	       // FEATURE DETECTION
#include "fhr_feature_classification.h"    // FEATURE CLASSIFICATION
#include "fhr_baseline.h"				   // BASELINE AND ADCEL 	
#include "fhr_app_log.h"				   // LOG APP OUTPUT TO FILE	
#include "fhr_heartrate_cal.h"			   // COMPUTE HEART RATE
#include "render_img.h"					   // PLPLOT RENDER IMAGE 	
#include "fhr_ua.h"						   // UA UNIT 	

// DEBUG AND PRINT 
#define FHR_APP_PROGRESS_PRINT 
// #define FHR_APP_PRINT
#define FHR_APP_LOG
// #define FHR_APP_DEBUG

// #define FHR_TKEO_IPROD_MODE "TKEO"        // TKEO MODE FOR R PREK DETECTION	
#define FHR_TKEO_IPROD_MODE "IPROD"          // TKEO MODE FOR R PREK DETECTION	
		
// MIN PEAK DISTANCE 
static int GP_MFPEAK_DMIN[2] = {10, 10};

// MIN HEIGHT MPEAK AND FPEAK FOR EACH CHANNEL 
static double GP_MFPEAK_HMIN[2 * FHR_NCHANNEL] = {4.0,4.0,4.0,4.0,
												  3.0,3.0,3.0,3.0};

// REFERENCE HEIGHT THRESHOLD OF EACH CHANNEL FOR RMV MPEAK FROM IPROD 
static double GP_RHEIGHT[FHR_NCHANNEL] = {1.0, 1.0, 1.0, 1.0};

// MXIMUM WIDTH OF QRS 
static int G_FHR_QRS_WMAX = 50; 

// SET CHANNEL FOR RENDER FHR TRACE AND UA TRACE
static const int G_BEST_MCH_ID    = 0;          // BEST MATERNAL CHANNEL 
static const int G_RENDER_FCH_ID  = 0; 			// CHANNEL FOR RENDER FETAL
static const int G_RENDER_UCH_ID  = 3; 			// CHANNEL FOR RENDER UA 

// HEART RATE ULIM LLIM AND DLIM 


// INPUT ARG: (1) INPUT_DATA_PATH, (2) INPUT_DATA_LENGTH, (3) OUTPUT_DATA_PATH
int main(int argc, char *argv[])
{
	//==================================PARSE INPUT=============================
	// CHECK INPUT ARGUMENT 
	if (argc != 4)
	{
		printf("IN CORRECT INPUT: \n");
		exit(-1);
	}

	char *p_datalen; 
	char *p_path_src = argv[1];
	char *p_path_des = argv[3];
	unsigned int ecglen = strtol(argv[2], &p_datalen, 10);
	
	// CONFIRM INTPUT ARGUMENT 
	printf("DATA SOURCE PATH: %s\n", p_path_src);
	printf("DATA DESTINATION PATH: %s\n", p_path_des);
	printf("ECG DATA LENGTH: %ul\n", ecglen);
	
	//==============================PREPROCESSING===============================
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("LOADING DATA FROM FILE DISK: \n");
	#endif
	
	double_vec *pp_ecg[FHR_NCHANNEL];
	pp_ecg[0] = double_vec_new(ecglen);
	pp_ecg[1] = double_vec_new(ecglen);
	pp_ecg[2] = double_vec_new(ecglen);
	pp_ecg[3] = double_vec_new(ecglen);

	// READ INPUT DATA FROM FILE 
	read_from_txt_cols(p_path_src, pp_ecg, FHR_NCHANNEL);

	// PREPROCESSING 
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("PREPROCESSING: \n");
	#endif
	preproc_preprocess_ecg(pp_ecg, FHR_NCHANNEL);

	// LOG PREPROCESSING 
	#ifdef FHR_APP_LOG
	write_to_txt(pp_ecg, FHR_NCHANNEL, str_concat(p_path_des, "clean_ecg.txt"));
	#endif

    //=================================== UA ===================================
	// BUFFER ECG DATA TO UA 
	double_vec *pp_ecg_ua[FHR_NCHANNEL];
	pp_ecg_ua[0] = double_vec_new(ecglen);
	pp_ecg_ua[1] = double_vec_new(ecglen);
	pp_ecg_ua[2] = double_vec_new(ecglen);
	pp_ecg_ua[3] = double_vec_new(ecglen);

	double_vec_copy(pp_ecg[0], pp_ecg_ua[0]);
	double_vec_copy(pp_ecg[1], pp_ecg_ua[1]);
	double_vec_copy(pp_ecg[2], pp_ecg_ua[2]);
	double_vec_copy(pp_ecg[3], pp_ecg_ua[3]);

	struct uatrace *pp_ua[FHR_NCHANNEL];

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("UA PROCESSING CHANNEL 1: \n");
	#endif
	pp_ua[0] = compute_uc_trace(pp_ecg_ua[0]);

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("UA PROCESSING CHANNEL 2: \n");
	#endif
	pp_ua[1] = compute_uc_trace(pp_ecg_ua[1]);

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("UA PROCESSING CHANNEL 3: \n");
	#endif
	pp_ua[2] = compute_uc_trace(pp_ecg_ua[2]);

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("UA PROCESSING CHANNEL 4: \n");
	#endif
	pp_ua[3] = compute_uc_trace(pp_ecg_ua[3]);

	#ifdef FHR_APP_LOG
	uatrace_write(pp_ua[0], str_concat(p_path_des, "uatrace_ch_1.txt"));
	uatrace_write(pp_ua[1], str_concat(p_path_des, "uatrace_ch_2.txt"));
	uatrace_write(pp_ua[2], str_concat(p_path_des, "uatrace_ch_3.txt"));
	uatrace_write(pp_ua[3], str_concat(p_path_des, "uatrace_ch_4.txt"));
	#endif

	//=========================FEATURE DETECTION ===============================
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("FEATURE DETECTION: \n");
	#endif

	// FEATURE DETECTION 
	struct fhrfeature *p_feature = detect_fhr_feature(pp_ecg);

	// LOG TKEO AND IPROD
	#ifdef FHR_APP_LOG
	write_to_txt(p_feature->pp_tkeo,  FHR_NCHANNEL, str_concat(p_path_des, "tkeo.txt"));
	write_to_txt(p_feature->pp_iprod, FHR_NCHANNEL, str_concat(p_path_des, "iprod.txt"));
	#endif

	//=========================FEATURE CLASSIFICATION ==========================
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("FEATURE CLASSIFICATION BEST MATERNAL CHANNEL: %d\n", G_BEST_MCH_ID);
	#endif

	double_vec **pp_iprod = malloc(FHR_NCHANNEL * sizeof(double_vec *));
	unsigned int iprodlen = p_feature->pp_tkeo[0]->len;
	pp_iprod[0] = double_vec_new(iprodlen);
	pp_iprod[1] = double_vec_new(iprodlen);
	pp_iprod[2] = double_vec_new(iprodlen);
	pp_iprod[3] = double_vec_new(iprodlen);
	unsigned int align = floor((2*FHR_TKWIN - FHR_IPWIN + 1) / 2.0); 

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("%s MODE\n", FHR_TKEO_IPROD_MODE);
	#endif

	// TKEO MODE MEANS TO SET IPROD TO TKEO 
	if (strcmp(FHR_TKEO_IPROD_MODE,"TKEO")==0)
	{
		for (unsigned int i = 0; i< FHR_NCHANNEL; i++)
		{
			for (unsigned int j = 0; j < iprodlen; j++)
			{
				pp_iprod[i]->p_data[j] = p_feature->pp_tkeo[i]->p_data[j];
			}
		}
	}

	// TKEO MODE MEANS TO SET IPROD TO TKEO 
	if (strcmp(FHR_TKEO_IPROD_MODE,"IPROD")==0)
	{
		// IPROD MODE MEANS ALIGN TIME IPROD TO TKEO 
		for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
		{
			for (unsigned int j = 0; j < iprodlen; j++)
			{
				pp_iprod[i]->p_data[j] = p_feature->pp_iprod[i]->p_data[align + j];
			}
		}
	}

	// FIND M AND FPEAK FROM TKEO AND IRPOD 
	struct mfpeak *p_mfpeak = compute_mfpeak(p_feature->pp_tkeo, pp_iprod, 
	FHR_FINDPEAK_WLEN, GP_MFPEAK_DMIN, GP_MFPEAK_HMIN, GP_RHEIGHT, G_FHR_QRS_WMAX,
	G_BEST_MCH_ID);
	
	// PRINT FEATURE CLASSIFICATION OUTPUT 
	#ifdef FHR_APP_PRINT
	printf("channel 1 has %ul mpeak: \n", p_mfpeak->p_mpeak[0]->len);
	printf("channel 2 has %ul mpeak: \n", p_mfpeak->p_mpeak[1]->len);
	printf("channel 3 has %ul mpeak: \n", p_mfpeak->p_mpeak[2]->len);
	printf("channel 4 has %ul mpeak: \n", p_mfpeak->p_mpeak[3]->len);

	printf("channel 1 has %ul fpeak: \n", p_mfpeak->p_fpeak[0]->len);
	printf("channel 2 has %ul fpeak: \n", p_mfpeak->p_fpeak[1]->len);
	printf("channel 3 has %ul fpeak: \n", p_mfpeak->p_fpeak[2]->len);
	printf("channel 4 has %ul fpeak: \n", p_mfpeak->p_fpeak[3]->len);
	#endif

	#ifdef FHR_APP_LOG
	struct peaklog_vec **pp_mpeaklog = log_mpeak(pp_ecg, p_feature, p_mfpeak);
	struct peaklog_vec **pp_fpeaklog = log_fpeak(pp_ecg, p_feature, p_mfpeak);

	peaklog_vec_write(pp_mpeaklog[0], str_concat(p_path_des, "mpeak_app_log_ch_1.txt"));
	peaklog_vec_write(pp_mpeaklog[1], str_concat(p_path_des, "mpeak_app_log_ch_2.txt"));
	peaklog_vec_write(pp_mpeaklog[2], str_concat(p_path_des, "mpeak_app_log_ch_3.txt"));
	peaklog_vec_write(pp_mpeaklog[3], str_concat(p_path_des, "mpeak_app_log_ch_4.txt"));

	peaklog_vec_write(pp_fpeaklog[0], str_concat(p_path_des, "fpeak_app_log_ch_1.txt"));
	peaklog_vec_write(pp_fpeaklog[1], str_concat(p_path_des, "fpeak_app_log_ch_2.txt"));
	peaklog_vec_write(pp_fpeaklog[2], str_concat(p_path_des, "fpeak_app_log_ch_3.txt"));
	peaklog_vec_write(pp_fpeaklog[3], str_concat(p_path_des, "fpeak_app_log_ch_4.txt"));
	#endif

	//========================= HEART RATE CALCULATION =========================
	// TODO DOUBLE CHECK 
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("COMPUTING HEART RATE FROM PEAK LOCS: \n");
	#endif

	struct rpeak_vec **pp_mheartrate =  malloc(FHR_NCHANNEL * sizeof(struct rpeak_vec *)); 
	struct rpeak_vec **pp_fheartrate =  malloc(FHR_NCHANNEL * sizeof(struct rpeak_vec *));

	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		pp_mheartrate[i] = compute_heartrate(p_mfpeak->p_mpeak[i], FHR_MBPM_ULIM, 
		FHR_MBPM_LLIM, FHR_MBPM_DLIM);

		pp_fheartrate[i] = compute_heartrate(p_mfpeak->p_fpeak[i], FHR_FBPM_ULIM,
		FHR_FBPM_LLIM, FHR_FBPM_DLIM);
	}

	#ifdef FHR_APP_LOG
	rpeak_vec_write(pp_mheartrate[0], str_concat(p_path_des, "mheartrate_log_ch_1.txt"));
	rpeak_vec_write(pp_mheartrate[1], str_concat(p_path_des, "mheartrate_log_ch_2.txt"));
	rpeak_vec_write(pp_mheartrate[2], str_concat(p_path_des, "mheartrate_log_ch_3.txt"));
	rpeak_vec_write(pp_mheartrate[3], str_concat(p_path_des, "mheartrate_log_ch_4.txt"));
	
	rpeak_vec_write(pp_fheartrate[0], str_concat(p_path_des, "fheartrate_log_ch_1.txt"));
	rpeak_vec_write(pp_fheartrate[1], str_concat(p_path_des, "fheartrate_log_ch_2.txt"));
	rpeak_vec_write(pp_fheartrate[2], str_concat(p_path_des, "fheartrate_log_ch_3.txt"));
	rpeak_vec_write(pp_fheartrate[3], str_concat(p_path_des, "fheartrate_log_ch_4.txt"));
	#endif

	//=============================BASELINE=====================================
	//SAMPLING HEART RATE FIX 250ms
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("FINDING BASELINE: \n");
	#endif

	double_vec **pp_ftrace = sample_heartrate(pp_fheartrate, ecglen);
	double_vec **pp_mtrace = sample_heartrate(pp_mheartrate, ecglen);

	//FIND BASELINE AND ADCEL 
	baseline **pp_baseline = malloc((FHR_NCHANNEL + 1) * sizeof(baseline *));
	for (unsigned int i = 0; i < FHR_NCHANNEL + 1; i++)
	{
		pp_baseline[i] = find_baseline(pp_ftrace[i], FHR_BIN_FREQ);
	} 


	#ifdef FHR_APP_LOG
	
	// FHR TRACE 
	write_to_txt(pp_ftrace, FHR_NCHANNEL + 1, str_concat(p_path_des, "fheartrate_trace_all.txt"));
	write_to_txt(pp_mtrace, FHR_NCHANNEL + 1, str_concat(p_path_des, "mheartrate_trace_all.txt"));

	//BASELINE 
	double_vec_write((pp_baseline[0]->p_base), str_concat(p_path_des, "baseline_ch1.txt"));
	double_vec_write((pp_baseline[1]->p_base), str_concat(p_path_des, "baseline_ch2.txt"));
	double_vec_write((pp_baseline[2]->p_base), str_concat(p_path_des, "baseline_ch3.txt"));
	double_vec_write((pp_baseline[3]->p_base), str_concat(p_path_des, "baseline_ch4.txt"));
	double_vec_write((pp_baseline[4]->p_base), str_concat(p_path_des, "baseline_ch5.txt"));

	//FASTWAVE
	double_vec_write((pp_baseline[0]->p_fast), str_concat(p_path_des, "fastwave_ch1.txt"));
	double_vec_write((pp_baseline[1]->p_fast), str_concat(p_path_des, "fastwave_ch2.txt"));
	double_vec_write((pp_baseline[2]->p_fast), str_concat(p_path_des, "fastwave_ch3.txt"));
	double_vec_write((pp_baseline[3]->p_fast), str_concat(p_path_des, "fastwave_ch4.txt"));
	double_vec_write((pp_baseline[4]->p_fast), str_concat(p_path_des, "fastwave_ch5.txt"));

	//SLOWWAVE
	double_vec_write((pp_baseline[0]->p_slow), str_concat(p_path_des, "slowwave_ch1.txt"));
	double_vec_write((pp_baseline[1]->p_slow), str_concat(p_path_des, "slowwave_ch2.txt"));
	double_vec_write((pp_baseline[2]->p_slow), str_concat(p_path_des, "slowwave_ch3.txt"));
	double_vec_write((pp_baseline[3]->p_slow), str_concat(p_path_des, "slowwave_ch4.txt"));
	double_vec_write((pp_baseline[4]->p_slow), str_concat(p_path_des, "slowwave_ch5.txt"));

	//ADCEL
	if(pp_baseline[0]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_des, "adcel_ch1.txt"), (pp_baseline[0]->p_ad));
	}

	if(pp_baseline[1]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_des, "adcel_ch2.txt"), (pp_baseline[1]->p_ad));
	}

	if(pp_baseline[2]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_des, "adcel_ch3.txt"), (pp_baseline[2]->p_ad));
	}

	if(pp_baseline[3]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_des, "adcel_ch4.txt"), (pp_baseline[3]->p_ad));
	}

	if(pp_baseline[4]->p_ad != NULL)
	{
		ad_point_vec_write(str_concat(p_path_des, "adcel_ch5.txt"), (pp_baseline[4]->p_ad));
	}
	#endif

	//============================= UA DATA ====================================
	// CALL UA COMPUTATION HERE 
	double_vec *pp_uat = double_vec_new(pp_ftrace[G_RENDER_UCH_ID]->len);
	unsigned int uaoffset = floor( (pp_uat->len - pp_ua[G_RENDER_UCH_ID]->p_uc->len) / 2); 

	for (unsigned int i = 0; i < pp_ua[G_RENDER_UCH_ID]->p_scaleuc->len; i++)
	{
		pp_uat->p_data[i + uaoffset] = pp_ua[G_RENDER_UCH_ID]->p_scaleuc->p_data[i];
	}

	//=============================PLPLOT RENDER IMAGE==========================
	#ifdef FHR_APP_PROGRESS_PRINT
	printf("PLPLOT RENDER IMAGE:\n");
	#endif

	// TRACE SPEED 1CM/MINUTE
	int renstate_1 = render_img(1, pp_ftrace[G_RENDER_FCH_ID], pp_mtrace[3], 
	pp_baseline[G_RENDER_FCH_ID], pp_uat, p_path_des);

	// TRACE SPEED 2CM/MINUTE
	int renstate_2 = render_img(2, pp_ftrace[G_RENDER_FCH_ID], pp_mtrace[3], 
	pp_baseline[G_RENDER_FCH_ID], pp_uat, p_path_des);

	// TRACE SPEED 3CM/MINUTE
	int renstate_3 = render_img(3, pp_ftrace[G_RENDER_FCH_ID], pp_mtrace[3], 
	pp_baseline[G_RENDER_FCH_ID], pp_uat, p_path_des);

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("RENDER IMAGE STATE: %d %d %d %s\n", renstate_1, renstate_2, renstate_3,
	p_path_des);
	#endif

	//=============================FREE MEM=====================================
	// FREE RAW ECG FHR 
	double_vec_delete(pp_ecg[0]);
	double_vec_delete(pp_ecg[1]);
	double_vec_delete(pp_ecg[2]);
	double_vec_delete(pp_ecg[3]);

	// FREE RAW ECG UA 
	double_vec_delete(pp_ecg_ua[0]);
	double_vec_delete(pp_ecg_ua[1]);
	double_vec_delete(pp_ecg_ua[2]);
	double_vec_delete(pp_ecg_ua[3]);

	// FREE PREPROCESSING OUTPUT 

	// FREE DETECTION OUTPUT
	double_vec_delete(p_feature->pp_tkeo[0]);
	double_vec_delete(p_feature->pp_tkeo[1]);
	double_vec_delete(p_feature->pp_tkeo[2]);
	double_vec_delete(p_feature->pp_tkeo[3]);
	double_vec_delete(p_feature->pp_iprod[0]);
	double_vec_delete(p_feature->pp_iprod[1]);
	double_vec_delete(p_feature->pp_iprod[2]);
	double_vec_delete(p_feature->pp_iprod[3]);
	free(p_feature); 

	// FREE CLASSIFICATION OUTPUT 
	double_vec_delete(pp_iprod[0]);
	double_vec_delete(pp_iprod[1]);
	double_vec_delete(pp_iprod[2]);
	double_vec_delete(pp_iprod[3]);

	delete_peak_vec(p_mfpeak->p_mpeak[0]);
	delete_peak_vec(p_mfpeak->p_mpeak[1]);
	delete_peak_vec(p_mfpeak->p_mpeak[2]);
	delete_peak_vec(p_mfpeak->p_mpeak[3]);

	delete_peak_vec(p_mfpeak->p_fpeak[0]);
	delete_peak_vec(p_mfpeak->p_fpeak[1]);
	delete_peak_vec(p_mfpeak->p_fpeak[2]);
	delete_peak_vec(p_mfpeak->p_fpeak[3]);

	#ifdef FHR_APP_LOG
	peaklog_vec_delete(pp_mpeaklog[0]);
	peaklog_vec_delete(pp_mpeaklog[1]);
	peaklog_vec_delete(pp_mpeaklog[2]);
	peaklog_vec_delete(pp_mpeaklog[3]);
	peaklog_vec_delete(pp_fpeaklog[0]);
	peaklog_vec_delete(pp_fpeaklog[1]);
	peaklog_vec_delete(pp_fpeaklog[2]);
	peaklog_vec_delete(pp_fpeaklog[3]);
	#endif

	// HEART RATE CALCULATION 
	rpeak_vec_delete(pp_mheartrate[0]);
	rpeak_vec_delete(pp_mheartrate[1]);
	rpeak_vec_delete(pp_mheartrate[2]);
	rpeak_vec_delete(pp_mheartrate[3]);

	rpeak_vec_delete(pp_fheartrate[0]);
	rpeak_vec_delete(pp_fheartrate[1]);
	rpeak_vec_delete(pp_fheartrate[2]);
	rpeak_vec_delete(pp_fheartrate[3]);

	// FREE BASELINE OUTPUT 
	double_vec_delete(pp_ftrace[0]);
	double_vec_delete(pp_ftrace[1]);
	double_vec_delete(pp_ftrace[2]);
	double_vec_delete(pp_ftrace[3]);
	double_vec_delete(pp_ftrace[4]);

	double_vec_delete(pp_mtrace[0]);
	double_vec_delete(pp_mtrace[1]);
	double_vec_delete(pp_mtrace[2]);
	double_vec_delete(pp_mtrace[3]);
	double_vec_delete(pp_mtrace[4]);

	double_vec_delete(pp_baseline[0]->p_base);
	double_vec_delete(pp_baseline[1]->p_base);
	double_vec_delete(pp_baseline[2]->p_base);
	double_vec_delete(pp_baseline[3]->p_base);
	double_vec_delete(pp_baseline[4]->p_base);

	double_vec_delete(pp_baseline[0]->p_fast);
	double_vec_delete(pp_baseline[1]->p_fast);
	double_vec_delete(pp_baseline[2]->p_fast);
	double_vec_delete(pp_baseline[3]->p_fast);
	double_vec_delete(pp_baseline[4]->p_fast);

	double_vec_delete(pp_baseline[0]->p_slow);
	double_vec_delete(pp_baseline[1]->p_slow);
	double_vec_delete(pp_baseline[2]->p_slow);
	double_vec_delete(pp_baseline[3]->p_slow);
	double_vec_delete(pp_baseline[4]->p_slow);

	double_vec_delete(pp_baseline[0]->p_diff);
	double_vec_delete(pp_baseline[1]->p_diff);
	double_vec_delete(pp_baseline[2]->p_diff);
	double_vec_delete(pp_baseline[3]->p_diff);
	double_vec_delete(pp_baseline[4]->p_diff);

	ad_point_vec_delete(pp_baseline[0]->p_ad);
	ad_point_vec_delete(pp_baseline[1]->p_ad);
	ad_point_vec_delete(pp_baseline[2]->p_ad);
	ad_point_vec_delete(pp_baseline[3]->p_ad);

	// FREE UA PLOT 
	double_vec_delete(pp_uat);

	// FREE PLOT OUTPUT 

	return 0;
}