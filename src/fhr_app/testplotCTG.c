/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 04 OCT 2019 
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             04  OCT 2019    T.H          Original Code
*			  24  OCT 2019    T.H          After tested with NUH1021, 1029, 1014 
*******************************************************************************/
// SYSTEM INCLUDES
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>

// LOCAL INCLUDES
#include "biorithm_aux.h"				   // AUXILIARY FUNCTION 	
#include "biorithm_wavelib.h"			   // WAVELIB 
#include "preproc.h"					   // PREPROCESSING
#include "fhr_feature_detection.h"	       // FEATURE DETECTION
#include "fhr_feature_classification.h"    // FEATURE CLASSIFICATION
#include "fhr_baseline.h"				   // BASELINE AND ADCEL 	
#include "fhr_app_log.h"				   // LOG APP OUTPUT TO FILE	
#include "fhr_heartrate_cal.h"			   // COMPUTE HEART RATE
#include "render_img.h"					   // PLPLOT RENDER IMAGE 	
#include "fhr_ua.h"						   // UA UNIT 	

// DEBUG AND PRINT 
#define FHR_APP_PROGRESS_PRINT 
// #define FHR_APP_PRINT
#define FHR_APP_LOG
// #define FHR_APP_DEBUG

// #define FHR_TKEO_IPROD_MODE "TKEO"        // TKEO MODE FOR R PREK DETECTION	
#define FHR_TKEO_IPROD_MODE "IPROD"          // TKEO MODE FOR R PREK DETECTION	
		
// MIN PEAK DISTANCE 
static int GP_MFPEAK_DMIN[2] = {10, 10};

// MIN HEIGHT MPEAK AND FPEAK FOR EACH CHANNEL 
static double GP_MFPEAK_HMIN[2 * FHR_NCHANNEL] = {4.0,4.0,4.0,4.0,
												  3.0,3.0,3.0,3.0};

// REFERENCE HEIGHT THRESHOLD OF EACH CHANNEL FOR RMV MPEAK FROM IPROD 
static double GP_RHEIGHT[FHR_NCHANNEL] = {1.0, 1.0, 1.0, 1.0};

// MXIMUM WIDTH OF QRS 
static int G_FHR_QRS_WMAX = 50; 

// SET CHANNEL FOR RENDER FHR TRACE AND UA TRACE
static const int G_BEST_MCH_ID    = 0;          // BEST MATERNAL CHANNEL 
static const int G_RENDER_FCH_ID  = 0; 			// CHANNEL FOR RENDER FETAL
static const int G_RENDER_UCH_ID  = 3; 			// CHANNEL FOR RENDER UA 

// HEART RATE ULIM LLIM AND DLIM 


// INPUT ARG: (1) INPUT_DATA_PATH, (2) INPUT_DATA_LENGTH, (3) OUTPUT_DATA_PATH
int plotCTG()
{
	char *p_path_des = "/home/ubuntu/hai/biorithm-femom-ctg/test/data/testplotCTG1027";
	unsigned int ecglen = 1236741;
	
    
    //========================= LOAD PEAK LOCS PYTHON ==========================
    struct mfpeak *p_mfpeak = malloc(sizeof(struct mfpeak));
	p_mfpeak->p_mpeak[0]=new_peak_vec(3449); 
	p_mfpeak->p_mpeak[1]=new_peak_vec(3448); 
	p_mfpeak->p_mpeak[2]=new_peak_vec(3449); 
	p_mfpeak->p_mpeak[3]=new_peak_vec(3449); 

	p_mfpeak->p_fpeak[0]=new_peak_vec(5696);  
	p_mfpeak->p_fpeak[1]=new_peak_vec(5571); 
	p_mfpeak->p_fpeak[2]=new_peak_vec(5662); 
	p_mfpeak->p_fpeak[3]=new_peak_vec(5782); 
    
    peak_vec_read("/home/ubuntu/hai/data/1027mlocsch1.txt",p_mfpeak->p_mpeak[0]);
    peak_vec_read("/home/ubuntu/hai/data/1027mlocsch2.txt",p_mfpeak->p_mpeak[1]);
    peak_vec_read("/home/ubuntu/hai/data/1027mlocsch3.txt",p_mfpeak->p_mpeak[2]);
    peak_vec_read("/home/ubuntu/hai/data/1027mlocsch4.txt",p_mfpeak->p_mpeak[3]);
    
    peak_vec_read("/home/ubuntu/hai/data/1027flocsch1.txt",p_mfpeak->p_fpeak[0]);
    peak_vec_read("/home/ubuntu/hai/data/1027flocsch2.txt",p_mfpeak->p_fpeak[1]);
    peak_vec_read("/home/ubuntu/hai/data/1027flocsch3.txt",p_mfpeak->p_fpeak[2]);
    peak_vec_read("/home/ubuntu/hai/data/1027flocsch4.txt",p_mfpeak->p_fpeak[3]);
    
	//========================= HEART RATE CALCULATION =========================
	struct rpeak_vec **pp_mheartrate =  malloc(FHR_NCHANNEL * sizeof(struct rpeak_vec *)); 
	struct rpeak_vec **pp_fheartrate =  malloc(FHR_NCHANNEL * sizeof(struct rpeak_vec *));

	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		pp_mheartrate[i] = compute_heartrate(p_mfpeak->p_mpeak[i], FHR_MBPM_ULIM, 
		FHR_MBPM_LLIM, FHR_MBPM_DLIM);

		pp_fheartrate[i] = compute_heartrate(p_mfpeak->p_fpeak[i], FHR_FBPM_ULIM,
		FHR_FBPM_LLIM, FHR_FBPM_DLIM);
	}

	#ifdef FHR_APP_LOG
	rpeak_vec_write(pp_mheartrate[0], str_concat(p_path_des, "mheartrate_log_ch_1.txt"));
	rpeak_vec_write(pp_mheartrate[1], str_concat(p_path_des, "mheartrate_log_ch_2.txt"));
	rpeak_vec_write(pp_mheartrate[2], str_concat(p_path_des, "mheartrate_log_ch_3.txt"));
	rpeak_vec_write(pp_mheartrate[3], str_concat(p_path_des, "mheartrate_log_ch_4.txt"));
	
	rpeak_vec_write(pp_fheartrate[0], str_concat(p_path_des, "fheartrate_log_ch_1.txt"));
	rpeak_vec_write(pp_fheartrate[1], str_concat(p_path_des, "fheartrate_log_ch_2.txt"));
	rpeak_vec_write(pp_fheartrate[2], str_concat(p_path_des, "fheartrate_log_ch_3.txt"));
	rpeak_vec_write(pp_fheartrate[3], str_concat(p_path_des, "fheartrate_log_ch_4.txt"));
	#endif

	//=============================BASELINE=====================================
	double_vec **pp_ftrace = sample_heartrate(pp_fheartrate, ecglen);
	double_vec **pp_mtrace = sample_heartrate(pp_mheartrate, ecglen);

	//FIND BASELINE AND ADCEL 
	baseline **pp_baseline = malloc((FHR_NCHANNEL + 1) * sizeof(baseline *));
	for (unsigned int i = 0; i < FHR_NCHANNEL + 1; i++)
	{
		pp_baseline[i] = find_baseline(pp_ftrace[i], FHR_BIN_FREQ);
	} 

	//============================= UA DATA ====================================
	double_vec *pp_uat = double_vec_new(pp_ftrace[G_RENDER_UCH_ID]->len);
	unsigned int uaoffset = 0; 

	for (unsigned int i = 0; i < pp_uat->len; i++)
	{
		pp_uat->p_data[i + uaoffset] = 10.0;
	}

	//=============================PLPLOT RENDER IMAGE==========================
	// TRACE SPEED 1CM/MINUTE
	int renstate_1 = render_img(1, pp_ftrace[G_RENDER_FCH_ID], pp_mtrace[3], 
	pp_baseline[G_RENDER_FCH_ID], pp_uat, p_path_des);

	// TRACE SPEED 2CM/MINUTE
	int renstate_2 = render_img(2, pp_ftrace[G_RENDER_FCH_ID], pp_mtrace[3], 
	pp_baseline[G_RENDER_FCH_ID], pp_uat, p_path_des);

	// TRACE SPEED 3CM/MINUTE
	int renstate_3 = render_img(3, pp_ftrace[G_RENDER_FCH_ID], pp_mtrace[3], 
	pp_baseline[G_RENDER_FCH_ID], pp_uat, p_path_des);

	#ifdef FHR_APP_PROGRESS_PRINT
	printf("RENDER IMAGE STATE: %d %d %d %s\n", renstate_1, renstate_2, renstate_3,
	p_path_des);
	#endif

	//=============================FREE MEM=====================================
	delete_peak_vec(p_mfpeak->p_mpeak[0]);
	delete_peak_vec(p_mfpeak->p_mpeak[1]);
	delete_peak_vec(p_mfpeak->p_mpeak[2]);
	delete_peak_vec(p_mfpeak->p_mpeak[3]);

	delete_peak_vec(p_mfpeak->p_fpeak[0]);
	delete_peak_vec(p_mfpeak->p_fpeak[1]);
	delete_peak_vec(p_mfpeak->p_fpeak[2]);
	delete_peak_vec(p_mfpeak->p_fpeak[3]);

	// HEART RATE CALCULATION 
	rpeak_vec_delete(pp_mheartrate[0]);
	rpeak_vec_delete(pp_mheartrate[1]);
	rpeak_vec_delete(pp_mheartrate[2]);
	rpeak_vec_delete(pp_mheartrate[3]);

	rpeak_vec_delete(pp_fheartrate[0]);
	rpeak_vec_delete(pp_fheartrate[1]);
	rpeak_vec_delete(pp_fheartrate[2]);
	rpeak_vec_delete(pp_fheartrate[3]);

	// FREE BASELINE OUTPUT 
	double_vec_delete(pp_ftrace[0]);
	double_vec_delete(pp_ftrace[1]);
	double_vec_delete(pp_ftrace[2]);
	double_vec_delete(pp_ftrace[3]);
	double_vec_delete(pp_ftrace[4]);

	double_vec_delete(pp_mtrace[0]);
	double_vec_delete(pp_mtrace[1]);
	double_vec_delete(pp_mtrace[2]);
	double_vec_delete(pp_mtrace[3]);
	double_vec_delete(pp_mtrace[4]);

	double_vec_delete(pp_baseline[0]->p_base);
	double_vec_delete(pp_baseline[1]->p_base);
	double_vec_delete(pp_baseline[2]->p_base);
	double_vec_delete(pp_baseline[3]->p_base);
	double_vec_delete(pp_baseline[4]->p_base);

	double_vec_delete(pp_baseline[0]->p_fast);
	double_vec_delete(pp_baseline[1]->p_fast);
	double_vec_delete(pp_baseline[2]->p_fast);
	double_vec_delete(pp_baseline[3]->p_fast);
	double_vec_delete(pp_baseline[4]->p_fast);

	double_vec_delete(pp_baseline[0]->p_slow);
	double_vec_delete(pp_baseline[1]->p_slow);
	double_vec_delete(pp_baseline[2]->p_slow);
	double_vec_delete(pp_baseline[3]->p_slow);
	double_vec_delete(pp_baseline[4]->p_slow);

	double_vec_delete(pp_baseline[0]->p_diff);
	double_vec_delete(pp_baseline[1]->p_diff);
	double_vec_delete(pp_baseline[2]->p_diff);
	double_vec_delete(pp_baseline[3]->p_diff);
	double_vec_delete(pp_baseline[4]->p_diff);

	ad_point_vec_delete(pp_baseline[0]->p_ad);
	ad_point_vec_delete(pp_baseline[1]->p_ad);
	ad_point_vec_delete(pp_baseline[2]->p_ad);
	ad_point_vec_delete(pp_baseline[3]->p_ad);

	// FREE UA PLOT 
	double_vec_delete(pp_uat);

	return 0;
}


int main()
{
    plotCTG();
    return 0; 
}