/*******************************************************************************
*      Copyright Ã‚Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 03 OCT 2019
*
*
* Revision History:
* Ref No      Date           Who          Detail
*******************************************************************************/
// SYSTEM INCLUDES
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>

// LOCAL INCLUDE 
#include "fhr_app_log.h"
#include "biorithm_aux.h"
#include "fhr_findpeaks.h"
#include "fhr_feature_detection.h"
#include "fhr_feature_classification.h"

/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
struct peaklog_vec *peaklog_vec_new(size_t npeak)
{
	// CHECK LENGHTH OF NPEAK 
	if (npeak <= 0 || npeak > SIZE_MAX / sizeof(struct peaklog)) 
	{
		ERROR_OVERFLOW;
		return NULL;
	}

	// ALLOCATE A POINTER OF STRUCT PEAKLOG_VEC
	struct peaklog_vec *p = malloc(sizeof(struct peaklog_vec));

	// CHECK NULL POINTER RETURNED 
	CHECK_ACCESS_NULL_POINTER(p);

	// ALLOCATE AN ARRAY NPEAK OF PEAKLOG 
	p->p_peaklog = malloc(npeak * sizeof(struct peaklog));

	// CHECK ALLOCATE ERROR 
	CHECK_ACCESS_NULL_POINTER(p->p_peaklog);

	// INIT VALUE TO ZERO 
	for (unsigned int i = 0; i < npeak; i++)
	{
		p->p_peaklog[i].tkloc = 0; 
		p->p_peaklog[i].iploc = 0; 
		p->p_peaklog[i].ecloc = 0; 
		p->p_peaklog[i].tkeoa = 0.0; 
		p->p_peaklog[i].ecga = 0.0; 
		p->p_peaklog[i].width = 0.0; 
		p->p_peaklog[i].flag = false;
	}

	// SET LENGTH 
	p->len = npeak;

	return p; 
}

/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
void peaklog_vec_delete(struct peaklog_vec *p)
{
	// CHECK ACCESS NULL POINTER 
	CHECK_ACCESS_NULL_POINTER(p);

	// FREE MEM 
	free(p->p_peaklog);
	free(p);
}

/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
void peaklog_vec_print(struct peaklog_vec *p, unsigned int npeak)
{
	CHECK_ACCESS_NULL_POINTER(p);

	for (unsigned int i = 0; i < npeak; i++)
	{
		printf("i=%ul,tkloc=%ul,iploc=%ul,ecloc=%ul,tkeoa=%3.15f,iproda=%3.15f,ecga=%3.15f,width=%3.15f,flag=%d\n", 
		i, p->p_peaklog[i].tkloc, p->p_peaklog[i].iploc, p->p_peaklog[i].ecloc, 
		p->p_peaklog[i].tkeoa, p->p_peaklog[i].iproda, p->p_peaklog[i].ecga, 
		p->p_peaklog[i].width, p->p_peaklog[i].flag);
	}
}
/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/

void peaklog_vec_write(struct peaklog_vec *p, const char *p_path)
{
	// FILE 
	FILE *p_file = fopen(p_path,"w");

	// CHECK NULL FILE 
	CHECK_ACCESS_NULL_POINTER(p_file);

	// CHECK ACCESS NULL DATA
	if (p!=NULL)
	{
		// WRITE DATA TO FILE 
		for (unsigned int i = 0; i < p->len; i++)
		{
			fprintf(p_file, "%ul %ul %ul %3.32f %3.32f %3.32f %3.32f %d\n", 
			p->p_peaklog[i].tkloc, p->p_peaklog[i].iploc, p->p_peaklog[i].ecloc, 
			p->p_peaklog[i].tkeoa, p->p_peaklog[i].iproda, p->p_peaklog[i].ecga, 
			p->p_peaklog[i].width, p->p_peaklog[i].flag);
		}

		// CLOSE FILE
		fclose(p_file);
	}
	else
	{
		printf("WARRNING WRITE NULL PEAKLOG TO FILE\n");
	}
}

/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
struct peaklog_vec *peaklog_vec_read(const char *p_path, unsigned int npeak)
{
	struct peaklog_vec *p = peaklog_vec_new(npeak);

	CHECK_ACCESS_NULL_POINTER(p);

	// FILE OPEN 
	FILE *p_file = fopen(p_path,"r");

	// READ INTO VECTOR OF PEAKLOG 
	unsigned int tkloc, iploc, ecloc = 0; 
	double ecga, tkeoa, iproda, width = 0.0; 
	int flag = false; 
	for (unsigned int i = 0; i < npeak; i++)
	{
		fscanf(p_file, "%d %d %d %lg %lg %lg %lg %d\n", &tkloc, &iploc, &ecloc, 
		&ecga, &tkeoa, &iproda, &width, &flag);

		p->p_peaklog[i].tkloc = tkloc;
		p->p_peaklog[i].iploc = iploc;
		p->p_peaklog[i].ecloc = ecloc;
		p->p_peaklog[i].ecga = ecga;
		p->p_peaklog[i].tkeoa = tkeoa;
		p->p_peaklog[i].iproda = iproda;
		p->p_peaklog[i].width = width;
		p->p_peaklog[i].flag = flag; 
	}

	// CLOSE FILE 
	fclose(p_file);

	// return 
	return p;
}

struct peaklog_vec **log_mpeak(double_vec **pp_ecg, struct fhrfeature *p_feature,
struct mfpeak *p_mfpeak)
{
	// CHECK ACCESS NULL POINTER 
	 CHECK_ACCESS_NULL_POINTER(pp_ecg);
	 CHECK_ACCESS_NULL_POINTER(p_feature);
	 CHECK_ACCESS_NULL_POINTER(p_mfpeak);
	// 
	unsigned int tkloc, iploc, ecloc = 0; 
	double tkeoa, iproda, ecga, width = 0.0; 
	bool flag = false;

 	// INIT OUTPUT 
	struct peaklog_vec **pp_mpeaklog = malloc(FHR_NCHANNEL * sizeof(struct peaklog_vec *));

	// NUMBER OF MPEAK OF EACH CHANNEL 
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		if (p_mfpeak->p_mpeak[i] != NULL)
		{
			pp_mpeaklog[i] = peaklog_vec_new(p_mfpeak->p_mpeak[i]->len);

			for (unsigned int j = 0; j < p_mfpeak->p_mpeak[i]->len; j++)
			{
				// MPEAK FOUND FROM TKEO 
				tkloc = p_mfpeak->p_mpeak[i]->p_peak[j].loc; 
				tkeoa = p_mfpeak->p_mpeak[i]->p_peak[j].mag;
				width = p_mfpeak->p_mpeak[i]->p_peak[j].width;
				flag  = p_mfpeak->p_mpeak[i]->p_peak[j].flag;  

				// MPEAK LOC AND AMP ON IPROD IPROLEN = N-IPWIN
				// iploc  = tkloc + FHR_TKWIN - floor(FHR_IPWIN / 2);
				iploc = tkloc;
				iproda = p_feature->pp_iprod[i]->p_data[iploc]; 

				// MPEAK LOC AN DAMP ON ECG IPROLEN = N-2*TKWIN-1   
				ecloc = tkloc + FHR_TKWIN; 
				ecga = pp_ecg[i]->p_data[ecloc]; 

				// FORM OUTPUT 
				pp_mpeaklog[i]->p_peaklog[j].tkloc  = tkloc;
				pp_mpeaklog[i]->p_peaklog[j].iploc  = iploc;
				pp_mpeaklog[i]->p_peaklog[j].ecloc  = ecloc;
				pp_mpeaklog[i]->p_peaklog[j].tkeoa  = tkeoa;
				pp_mpeaklog[i]->p_peaklog[j].iproda = iproda;
				pp_mpeaklog[i]->p_peaklog[j].ecga   = ecga;
				pp_mpeaklog[i]->p_peaklog[j].width  = width;
				pp_mpeaklog[i]->p_peaklog[j].flag   = flag;

			}
		}
	}

	return pp_mpeaklog; 
}


struct peaklog_vec **log_fpeak(double_vec **pp_ecg, struct fhrfeature *p_feature,
struct mfpeak *p_mfpeak)
{
	// CHECK ACCESS NULL POINTER 
	 CHECK_ACCESS_NULL_POINTER(pp_ecg);
	 CHECK_ACCESS_NULL_POINTER(p_feature);
	 CHECK_ACCESS_NULL_POINTER(p_mfpeak);

	// 
	unsigned int tkloc, iploc, ecloc = 0; 
	double tkeoa, iproda, ecga, width = 0.0; 
	bool flag = false;

 	// INIT OUTPUT 
	struct peaklog_vec **pp_fpeaklog = malloc(FHR_NCHANNEL * sizeof(struct peaklog_vec *));

	// NUMBER OF MPEAK OF EACH CHANNEL 
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		if (p_mfpeak->p_fpeak[i] != NULL)
		{
			pp_fpeaklog[i] = peaklog_vec_new(p_mfpeak->p_fpeak[i]->len);

			for (unsigned int j = 0; j < p_mfpeak->p_fpeak[i]->len; j++)
			{
				// MPEAK FOUND FROM IPROD 
				iploc  = p_mfpeak->p_fpeak[i]->p_peak[j].loc; 
				iproda = p_mfpeak->p_fpeak[i]->p_peak[j].mag;
				width  = p_mfpeak->p_fpeak[i]->p_peak[j].width;
				flag   = p_mfpeak->p_fpeak[i]->p_peak[j].flag;  

				// MPEAK LOC AND AMP ON IPROD TKEOLEN = N-2*TKWIN-1
				// tkloc  = iploc - FHR_TKWIN +  floor(FHR_IPWIN / 2);
				tkloc = iploc;
				tkeoa  = p_feature->pp_tkeo[i]->p_data[tkloc]; 

				// MPEAK LOC AN DAMP ON ECG IPROLEN = N - IPWIN    
				// ecloc = iploc + floor(FHR_IPWIN / 2); 
				ecloc = tkloc + FHR_TKWIN; 
				ecga = pp_ecg[i]->p_data[ecloc]; 

				// FORM OUTPUT 
				pp_fpeaklog[i]->p_peaklog[j].tkloc  = tkloc;
				pp_fpeaklog[i]->p_peaklog[j].iploc  = iploc;
				pp_fpeaklog[i]->p_peaklog[j].ecloc  = ecloc;
				pp_fpeaklog[i]->p_peaklog[j].tkeoa  = tkeoa;
				pp_fpeaklog[i]->p_peaklog[j].iproda = iproda;
				pp_fpeaklog[i]->p_peaklog[j].ecga   = ecga;
				pp_fpeaklog[i]->p_peaklog[j].width  = width;
				pp_fpeaklog[i]->p_peaklog[j].flag   = flag;

			}
		}
	}

	return pp_fpeaklog; 
}