/*******************************************************************************
*      Copyright Ã‚Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: fhr_app_log.h
* Original Author: TRAN MINH HAI
* Date created: 03 OCT 2019
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
********************************************************************************/
#ifndef FHR_APP_LOG_H
#define FHR_APP_LOG_H

// SYSTEM INCLUCDE
#include <math.h>
#include <float.h>
#include <errno.h>
#include <stdio.h>

// LOCAL INCLUDE 
#include "fhr_findpeaks.h"
#include "fhr_app_log.h"
#include "biorithm_aux.h"
#include "fhr_findpeaks.h"
#include "fhr_feature_detection.h"
#include "fhr_feature_classification.h"

#ifndef FHR_NCHANNEL
#define FHR_NCHANNEL				4
#endif


// LOG MODE 

// PEAK LOG STRUCT 
struct peaklog
{	
	unsigned int tkloc; 				// PEAK LOCATION IN TKEO 
	unsigned int iploc;					// PEAK LOCATION IN IRPOD
	unsigned int ecloc;					// PEAK LOCATION IN ECG
	double ecga;						// PEAK AMPLTITUDE ON ECG CLEAN
	double tkeoa;						// PEAK AMPLTITUDE ON TKEO
	double iproda; 						// PEAK AMPLTITUDE ON IPROD
	double width; 						// PEAK WIDTH
	bool flag; 							// PEAK TYPE M OR F 
};

// PEAK LOG VECTOR 
struct peaklog_vec
{
    struct peaklog *p_peaklog; 
    unsigned int len; 
};


/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
struct peaklog_vec *peaklog_vec_new(size_t npeak);

/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
void peaklog_vec_delete(struct peaklog_vec *p);


/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
void peaklog_vec_print(struct peaklog_vec *p, unsigned int npeak);


/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/

void peaklog_vec_write(struct peaklog_vec *p, const char *p_path);

/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
struct peaklog_vec *peaklog_vec_read(const char *p_path, unsigned int npeak);

/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
struct peaklog_vec **log_mpeak(double_vec **pp_ecg, struct fhrfeature *p_feature, struct mfpeak *p_mfpeak);

/*------------------------------------------------------------------------------
* Function name:
* Description: 
------------------------------------------------------------------------------*/
struct peaklog_vec **log_fpeak(double_vec **pp_ecg, struct fhrfeature *p_feature, struct mfpeak *p_mfpeak);

#endif //FHR_APP_LOG_H

