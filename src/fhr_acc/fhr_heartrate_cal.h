/*******************************************************************************
*      Copyright Ã‚Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.h
* Original Author: TRAN MINH HAI
* Date created: 04 OCT 2019
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             04 OCT 2019    T.H          Original Code
********************************************************************************/
#ifndef FHR_HEARTRATE_CAL_H
#define FHR_HEARTRATE_CAL_H

// SYSTEM INCLUCDE
#include <math.h>
#include <float.h>
#include <errno.h>
#include <stdio.h>

// LOCAL INCLUDE 
#include "biorithm_aux.h"
#include "fhr_findpeaks.h"

// CONSTANT AND PARAMETER 
#define FHR_FBPM_LLIM		 	100		// LOWER LIMIT OF FETAL BPM 
#define FHR_FBPM_ULIM 			200		// UPPER LIMIT OF FETAL BPM
#define FHR_FBPM_DLIM 			10       // DEVIATION LIMIT OF FETAL BPM
#define FHR_MBPM_LLIM 			60		// LOWER LIMIT OF MATERNAL BPM
#define FHR_MBPM_ULIM 			100 	// UPPER LIMIT OF MATERNAL BPM
#define FHR_MBPM_DLIM 			10 		// DEVIATION LIMIT OF MATERNAL BPM


#define FHR_BPM_UOUTLIER        300		// UPPER OUTLIER OF FETAL BPM
#define FHR_BPM_LOUTLIER 	    0		// LOWER OUTLIER OF FETAL BPM

#define FHR_BIN_LEN				125     // BIN LENGTH TO SAMPLE FHR 
#define FHR_BIN_FREQ            4       // FREQUENCY SAMPLING FHR 


#ifndef FHR_SAMPLING_FREQ
#define FHR_SAMPLING_FREQ 500 			// SAMPLING FREQUENCY 
#endif 

#ifndef FHR_NCHANNEL 
#define FHR_NCHANNEL 4					// NUMBER OF ELECTRODE 
#endif 

// DEBUG ENALBE
#define FHR_HEARTRATE_CAL_DEBUG

// PRINT MODE 
#define FHR_HEARTRATE_CAL_PRINT


// STRUCT OF R PEAK 
struct rpeak
{
	unsigned int loc; 					// R PEAK LOCATION 
	unsigned int rri; 					// R PEAK INTERVAL 
	double bpm; 						// R PEAK BEAT PER MINUTE 
};

struct rpeak_vec 
{
	struct rpeak *p_rpeak; 				// ARRAY OF R PREAK STRUCT 
	unsigned int len;					// LENGTH OF THE ARRAY
};


// FUNCTION 
/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
struct rpeak_vec *rpeak_vec_new(size_t npeak);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
void rpeak_vec_delete(struct rpeak_vec *p);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
void rpeak_vec_print(struct rpeak_vec *p, unsigned int npeak);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
void rpeak_vec_write(struct rpeak_vec *p, const char *p_path);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
struct rpeak_vec *rpeak_vec_read(const char *p_path, unsigned int npeak);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
struct rpeak_vec *compute_heartrate(peak_vec *p, double ulim, double llim, 
double dlim);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
double_vec **sample_heartrate(struct rpeak_vec **pp_rpks, unsigned int siglen);


#endif // FHR_HEARTRATE_CAL_H

