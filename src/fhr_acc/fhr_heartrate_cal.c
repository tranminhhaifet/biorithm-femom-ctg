/*******************************************************************************
*      Copyright Ã‚Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 04 OCT 2019
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             04 OCT 2019    T.H          Original Code
*******************************************************************************/
// SYSTEM INCLUDES
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <float.h>

// LOCAL INCLUDE 
#include "fhr_heartrate_cal.h"
#include "biorithm_aux.h"
#include "fhr_findpeaks.h"

static const double G_FHR_NSAMPLE_MINUTE = 60 * FHR_SAMPLING_FREQ; 

// FUNCTION 
/*------------------------------------------------------------------------------
* Function Name: 
* Description  : read peak from file 
*-----------------------------------------------------------------------------*/ 
struct rpeak_vec *compute_heartrate(peak_vec *p, double ulim, double llim, 
double dlim)
{
	// RRR INTERVAL 
	unsigned int rri = 0; 
	// BEAT PER MINUTE 		
	double bpm = 0.0; 
	// NUMBER OF PEAK 
	unsigned npeak = p->len;

	// CHECK NULL INPUT POINTER 
	if (p==NULL)
	{
		printf("ACCESS NULL POINTER (%s:%d)\n", __FILE__, __LINE__);
		return NULL;
	}

	// INIT OUTPUT 
	struct rpeak_vec *p_heartrate = rpeak_vec_new(npeak);

	// CHECK ALLOCATE RETURN NULL 
	if (p_heartrate == NULL)
	{
		return NULL;
	}

	// LOOP THROUGH EACH PEAK 
	for (unsigned int i = 0; i < npeak - 1; i++)
	{
		// R PREAK LOCATION
		p_heartrate->p_rpeak[i].loc = p->p_peak[i].loc;
		
		// RR INTERVAL
		rri = p->p_peak[i+1].loc - p->p_peak[i].loc;  

		// BPM CHECK DIVINDING BY ZERO  
		if (rri < FLT_MIN)
		{
			bpm = FHR_BPM_UOUTLIER;
		} 
		else 
		{
			bpm = (G_FHR_NSAMPLE_MINUTE) / rri;
		}
		// CHECK BPM WITH ULIM AN DLIM 
		if (bpm <= llim)
		{
			bpm = FHR_BPM_LOUTLIER; 
		} 

		if (bpm >= ulim)
		{
			bpm = FHR_BPM_UOUTLIER;
		}

		// FORM OUTPUT 
		p_heartrate->p_rpeak[i].rri = rri; 
		p_heartrate->p_rpeak[i].bpm = bpm; 
	}

	// THE LAST VALUE 

	// CHECK DLIM 

	// RETURN 
	return p_heartrate;
}


/*------------------------------------------------------------------------------
* Function Name: 
* Description  : read peak from file 
*-----------------------------------------------------------------------------*/ 
struct rpeak_vec *rpeak_vec_new(size_t npeak)
{
	// CHECK INPUT 
	if (npeak == 0 | npeak > SIZE_MAX)
	{
		printf("OVERFLOW ALLOCATE\n");
		return NULL;
	}

	// ALLOCATE NPEAK IN HEAP 
	struct rpeak_vec *p = malloc(npeak * sizeof(struct rpeak_vec));

	// CHECK ALLOCATE RETURN NULL 
	if (p == NULL)
	{
		printf("ALLOCATE ERROR RETURN NULL\n");
		return NULL;
	}

	// ALLLOCATE ARRAY OF NPEAK 
	p->len = npeak;
	p->p_rpeak = malloc(npeak * sizeof(struct rpeak));

	if (p->p_rpeak == NULL)
	{
		printf("ALLOCATE ERROR RETURN NULL\n");
		return NULL;
	}

	// INIT TO ZERO
	for (size_t i = 0; i < npeak; i++)
	{	
		p->p_rpeak[i].loc = 0;
		p->p_rpeak[i].rri = 0; 
		p->p_rpeak[i].bpm = 0.0;
	}

	// RETURN 
	return p;
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : read peak from file 
*-----------------------------------------------------------------------------*/ 
void rpeak_vec_delete(struct rpeak_vec *p)
{
	free(p->p_rpeak);
	free(p);
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : read peak from file 
*-----------------------------------------------------------------------------*/ 
void rpeak_vec_print(struct rpeak_vec *p, unsigned int npeak)
{
	// CHECH ACCESS NULL POINTER 
	if (p==NULL)
	{
		printf("ACCESS NULL VECTOR: %s:%d\n", __FILE__, __LINE__);
		exit(-1);
	}

	// PRINT
	for (unsigned int i = 0; i < npeak; i++)
	{
		printf("i=%ul, loc=%ul, rri=%ul, bpm=%3.15f\n", i, p->p_rpeak[i].loc, 
		p->p_rpeak[i].rri, p->p_rpeak[i].bpm);
	}
}

void rpeak_vec_write(struct rpeak_vec *p, const char *p_path)
{
 	// CHECH ACCESS NULL POINTER 
 	if (p==NULL)
 	{
 		printf("ACCESS NULL VECTOR %s:%d\n",__FILE__, __LINE__);
 		exit(-1); 
 	}

 	// OPEN FILE AND CHECH IVALID FILE PATH 
 	FILE *p_file = fopen(p_path,"w");

 	if(p_file == NULL)
 	{
 		printf("INVALID FILE PATH %s:%d\n", __FILE__, __LINE__);
 		exit(-1);
 	}

 	for (unsigned int i = 0; i < p->len; i++)
 	{
 		fprintf(p_file, "%d %d %3.15f\n", p->p_rpeak[i].loc, p->p_rpeak[i].rri, 
 		p->p_rpeak[i].bpm);
 	}

 	fclose(p_file);
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : read peak from file 
*-----------------------------------------------------------------------------*/ 
struct rpeak_vec *rpeak_vec_read(const char *p_path, unsigned int npeak)
{
	//
	unsigned int loc, rri = 0; 
	double bpm = 0.0; 

	if (npeak <= 0)
	{
		printf("CANNOT ALLOCATE ZERO PEAK %s:%d\n",__FILE__,__LINE__);
		return NULL;
	}

	// INIT OUTPUT 
	struct rpeak_vec *p = rpeak_vec_new(npeak);

	// CHECK ALLOCATE ERROR 
	if(p==NULL)
	{
		printf("ALLOCATE ERROR %s:%d\n", __FILE__, __LINE__);
		return NULL;
	}

	// READ DATA FROM FILE 
	FILE *p_file = fopen(p_path,"r");

	if(p_file == NULL)
	{
		printf("INVALID FILE PATH %s:%d\n", __FILE__, __LINE__);
		return NULL;
	}

	for (unsigned int i = 0; i < npeak; i++)
	{
		fscanf(p_file, "%d %d %lgf\n", &loc, &rri, &bpm);
		p->p_rpeak[i].loc = loc; 
		p->p_rpeak[i].rri = rri;
		p->p_rpeak[i].bpm = bpm;
	}

	// CLOSE FILE 
	fclose(p_file);

	// RETURN 
	return p;
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
double_vec **sample_heartrate(struct rpeak_vec **pp_rpks, unsigned int siglen)
{
	// CHECK INPUT NULL  

	// NUMBER OF BIN 
	unsigned int binid = 0; 
	unsigned int nbin = siglen / FHR_BIN_LEN; 
	double_vec **pp_trace = malloc((FHR_NCHANNEL + 1) * sizeof(double_vec *));

	// INIT OUTPUT 
	for (unsigned int i = 0; i < FHR_NCHANNEL + 1; i++)
	{
		// EACH CHANNEL HAS NBIN 
		pp_trace[i] = double_vec_new(nbin);

		for (unsigned int j = 0; j < nbin; j++)
		{
			pp_trace[i]->p_data[j] = 0.0; 
		}
	}

	// MAP R RPEAK LOC TO CORRESPONDING BIN 
	// TODO: CLEAN MORE THAN ONE R PEAK IS WITHIN A BIN 
	// FOR EACH CHANNEL 
	for (unsigned int i = 0; i < FHR_NCHANNEL; i++)
	{
		// FOR EACH PEAK 
		for (unsigned int j = 0; j < pp_rpks[i]->len; j++)
		{
			binid = pp_rpks[i]->p_rpeak[j].loc / FHR_BIN_LEN;
			pp_trace[i]->p_data[binid] = pp_rpks[i]->p_rpeak[j].bpm;

		}
	}

	// COMBINE ALL CHANNEL 
	int count = 0; 
	double sum = 0.0; 
	for (unsigned int j = 0; j < nbin; j++)
	{
		sum = 0.0; 
		count = 0; 

		// AVERAGE HEART RATE 
		for (int k = 0; k < FHR_NCHANNEL; k++)
		{
			if (pp_trace[k]->p_data[j] > FHR_FBPM_LLIM && 
				pp_trace[k]->p_data[j] < FHR_FBPM_ULIM)
			{
				count = count + 1; 
				sum = sum + pp_trace[k]->p_data[j]; 

			}
		}

		if (count > 0)
		{
			pp_trace[FHR_NCHANNEL]->p_data[j] = sum / count; 
		}
		else
		{
			pp_trace[FHR_NCHANNEL]->p_data[j] = 0.0; 
		}

		
	}

	// RETURN 
	return pp_trace;
}






