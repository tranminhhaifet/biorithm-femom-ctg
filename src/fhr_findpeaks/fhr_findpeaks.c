/*******************************************************************************
*      Copyright ©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_findpeaks.c
* Original Author: TRAN MINH HAI 
* Date created: 18 JUL 2019
* Purpose: Implement findpeaks algorithm
*
*
* Revision History:
* Ref No      Date           Who          	Detail
* <task ID>   <originated>   <originator> 	Original Code
* <task ID>   <DD MMM YYYY>  <initials>   	<fix/change description>
* NA 		  24 SEP 2019	 TRAN MINH HAI   
*******************************************************************************/
// SYSTEM INCLUDE 
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// LOCAL INCLUDE 
#include "fhr_findpeaks.h"
#include "biorithm_aux.h"

// FUNCTION 
/*------------------------------------------------------------------------------
// READ PEAK FROM FILE 
// Input: filepath 
// Output: read peak information from filepath to peak vector 
------------------------------------------------------------------------------*/
void peak_vec_read(const char *filepath, peak_vec *p)
{ 
	// init param 
	double pks, width, prominence = 0.0; 
	unsigned int locs, ilb, irb, flag = 0; 

	// open the file 
	FILE *p_file = fopen(filepath, "r");

	// check error 
	CHECK_ACCESS_NULL_POINTER(p_file);

	// read from file 
	for (unsigned i = 0; i < p->len; i++)
	{
		fscanf(p_file, "%u %u %u %lg %lg %lg %u\n", &locs, &ilb, &irb, &pks, &width, &prominence, &flag);
		PEAK_CONSTRUCT(p->p_peak[i], locs, ilb, irb, pks, width, prominence, false);
	}
	fclose(p_file);
}

/*------------------------------------------------------------------------------
// WRITE PEAK TO FILE 
// Input: filepath 
// Output: write peak information to filepath from peak vector 
------------------------------------------------------------------------------*/
void peak_vec_write(const char *p_path, peak_vec *p)
{ 
	// OPEN FILE FOR WRITTING
	FILE *p_file;
	p_file = fopen(p_path, "w");

	// CHECK ACCESS NULL POINTER 
	CHECK_ACCESS_NULL_POINTER(p_file);

	// WRITE PEAK TO FILE 
	for (unsigned int i = 0; i < p->len; i++)
	{
		fprintf(p_file, "%d %d %d %3.32f %3.32f %3.32f %d\n", 
			p->p_peak[i].loc, p->p_peak[i].ilb, p->p_peak[i].irb,
			p->p_peak[i].mag, p->p_peak[i].width, p->p_peak[i].prominence,
			p->p_peak[i].flag);
	}

	fclose(p_file);
}

/*------------------------------------------------------------------------------
* Function Name: new_peak_vec(unsigned int len)
* Description  : create a vector of peak 
*-----------------------------------------------------------------------------*/ 
peak_vec *new_peak_vec(unsigned int len)
{
	peak_vec *p = malloc(sizeof(peak_vec));
	if (len == 0)
	{
		p->len = 0; 
		p->p_peak = NULL;
	} 
	else 
	{
		p->len = len;
		p->p_peak = malloc(len * sizeof(peak));
		for (int i = 0; i < len; i++)
		{
			p->p_peak[i].loc 		= 0; 	 // location 
			p->p_peak[i].ilb 		= 0; 	 // left index
			p->p_peak[i].irb 		= 0; 	 // right index
			p->p_peak[i].mag 		= 0.0;   // magnitude
			p->p_peak[i].width  	= 0.0; 	 // width
			p->p_peak[i].prominence = 0.0;   // prominence 
			p->p_peak[i].flag 		= false; // flag maternal or fetal
		}
	}

	return p;
}

/*------------------------------------------------------------------------------
* Function Name: print_peak_vec(peak_vec *p, int n)
* Description  : print peak vec for debug 
*-----------------------------------------------------------------------------*/ 
void print_peak_vec(peak_vec *p, int n)
{
	for (int i = 0; (i<VEC_LEN(p) && i < n); i++)
	{	printf("i=%d, ", i);
		PRINT_PEAK(P_VEC(p, i));
	}
}

/*------------------------------------------------------------------------------
* Function Name: int_vec_new
* Description  : allocate a vector of integer, init to ZERO
*-----------------------------------------------------------------------------*/ 
void delete_peak_vec(peak_vec *p)
{
	free(p->p_peak);
	free(p);
}

/*------------------------------------------------------------------------------
* Function Name: compare_peak_g(const void *p1, const void *p2) 
* Description  : to use standard qsort, comparator greater needed 
*-----------------------------------------------------------------------------*/ 
int compare_peak_mag_g(const void *p1, const void  *p2)
{
	 if (((peak *)p1)->mag < ((peak *)p2)->mag)
	 {
	 	return 1;
	 } else 
	 {
	 	return -1;
	 }
}

/*------------------------------------------------------------------------------
* Function Name: compare_peak_l(const void *p1, const void *p2)
* Description  : to use standard qsort, comparator less needed 
*-----------------------------------------------------------------------------*/ 
int compare_peak_loc_l(const void *p1, const void  *p2)
{
	if (((peak *)p1)->loc > ((peak *)p2 )->loc)
	{
		return 1;
	} else
	{
		return -1;
	}
}

/*------------------------------------------------------------------------------
* Function Name: compute_diff(double_vec *x0)
* Description  : first derivative of a double vector 
*-----------------------------------------------------------------------------*/ 
double_vec *compute_diff(double_vec *x0)
{
	double_vec *dx0 = double_vec_new(x0->len - 1);

	for(int i = 0; i < x0->len-1; i++)
	{
		dx0->p_data[i] = x0->p_data[i+1] - x0->p_data[i]; 

		// check dx0 == 0 
		if(fabs(dx0->p_data[i]) < ESP)
		{
			dx0->p_data[i] = -ESP;
		}
	}

	return dx0;
}

/*------------------------------------------------------------------------------
* Function Name: compute_ind(double_vec *x0, double_vec *dx0)
* Description  : find index where the first derivative (d) changes sign
*-----------------------------------------------------------------------------*/ 
int_vec *compute_ind(double_vec *x0, double_vec *dx0)
{

	// initialize  
	int count = 0; 
	int tmp[dx0->len]; 

	// check where the fist div change sign 
	for (int i = 0; i < dx0->len - 1; i++)
	{
        if (dx0->p_data[i] * dx0->p_data[i + 1] < 0.0)
        {
        	tmp[count] = i + 1; // locs where div change sign
        	count += 1;  
        }
	} 

	// allocate ind[count+2]
	int_vec *ind = int_vec_new(count + 2);
	
	// include bounderies 
	ind->p_data[0] = 0; 
	ind->p_data[count + 1] = x0->len - 1;

	// copy tmp into ind 
	for(int j = 0; j < count; j++)
	{
		ind->p_data[j + 1] = tmp[j]; 
	}
	return ind; 
}

/*------------------------------------------------------------------------------
* Function Name: compute_x_ind(double_vec *x0, int_vec *ind)
* Description  : get x at index where first derivative (d) change sign
*-----------------------------------------------------------------------------*/ 
double_vec *compute_x_ind(double_vec *x0, int_vec *ind)
{

    // allocate x[ind->len]
    double_vec *x = double_vec_new(ind->len);

    // extract x at indexcies where dx0 changes sign 
    for(int i = 0; i < ind->len; i++)
    {

    	int idx = ind->p_data[i];

    	x->p_data[i] = x0->p_data[idx];
    }

    return x;

}

/*------------------------------------------------------------------------------
* Function Name: find_peak(double_vec *x0, double sel, double thresh)
* Description  : find peak {loc, ilb, irb, width, height, pproninence} given 
* sel and thresh. height should be greater than thresh. 
*-----------------------------------------------------------------------------*/ 
peak_vec *find_peak_sel(double_vec *x0, double sel, double thresh)
{
	// initialize 
	peak_vec *p_vec = NULL; // output 
	double_vec *dx0 = NULL; // first derivative 
	int_vec *ind    = NULL; // index where first derivative dx0 change sign
	double_vec *x   = NULL; // data at index where dx0 changes sign

	// compute dx0 
	dx0 = compute_diff(x0);

	#ifdef FHR_FINDPEAKDS_DEBUG
	// double_vec_print(dx0, dx0->len);
	#endif

	// compute ind 
	ind = compute_ind(x0, dx0);

	#ifdef FHR_FINDPEAKDS_DEBUG
	// int_vec_print(ind, ind->len);
	#endif

	// compute x 
	x = compute_x_ind(x0, ind);

	#ifdef FHR_FINDPEAKDS_DEBUG
	// double_vec_print(x, x->len);
	#endif

	// init 
	double minMag       = find_min(x0);
	double tmpMag       = minMag;
	double minLeft      = x0->p_data[0];
	bool flag           = false;  
	unsigned int len    = x->len - 1; // double check  
	unsigned int tmpLoc = 0; 
	unsigned int cInd   = 0; 
	
	// array of peak with length of input data x, and init to zero 
	// consider all data samples are peak candidate  
	peak p_array[x->len]; 
	for(int i = 0; i < x->len; i++)
	{
		p_array[i].loc 		  = 0; 
		p_array[i].ilb 		  = 0; 
		p_array[i].irb 		  = 0; 
		p_array[i].mag 		  = 0.0; 
		p_array[i].width 	  = 0.0;
		p_array[i].prominence = 0.0;
		p_array[i].flag 	  = false;
	}

	// init k
	int k = 0; 
	if( x->p_data[0] >= x->p_data[1])
	{
		k = -1;
	}
	else
	{
		k = 0;
	}

	// loop through data sample where dx0 changes sign 
	// for each candidate, go to the left and then go to the right to see 
	// if the candidate is an actual peak. 
	while( k < (int)len)
	{
		k = k + 1; 

		// reset flag c
		if(flag==true)
		{
			flag   = false; 
			tmpMag = minMag;
		} 

		// rising edge 
		if(x->p_data[k] > tmpMag && x->p_data[k] > minLeft + sel 
		&& x->p_data[k] > thresh)
		{
			tmpMag = x->p_data[k];
			tmpLoc = ind->p_data[k];
		}

		// check boundary of k 
		if(k == len)
		{
			break;
		}

		k = k + 1; 
		// printf("k = %d \n", k);

		// falling edge 
		if(flag==false && tmpMag > x->p_data[k] + sel && tmpMag > thresh)
		{
			flag = true;
			p_array[cInd].loc = tmpLoc; 
			p_array[cInd].mag = tmpMag;
			p_array[cInd].irb = ind->p_data[k];
			minLeft = x->p_data[k];
			cInd += 1;   
			p_array[cInd].ilb = ind->p_data[k];

		} else if( x->p_data[k] <= minLeft)
		{
			minLeft = x->p_data[k];
			p_array[cInd].ilb = ind->p_data[k];
		}
	}

	// output 
	p_vec = new_peak_vec(cInd);
	for (int i = 0; i < cInd; i++)
	{
		p_vec->p_peak[i].loc = p_array[i].loc;
		p_vec->p_peak[i].mag = p_array[i].mag; 
		p_vec->p_peak[i].ilb = p_array[i].ilb;
		p_vec->p_peak[i].irb = p_array[i].irb;

	}

	// free mem 
	double_vec_delete(dx0);
	double_vec_delete(x);
	int_vec_delete(ind);

	return p_vec; 
}

/*------------------------------------------------------------------------------
* Function Name: clean_peak(peak_vec *p, unsigned int pid, int dmin)
* Description  : given a peak at location pid, mark neightbor peak < dmin 
*-----------------------------------------------------------------------------*/ 
void clean_peak(peak_vec *p, unsigned int pid, int dmin)
{

	static int count = 0; 

	int R = pid + dmin;
	int L = pid - dmin; 

	// H.T OCT 24 2019
	if ( L < 0 )
	{
		L = 0;
	}

	// find neighbor which closer than dmin to the right or left side 
	for(int i = 0; i < p->len; i++)
	{
		unsigned int tmp = p->p_peak[i].loc;

		if(tmp >= L && tmp <= R && tmp != pid){

			p->p_peak[i].flag = true; 
		}
	}
}

/*------------------------------------------------------------------------------
* Function Name: find_peak_dmin
* Description  : find peak with dmin which means distance between two peak 
* should be greater than dmin
*-----------------------------------------------------------------------------*/ 
peak_vec *find_peak_dmin(peak_vec *p, int dmin)
{
	// sort peaks by magnitude from large to small 
	qsort(p->p_peak, p->len, sizeof(peak), compare_peak_mag_g);

	// for each peak clean close peaks 
	for(int i = 0; i < p->len; i++)
	{
		if((p->p_peak[i].flag) == false)
		{
			clean_peak(p, p->p_peak[i].loc, dmin); 
		} 
	}

	// re-sort by loc from small to large  
	qsort(p->p_peak, p->len, sizeof(peak), compare_peak_loc_l);

	int num_false = 0; 	
	for(int i = 0; i < p->len; i++)
	{
		if(p->p_peak[i].flag == false)
		{
			num_false = num_false + 1; 
		}
	}

	// output 
	peak_vec *p_new = new_peak_vec(num_false);
	int cc = 0; 
	for(int i = 0; i < p->len; i++)
	{
		if((p->p_peak[i].flag) == false)
		{
			PEAK_COPY(p_new->p_peak[cc], p->p_peak[i]);
			cc += 1;
		}
	}

	return p_new;
}

/*------------------------------------------------------------------------------
* Function Name: linterp(point a, point b, double yc, double bc)
* Description  : find linear interpolation/intercept between a line (a,b) and
* a horizontal line (yc, bc)
*-----------------------------------------------------------------------------*/ 
double linterp(point a, point b, double yc, double bc)
{
	// init 
	double xc = 0.0; 

	// compute intercept ponint
	xc = a.x + (b.x-a.x)*(0.5*(yc+bc)-a.y) / (b.y-a.y);

	return xc;  
}

/*------------------------------------------------------------------------------
* Function Name: find_left_intercept(double_vec *y, unsigned int ipk, 
* unsinged int border_idx, double height)
* Description  : 
*-----------------------------------------------------------------------------*/ 
unsigned int find_left_intercept(double_vec *y, unsigned int ipk, 
	unsigned int border_idx, double height)
{
	// init 
	unsigned int idx = ipk; 

	// TODO: replace while by for 
	while (idx>=border_idx && y->p_data[idx] > height && idx >0)
	{
    	idx = idx - 1; 
	}

	return idx;
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
unsigned int find_right_intercept(double_vec *y, unsigned int ipk, 
	unsigned int border_idx, double height)
{

	// init 
	unsigned int idx = ipk; 

	// TODO: replace while by for 
	while(idx<=border_idx && y->p_data[idx] > height && idx>0)
	{

		idx = idx + 1; 
	}

	return idx; 

}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
point get_max_half_bound(double_vec *y, unsigned int ipk, double base,  
	unsigned int ilbh, unsigned int irbh)
{
	// init 
	double height = 0.0; 
	unsigned int i_left = 0; 
	unsigned int i_right = 0; 
	point bound = {0.0, 0.0};

	// compute heigh 
	height = y->p_data[ipk] / 2.0; 

	// compute intercept left 
	i_left = find_left_intercept(y, ipk, ilbh, height);
	#ifdef FHR_FINDPEAKDS_DEBUG
	printf("ileft=%d, ", i_left);
	#endif

	// compute the left bound 
	if(i_left < ilbh)
	{
		bound.x = ilbh;

	} else
	{
		point a = {i_left, y->p_data[i_left]};
		point b = {i_left+1, y->p_data[i_left+1]};
		bound.x = linterp(a, b, y->p_data[ipk], base); 
	}


	// compute right intercept 
	i_right = find_right_intercept(y, ipk, irbh, height);
	#ifdef FHR_FINDPEAKDS_DEBUG
	printf("iright=%d\n", i_right);
	#endif


	// compute the right bound 
	if(i_right > irbh)
	{

		bound.y = irbh;

	} else
	{
		point c = {i_right, y->p_data[i_right]};
		point d = {i_right-1, y->p_data[i_right-1]};
		bound.y = linterp(c, d, y->p_data[ipk], base);
	}

	return bound;
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
point  *get_peak_width(double_vec *y, peak_vec *p)
{
	// init 
	double base = 0.0; 
	unsigned int ilbh = 0;
	unsigned int irbh = 0; 
	point *bound = NULL;
	bound = malloc(sizeof(point)*p->len);

	for(int k = 0; k < p->len; k++)
	{

		// compute ilbh min()
		if (k==0)
		{
			ilbh = p->p_peak[0].ilb;

		} else
		{
			ilbh = MAX(p->p_peak[k-1].irb, p->p_peak[k].ilb);
		}

		if (k==p->len-1)
		{
			irbh = p->p_peak[p->len-1].irb;
		} else
		{
			irbh = MIN(p->p_peak[k].irb, p->p_peak[k+1].ilb);
		}

		// compute peak width 
		bound[k] = get_max_half_bound(y, p->p_peak[k].loc, base, ilbh, irbh);
		#ifdef FHR_FINDPEAKDS_DEBUG
		printf("ilb=%d, irb=%d, intercept1=%3.5f, intercept2=%3.5f\n", 
			ilbh, irbh, bound[k].x, bound[k].y);
		#endif 

		// update peak vec  
		p->p_peak[k].width = bound[k].y - bound[k].x;
	}

	return bound; 
}

/*------------------------------------------------------------------------------
* Function Name: find_peaks(double_vec *p, double sel, double hmin, int dmin)
* Description  : hmin is min height, dmin is min peak distance 
* return peak location, left index, right index, magnitude, width, prominence, 
* and flag.
*-----------------------------------------------------------------------------*/ 
peak_vec *find_peaks(double_vec *p_x, double sel, double hmin, int dmin)
{
	// init output 
	peak_vec *p_dmin = NULL;
	peak_vec *p_sel  = NULL;

	// find peak sel and thresh 
	p_sel = find_peak_sel(p_x, sel, hmin); 

	// find peak dmin 
	p_dmin = find_peak_dmin(p_sel, dmin);

	// get peak width half height 
	get_peak_width(p_x, p_dmin);

	// free mem 
	delete_peak_vec(p_sel);

	// return 
	return p_dmin;
}



