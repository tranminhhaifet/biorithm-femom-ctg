/*******************************************************************************
*      Copyright ©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_findpeaks.h
* Original Author: TRAN MINH HAI 
* Date created: 24 SEP 2019
* Purpose: Implement findpeaks algorithm 
*
*
* Revision History:
* Ref No      Date           Who          	Detail
* <task ID>   <originated>   <originator> 	Original Code
* NA          24 SEP 2019 	 TRAN MINH HAI  
*******************************************************************************/
#ifndef FHR_FINDPEAKS_HEADER
#define FHR_FINDPEAKS_HEADER

// SYSTEM INCLUDES
#include<stdbool.h>
#include<stdlib.h>

// LOCAL INCLUDES
#include "biorithm_aux.h"

// DEBUG MODE 
// #define FHR_FINDPEAKDS_DEBUG 

// EPSILON FOR COMPARING TWO DOUBLE NUMBER 
#define ESP 2.2204E-16

// MACRO ACCESS VECTOR ELEMENT 
#define VEC(p, i)(p->p_data[i])

// MACRO GET VECTOR LENGTH 
#define VEC_LEN(p)((p)->len)

// MACRO GET PEAK VECTOR LENGTH 
#define P_VEC(p,i)(p->p_peak[(i)])										    

// MACRO CONSTRUCT POINT STRUCT 
#define POINT_CONTRUCT(p, x_val, y_val) 									\
{ 																			\
	p.x=x_val; 																\
	p.y=y_val; 																\
} 																			\

// MACRO COPY PONIT STRUCT 
#define POINT_COPY(p1, p2) 	                                  				\
{																			\
	p1.x=p2.x; 	                                  							\
	p1.y=p2.y;                                   							\
}                                   										\

// MACRO PRINT PEAK STRUCT 
#define PRINT_PEAK(p) 														\
{ 																			\
    printf("loc=%-5u,ilb=%-5u,irb=%-5u,mag=%-3.5f,width=%-3.5f",			\
	p.loc, p.ilb, p.irb, p.mag, p.width);					                \
	printf(",prominence=%-3.5f,flag=%1d\n", p.prominence, p.flag);			\
}

// MACRO PRINT PEAK STRUCT 
#define PRINT_PEAK_POINTER(p)											    \
{ 																			\
    printf("loc=%-5u,ilb=%-5u,irb=%-5u,mag=%-3.5f,width=%-3.5f",			\
	(p)->loc, (p)->ilb, (p)->irb, (p)->mag, (p)->width);					\
	printf(",prominence=%-3.5f,flag=%1d\n", (p)->prominence, (p)->flag);	\
}        														            \

// MACRO PEAK_COPYT
#define PEAK_COPY(p1, p2)													\
{																			\
	p1.loc  		= p2.loc;   											\
	p1.ilb 			= p2.ilb; 												\
	p1.irb 			= p2.irb; 												\
    p1.mag 			= p2.mag; 												\
    p1.width 		= p2.width;               								\
	p1.prominence 	= p2.prominence; 										\
	p1.flag 		= p2.flag; 												\
}						 													\

// MACRO CONSTRUCT A PEAK 
#define PEAK_CONSTRUCT(p, loc_val, ilb_val, irb_val, mag_val, width_val,    \
					   prominence_val, flag_val) 						    \
{                    														\
	p.loc   	 	= loc_val; 												\
	p.ilb   		= ilb_val; 												\
	p.irb        	= irb_val;                  							\
	p.mag   	 	= mag_val;                                           	\
	p.width 	 	= width_val; 											\
	p.prominence 	= prominence_val; 										\
	p.flag       	= flag_val; 						 					\
}       																	\

// MACRO MIN MAX OF TWO NUMBER 
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

// PEAK STRUCT  
typedef struct {
	unsigned int loc; 		// location 
	unsigned int ilb;		// left index
	unsigned int irb; 		// right index
	double mag; 			// magnitude
	double width; 			// width
	double prominence; 		// prominence
	bool flag; 				// flag=0 fetal, flag=1 maternal
} peak;

// PEAK VECTOR 
typedef struct {
	unsigned int len; 
	peak *p_peak;
} peak_vec;


// AUXILIARY
/*------------------------------------------------------------------------------
* Function Name: 
* Description  : read peak from file 
*-----------------------------------------------------------------------------*/ 
void peak_vec_read(const char *filepath, peak_vec *p);

/*------------------------------------------------------------------------------
// WRITE PEAK TO FILE 
// Input: filepath 
// Output: write peak information to filepath from peak vector 
------------------------------------------------------------------------------*/
void peak_vec_write(const char *p_path, peak_vec *p);

/*------------------------------------------------------------------------------
* Function Name: print_peak_vec(peak_vec *p, int n)
* Description  : print peak vec for debug 
*-----------------------------------------------------------------------------*/ 
void print_peak_vec(peak_vec *p, int n);

/*------------------------------------------------------------------------------
* Function Name: new_peak_vec(unsigned int len)
* Description  : create a vector of peak 
*-----------------------------------------------------------------------------*/ 
peak_vec *new_peak_vec(unsigned int len);

/*------------------------------------------------------------------------------
* Function Name: delete_peak_vec(peak_vec *p_vec)
* Description  : delete vector of peak 
*-----------------------------------------------------------------------------*/ 
void delete_peak_vec(peak_vec *p);

/*------------------------------------------------------------------------------
* Function Name: compare_peak_mag_g(onst void *p1, const void *p2)
* Description  : it is rquired for qsort(peak_vec)
*-----------------------------------------------------------------------------*/ 
int compare_peak_mag_g(const void *p1, const void  *p2);

/*------------------------------------------------------------------------------
* Function Name: commpare_peak_loc_l(const void *p1, const void *p2)
* Description  : it is required for qsort(peak_vec)
*-----------------------------------------------------------------------------*/ 
int compare_peak_loc_l(const void *p1, const void  *p2);
//==============================================================================

// FINDPEAKS 
/*------------------------------------------------------------------------------
* Function Name: compute_diff(double_vec *x0)
* Description  : compute the first derivative of a double vector
*-----------------------------------------------------------------------------*/ 
double_vec *compute_diff(double_vec *x0);

/*------------------------------------------------------------------------------
* Function Name: compute_ind(double_vec *x0, double_vec *dx0)
* Description  : return index where dx0 change sign
*-----------------------------------------------------------------------------*/ 
int_vec *compute_ind(double_vec *x0, double_vec *dx0);

/*------------------------------------------------------------------------------
* Function Name: compute_x_ind(double_vec *x0, int_vec *ind)
* Description  : return x[where dx0 change sign]
*-----------------------------------------------------------------------------*/ 
double_vec *compute_x_ind(double_vec *x0, int_vec *ind);

/*------------------------------------------------------------------------------
* Function Name: find_peak(double_vec *x0, double sel, double thresh)
* Description  : find peaks which greater than thresh and prominence greater 
* than sel 
*-----------------------------------------------------------------------------*/ 
peak_vec *find_peak_sel(double_vec *x0, double sel, double thresh);

/*------------------------------------------------------------------------------
* Function Name: clean_peak(peak_vec *p, unsigned int pid, int dmin)
* Description  : for the peak pid, mark its neighbor which closer than dmin
* to rmove those close neigbor 
*-----------------------------------------------------------------------------*/ 
void clean_peak(peak_vec *p, unsigned int pid, int dmin);

/*------------------------------------------------------------------------------
* Function Name: find_peak_dmin(peak_vec *p, int dmin)
* Description  : 
*-----------------------------------------------------------------------------*/ 
peak_vec *find_peak_dmin(peak_vec *p, int dmin);

/*------------------------------------------------------------------------------
* Function Name: linterp(point a, point b, double yc, double bc)
* Description  : 
*-----------------------------------------------------------------------------*/ 
double linterp(point a, point b, double yc, double bc);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
unsigned int find_left_intercept(double_vec *y, unsigned int ipk, 
	unsigned int border_idx, double height);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
unsigned int find_right_intercept(double_vec *y, unsigned int ipk, 
	unsigned int border_idx, double height);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
point get_max_half_bound(double_vec *y, unsigned int ipk, double base,  
	unsigned int ilbh, unsigned int irbh);

/*------------------------------------------------------------------------------
* Function Name: get_peak_width(double_vec *y, peak_vec *p
* Description  : find width of each peak. 
* TOD		   : get_peak_width(double_vec *y, peak_vec *p, double thresh)
*-----------------------------------------------------------------------------*/ 
point  *get_peak_width(double_vec *y, peak_vec *p);

/*------------------------------------------------------------------------------
* Function Name: find_peaks(double_vec *p, double sel, double hmin, int dmin)
* Description  : hmin is min height, dmin is min peak distance 
* return peak location, left index, right index, magnitude, width, prominence, 
* and flag.
*-----------------------------------------------------------------------------*/ 
peak_vec *find_peaks(double_vec *p, double sel, double hmin, int dmin);


#endif //FHR_FINDPEAKS_HEADER