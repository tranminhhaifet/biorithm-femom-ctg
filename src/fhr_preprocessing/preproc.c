/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.c
* Original Author: TRAN MINH HAI
* Date created: 30 SEP 2019 
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             30 SEP 2019    T.H          Original Code
*******************************************************************************/
// SYSTEM INCLUDES
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>

// WAVELIB 

// LOCAL INCLUDES
#include "biorithm_aux.h"
#include "preproc.h"
#include "biorithm_wavelib.h"

/*------------------------------------------------------------------------------
* Function name: iirnotch
* Description: return num and denum of iirotch filter at W0. Same as Matlab 
* iirnotch.m. Reference: [1] Sophocles J. Orfanidis, Introduction To Signal 
* Processing Prentice-Hall 1996. Example: % Design a filter with a Q-factor of 
* Q=35 to remove a 60 Hz tone from system running at 300 Hz.
% Wo = 60/(300/2);  BW = Wo/35;
% [b,a] = iirnotch(Wo,BW);  
------------------------------------------------------------------------------*/
struct iir_filter * preproc_iirnotch(double fs, double f0)
{
    // init output 
   struct iir_filter *p_filter = (struct iir_filter*) malloc(sizeof(struct iir_filter));

   // W0 is frequency wanted to be notched
   double W0 = M_PI * f0 / (fs / 2.0);

    // compute a IIR filter coefficients
    p_filter->p_num[0] = PREPROC_IIR_K;
    p_filter->p_num[1] = -PREPROC_IIR_K * 2.0 * cos(W0);
    p_filter->p_num[2] = PREPROC_IIR_K;
    p_filter->p_dnum[0] = 1.0;
    p_filter->p_dnum[1] = -2.0 * PREPROC_IIR_R * cos(W0);
    p_filter->p_dnum[2] = PREPROC_IIR_R * PREPROC_IIR_R;

   // 50Hz
   if (fabs(f0 - 50.0) < DBL_EPSILON)
   {
       p_filter->p_num[0]  = 0.991103635646810;
       p_filter->p_num[1]  = -1.603639368850131;
       p_filter->p_num[2]  = 0.991103635646810;
       p_filter->p_dnum[0] = 1.000000000000000;
       p_filter->p_dnum[1] = -1.603639368850131;
       p_filter->p_dnum[2] = 0.982207271293620;
   }

   // 60Hz
   if (fabs(f0 - 60.0) < DBL_EPSILON)
   {
       p_filter->p_num[0]  = 0.989343199319934;
       p_filter->p_num[1]  = -1.442400308113921;
       p_filter->p_num[2]  = 0.989343199319934;
       p_filter->p_dnum[0] = 1.000000000000000;
       p_filter->p_dnum[1] = -1.442400308113921;
       p_filter->p_dnum[2] = 0.978686398639868;
   }

    return p_filter;
}

/*------------------------------------------------------------------------------
* Function name:
* Description:
------------------------------------------------------------------------------*/
void preproc_filter(double_vec *p_sig, struct iir_filter *p_filter)
{
    // numerator coefficient
    double b_0 = p_filter->p_num[0];
    double b_1 = p_filter->p_num[1];
    double b_2 = p_filter->p_num[2];

    // denominator coefficient
    double a_0 = p_filter->p_dnum[0];
    double a_1 = p_filter->p_dnum[1];
    double a_2 = p_filter->p_dnum[2];

    // inputs for rational transfer function
    double x_1 = 0.0;
    double x_2 = 0.0;
    double y_1 = 0.0;
    double y_2 = 0.0;
    double tmp = 0.0;

    //applying filter on input signal using IIR notch filter coefficients
    for(unsigned int i = 0; i < p_sig->len; i++)
    {
        //rational transfer function
        tmp = b_0 * (p_sig->p_data[i]) +
                (b_1 * (x_1)) + (b_2 * (x_2)) - (a_1 * y_1) - (a_2 *y_2);
        x_2 = x_1;
        x_1 = p_sig->p_data[i];
        y_2 = y_1;
        y_1 = tmp;
        p_sig->p_data[i] = tmp;
    }
}
/*------------------------------------------------------------------------------
* Function name: replace_nan
* Description: replace nan value in raw ecg by FLT_MIN
*
*------------------------------------------------------------------------------*/
void preproc_replace_nan(double_vec **p_ecg, int nchannel)
{
    // loop through each channel 
    for (int i = 0; i < nchannel; i++)
    {
      // check each value 
      for (unsigned int j = 0; j < p_ecg[i]->len; j++)
      {
          if (isnan((p_ecg[i]->p_data[j])))
          {
              p_ecg[i]->p_data[j] = DLB_MIN;
          }
      }
    }
}

/*------------------------------------------------------------------------------
* Function name: remove_powerline
* Description: remove powerline interference by using notch filter at 50 and 60
* Hz.
*------------------------------------------------------------------------------*/
void preproc_rmv_powerline(double_vec **p_ecg, int nchannel)
{
    // iirnotch filter 50Hz
    struct iir_filter *p_iir_1 = preproc_iirnotch(FHR_SAMPLING_FREQ, PREPROC_POWLINE_1);

    // iirnotch filter 60Hz 
    struct iir_filter *p_iir_2 = preproc_iirnotch(FHR_SAMPLING_FREQ, PREPROC_POWLINE_2);

    // for each channel notch filter 50 and 60 Hz
    for (int i = 0; i < nchannel; i++)
    {
        // filter 50Hz
        preproc_filter(p_ecg[i], p_iir_1);

        // filter 60Hz
        preproc_filter(p_ecg[i], p_iir_2);  
    }
    
    // free mem
    free(p_iir_1);
    free(p_iir_2);
}


/*------------------------------------------------------------------------------
* Function name: rmv_baseline
* Description: remove baseline wander by using wavelet
*
*
------------------------------------------------------------------------------*/
void preproc_rmv_baseline(double_vec **p_ecg, int nchannel)
{
    // set wavelet length, nlayer, wname 
    struct param_wave wave = {p_ecg[0]->len, PREPROC_BLINE_NLAYER, 
      PREPROC_BLINE_WNAME};

    // set weight to nulling approximation layer 
     double weight[PREPROC_BLINE_NLAYER + 1] = {0.0, 1.0, 1.0, 1.0, 1.0, 
      1.0, 1.0, 1.0, 1.0}; 

    // construct wavelib object 
    struct wavelib_object *p_wave = create_wavelib_object(wave);

    // for each ECG channel 
    for (int i = 0; i < nchannel; i++)
    {
        // decompose into 8 layer 
        dwt(p_wave->wt, p_ecg[i]->p_data);

        // set approximation coefficient to zero 
        weight_wlayer(p_wave->wt, weight);

        // reconstruct by idwt 
        idwt(p_wave->wt, p_ecg[i]->p_data);
    }

    // free mem 
    delete_wavelib_object(p_wave);
}

/*------------------------------------------------------------------------------
* Function name: rmv_highfreq_noise
* Description: remove high frequency noise by using wavelet base denoise
* the function also estimate SQI (signal quality indicator) to indicate the
* channel with best maternal QRS. Maternal QRS frequency range is in detail
* layer CD3, CD4, CD5 or from 5Hz to 61Hz.
*-----------------------------------------------------------------------------*/
void preproc_rmv_highfreq_noise(double_vec **p_ecg, int nchannel)
{
    // set wavelet length, nlayer, wname 
    struct param_wave wave = {p_ecg[0]->len, PREPROC_DDEN_NLAYER, 
      PREPROC_DDEN_WNAME}; 

    // loop through each channel to do denoise by wavelet 
    for (int i = 0; i < nchannel; i++)
    {
        wdenoise(p_ecg[i], wave, "SOFT", true);
    }

}


/*------------------------------------------------------------------------------
* Function name: preprocess_ecg
* Description: preprocessing all raw ecg channel and return clean ecg of channel
* and also return SQI of each channel.
------------------------------------------------------------------------------*/
void preproc_preprocess_ecg(double_vec **pp_ecg, int nchannel)
{
    // replace nan by DLB_MIN 
    preproc_replace_nan(pp_ecg, nchannel);

    // remove powerline 50Hz and 60Hz
    preproc_rmv_powerline(pp_ecg, nchannel);

    // remove baseline by wavelet
    preproc_rmv_baseline(pp_ecg, nchannel);

    // remove high frequency noise by wavelet
    preproc_rmv_highfreq_noise(pp_ecg, nchannel); 

}

