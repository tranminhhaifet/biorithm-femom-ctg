/*******************************************************************************
*      Copyright Â©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: biorithm_aux.h
* Original Author: TRAN MINH HAI
* Date created: 30 SEP 2019
* Purpose: 
*
*
* Revision History:
* Ref No      Date           Who          Detail
*             30 SEP 2019    T.H          Original Code
********************************************************************************/
#ifndef PREPROC_H
#define PREPROC_H

// SYSTEM INCLUCDE
#include <math.h>
#include <float.h>
#include <errno.h>
#include <stdio.h>

// LOCAL INCLUDE 
#include "biorithm_aux.h"

// CONSTANT 
#ifndef FHR_NCHANNEL
#define FHR_NCHANNEL           4
#endif

#ifndef FHR_SAMPLING_FREQ
#define FHR_SAMPLING_FREQ       500
#endif

#ifndef DLB_MIN
#define DLB_MIN                 1e-16  // MIN FLOAT VALUE FOR REPLACING NAN
#endif

#ifndef M_PI
#define M_MI                    3.141592653589793
#endif

#define PREPROC_IIR_K           0.991103635646810
#define PREPROC_IIR_R           0.991103635646810
#define PREPROC_POWLINE_1 		50.0
#define PREPROC_POWLINE_2		60.0 
#define PREPROC_BLINE_NLAYER    8 
#define PREPROC_BLINE_WNAME     "db6"
#define PREPROC_DDEN_NLAYER     4 
#define PREPROC_DDEN_WNAME      "coif4"

// #ifndef DBL_EPSILON 2.2204460492503131e-016; /* smallest 1.0+DBL_EPSILON != 1.0 */
// #define DBL_EPSILON 
// #endif 

// #ifndef FLT_MIN 
// #define FLT_MIN FLT_MIN      =   1.175494351e-38F; 
// #endif 


// IIR FILTER STRUCT 
struct iir_filter
{
    double p_num[3];
    double p_dnum[3];
};

// CLEAN ECG STRUCT 
struct cleanecg 
{
	double_vec *p_ecg[FHR_NCHANNEL];
	double sqi[FHR_NCHANNEL];
};

/*------------------------------------------------------------------------------
* Function name: replace_nan
* Description: replace nan value in raw ecg by FLT_MIN
*
*------------------------------------------------------------------------------*/
void preproc_replace_nan(double_vec **pp_ecg, int nchannel);

/*------------------------------------------------------------------------------
* Function name: remove_powerline
* Description: remove powerline interference by using notch filter at 50 and 60
* Hz.
*------------------------------------------------------------------------------*/
void preproc_rmv_powerline(double_vec **pp_ecg, int nchannel);

/*------------------------------------------------------------------------------
* Function name:
* Description:
*------------------------------------------------------------------------------*/
struct iir_filter * preproc_iirnotch(double fs, double f0);

/*------------------------------------------------------------------------------
* Function name:
* Description:
*------------------------------------------------------------------------------*/
void preproc_filter(double_vec *p_sig, struct iir_filter *p_filter);


/*------------------------------------------------------------------------------
* Function name: rmv_baseline
* Description: remove baseline wander by using wavelet
*
*
------------------------------------------------------------------------------*/
void preproc_rmv_baseline(double_vec **pp_ecg, int nchannel);

/*------------------------------------------------------------------------------
* Function name: rmv_highfreq_noise
* Description: remove high frequency noise by using wavelet base denoise
* the function also estimate SQI (signal quality indicator) to indicate the
* channel with best maternal QRS. Maternal QRS frequency range is in detail
* layer CD3, CD4, CD5 or from 5Hz to 61Hz.
*-----------------------------------------------------------------------------*/
void preproc_rmv_highfreq_noise(double_vec **pp_ecg, int nchannel);


/*------------------------------------------------------------------------------
* Function name: preprocess_ecg
* Description: preprocessing all raw ecg channel and return clean ecg of channel
* and also return SQI of each channel.
------------------------------------------------------------------------------*/
void preproc_preprocess_ecg(double_vec **pp_ecg, int nchannel);


#endif //PREPROC_H
