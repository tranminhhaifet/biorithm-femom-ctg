/*********************************************************************************
*      Copyright ©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: fhr_baseline.c
* Original Author: HAI TRAN
* Date created: 15 AUG 2019
* Purpose: compute baselien, accelleration, deceleration, max variability and 
* mean variability of the baseline excluded accel, decel and outliers. 
* Revision History:
* Ref No      Date           Who            Detail
*             15 AUG 2019    TRAN MINH HAI  
*             06 OCT 2019    TRAN MINH HAI  REWORK 
*********************************************************************************/
// SYSTEM INCLUDES  
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <math.h>

// LOCAL INCLUDES 
#include "biorithm_aux.h"
#include "fhr_baseline.h"

static const double g_accel_increase_thresh =  15.0;  // bpm
static const double g_decel_decrease_thresh = -15.0;  // bpm
static const double g_accel_last_thresh     =  15.0;  // second
static const double g_decel_last_thresh     =  15.0;  // second
static const double g_gausswin_alpha        =  2.5;   // default gauss win
static const double g_slow_wave_window      =  180.0; // window cover adecel
static const double g_fast_wave_window      =  15.0;  // window not cover adecel  
static const double g_baseline_window       =  3.75;  // window for baseline

/*------------------------------------------------------------------------------
* Function Name: ad_point_vec_new
* Description  : create a vector of ad_point (acceleration and decelleration) 
* which are vector of acceleration and deceleration point. ad_point is a struct 
* containing {starting time of adcel, ending time of adcel, and width of adcel}
*-----------------------------------------------------------------------------*/
ad_point_vec *ad_point_vec_new(unsigned int len)
{
    // ad_point_vec contains location and with of accel and decel 
    ad_point_vec *p = malloc(sizeof (ad_point_vec));

    // TOTO CHECK RETURN NULL 

    p->len = len;
    p->p_data = malloc(len * sizeof (ad_point));

    // INIT TO ZERO
    for (unsigned int i = 0; i < len; i++) 
    {
        p->p_data[i].x = 0.0; // starting time of adcel
        p->p_data[i].y = 0.0; // ending time of adcel
        p->p_data[i].z = 0;   // lasting or width of adcel 
    }
    return p;
}

/*------------------------------------------------------------------------------
* Function Name: ad_point_vec_delete
* Description  : free heap memory allocated for a vector of adcel points
*-----------------------------------------------------------------------------*/
void ad_point_vec_delete(ad_point_vec *p)
{
    free(p->p_data);
    free(p);
}

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/
void ad_point_vec_write(const char* p_filepath, ad_point_vec *p_ad)
{
    FILE *p_file;
    p_file = fopen(p_filepath,"w");

    // CHECK OPEN FILE RETURN NULL 
    if (p_file == NULL)
    {
        printf("ERROR INVALID FILE PATH\n");
    }

    // PRINT DATA TO FILE 
    for (unsigned int i = 0; i < p_ad->len; i++) 
    {
        fprintf(p_file, "%4.15f %4.15f %4.15f\n", 
        p_ad->p_data[i].x, p_ad->p_data[i].y, p_ad->p_data[i].z);
    }

    // CLOSE FILE 
    fclose(p_file);
}

/*------------------------------------------------------------------------------
* Function Name: is_fhr_outlier
* Description  : check is a value of FHR outlier
% TOD: need to specify threshold of outliers included lost signal of FHR 
*-----------------------------------------------------------------------------*/
int is_fhr_outlier(double x)
{
    // TODO: define threshold of outliers of FHR 
    if (isnan(x))
    {
        return 1;

    } else if (fabs(x) < FHR_FBPM_LLIM || fabs(x) > FHR_FBPM_ULIM)
    {
        return 1;
    } else 
    {
        return 0;
    }
}

/*------------------------------------------------------------------------------
* Function Name: find_outlier
* Description  : find indecies of outlier FHR
*-----------------------------------------------------------------------------*/
double_vec *find_outlier(double_vec *p)
{
    // init p_mark for marking location of outliers 
    double_vec *p_mark = double_vec_new(p->len);
    for(unsigned int i = 0; i < p->len; i++) 
    {
        p_mark->p_data[i] = 1.0;
    }

    // outliers 
    for (unsigned int i = 0; i < p->len; i++) 
    {
        if(is_fhr_outlier(p->p_data[i])==1) 
        {
            p_mark->p_data[i] = 0.0;
        }
    }
    return p_mark;
}

/*------------------------------------------------------------------------------
* Function Name: get_fast_fhr
* Description  : get fast wave of FHR which tracks accel, decel points and should 
* ignore outliers such as lost signal points. get_fast_fhr uses a moving Gaussian
* window and window length is less than 15 seconds. The default alpha of Gaussian
* window is 2.5. 
*-----------------------------------------------------------------------------*/
void get_fast_fhr(double_vec *p, double fs)
{
    // WINDOW LENGTH FOR FAST WAVE 
    unsigned int wlen = (unsigned int)(((int)(g_fast_wave_window * fs)/2) *2 + 1);

    // smooth(p, wlen, "gaussian", g_gausswin_alpha);

    // COMPUTE WEIGHT TO IGNORE OUTLIERS FHR 
    double_vec *p_weight = double_vec_new(p->len);

    for (unsigned int i = 0; i < p->len; i++)
    {
        if ( p->p_data[i] <= FHR_FBPM_LLIM  || p->p_data[i] >= FHR_FBPM_ULIM)
        {
            p_weight->p_data[i] = 0.0000001;
        }
        else
        {
            p_weight->p_data[i] = 1.0;
        }
    }

    // WEIGHTED GAUSSIAN SMOOTH 
    smoothwg(p, wlen, g_gausswin_alpha, p_weight);

    // FREE MEM
    double_vec_delete(p_weight);
}

/*------------------------------------------------------------------------------
* Function Name: get_slow_fhr
* Description  : get slow wave FHR which ignore acell and decel and outliers 
* by using robust weighted Gaussian smooth method. The method is based on 
* robust locally weighted linear regression. In short, Gaussian win is weighted
* to ignore decel, acell and outliers. Weights are computed in an iteratively 
* manner. Weighted Gaussian window is derived to reduce computation cost since 
* it does not requires solving least square equation for each local smoothed point
* REF. "Robust Locally Weighted Regression and Smoothing Scatterplots", William 
* S. Cleveland, 1979
*-----------------------------------------------------------------------------*/
void get_slow_fhr(double_vec *p, double fs)
{
    unsigned int wlen = (unsigned int)(((int)(g_slow_wave_window * fs)/2) *2 + 1);
    smooth(p, wlen, "rgaussian", g_gausswin_alpha);
}

/*------------------------------------------------------------------------------
* Function Name: get_adcel_width
* Description  : check each zero crossing segment of diff = (fast_wave - slow_wave)
* and identify is the segment a accel or decel (adcel)? then also compute width 
* of adcel. Usually, a accel means +15bmp (g_accel_increase_thresh) bumps from 
* baseline and last at least 15 seconds (g_accel_last_thresh), a decel means -15bmp 
* (g_decel_decrease_thresh) from baseline and last at least 15 seconds (decel_last
* _thresh) . 
*-----------------------------------------------------------------------------*/
double get_adcel_width(double x_s, double x_e, double_vec *p_x, double fs, char *method)
{
    // init 
    double width = 0.0;
    unsigned int x_si = (unsigned int)(round(x_s)); // start idx of a zc segment
    unsigned int x_ei = (unsigned int)(round(x_e)); // send idx of a zc segment
    int head = 0; // head of adcel
    int tail = 0; // tail of adcel 

    // adcel width is width of zc segment 
    if (strcmp(method, "zc_width") == 0) 
    {
        // adcel found if segment contains a sample greater than threshold
        for (unsigned int i = x_si; i < x_ei; i++) 
        {
            if (p_x->p_data[i] > g_accel_increase_thresh || p_x->p_data[i] < g_decel_decrease_thresh) 
            {
                width = x_e - x_s; 
                break;
            }
        }
    }

    // adcel width from a horizontal threshold
    if (strcmp(method, "th_width") == 0) 
    {
        // left first point greater than threshold 
        for (head = x_si; head < x_ei; head++) 
        {
            if (p_x->p_data[head] > g_accel_increase_thresh || p_x->p_data[head] < g_decel_decrease_thresh) 
            {
                break;
            }
        }

        // right first point greater than threshold 
        for (tail = x_ei; tail > x_si; tail--) 
        {
            if (p_x->p_data[tail] > g_accel_increase_thresh || p_x->p_data[tail] < g_decel_decrease_thresh) 
            {
                break; 
            }
        }

        // double check going to end of segment and update width 
        // + for accel and - for decel 
        if (head < x_ei && tail > x_si) 
        {
            width = p_x->p_data[head] > 0 ? (tail-head) : (head-tail);
        } else 
        {
            width = 0.0;
        }

        // convert to second and optional check width 
        // can keep all width for visualiztion 
        width = width / fs;
        if (width  > g_accel_last_thresh || width < - g_decel_last_thresh) 
        {

        } else 
        {
            width = 0.0; // mean no adcel 
        }
    }

    return width;
}

/*------------------------------------------------------------------------------
* Function Name: find_adcel_locs
* Description  : find starting and ending point of each accel and decel. 
*-----------------------------------------------------------------------------*/
ad_point_vec *find_adcel_locs(double_vec *p_y, double fs)
{
    // INIT OUTPUT 
    ad_point_vec *p_ad = NULL; // p_ad contains location of acel/decel
    double ad_width = 0.0;     // width of acceleration 
    unsigned int count = 0; 

    // INIT PX LOCATION
    double_vec *p_x = double_vec_new(p_y->len);
    for (unsigned int i = 0; i < p_y->len; i++) 
    {
        p_x->p_data[i] = i;
    }

    // FIND ZERO CROSSING 
    double_vec *p_z =  find_cross_zero(p_x, p_y);

    // IDENTIFY ACCELL, DECEL, NORMAL FOR EACH ZERO-CROSSING ROOT 
    if (p_z != NULL) 
    {
        p_ad = ad_point_vec_new(p_z->len);
        for (unsigned int i = 0; i < p_z->len-1; i++) 
        {
            ad_width = get_adcel_width(p_z->p_data[i], p_z->p_data[i+1], p_y, fs, "th_width");  
            p_ad->p_data[i].x = p_z->p_data[i];
            p_ad->p_data[i].y = p_z->p_data[i+1];
            p_ad->p_data[i].z = ad_width;
        }
    }

    // FREE MEM 
    double_vec_delete(p_x);
    double_vec_delete(p_z);

    // RETURN 
    return p_ad;
}

/*------------------------------------------------------------------------------
* Function Name: find_decel
* Description  : compute decel/accel beginning and ending point 
* NOTE: find_adcel does not change the input p_x
*-----------------------------------------------------------------------------*/
void find_adcel(double_vec *p_x, double fs, baseline *p_b_obj)
{
    // INIT 
    ad_point_vec *p_ad = NULL; // OUTPUT 
    double_vec *p_s = double_vec_new(p_x->len); // SLOW WAVE
    double_vec *p_f = double_vec_new(p_x->len); // FAST WAVE
    double_vec *p_d = double_vec_new(p_x->len); // DIFF 
    

    // GET FAST WAVE 
    double_vec_copy(p_x, p_f);
    get_fast_fhr(p_f, fs);
    
    // GET SLOW WAVE
    // double_vec_copy(p_x, p_s);
    // H.T OCT 08 2019 COPY FASTWAVET INTO SLOVE WAVE AS INIT VALUE
    double_vec_copy(p_f, p_s);
    get_slow_fhr(p_s, fs);

    // INIT DIFF VECTOR 
    double_vec_copy(p_f, p_d);
    
    // DIFF BETWEEN FAST AND SLOW WAVE 
    double_vec_sub(p_s, p_d);

    // FIND ACCEL AND DECEL POINTS 
    p_ad = find_adcel_locs(p_d, fs); 

    // FORM OUTPUT 
    p_b_obj->p_ad   = p_ad; 
    p_b_obj->p_fast = p_f; 
    p_b_obj->p_slow = p_s;
    p_b_obj->p_diff = p_d; 
}

/*------------------------------------------------------------------------------
* Function Name: find_ltv
* Description  : find long term variability of the FHR. First, a baseline is 
* computed by smoothing the input FHR using a Gaussian window of 3.75seconds. 
* Second, maximum var and mean var of the baseline is computed excluded accel 
* decel and outlier samples. Indecies or locations of excluded samples is indicated
* by a mark vector called p_m. 
* NOTE: find_ltv does not change the input p_x
*-----------------------------------------------------------------------------*/
double_vec *find_ltv(double_vec *p_x, baseline *p_b_obj, double fs)
{
    // INIT 
    double max_var = 0.0; 
    double mean_var = 0.0; 
    double tmp = 0.0; 
    unsigned int count = 0; 

    // LOCATIONS OF ADCEL AND OUTLIERS 
    double_vec *p_m = double_vec_new(p_x->len);

    // MARK OUTLIER LOCATION 
    p_m = find_outlier(p_x);

    // MARK ADCEL LOCATION
    for (unsigned int i = 0; i < (p_b_obj->p_ad->len); i++) 
    {
        if (p_b_obj->p_ad->p_data[i].z > 0.0 || p_b_obj->p_ad->p_data[i].z < 0.0) 
        {
            unsigned int sidx = (unsigned int)(round(p_b_obj->p_ad->p_data[i].x)); 
            unsigned int eidx = (unsigned int)(round(p_b_obj->p_ad->p_data[i].y)); 
            for (unsigned int j = sidx; j < eidx; j++) 
            {
                p_m->p_data[i] = 0.0; 
            }
        }
    }

    // MAX VAR  
    for (unsigned int i = 0; i < p_x->len; i++) 
    {
        // EXCLUTE OUTLIER, ADECEL, SIGNAL LOST 
        if ( p_m->p_data[i] > 0 ) 
        {
            tmp = fabs(p_x->p_data[i] - p_b_obj->p_base->p_data[i]); 
            if (tmp > max_var) 
            {
                max_var = tmp;
            }
            mean_var = mean_var + tmp;
            count = count + 1; 
        }
    }

    // MEAN VAR 
    mean_var = mean_var / count; 

    // FORM OUTPUT  
    double_vec *p_var = double_vec_new(2);
    p_var->p_data[0] = max_var;
    p_var->p_data[1] = mean_var;

    return p_var;
}

/*------------------------------------------------------------------------------
* Function Name: find_baseline
* Description: the function return a baseline object which contains following 
* information: baseline, slow wave, fast wave, diff, num. accel, num. decel, 
* location and width of accel/decel, max var, mean var. 
* find_baseline does not change the input p_x
*-----------------------------------------------------------------------------*/
baseline *find_baseline(double_vec *p_x, double fs)
{
    // INIT 
    baseline *p_b_obj = malloc(sizeof(baseline));
    p_b_obj->max_var = 0.0;  // max var of baseline  
    p_b_obj->mean_var = 0.0; // mean var of baseline
    p_b_obj->ndecel = 0;     // number of decel
    p_b_obj->naccel = 0;     // number of accel 
    p_b_obj->p_base = double_vec_new(p_x->len); // the baseline
    p_b_obj->p_fast = double_vec_new(p_x->len); // fast wave
    p_b_obj->p_slow = double_vec_new(p_x->len); // slow wave
    p_b_obj->p_diff = double_vec_new(p_x->len); // diff 
    p_b_obj->p_ad = NULL; // ad vector contain location and width of adcel

    // COPY INPUT TO A BUFFER VECTOR P_O
    double_vec *p_o = double_vec_new(p_x->len);
    double_vec_copy(p_x, p_o);

    // find outlier and lost signal 
    // p_m = find_outlier(p_x);

    // FIND THE BASELINE BY GAUSSIAN SMOOTH FAST WAVE 
    unsigned int wlen = (unsigned int)(floor(g_baseline_window * fs / 2) * 2 + 1);
    // smooth(p_x, wlen, "gaussian", g_gausswin_alpha); 
    // H.T OCT 08 2019
    get_fast_fhr(p_o, fs);

    // COPY THE BASELINE TO OUTPUT p_b_obj
    double_vec_copy(p_o, p_b_obj->p_base);

    // FIND ACCEL AND DECEL 
    find_adcel(p_x, fs, p_b_obj);

    // COUNT NUMBER OF ACCEL AND DECEL 
    unsigned int count_d = 0; // NUMBER OF DECEL
    unsigned int count_a = 0; // NUMBER OF ACCEL

    if (p_b_obj->p_ad != NULL) 
    {
        for (unsigned int i = 0; i < p_b_obj->p_ad->len; i++) 
        {
            if (p_b_obj->p_ad->p_data[i].z > 0.0) 
            {
                count_a += 1; 
            }
            if (p_b_obj->p_ad->p_data[i].z < 0.0) 
            {
                count_d += 1; 
            }
        }
    }
    p_b_obj->naccel = count_a;
    p_b_obj->ndecel = count_d;

    // FIND LONG TERM VAR AND BASE VAR 
    double_vec *p_var  = find_ltv(p_x, p_b_obj, fs);
    p_b_obj->max_var   = p_var->p_data[0];
    p_b_obj->mean_var  = p_var->p_data[1];

    // RETURN 
    return p_b_obj;
}

