/*********************************************************************************
*      Copyright ©2019-2020. Biorithm Pte Ltd. All Rights Reserved.
* This software is the property of Biorithm Pte Ltd. It must not be copied, printed, 
* distributed, or reproduced in whole or in part or otherwise disclosed without
* prior written consent from Biorithm.
* This document shall follow Biorithm Software Coding Guidelines.
*        
* Filename: fhr_baseline.h
* Original Author: HAI TRAN
* Date created: 15 AUG 2019
* Purpose: function prototype or declaration, user datatype define for ecg unit
*
*
* Revision History:
* Ref No      Date           Who            Detail
*             15 AUG 2019    TRAN MINH HAI  
*             06 OCT 2019    TRAN MINH HAI  REWORK 
*********************************************************************************/
#ifndef FHR_BASELINE_H
#define FHR_BASELINE_H

// LOCAL INCLUDES 
#include "biorithm_aux.h"

// DEFINE FETAL HEART RATE OUTLIER 
#ifndef FHR_FBPM_LLIM
#define FHR_FBPM_LLIM		 	100		// LOWER LIMIT OF FETAL BPM 
#endif

#ifndef FHR_FBPM_ULIM
#define FHR_FBPM_ULIM 			200		// UPPER LIMIT OF FETAL BPM
#endif

// ACCEL AND DECEL (AD) STRUCT 
typedef struct ad_point 
{
	double x; 
	double y; 
	double z; 
} ad_point;

// VECTOR OF AD (ACCEL AND DECEL)
typedef struct ad_point_vec
{
    ad_point *p_data;
    unsigned int len;
} ad_point_vec;

// BASELINE STRUCT 
typedef struct  baseline 
{
	double max_var; 
	double mean_var; 
	unsigned int ndecel;
	unsigned int naccel; 
	double_vec *p_base; 
	double_vec *p_fast;
	double_vec *p_slow;
	double_vec *p_diff;
	ad_point_vec *p_ad; 
} baseline;

// MACRO FOR PRINT AD POINT 
#define PRINT_AD_POINT(p){printf("x = %3.15f, y = %3.15f, z = %3.15f\n", p.x, p.y, p.z);}

// MACRO FOR CONTRUCT AD POINT 
#define AD_POINT_CONSTRUCT(p, x_val, y_val, z_val){p.x=x_val; p.y=y_val; p.z=z_val;}

// FUNCTION 
/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
ad_point_vec *ad_point_vec_new(unsigned int len);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
void ad_point_vec_delete(ad_point_vec *p);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/
void ad_point_vec_write(const char* p_filepath, ad_point_vec *p_ad);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
int is_fhr_outlier(double x);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
double_vec *find_outlier(double_vec *p);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
void get_slow_fhr(double_vec *p, double fs);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
void  get_fast_fhr(double_vec *p, double fs);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
ad_point_vec *find_adcel_locs(double_vec *p_y, double fs); 

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
void find_adcel(double_vec *p_x, double fs, baseline *p_b_obj);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
double get_adcel_width(double x_s, double x_e, double_vec *p_x, double fs, 
char *method);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
double_vec *find_ltv(double_vec *p_x, baseline *p_b_obj, double fs);

/*------------------------------------------------------------------------------
* Function Name: 
* Description  : 
*-----------------------------------------------------------------------------*/ 
baseline *find_baseline(double_vec *p_x, double fs);

#endif // FHR_BASELINE_H
